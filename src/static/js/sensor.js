import Taro from '@tarojs/taro'

let sensors = null;
let evn = process.env.TARO_ENV;//环境判断
// if (evn == 'weapp') {
//     sensors = require('./sensorsdata-weapp.min.js')
// } else if (evn == 'alipay') {
//     sensors = require('./sensorsdata-alipay.min.js')
// }
/* 初始化神策到taro的消息中心--适配双端 */
// Taro.eventCenter.on('sensorInit', () => {
//     /* 初始化神策 */
//     console.log(sensors)
//     sensors.init()
// })
// Taro.eventCenter.on('sensor', (data = {
//     /* 调用神策的方法 */
//     eventName: 'eventName',
//     properties: null
// }) => {
//     sensors.track(data.eventName, data.properties)
// })

/* 
//app.js中如下引入，并且初始化
import './static/js/sensor'//引入神策初始化文件
Taro.eventCenter.trigger('sensorInit')


//页面中如下使用
Taro.eventCenter.trigger('sensor', {
    调用神策的方法 
    eventName: 'ViewProduct',
        properties: {
            ProductId: '123456',
            ProductCatalog: "Laptop Computer",
            ProductName: "MacBook Pro",
            ProductPrice: 12345
        }
    });
*/
