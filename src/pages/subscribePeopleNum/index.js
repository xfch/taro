import Taro, { Component } from '@tarojs/taro'
import { View, Text, Image } from '@tarojs/components'
import './index.scss'

import Other from '../../components/chooseSubscribe/other'
import Artificer from '../../components/chooseSubscribe/artificer'

import { set as setGlobalData, get as getGlobalData } from '../../static/js/global_data'

import api from '../../service/api'
import common from '../../service/common'
import base from '../../static/js/base'

export default class Index extends Component {
    config = {
        navigationBarTitleText: ''
    }
    constructor() {
        super(...arguments)
        this.state = {
            industry: getGlobalData('industry')
        }
    }

    componentWillMount() {
        let title = getGlobalData('industry') == '390' ? '请选择技师/服务人员' : '选择预约'
        Taro.setNavigationBarTitle({
            title
        })
        this.setState({
            peopleNum: this.$router.params.peopleNum || 0
        })
    }
    setVal(val){
        this.setState({
            searchVal: val,
            searchState: false,
            hasView: false
        })
    }
    
    render() {
        let {industry,peopleNum} = this.state
        return (
            <View className={`chooseSubscribe ${industry == 390 ? 'bg-body' : 'bg-white'}`}>
                {industry == '390' &&
                    <Artificer>美业</Artificer>
                }
                {industry != '390' &&
                    <Other peopleNum={peopleNum}></Other>
                }    
            </View>
        )
    }
}

