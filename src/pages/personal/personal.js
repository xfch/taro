import Taro, { Component } from '@tarojs/taro'
import { View, Text } from '@tarojs/components'
import './personal.scss'

import Mbinfo from '../../components/member/mbinfo/mbinfo'
import Mbwallet from '../../components/member/mbwallet/mbwallet'
import Mbentry from '../../components/member/mbentry/mbentry'
import TabBar from '../../components/tabBar'

import Authorize from '../../components/authorize/authorize'

import { set as setGlobalData, get as getGlobalData } from '../../static/js/global_data'
import api from '../../service/api'
import base from '../../static/js/base'
import common from '../../service/common'
import coupon from '../../service/coupon'

export default class Index extends Component {
    config = {
        navigationBarTitleText: '我的',
        navigationBarBackgroundColor: "#fe690f",
    }
    constructor() {
        super(...arguments)
        this.state = {
            wallet: {
                'balance': '0',
                'list': [
                    {'num': '0','txt': '优惠券','url': '/pages/viewCoupon/viewCoupon'},
                    {'num': '0','txt': '购物车','url': '/pages/shopcart/shopcart'},
                    {'num': '0','txt': '收藏夹','url': '/pages/collect/collect'}
                ]
            },
            order: {
                title: '我的订单',
                more: { 'txt': '查看全部订单', 'id': '0' },
                list: [
                    { 'icon': 'iconfont icon-mainOrder', 'txt': '待付款', 'id': '1', 'num': 0 },
                    { 'icon': 'iconfont icon-mainOrder', 'txt': '待发货', 'id': '2', 'num': 0 },
                    { 'icon': 'iconfont icon-mainOrder', 'txt': '待收货', 'id': '3', 'num': 0 },
                    { 'icon': 'iconfont icon-mainOrder', 'txt': '待自提', 'id': '5', 'num': 0 },
                    { 'icon': 'iconfont icon-mainOrder', 'txt': '已完成', 'id': '4', 'num': 0 },
                    { 'icon': 'iconfont icon-mainOrder', 'txt': '售后', 'id': '6', 'num': 0 },
                    { 'icon': 'iconfont icon-mainOrder', 'txt': '我的预定', 'id': '7', 'num': 0 }
                ]
            },
            tool: {
                title: '必备工具',
                list: [
                    { 'icon': 'iconfont icon-mainOrder', 'txt': '我的评论', 'id': '1' },
                    { 'icon': 'iconfont icon-mainOrder', 'txt': '我的兑换', 'id': '2' },
                    { 'icon': 'iconfont icon-mainOrder', 'txt': '我的寄存', 'id': '3' },
                    { 'icon': 'iconfont icon-mainOrder', 'txt': '购物卡', 'id': '4' },
                    { 'icon': 'iconfont icon-mainOrder', 'txt': '修改密码', 'id': '5',},
                    { 'icon': 'iconfont icon-mainOrder', 'txt': '客服', 'id': '6' },
                ]
            },
            pageindex: 1,
            testState: true,
            edition: getGlobalData('edition')
        }
    }
    // authorize获取作用域
    getIsLogin(obj){
        if(obj){
            if(obj.C_MB_NO){ 
                this.setState({userInfo: obj})
                this.isBindPhone(obj)
                Taro.hideLoading()
            }else{
                obj.isLogin(() => {
                    const userInfo = Taro.getStorageSync('userInfo')
                    this.isBindPhone(userInfo)
                    Taro.hideLoading()
                })
            }
        }
    }
    componentWillMount() {
        const that = this
        common.tabList(that,function(res){
            that.setState({ tabList: res})
        })
    }

    componentDidShow(obj) {
        this.getIsLogin(obj)
    }

    // 是否绑定手机号
    isBindPhone(data){
        const that = this;
        const env = process.env.TARO_ENV
        if(data){
            this.getOrderNum(data.C_MB_NO)
            this.getCouponNum(data.C_MB_NO)
            // 收藏商品
            common.getGoodList(that,(goodLists, hasEd, res) => {
                that.state.wallet.list[2].num = res.PageRecord ? res.PageRecord : 0
                that.setState({wallet: that.state.wallet})
            })
            // 收货地址数量
            if(env != 'weapp'){
                that.state.wallet.list[3] = {'num': '0','txt': '收货地址','url': '/pages/addressList/addressList'}
                common.getAddressList(that,(res) => {
                    that.state.wallet.list[3].num = res.address ? res.address.length : 0
                    that.setState({wallet: that.state.wallet})
                })
            }
            // 购物车数量
            common.getCartNum(that,(res) => {
                that.state.wallet.list[1].num = res.goodsnum
                that.setState({wallet: that.state.wallet})
            })
            
            if(!data.C_MB_PHONE || data.C_MB_PHONE == ''){
                // base.showModal("账号未绑定手机号，请先绑定手机号",false,function(){
                //     Taro.navigateTo({url: '/pages/editMember/editMember?activeIndex=1'})
                // })
            }
        }
    }
    // 获取优惠券数量
    getCouponNum(mebno){
        coupon.getCouponNum(null,(res) => {
            this.state.wallet.list[0].num = res.CouponsNum
            this.setState({wallet: this.state.wallet})
        })
    }
    // 点击登录后
    setUserInfo(obj){
        if(obj){
            if(obj.C_MB_NO){
                this.isBindPhone(obj)
                this.setState({userInfo: obj})
            }
        }

    }
    // 获取订单数量
    getOrderNum(mebno){
        const str = `mebno&ShopNo`
        let params = {
            bNo: 400,
            mebno: mebno,
        }
        api.get(str, params, (res) => {
            this.state.order.list[0].num = res.dfk
            this.state.order.list[1].num = res.dfh
            this.state.order.list[2].num = res.dsh
            this.state.order.list[3].num = res.dzt
            this.state.order.list[4].num = res.ywc
            this.state.order.list[5].num = res.zdth
            this.setState({
                order: this.state.order
            })
        })
    }
    // 退出
    signout() {
        Taro.clearStorageSync()
        setGlobalData('isLoginState', true)
        Taro.reLaunch({ url: '/pages/login/login?pages=login' })
    }
    // 钱包跳转
    mbWalletNavTo(url){
        console.log(url)
        Taro.navigateTo({url: url})
    }
    // 订单跳转
    orderNavTo(id) {
        if(id == '7'){
            Taro.navigateTo({ url: '/pages/subscribeList/index'})
        }else{
            Taro.navigateTo({ url: '/pages/order/order?stateId=' + id })
        }
    }
    // 必备工具
    onMbenClick(id) {
        if (id == '1') {
            Taro.navigateTo({ url: '/pages/commentList/index' })
        }
        else if(id == '2'){
            Taro.navigateTo({ url: '/pages/exchangeOrderList/index' })
        }
        else if(id == '4'){
            Taro.navigateTo({ url: '/pages/shopCard/shopCard' })
        }
        else if(id == '5'){
            Taro.navigateTo({ url: '/pages/editPass/editPass' })
        }
    }
    render() {
        let {edition} = this.state
        return (
            <View className='mainPersonal'>
                <View className='mbinfo'>
                    <Mbinfo onSignout={this.signout} userInfo={this.state.userInfo}></Mbinfo>
                </View>
                <View className='mbentry-container'>
                    {/* 钱包 */}
                    {this.state.testState && 
                        <View className='mbwallet entry-item'>
                            <Mbwallet wallet={this.state.wallet} userInfo={this.state.userInfo} onNavTo={this.mbWalletNavTo.bind(this)}></Mbwallet>
                        </View>
                    }
                    {/* 订单 */}
                    <View className='mbentry entry-item'>
                        <Mbentry entry={this.state.order} orderNum={this.state.orderNum} onNavTo={this.orderNavTo.bind(this)}></Mbentry>
                    </View>
                    {/* 必备工具 */}
                    {this.state.testState && 
                        <View className='mbentry entry-item'>
                            <Mbentry entry={this.state.tool} onNavTo={this.onMbenClick.bind(this)}></Mbentry>
                        </View>
                    }
                </View>
                <View className='edition ac p20 text-gray'>当前版本：{edition} </View>
                <TabBar tabId='306' tabList={this.state.tabList}></TabBar>
                <Authorize page='personal' onSetUserInfo={this.getIsLogin.bind(this)}></Authorize>
            </View>
        )
    }
}

