import Taro, { Component } from '@tarojs/taro'
import { View, Text, Radio } from '@tarojs/components'

import './index.scss'

import Authorize from '../../components/authorize/authorize'

import { set as setGlobalData, get as getGlobalData } from '../../static/js/global_data'
import api from '../../service/api'
import base from '../../static/js/base'
import common from '../../service/common'
import order from '../../service/order'

export default class Index extends Component {
    config = {
        navigationBarTitleText: '有桌点餐-店铺名称',
    }
    constructor() {
        super(...arguments)
        this.state = {
            
        }
    }
    // authorize获取作用域
    getIsLogin(obj){
        if(obj){
            if(obj.C_MB_NO){ 
            }else{
                obj.isLogin(() => {

                })
            }
        }	
    }
    componentWillMount(obj) {
        this.getIsLogin(obj)
    }

    render() {
        let {} = this.state
        return (
            <View className='main-orderFood'>
                {/* 登陆 */}
                <Authorize page='onSetUserInfo' onSetUserInfo={this.getIsLogin.bind(this)}></Authorize>
            </View>
        )
    }
}

