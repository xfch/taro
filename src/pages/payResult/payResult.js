import Taro, { Component } from '@tarojs/taro'
import { View, Text } from '@tarojs/components'
import './payResult.scss'

export default class Index extends Component {
    config = {
        navigationBarTitleText: '付款结果',
    }
    constructor() {
        super(...arguments)
        this.state = {
            result: 0, // 0: 成功，1：失败
            orderno: '',
            checkOrder: 1, // 是否显示查看订单按钮 1: 显示 0：隐藏
        }
    }
    componentWillMount() { 
        console.log(this.$router.params)
        let params = this.$router.params
        this.setState({
            result: params.result,
            orderno: params.orderno,
            checkOrder: params.checkOrder
        })
    }
    navTo(id){
        if(id == 'index'){
            Taro.reLaunch({url: '/pages/index/index'})
        }
        else if(id == 'detail'){
            Taro.navigateTo({url: `/pages/orderDetail/orderDetail?orderId=${this.state.orderno}`})
        }
    }
    render() {
        let {result,orderno,checkOrder} = this.state
        return (
            <View className='payResult bg-white ac'>
                <View className={result == '0' ? 'payResult-success' : 'payResult-fail'}>
                    <View className={`iconfont ${result == '0' ? 'icon-pay-success' : 'icon-pay-fail'}`}></View>
                    <View className='text-result'>{result == '0' ? '付款成功' : '付款失败'}</View>
                    <View className='text-small text-gray'>单号：{orderno}</View>
                    <View className='btns-group'>
                        <Button type='default' size='mini' className='weui-btn' onClick={this.navTo.bind(this,'index')}>返回首页</Button>
                        {checkOrder == '1' && <Button type='default' size='mini' className='weui-btn' onClick={this.navTo.bind(this,'detail')}>查看订单</Button>}
                    </View>
                </View>
            </View>
        )
    }
}

