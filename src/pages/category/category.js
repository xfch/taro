import Taro, { Component } from '@tarojs/taro'
import { View, Text } from '@tarojs/components'
import './category.scss'

import Cgnav from '../../components/category/cgnav'
import CgnavSub from '../../components/category/cgnavSub'
import CgList from '../../components/category/cglist'
import Cartbar from '../../components/category/cartbar'
import Authorize from '../../components/authorize/authorize'

import TabBar from '../../components/tabBar'

import { set as setGlobalData, get as getGlobalData } from '../../static/js/global_data'
import api from '../../service/api'
import base from '../../static/js/base'
import common from '../../service/common'

export default class Index extends Component {
    config = {
        navigationBarTitleText: '全部分类',
    }
    constructor() {
        super(...arguments)
        this.state = {
            tabActive: 0,
            navList: null,
            pageindex: 1,
            hasEd: false,
            hasGoodList: false,
            goodList: [],
            cartNum: '0',
        }
    }

    componentWillMount() { 
        const that = this
        base.showLoading()
        common.tabList(that,function(res){
            res.map(function(item,i){
                if(item.C_Navigation_Name == '分类'){
                    that.state.tabActive = i
                }
            })
            that.setState({
                tabList: res,
                tabActive: that.state.tabActive
            })
        })
        common.getUserInfo(that,function(data){
            that.setState({userInfo: data})
        })
        common.getCategory(that,function(category){
            that.state.cid = category[0].ID
            that.setGoodList(that)
            let cgnav = [],cgnav_str = ''; //一级分类
            let cgnavsub = [{'Name': '全部','ID': 'all'}],cgnavsub_str = ''; //二级分类
            category.map(function(item,i){
                cgnav_str = { ID: item.ID, name: item.name };
                cgnav.push(cgnav_str)
                if(i == 0){
                    item.sendcate.map(function(items,j){
                        if(items.type == 1){
                            cgnavsub_str = {ID: items.ID, Name: item.Name}
                            cgnavsub.push(cgnavsub_str)
                        }
                    })
                }
            })
            that.setState({
                cgnav: cgnav,
                cgnavId: cgnav[0].ID,
                cgnavsub: cgnavsub,
                cgnavsubId: cgnavsub[0].ID,
                category: category
            })

        })
        common.getCartNum(that)
    }
    // 一级分类
    tabNav(id){
        const that = this;
        if(id == that.state.cgnavId){return}
        let cgnavsub = [{'Name': '全部','ID': 'all'}],cgnavsub_str = ''; //二级分类
        this.state.category.map(function(item,i){
            if(item.ID == id){
                item.sendcate.map(function(items,j){
                    if(items.type == 1){
                        cgnavsub_str = {ID: items.ID, Name: items.Name}
                        cgnavsub.push(cgnavsub_str)
                    }
                })
            }
        })
        that.state.cid = id
        that.setState({
            hasGoodList: false,
            cgnavId: id,
            cgnavsub: cgnavsub,
            cgnavsubId: cgnavsub[0].ID,
            goodList: [],
            hasGoodList: false,
            hasEd: false,
            pageindex: 1,
        })
        base.showLoading()
        that.setGoodList(that)
    }
    // 二级分类
    tabNavSub(id){
        const that = this;
        that.state.cid = id == 'all' ? that.state.cgnavId : id;
        that.setState({
            cgnavsubId: id,
            goodList: [],
            hasGoodList: false,
            hasEd: false,
            pageindex: 1,
        })
        base.showLoading()
        that.setGoodList(that)
    }
    // 设置商品加入购物车的数量
    setGoodList(that){
        common.getUserCart(that,null,(cartList) => {
            common.getGoodList(that,(goodList,hasEd) => {
                let idArr = []
                cartList.map((item,i) => {
                    idArr.push(item.N_GOODS_ID)
                })
                if(goodList){
                    goodList.map((item,i) => {
                        item.N_NUM = 0;
                        for(let j = 0;j < idArr.length;j++){
                            if(item.ID == idArr[j]){
                                item.N_NUM = cartList[j].N_NUM
                            }
                        }  
                    })
                }
                that.setState({
                    goodList: goodList,
                    hasGoodList: true,
                    hasEd: hasEd
                })
                Taro.hideLoading()
            })
        })
    }
    // 获取购物车列表
    getCartNum(index,bNo,types){
        this.isLogin.isLogin(() => {
            common.setCartList(this,'goodList',index,bNo,types,() => {
                if(types == 'add'){ // +
                    this.state.cartNum++;
                }else if(types == 'minus'){ // -
                    this.state.cartNum--;
                }
                this.setState({cartNum: this.state.cartNum})
            }) 
        })
    }
    //  authorize获取作用域
    getIsLogin(obj){
        if(obj.C_MB_NO){ 
            console.log('获取userInfo登陆后的操作') 
        }else{
            this.isLogin = obj
        }	
    }
    // 列表加载更多
    scrollToLowerList(){
        if(this.state.hasEd){return}
        this.state.pageindex++;
        base.showLoading()
        this.setGoodList(this)
    }
    render() {
        return (
            <View className='mainCategory bg-white'>
                {/* 一级分类 */}
                <View className='mainNav'>
                    <Cgnav cgnav={this.state.cgnav} cgnavId={this.state.cgnavId} onTabNav={this.tabNav.bind(this)}></Cgnav>
                </View>
                <View className='mainContent'>
                    {/* 二级分类 */}
                    <View className='mainNavSub'>
                        <CgnavSub cgnavsub={this.state.cgnavsub} cgnavsubId={this.state.cgnavsubId} onTabNavSub={this.tabNavSub.bind(this)}></CgnavSub>
                    </View>
                    {/* 分类商品列表 */}
                    <View className='mainList'>
                        <CgList goodList={this.state.goodList} hasEd={this.state.hasEd} hasGoodList={this.state.hasGoodList}  onGetcartNum={this.getCartNum.bind(this)} onScrollToLowerList={this.scrollToLowerList}></CgList>
                    </View>
                    <View className='cart-bar'>
                        <Cartbar cartNum={this.state.cartNum}></Cartbar>
                    </View>
                </View>
                <TabBar tabId='113' tabList={this.state.tabList}></TabBar>
                <Authorize page='onSetUserInfo' onSetUserInfo={this.getIsLogin.bind(this)}></Authorize>
            </View>
        )
    }
}

