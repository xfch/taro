import Taro, { Component } from '@tarojs/taro'
import { View, Text, Checkbox, Icon } from '@tarojs/components'
import './orderDetail.scss'
import PreView from '../../components/weui/preview'

import { set as setGlobalData, get as getGlobalData } from '../../static/js/global_data'
import api from '../../service/api'
import common from '../../service/common'
import base from '../../static/js/base'

export default class Index extends Component {
    config = {
        navigationBarTitleText: '订单详情'
    }
    constructor() {
        super(...arguments)
        this.state = {
            domainImg: getGlobalData("domainImg"),
            hasView: false
        }
    }

    componentWillMount() {
        console.log(this.$router.params)
        let orderNo = this.$router.params.orderId
        let myExchange = this.$router.params.myExchange || null
        this.setState({myExchange})
        this.getOrderDetal(orderNo,myExchange)
    }
    // 获取订单详情
    getOrderDetal(orderNo,myExchange){
        // let str = 'orderNO&ShopNo'
        // let params = {
        //     bNo: 405,
        //     orderNO: orderNo
        // }
        let str = 'mebNo&ShopNo&subOrderNo'
        let params = {
            bNo: 'GO0102',
            mebNo: 1009047,
            ShopNo: 100233,
            subOrderNo: 'DS191105155110023300157'
        }
        if(myExchange){// 积分兑换订单详情
            str = `ShopNo&N_IGR_NO`
            params = {
                bNo:1306,
                N_IGR_NO: orderNo,
            };
        }
        api.get(str, params, (res) => {
            console.log(res)
            if(myExchange){// 积分兑换订单详情
                this.setState({
                    changeRecord: res.ChangeRecord,
                    hasView: true
                })
            }else{
                if(res.code == '101'){
                    base.showModal(res.msg,null,() => {
                        Taro.navigateBack()
                    })
                }else{
                    res.data.orderInfo.createTime = res.data.orderInfo.createTime.substring(0,10)+" "+res.data.orderInfo.createTime.substring(11,res.data.orderInfo.createTime.length);
                    this.setState({
                        orderDetail: res.data,
                        hasView: true
                    })
                }
            }
            
        })
    }

    render() {
        let {hasView,orderDetail,myExchange,changeRecord} = this.state
        return (

            <View className='mainOrderDetail'>
                {hasView && !myExchange ? 
                    <Block>
                        <View className='p20'>
                            <View className='text-sub'>{orderDetail.orderInfo.createTime}</View>
                        </View>
                        <View className='weui-cells weui-cells-mt0'>
                            {orderDetail.goodsList && orderDetail.goodsList.map((item,i) => {
                                return (
                                    <View for={i} key={i} className='weui-cell'>
                                        <View className='weui-cell__hd'>
                                            <Image className='item-img' src={domainImg + item.goodsImageUrl}></Image>
                                        </View>
                                        <View className='weui-cell__bd'>
                                            <View >{item.goodsName}</View>
                                            <View className='text-small text-gray'>× {item.num}</View>
                                        </View>
                                    </View>
                                )
                            })}
                        </View>
                        <View className='mt20'>
                            <PreView type='view'>
                                <View className='text-big al text-def'>订单详情</View>
                                <PreView label='订单编号：' value={orderDetail.orderInfo.mainOrderNo}></PreView>
                                <PreView label='订单状态：' value={orderDetail.orderInfo.orderState}></PreView>
                                <PreView label='订单金额：' value={orderDetail.orderInfo.totalMoney}></PreView>
                                <PreView label='下单时间：' value={orderDetail.orderInfo.createTime}></PreView>
                                <PreView label='收货人：' value={orderDetail.orderInfo.consigneeName}></PreView>
                                <PreView label='收货地址：' value={orderDetail.orderInfo.consigneeAddress}></PreView>
                                <PreView label='买家留言：' value={orderDetail.orderInfo.remark}></PreView>
                                {/* <PreView label='发货门店：' value={orderDetail.c_shop_name}></PreView> */}
                            </PreView>
                        </View>
                        <View className='mt20'>
                            <PreView type='view'>
                            <View className='text-big al text-def'>配送方式</View>
                                <PreView label='配送方式：' value={orderDetail.deliveryInfo.method}></PreView>
                                {orderDetail.deliveryInfo.method == '自提' &&
                                    <Block>
                                        <PreView label='自提门店：' value={orderDetail.deliveryInfo.storeName}></PreView>
                                        <PreView label='门店店主：' value={orderDetail.deliveryInfo.storeOwner}></PreView>
                                        <PreView label='门店电话：' value={orderDetail.deliveryInfo.storePhone}></PreView>
                                        <PreView label='门店地址：' value={orderDetail.deliveryInfo.storeAddrs}></PreView>
                                    </Block>
                                }
                                <View className='salephone'>联系卖家 { orderDetail.salePhone }</View>
                            </PreView>
                        </View>
                    </Block> :
                    <Block>
                        {changeRecord && 
                            <Block>
                                <View className='weui-cells weui-cells-mt0'>
                                    {changeRecord.RecordDetail && changeRecord.RecordDetail.map((item,i) => (
                                        <View for={i} key={i} className='weui-cell'>
                                            <View className='weui-cell__hd'>
                                                <Image className='item-img' src={domainImg + item.C_MAINPICTURE}></Image>
                                            </View>
                                            <View className='weui-cell__bd'>
                                                <View >{item.GOODS_NAME}</View>
                                                <View className='text-small text-gray'>× {item.N_IGRD_NUM}</View>
                                            </View>
                                        </View>
                                    ))}
                                </View>
                                <View className='mt20'>
                                    <PreView type='view'>
                                        <View className='text-big al text-def'>订单详情</View>
                                        <PreView label='订单编号：' value={changeRecord.N_IGR_NO}></PreView>
                                        <PreView label='花费积分：' value={changeRecord.N_IGR_INTEGRAL + '积分'}></PreView>
                                        <PreView label='下单时间：' value={changeRecord.D_APL_DATE}></PreView>
                                        <PreView label='收货人：' value={changeRecord.C_Name}></PreView>
                                        <PreView label='收货地址：' value={changeRecord.C_Address}></PreView>
                                    </PreView>
                                </View>
                                <View className='mt20'>
                                    <PreView type='view'>
                                        <View className='text-big al text-def'>配送方式</View>
                                        <PreView label='配送方式：' value={changeRecord.ps_method}></PreView>
                                        {changeRecord.ps_method == '上门自提' && 
                                            <Block>
                                                <PreView label='自提门店：' value={changeRecord.TH_ShopName}></PreView>
                                                <PreView label='门店店主：' value={changeRecord.TH_Name}></PreView>
                                                <PreView label='门店电话：' value={changeRecord.TH_Phone}></PreView>
                                                <PreView label='门店地址：' value={changeRecord.TH_Address}></PreView>
                                            </Block>
                                        }
                                    </PreView>
                                </View>
                            </Block>
                        }
                    </Block>
                }
                

                
                
                {/* <View className='footer'>
                    <Footer orderprice={this.state.orderprice} onFootBtn={this.footBtn.bind(this)}></Footer>
                </View> */}


            </View>
        )
    }
}

