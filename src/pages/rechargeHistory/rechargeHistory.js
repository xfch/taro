import Taro, { Component } from '@tarojs/taro'
import { View, Text, Input, Button, Image, Textarea } from '@tarojs/components'

import './rechargeHistory.scss'


import { set as setGlobalData, get as getGlobalData } from '../../static/js/global_data'
import api from '../../service/api'
import common from '../../service/common'
import base from '../../static/js/base'

export default class Index extends Component {
    config = {
        navigationBarTitleText: '充值记录'
    }
    constructor() {
        super(...arguments)
        this.state = {
            domainImg: getGlobalData('domainImg'),
            pageindex: 1,
            pagesize: getGlobalData('pagesize'),
            hasEnd: false,
            hasView: false
        }
    }

    componentWillMount() {
        const prevState = base.prevPage()
        base.showLoading()
        this.getRechargeHistory(prevState.state.userInfo.C_MB_NO)
        this.setState({
            domainImg: prevState.state.domainImg,
            userInfo: prevState.state.userInfo
        })
    }
    getRechargeHistory(C_MB_NO) {
        const str = `mebno&pagesize&pageindex&ShopNo`;
        let params = {
            bNo: 418,
            mebno: C_MB_NO,
            pagesize: this.state.pagesize,
            pageindex: this.state.pageindex,
        };
        let chargedatas = []
        api.get(str, params, (res) => {
            if (res.chargedata) {
                if (res.chargedata.length < this.state.pagesize) {
                    this.state.hasEnd = true
                }
                chargedatas = this.state.pageindex == 1 ? res.chargedata : res.chargedata.concat(this.state.chargedata)
            } else {
                this.state.hasEnd = true
            }
            this.setState({
                chargedata: chargedatas,
                hasEnd: this.state.hasEnd,
                hasView: true
            })
        }, 
        (err) => {},
        (com) => {Taro.hideLoading()
        })
    }
    onReachBottom(){
        if(this.state.hasEnd){return}
        base.showLoading()
        this.state.pageindex++;
        this.getRechargeHistory(this.state.userInfo.C_MB_NO)
    }

    render() {
        return (
            <View className='main-rechargeHistory bg-white'>
                {this.state.hasView &&
                    <Block>
                        {this.state.chargedata.length > 0 &&
                            <Block>
                                {this.state.chargedata.map((item, i) => {
                                    return (
                                        <View for={i} key={i} className='weui-cell'>
                                            <View className='weui-cell__bd'>
                                                <View className=''>订单编号：{item.C_ORD_NO}</View>
                                                <View className='text-small text-gray'>{item.D_PAY_TIME}</View>
                                            </View>
                                            <View className='weui-cell__ft'>
                                                <View className='text-sub'><Text className='text-big'>+{item.N_CHARGE_MONEY}</Text> <Text className='text-little'>元</Text></View>
                                                <View className='text-sub'><Text className=''>+{item.N_INTER}</Text> <Text className='text-little'>积分(获赠)</Text></View>
                                            </View>
                                        </View>
                                    )
                                })}
                                {this.state.hasEnd &&
                                    <View className='weui-cell'>
                                        <View className='weui-cell__bd ac'>没有更多数据了</View>
                                    </View>
                                }
                            </Block>
                        }
                        {this.state.chargedata.length == 0 &&
                            <View className='text-big'>暂无数据</View>
                        }
                    </Block>
                }
            </View>
        )
    }
}

