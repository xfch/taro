import Taro, { Component } from '@tarojs/taro'
import { View, Text, Input, Button } from '@tarojs/components'
import './addressEdit.scss'

import { set as setGlobalData, get as getGlobalData } from '../../static/js/global_data'
import api from '../../service/api'
import base from '../../static/js/base'

import AllAreaInfo from '../../components/allAreaInfo'
import ItemInput from '../../components/itemInput'

export default class Index extends Component {
    config = {
        navigationBarTitleText: '添加地址'
    }
    constructor() {
        super(...arguments)
        this.state = {
            disabled: true, //提交按钮状态
            areaInfoState: false, //地址弹窗是否显示
            addrid: '',//地址id
            edit: false, //默认新增 true 为编辑,
            hasAddress: false,
			addressValue: [0,0,0],
            addressItem: {}, // 新增
        }
    }

    componentWillMount() {
        console.log(this.$router.params)
        console.log(base.prevPage().state)
        this.state.addressItem.C_MB_NAME = ''
        this.state.addressItem.C_MB_PHONE = ''
        this.state.addressItem.C_PROVINCE = ''
        this.state.addressItem.C_CITY = ''
        this.state.addressItem.C_COUNTY = ''
        this.state.addressItem.C_ADDRESS = ''
        let listIndex = this.$router.params.listIndex
        let addressList = base.prevPage().state.addressList
        this.setState({
            addressItem: listIndex ? addressList[listIndex] : this.state.addressItem, //编辑
            disabled: listIndex ? false : true,
            edit: listIndex ? true : false,
            hasAddress: listIndex ? true : false ,
        })
    }
    // 选择地址
    onChange(types, data) {
        if (types == 'show') {
            this.setState({ areaInfoState: true })
        }
    }
    // 选择地址
    selectArea(type,selectArr,province,city,county){
        console.log(1234)
        if(type == '3' || selectArr.length == 0){
            this.state.addressItem.C_PROVINCE = province
            this.state.addressItem.C_CITY = city
            this.state.addressItem.C_COUNTY = county
            this.setState({
                addressItem: this.state.addressItem,
                areaInfoState: false,
                hasAddress: true
            })
        }
    }

    onInput(name,e) {
        let value = e.detail.value
        let addressItem = this.state.addressItem || {}
        switch (name) {
            case 'name':
                addressItem.C_MB_NAME = value
                break;
            case 'phone':
                addressItem.C_MB_PHONE = value
                break;
            case 'address':
                addressItem.C_ADDRESS = value
                break;
            default:    
        }
        this.setState({addressItem: addressItem},()=>{
            this.setDisabled()
        }) 
    }

    setDisabled(){
        let {addressItem} = this.state
        let isAddress = addressItem.C_MB_NAME != '' && addressItem.C_MB_PHONE != '' && addressItem.C_PROVINCE != '' && addressItem.C_CITY != '' && addressItem.C_ADDRESS != ''
        this.setState({disabled: isAddress ? false : true})
    }
    // 提交
    submit() {
        let {addressItem} = this.state
        let userInfo = Taro.getStorageSync('userInfo')
        let openid = Taro.getStorageSync('openid')
        let str = `mebno&province&city&county&addrid&name&phone&address&openid&ShopNo`
        let params = {
            bNo: 1004,
            mebno: userInfo.C_MB_NO,//用户信息id
            province: addressItem.C_PROVINCE,
            city: addressItem.C_CITY,
            county: addressItem.C_COUNTY,
            addrid: addressItem.N_MA_ID || '',
            name: addressItem.C_MB_NAME,
            phone: addressItem.C_MB_PHONE,
            address: addressItem.C_ADDRESS,
            openid: openid,
        }
        api.get(str,params,(res) => {
            base.showToast(res.msg,'success')
            base.prevPage().setState({
                refresh: true
            })
            Taro.navigateBack()
        })
    }
    render() {
        let {addressItem,areaInfoState} = this.state
        return (
            <View className='main-address'>
                <View class="weui-cells weui-cells-none">
                    <ItemInput label='姓名' placeholder='请输入姓名' onInput={this.onInput.bind(this, 'name')} value={ addressItem.C_MB_NAME}></ItemInput>
                    <ItemInput label='手机号码' type='number' placeholder='请输入手机号码' onInput={this.onInput.bind(this, 'phone')} value={ addressItem.C_MB_PHONE}></ItemInput>
                    <ItemInput label='选择地区' contentType='children'>
                        <View className={`weui-input area-input ${this.state.hasAddress ? '' : 'text-placeholder'}`} onClick={this.onChange.bind(this, 'show')}>{this.state.hasAddress ? addressItem.C_PROVINCE + ' ' + addressItem.C_CITY + ' ' + addressItem.C_COUNTY : '请选择地区'}</View>
                    </ItemInput>
                    <ItemInput label='详细地址' placeholder='请输入详细地址' onInput={this.onInput.bind(this, 'address')} value={addressItem.C_ADDRESS}></ItemInput>
                </View>
                {/* 选择地址弹窗 */}
                {areaInfoState && <AllAreaInfo onSelectArea={this.selectArea.bind(this)}></AllAreaInfo>}
                <View className='weui-btn-area pt20'>
                    <View className='btn-groups'>
                        <Button className={this.state.disabled ? 'btn-default' : 'btn-main'} disabled={this.state.disabled}  onClick={this.submit}>提交</Button>
                    </View>
                </View>
            </View>
        )
    }
}

