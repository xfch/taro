import Taro, { Component } from '@tarojs/taro'
import { View, Image, Text } from '@tarojs/components'
import './index.scss'
import { get as getGlobalData } from '../../static/js/global_data'
import api from '../../service/api'

import StarEva from '../../components/starEva'
import Authorize from '../../components/authorize/authorize'



export default class Index extends Component {
    config = {
        navigationBarTitleText: '我的评论'
    }
    constructor() {
        super(...arguments)
    }
    state = {
        urlImg: getGlobalData('domainImg'),
        comList: [],
        pageindex: 1,
        hasList: false
    }
    // authorize获取作用域
    getIsLogin(obj){
        if(obj){
            if(obj.C_MB_NO){ 
                this.getComList()
            }else{
                obj.isLogin(() => {
                    this.getComList()
                })
            }
        }	
    }
    componentWillMount(obj) {
        this.getIsLogin(obj)
    }

    getComList() {
        //获取评论
        let userInfo = Taro.getStorageSync('userInfo')
        let { pageindex, } = this.state;
        let str = `pageindex&pagesize&gid&mebno&ShopNo`
        let params = {
            bNo: 1902,
            pageindex: pageindex,
            pagesize: 10,
            gid: "",
            mebno: userInfo ? userInfo.C_MB_NO : '',//用户信息id
        }
        Taro.showLoading({ title: '加载中' })
        api.get(str, params, (res) => {
            if (pageindex < res.PageCount) {
                pageindex++
            }
            this.setState({
                pageindex,
                comList: res.commentList,
                hasList: true
            })
        }, (res) => {
            Taro.showToast({ title: res.msg })
        }, () => {
            Taro.hideLoading()
            if(pageindex == 1){
                this.setState({
                    userInfo
                })
            }
        })
    }
    onDel(id) {
        /* 删除 */
        Taro.showModal({
            title: '提示',
            content: '确认删除当前评价吗？',
            success: (res) => {
                if (res.confirm) {
                    let str = "comid&ShopNo"
                    let params = {
                        bNo: 1905,
                        comid: id,
                        ShopNo: getGlobalData('SHOP_NO'),
                    };
                    api.get(str, params, () => {
                        this.getComList()
                        Taro.showToast({ title: '删除成功', icon: 'success' })
                    }, (r) => {
                        Taro.showToast({ title: r.msg })
                    }, () => {
                        Taro.hideLoading()
                    })
                }
            }
        })
    }
    onReachBottom() {
        //触底加载
        this.getComList()
    }
    render() {
        const { userInfo, urlImg, comList } = this.state;
        return (
            <View className='commentList'>
                <View className='comHead'>
                    <Image className='comUserImg' src={userInfo.C_MB_PIC}></Image>
                    <View className='comUserName'>{userInfo.C_MB_NAME}</View>
                </View>
                {this.state.hasList &&
                    <View className='list'>
                        {
                            comList.length > 0 ? comList.map((val, i) => (
                                <View className='item' key={'comList' + i}>
                                    <View className='itemL'>
                                        {val.meb_headimg == '' ? <View className='itemUserImg bg-img'></View> : <Image className='itemUserImg' src={val.meb_headimg}></Image>}
                                    </View>
                                    <View className='itemR'>
                                        <View className='itemRItem'>
                                            <Text className='name'>{val.meb_name == '' ? '匿名评论' : val.meb_name}</Text>
                                            <Text className='time'>{val.adddate}</Text>
                                        </View>
                                        <View className='itemRItem'>
                                            <View className='itemRItemStar'>
                                                商家：<StarEva star={val.star}></StarEva>
                                            </View>
                                        </View>
                                        <View className='itemRItem'>
                                            {val.c_content}
                                        </View>
                                        <View className='imgBox'>
                                            {
                                                val.imgsList.map((value, index) => (
                                                    <Block key={'img' + index}>{value.Images&&value.Images != '' ? <Image className='imgs' src={urlImg + value.Images}></Image> : <View className='imgs'></View>}</Block>
                                                ))
                                            }
                                        </View>
                                        <View className='del' onClick={this.onDel.bind(this, val.Id)}>
                                            <Text className='iconfont icon-mainOrder icon'></Text>
                                            <Text>删除</Text>
                                        </View>
                                    </View>
                                </View>
                            )) : <View className='ac'>暂无评论</View>
                        }
                    </View>
                }
                {/* 登陆 */}
                <Authorize page='onSetUserInfo' onSetUserInfo={this.getIsLogin.bind(this)}></Authorize>
            </View >
        )
    }
}

