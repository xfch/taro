import Taro, { Component } from '@tarojs/taro'
import { View, Icon } from '@tarojs/components'
import './index.scss'


import Authorize from '../../components/authorize/authorize';// 授权登陆弹窗
import TabBar from '../../components/tabBar';// 获取底部tab
import TellBtn from '../../components/tellBtn';//客服组件
import OrdSearch from '../../components/index/ordSearch';//所有店铺(搜索)
import Search from '../../components/index/search';// 搜索输入
import ShopInfo from '../../components/index/shopInfo'
import ShopInfos from '../../components/index/shopInfos';// 地图标记
import Banner from '../../components/index/banner';// 跳转
import ProductShow from '../../components/index/productShow';//如果是瀑布流
import UniversalTitle from '../../components/index/universalTitle';	// 点击跳转页面  商品栏目样式有跳转、栏目样式无跳转
import KillZone from '../../components/index/killZone';//活动倒计时
import DiyAdvertisement from '../../components/index/diyAdvertisement';// 点击跳转页面
import GroupBook from '../../components/index/groupBook';// 点击跳转页面
import Coupon from '../../components/index/coupon';// 点击跳转页面  营销-优惠券
import ShopInlet from '../../components/index/shopInlet';//跳转到购物车页面
import Navigation from '../../components/index/navigation';// 点击跳转页面
import DiyExchange from '../../components/index/diyExchange';// 点击跳转页面
import Subscribe from '../../components/index/subscribe';//// 提交信息
import SubscribeEntry from '../../components/index/subscribeEntry';// 跳转'预约服务'
import Article from '../../components/index/article';// 点击跳转页面
import ArticleList from '../../components/index/articleList';// 点击跳转页面
import ShopNotices from '../../components/index/shopNotices';// 点击跳转页面

import { set as setGlobalData, get as getGlobalData } from '../../static/js/global_data';
import api from '../../service/api'
import common from '../../service/common'
import base from '../../static/js/base'

export default class Index extends Component {
    config = {
        navigationBarTitleText: '首页',
        tabBarState: false, //是否有底部菜单
        "pullRefresh": true,
        enablePullDownRefresh: true
    }
    constructor() {
        super(...arguments)
        this.state = {
            diyData: [],
            diyListState: false,
            isLoginState: false,
        }
    }

    componentWillMount() {
        let env = process.env.TARO_ENV
        this.setState({
            env,
            authorize: env == 'weapp' ? true : false
        })
        if(env == 'weapp'){
            common.commonLogin(() => {
                console.log(123)
                this.getDiyList()
            })
        }else{ // 支付宝
            // 获取店铺信息
            base.getExtConfig(() => {
                common.getAddress()
                common.getShopInfo((res) => {
                    Taro.setNavigationBarTitle({
                        title: res.model.c_name
                    })
                    this.getDiyList()
                })
            })
        }  
    }

    componentDidShow() {
        let env = process.env.TARO_ENV
        if(env == 'weapp'){
           const loginType = Taro.getStorageSync('loginType')
            if (loginType) {
                this.state.isLoginState = false
            } else {
                this.state.isLoginState = getGlobalData('isLoginState')
            }
            this.setState({
                isLoginState: this.state.isLoginState
            }) 
        }
        // const loginType = Taro.getStorageSync('loginType')
        // if (loginType) {
        //     this.state.isLoginState = false
        // } else {
        //     this.state.isLoginState = getGlobalData('isLoginState')
        // }
        // this.setState({
        //     isLoginState: this.state.isLoginState
        // })
    }

    componentWillUnmount() {
        // Taro.eventCenter.trigger('sensor', {
        //     /* 调用神策的方法 */
        //     eventName: 'ViewProduct',
        //     properties: {
        //         ProductId: '123456',
        //         ProductCatalog: "Laptop Computer",
        //         ProductName: "MacBook Pro",
        //         ProductPrice: 12345
        //     }
        // });
    }

    componentDidHide() { }

    onPullDownRefresh() {
        this.getDiyList()
    }
    getDiyList() {
        if(this.state.diyListState){ return}
        this.state.diyListState = true
        let str = `pageid&ShopNo`
        let params = {
            bNo: 'GetShopDiy',
            pageid: 1,
            ShopNo: getGlobalData('mainShopNo')
        }
        Taro.showLoading({ title: '加载中...' })
        api.get(str, params, (res) => {
            console.log(res)
            this.state.diyListState = false
            this.setState({
                diyData: []
            }, () => {
                Taro.stopPullDownRefresh()
                Taro.hideLoading()
                const diyData = JSON.parse(res.diyJson)
                console.log(diyData)
                this.state.tabBarState = diyData.some((item) => {
                    return item.modelId == 17
                })
                this.setState({
                    diyData: diyData,
                    tabBarState: this.state.tabBarState 
                }, () => {
                    Taro.stopPullDownRefresh()
                    Taro.hideLoading()
                })
            })
        })
    }


    onReachBottom() {
        if (this.fallsVh) {
            this.fallsVh.setUpData()
        }
        if (this.fallsVw) {
            this.fallsVw.setUpData()
        }
    }
    down() {
        console.log(456)
        this.getDiyList()
    }

    getFallsVw(obj) {
        //获取瀑布流的作用域
        this.fallsVw = obj
    }
    getFallsVh(obj) {
        //获取瀑布流的作用域
        this.fallsVh = obj
    }
    /* 
    活动组件说明：
        1.组件中活动展示，在商品列表中无指定活动（如秒杀拼团等组件）则默认展示第一个活动-如果第一个活动是满减，则展示下一个活动，
        2.组件中展示的活动哪个活动则进入商品详情页也会展示对应的活动，
        3.提交订单后还可以选择参与指定的活动。。
    */
    render() {
        const { diyData } = this.state;
        return (
            <View className={`mainIndex ${this.state.tabBarState ? "mainIndexPb" : ""}`}>
                {/* <Navigator url='/pages/subscribeProject/index'>pages/subscribeProject/index</Navigator> */}
                {
                    diyData.length > 0 ? diyData.map((v, i) => {
                         
                        if (v.modelId == 17) {
                            //底部导航栏
                            return <TabBar tabId='116' tabList={this.state.tabList} key={'moudel' + i}></TabBar>
                        } else if (v.modelId == 18) {
                            /* 带店铺选择的 点击跳转到搜索页面的搜索组件  带店铺选择的搜索框 */
                            /* 这个不能用因为没有做搜索页面 */
                            return <OrdSearch type='shop' key={'moudel' + i} />



                            /* 商品----------------------------------- */
                        } else if (v.modelId == 27) {/* 商品-单商品-横幅 */
                            return <DiyAdvertisement modelId={v.modelId} data={v.data} key={'moudel' + i} />
                        } else if (v.modelId == 4) {/*商品-多商品-横向商品*/
                            return <ProductShow type='vw' proData={v.data} key={'moudel' + i} />
                        } else if (v.modelId == 5) {/*商品-多商品-纵向商品*/
                            return <ProductShow type='vh' proData={v.data} key={'moudel' + i} />
                        } else if (v.modelId == 10) {/*商品-多商品-纵向瀑布流商品可下拉加载*/
                            return <ProductShow onGetThis={this.getFallsVw.bind(this)} type='vw' isFalls proData={v.data} key={'moudel' + i} />
                        } else if (v.modelId == 11) {/*商品-多商品-横向瀑布流商品可下拉加载*/
                            return <ProductShow onGetThis={this.getFallsVh.bind(this)} type='vh' isFalls proData={v.data} key={'moudel' + i} />
                        } else if (v.modelId == 16) {
                            /*商品-多商品-点餐 - 没做*/
                        } else if (v.modelId == 26) {/*商品-多商品-积分兑换*/
                            return <DiyExchange modelId={v.modelId} proData={v.data} key={'moudel' + i}></DiyExchange>


                            /* 信息----------------------------------- */
                        } else if (v.modelId == 7) {/* 信息-单条新闻-推荐新闻 */
                            return <Article modelId={v.modelId} data={v.data} key={'moudel' + i} />
                        } else if (v.modelId == 28) {/* 信息-单条新闻-单条新闻 */
                            return <Article modelId={v.modelId} data={v.data} key={'moudel' + i} />
                        } else if (v.modelId == 8) {/* 信息-多条新闻-纵向新闻 */
                            return <ArticleList isLoading='true' modelId={v.modelId} data={v.data} key={'moudel' + i} />
                        } else if (v.modelId == 25) {/* 信息-多条新闻-横向新闻 */
                            return <ArticleList isLoading='true' modelId={v.modelId} data={v.data} key={'moudel' + i} />
                        } else if (v.modelId == 15) {/* 信息-多条新闻-橱窗式新闻 */
                            return <ArticleList isLoading='true' modelId={v.modelId} data={v.data} key={'moudel' + i} />
                        } else if (v.modelId == 41) {/* 信息-栏目入口-新闻栏目-样式1*/
                            return <UniversalTitle modelId={v.modelId} titleData={v.data} key={'moudel' + i} />
                        } else if (v.modelId == 42) {/* 信息-栏目入口-新闻栏目-样式1*/
                            return <UniversalTitle modelId={v.modelId} titleData={v.data} key={'moudel' + i} />
                        } else if (v.modelId == 43) {/* 信息-栏目入口-新闻栏目-样式1*/
                            return <UniversalTitle modelId={v.modelId} titleData={v.data} key={'moudel' + i} />

                            /* 营销------------------------------ */
                        } else if (v.modelId == 3) {/* 营销-优惠卷组件*/
                            return <Coupon modelId={v.modelId} data={v.data} key={'moudel' + i} />
                        } else if (v.modelId == 6) {/* 营销-秒杀组件*/
                            return <KillZone killData={v.data} key={'moudel' + i} />
                        } else if (v.modelId == 61) {/* 营销-拼团 */
                            return <GroupBook modelId={v.modelId} data={v.data} key={'moudel' + i} />

                               /* 店铺相关----------------------------------- */
                        } else if (v.modelId == 13) {/* 店铺相关-店铺公告 */
                            return <ShopNotices modelId={v.modelId} data={v.data} key={'moudel' + i} />
                        } else if (v.modelId == 12) {/* 店铺相关-单门店-单门店-样式1 */
                            return <ShopInfo modelId={v.modelId} proData={v.data} key={'moudel' + i} />
                        } else if (v.modelId == 24) {/* 店铺相关-单门店-单门店-样式2 */
                            return <ShopInfo modelId={v.modelId} proData={v.data} key={'moudel' + i} />
                        } else if (v.modelId == 34) {/* 店铺相关-单门店-单门店点餐入口 */
                            return <ShopInlet data={v.data} key={'moudel' + i} />
                        } else if (v.modelId == 21) {/* 店铺相关-多门店-多门店样式1 */
                            return <ShopInfos onChangeShopList={this.down} modelId={v.modelId} proData={v.data} key={'moudel' + i} />
                        } else if (v.modelId == 22) {/* 店铺相关-多门店-多门店样式2 */
                            return <ShopInfos modelId={v.modelId} proData={v.data} key={'moudel' + i} />
                        } else if (v.modelId == 23) {/* 店铺相关-多门店-多门店入口-样式1 */
                            return <ShopInfos modelId={v.modelId} proData={v.data} key={'moudel' + i} />



                            /* 公共组件-------------------------------- */
                        } else if (v.modelId == 0) {/* 公共组件-搜索-搜索*/
                            // return <OrdSearch type='skip' key={'moudel' + i} />
                            return <Search modelId={v.modelId} type='style' key={'moudel' + i} />
                        } else if (v.modelId == 14) {/* 公共组件-搜索-浮动式搜索*/
                            return <Search modelId={v.modelId} type='absolute' key={'moudel' + i} />
                        }else if (v.modelId == 1) {/* 公共组件-轮播图*/
                            return <Banner modelId={v.modelId} data={v.data} key={'moudel' + i}></Banner>
                        }else if (v.modelId == 9) {/* 公共组件-预约服务*/
                            return <Subscribe modelId={v.modelId} data={v.data} key={'moudel' + i}></Subscribe>
                        }else if (v.modelId == 71) {/* 公共组件-预约入口-样式1  //203 n_yjhid 390美业 316餐饮 2626零售*/
                            return <SubscribeEntry modelId={v.modelId} data={v.data} key={'moudel' + i}></SubscribeEntry>



                        } else if (v.modelId == 30) {/* 公共组件-栏目入口-栏目 - 样式1*/
                            return <UniversalTitle modelId={v.modelId} titleData={v.data} key={'moudel' + i} />
                        } else if (v.modelId == 47) {/* 公共组件-栏目入口-栏目 - 样式2 */
                            return <UniversalTitle modelId={v.modelId} titleData={v.data} key={'moudel' + i} />
                        } else if (v.modelId == 48) {/* 公共组件-栏目入口-栏目 - 样式3 */
                            return <UniversalTitle modelId={v.modelId} titleData={v.data} key={'moudel' + i} />
                        } else if (v.modelId == 31) {/* 公共组件-栏目入口-商品栏目 - 样式1 */
                            return <UniversalTitle modelId={v.modelId} titleData={v.data} key={'moudel' + i} />
                        } else if (v.modelId == 32) {/* 公共组件-栏目入口-商品栏目 - 样式2 */
                            return <UniversalTitle modelId={v.modelId} titleData={v.data} key={'moudel' + i} />
                        } else if (v.modelId == 33) { /* 公共组件-栏目入口-商品栏目 - 样式3 */
                            return <UniversalTitle modelId={v.modelId} titleData={v.data} key={'moudel' + i} />
                        } else if (v.modelId == 51) {/* 公共组件-栏目入口-导航样式1  只做了商品列表跳转*/
                            return <Navigation data={v.data} modelId={v.modelId} key={'moudel' + i} />
                        } else if (v.modelId == 52) {/* 公共组件-栏目入口-导航样式2 */
                            return <Navigation data={v.data} modelId={v.modelId} key={'moudel' + i} />
                        } else if (v.modelId == 53) {/* 公共组件-栏目入口-导航样式3 */
                            return <Navigation data={v.data} modelId={v.modelId} key={'moudel' + i} />
                        } else if (v.modelId == 54) {/* 公共组件-栏目入口-导航样式4 */
                            return <Navigation data={v.data} modelId={v.modelId} key={'moudel' + i} />
                        } else if (v.modelId == 55) {/* 公共组件-栏目入口-导航样式5 */
                            return <Navigation data={v.data} modelId={v.modelId} key={'moudel' + i} />
                        }
                        
                    }) : ''
                }
                {this.state.env == 'weapp'&& <Authorize></Authorize>}
                <TellBtn></TellBtn>
            </View>
        )
    }
}

