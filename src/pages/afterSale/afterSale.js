import Taro, { Component } from '@tarojs/taro'
import { View, Text, Input, Button } from '@tarojs/components'
import './afterSale.scss'


import { set as setGlobalData, get as getGlobalData } from '../../static/js/global_data'
import api from '../../service/api'
import common from '../../service/common'
import base from '../../static/js/base'

import Checkboxbar from '../../components/checkboxbar'
import ServiceItem from '../../components/afterSale/serviceItem'
import Good from '../../components/afterSale/good'
import Reson from '../../components/afterSale/reson'

export default class Index extends Component {
    config = {
        navigationBarTitleText: '售后申请'
    }
    constructor() {
        super(...arguments)
        this.state = {
            btnList: [
                {'txt': '仅退款','relx': '1'},
                {'txt': '退款退货','relx': '2'},
            ],
            domainImg: getGlobalData('domainImg'),
            orderItem: {
                glist: null
            },
        }
    }

    componentWillMount() {
        let orderItem = JSON.parse(this.$router.params.orderItem)
        console.log(orderItem)
        orderItem.glist.map((item,i) => {
            item.select = false
            item.nums = item.CanReNum
        })
        this.setState({orderItem: orderItem})
    }
    // 服务项
    changeRelx(relx){
        this.setState({relx: relx})
    }
    // 选择商品
    selectCheck(glist){
        glist.map((item,i) => {
            if(item.select && item.nums == 0){
                item.nums = item.CanReNum
            }
        })
        this.state.orderItem.glist = glist
        this.setState({glist: this.state.orderItem})
    }
    // 选择退款件数
    changeNum(index,types){
        const orderItem = this.state.orderItem
        if(types == 'add'){ // +
            if(orderItem.glist[index].nums >= orderItem.glist[index].CanReNum){
                base.showToast('不能再加了！', 'none')
                return
            }
            orderItem.glist[index].nums++
        }else if(types == 'minus'){ // -
            orderItem.glist[index].nums--
            if(orderItem.glist[index].nums == 0){
                orderItem.glist[index].select = false 
            }
        }
        this.setState({orderItem: orderItem})
    }
    // 退款原因
    onInput(reson){
        this.setState({reson})
    }
    // 提交
    submit(){
        let ReGoods = []
        this.state.orderItem.glist.map((item,i) => {
            if(item.select){
                ReGoods.push(`${item.N_GOODS_ID},${item.nums}`)
            }
        })
        if(!this.state.relx){base.showToast('请选择服务项','none'); return}
        if(ReGoods.length == 0){base.showToast('请选择退款/退货商品','none'); return}
        if(!this.state.reson || this.state.reson.length < 10) {
            base.showToast('请至少输入10个字的退款原因','none')
            return
        }
        let str = `orderNo&ReGoods&reson&relx&ShopNo`
        let params = {
            bNo: 427,
            orderNo: this.state.orderItem.c_ord_no,
            reson: this.state.reson,
            ReGoods: ReGoods.join(";"),
            relx: this.state.relx,
        }
        api.get(str,params,(res) => {
            base.showToast(res.msg)
            setTimeout(function(){
                Taro.navigateTo({url: '/pages/order/order?stateId=0'})
            },500)
        })
    }
    render() {
        return (
            <View className='main-after-sale'>
                {/* 服务项 */}
                <View className='weui-cells'>
                    <ServiceItem btnList={this.state.btnList} relx={this.state.relx} onChangeRelx={this.changeRelx.bind(this)} class-mr='mr20'></ServiceItem>
                </View>
                {/* 订单商品列表 */}
                <View className='weui-cells'>
                    <Good onSelectCheck={this.selectCheck.bind(this)} onChangeCartNum={this.changeNum.bind(this)} domainImg={this.state.domainImg} glist={this.state.orderItem.glist} class-mr='mr20'></Good>
                </View>
                {/* 订单退款原因 */}
                <View className='weui-cells'>
                    <View className='p20'><Reson class-pb='pb10' onInput={this.onInput.bind(this)}></Reson></View>
                </View>
                <View className='weui-btn-area'>
                    <Button className='weui-btn' type='main' onClick={this.submit}>提交</Button>
                </View>
            </View>
        )
    }
}

