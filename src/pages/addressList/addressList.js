import Taro, { Component } from '@tarojs/taro'
import { View, Text, Input, Button } from '@tarojs/components'
import './addressList.scss'


import { set as setGlobalData, get as getGlobalData } from '../../static/js/global_data'
import api from '../../service/api'
import common from '../../service/common'
import base from '../../static/js/base'

import Checkboxbar from '../../components/checkboxbar'
import Authorize from '../../components/authorize/authorize'

export default class Index extends Component {
    config = {
        navigationBarTitleText: '收货地址'
    }
    constructor() {
        super(...arguments)
        this.state = {
            addressList: [],
            ViewShow: false
        }
    }
    // authorize获取作用域
    getIsLogin(obj){
        if(obj){
            if(obj.C_MB_NO){ 
                common.getAddressList(this)
            }else{
                obj.isLogin(() => {
                    common.getAddressList(this)
                })
            }
        }	
    }
    componentWillMount(obj) {
        let orderSubmit = base.prevPage().state.orderSubmit
        this.getIsLogin(obj)
        this.setState({
            orderSubmit: orderSubmit || false //是否是提交订单页进来的
        })
    }
    componentDidShow() {
        if (this.state.refresh) {
            common.getAddressList(this)
        }
    }
    // 删除、修改
    changeAddress(id, index) {
        if (id == 'edit') { //编辑
            Taro.navigateTo({ url: '/pages/addressEdit/addressEdit?listIndex=' + index })
        } else if (id == 'del') { // 删除
            console.log(this.state.addressList[index])
            base.showModals('是否确定删除？', null, (res) => {
                if (res.confirm) {
                    this.delAddress(this.state.addressList[index].N_MA_ID, () => {
                        let Index = null
                        let address = null
                        this.state.addressList.splice(index, 1)
                        this.state.addressList.map((item, i) => {
                            if(item.N_ISDEFAULT == 1){
                                Index = i.toString()
                                address = item
                            }
                        })
                        if(!Index && this.state.addressList.length > 0){
                            this.state.addressList[0].N_ISDEFAULT = 1
                            address = this.state.addressList[0]
                        }
                        if (this.state.orderSubmit) {
                            base.prevPage().setState({
                                address: address,
                            })
                        }
                        this.setState({
                            addressList: this.state.addressList
                        })
                    })
                }
            })
        }
    }
    // 删除地址
    delAddress(addrid, callback) {
        let str = 'maid'
        let params = {
            bNo: '1006',
            maid: addrid
        }
        api.get(str, params, (res) => {
            console.log(res)
            if (callback) { callback() }
        }, (err) => {
            console.log(err)
        })
    }
    // 设置默认地址
    selectCheck(maid) {
        const that = this
        console.log(maid)
        let address = {}
        common.addressDefault(maid, (res) => {
            that.state.addressList.map((item, i) => {
                item.N_ISDEFAULT = maid == item.N_MA_ID ? 1 : 0
                if (item.N_ISDEFAULT == 1) {
                    address = item
                }
            })
            if (this.state.orderSubmit) {
                base.prevPage().setState({
                    address: address,
                })
            }
            that.setState({ addressList: that.state.addressList })
        })
    }
    // 选择地址
    selectAdd(index) {
        console.log(this.state.addressList)
        let address = this.state.addressList[index];
        if (this.state.orderSubmit) {
            base.prevPage().setState({
                address: address,
            })
            Taro.navigateBack()
        }
    }
    // 添加地址
    submit(type) {
        Taro.navigateTo({ url: '/pages/addressEdit/addressEdit' })
    }
    render() {
        return (
            <View className='main-address'>
                {this.state.addressList && this.state.addressList.map((item, i) => {
                    return (
                        <View for={i} key={'address' + i} class="weui-cells weui-cells-none">
                            <View className='weui-cell' onClick={this.selectAdd.bind(this, i)}>
                                <View className='weui-cell__bd'>
                                    <View className='text-big'>{item.C_MB_NAME}，{item.C_MB_PHONE}</View>
                                    <View className='text-gray'>{item.C_PROVINCE + ' ' + item.C_CITY + ' ' + item.C_COUNTY + ' ' + item.C_ADDRESS}</View>
                                </View>
                            </View>
                            <View className='weui-cell'>
                                <View className='weui-cell__bd'>
                                    <Checkboxbar onSelectCheck={this.selectCheck.bind(this, item.N_MA_ID)} select={item.N_ISDEFAULT}>
                                        <Text className='pl20' onClick={this.selectCheck.bind(this, item.N_MA_ID)}>设为默认</Text>
                                    </Checkboxbar>
                                </View>
                                <View className='weui-cell__ft'>
                                    <Text className='plr10' onClick={this.changeAddress.bind(this, 'edit', i)}>修改</Text>
                                    <Text className='plr10' onClick={this.changeAddress.bind(this, 'del', i)}>删除</Text>
                                </View>
                            </View>
                        </View>
                    )
                })}
                <View className='weui-btn-area pt20'>
                    <View className='btn-groups'>
                        <Button className='btn-main' onClick={this.submit.bind(this, 'add')}>添加地址</Button>
                    </View>
                </View>
                {/* 登陆 */}
                <Authorize page='onSetUserInfo' onSetUserInfo={this.getIsLogin.bind(this)}></Authorize>
            </View>
        )
    }
}

