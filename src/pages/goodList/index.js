import Taro, { Component } from '@tarojs/taro'
import { View, Text, Block, Image } from '@tarojs/components'
import './index.scss'

import ProductShow from '../../components/index/productShow'

import { set as setGlobalData, get as getGlobalData } from '../../static/js/global_data'
import api from '../../service/api'
import base from '../../static/js/base'
import common from '../../service/common'

export default class Index extends Component {
    config = {
        navigationBarTitleText: '',
        navigationBarBackgroundColor: "#fe690f",
    }
    constructor() {
        super(...arguments)
        this.state = {
            pageindex: 1,
            hasEd: false,
            hasList: false,
            proData: {
                cname: null,
                isbtm: 0
            },
            hasParams: false,
        }
    }
    componentWillMount() {
        const cname = this.$router.params.gcid
        if(cname){
            this.state.proData.cname = cname
        }
        this.setState({
            proData: this.state.proData,
            hasParams: true
        })
        Taro.setNavigationBarTitle({
            title: cname
        })
    }
    onReachBottom() {
        if (this.fallsVh) {
            this.fallsVh.setUpData()
        }
        if (this.fallsVw) {
            this.fallsVw.setUpData()
        }
    }
    getFallsVw(obj) {
        //获取瀑布流的作用域
        this.fallsVw = obj
    }
    getFallsVh(obj) {
        //获取瀑布流的作用域
        this.fallsVh = obj
    }
    
    render() {
        let {hasParams,proData} = this.state
        return (
            <View className='main-goodlist'>
                {hasParams && <ProductShow onGetThis={this.getFallsVw.bind(this)} type='vw' proData={proData} isFalls='true' />}
            </View>
        )
    }
}

