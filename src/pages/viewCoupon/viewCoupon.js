import Taro, { Component } from '@tarojs/taro'
import { View, Text } from '@tarojs/components'
import './viewCoupon.scss'

import base from '../../static/js/base'
import coupon from '../../service/coupon'

export default class Index extends Component {
    config = {
        navigationBarTitleText: '我的优惠卷'
    }
    constructor() {
        super(...arguments)
    }
    state = {
        couponList: [],
        hasList: false
    } 
    componentDidShow(){
        console.log(this.$router.params)
        let params = this.$router.params
        if(params.choose){
            if(params.amount && params.amount > 0){
                let param = {
                    OrderPrice: params.amount
                }
                base.showLoading()
                coupon.getCouponUse(param,(res) => {
                    if(!res.Coupons){res.Coupons = []}
                    if(params.MarketingCId){
                        res.Coupons.map((item) => {
                            if(params.MarketingCId == item.MarketingCId){
                                item.checked = true
                            }
                        })
                    }
                    this.setState({
                        couponList: res.Coupons,
                        choose: true,
                        hasList: true
                    })
                    Taro.hideLoading()
                })
                coupon.getCouponList(null,(res) => {
                    console.log(res)
                })
            }
        }else{
            base.showLoading()
            coupon.getCouponList(null,(res) => {
                if(!res.Coupons){res.Coupons = []}
                this.setState({
                    couponList: res.Coupons,
                    hasList: true
                })
                Taro.hideLoading()
            })
        }   
    }
    // 选择优惠券
    selectCoupon(index,cp){
        let {couponList} = this.state
        couponList.map((item) => {
            item.checked = false
            if(cp.MarketingCId == item.MarketingCId){
                item.checked = true
            }
        })
        this.setState({
            couponList,
        })
        base.prevPage().setState({
            couponed: couponList[index]
        })
        Taro.navigateBack()
        console.log(cp)
    }
    // 跳转领取优惠券页面
    navTo(){
        Taro.navigateTo({
            url: '/pages/couponPage/index'
        })
    }
    render() {
        const { choose,couponList,hasList } = this.state;
        return (
            <View className='couponPage'>
                {hasList && 
                    <Block>
                        {
                            couponList && couponList.length > 0 ? couponList.map((val, i) => (
                                <View className='couponItem' key={'couponItem' + i}>
                                    {choose ? 
                                        <Block>
                                            <View className='couponLeft'>
                                                <Text>￥</Text>
                                                <Text className='couponMoney'>{val.MarketingPrice}</Text>
                                            </View>
                                        </Block> :
                                        <Block>
                                            {
                                                val.N_MC_BANKNOTE ? (
                                                    <View className='couponLeft'>
                                                            {val.N_MC_TYPE == 1 || val.N_MC_TYPE == 3 ? <Text>￥</Text> : ''}
                                                            <Text className='couponMoney'>{val.N_MC_BANKNOTE}</Text>
                                                            {val.N_MC_TYPE == 2 && <Text>折</Text>}
                                                    </View>
                                                ) : (
                                                        <View className='couponLeft'>
                                                            <Text>随机代金券</Text>
                                                        </View>
                                                    )
                                            }
                                        </Block> 
                                    }
                                    
                                    <View className='couponCenter'>
                                        <View className='ctop'>{val.C_MC_NAME || val.c_mc_name || val.MarketingName}</View>
                                        {!choose && <View className='cbottom'>{val.N_MC_Scope}<Text>({val.C_MC_TYPE})</Text></View>}
                                    </View>
                                    <View className='couponRight'>
                                        {choose ? 
                                            <Block>
                                                {!val.checked ? <View className='couponGet' onClick={this.selectCoupon.bind(this,i,val)}>选择</View> : <View className='couponRedio'>已选择</View>}
                                            </Block> : 
                                            <Block>
                                                {val.N_MCR_STATE == 1 ? <View className='couponGet'>未使用</View> : <View className='couponRedio'>已使用</View>}
                                            </Block>
                                        }
                                    </View>
                                </View>
                            )) : <View className='hasNoImg'>暂无数据</View>
                        }
                        <View className='weui-cell'>
                            <View className='weui-cell__bd'><Text className='text-big' onClick={this.navTo}>领取优惠券</Text></View>
                        </View>
                    </Block>
                }
            </View>
        )
    }
}

