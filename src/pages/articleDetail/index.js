import Taro, { Component } from '@tarojs/taro'
import { View, Swiper, SwiperItem, Image, Text, RichText } from '@tarojs/components'
import './index.scss'

import { set as setGlobalData, get as getGlobalData } from '../../static/js/global_data'

import base from '../../static/js/base'
import article from '../../service/article'

export default class Index extends Component {
    config = {
        navigationBarTitleText: '文章详情',
        navigationBarBackgroundColor: "#fe690f",
    }
    constructor() {
        super(...arguments)
        this.state = {
            richTxt: null,
            hasView: false,
            fontSize: [{'text': '小','size': '26rpx'},{'text': '中','size': '30rpx'},{'text': '大','size': '34rpx'}],
            fontSizeIndex: 1,
        }
    }

    componentDidShow(){
        const that = this;
        this.state.ntid = this.$router.params.ntid
        base.showLoading()
        article.getNewDetail(that,(res,richTxt) => {
            that.setState({
                hasView: true,
                newDetail: res.sys_notice,
                richTxt: richTxt
            })
            Taro.hideLoading()
        })
    }
    // 切换字号
    changeFontSize(index){
        this.setState({fontSizeIndex: index})
    }

    render() {
        let { hasView,newDetail,richTxt,fontSize,fontSizeIndex} = this.state
        return (
            <View className='main-acticleDetail'>
                {hasView && 
                    <Block>
                        <View className='weui-cell bottomLine'>
                            <View className='weui-cell__bd'>
                                <View className='text-big'>{newDetail.C_SN_TITLE}</View>
                                <View className='text-gray text-small'>
                                    {newDetail.D_OP_DATE}<Text className='plr20'>|</Text>
                                    阅读：{newDetail.N_CLICK}<Text className='plr20'>|</Text>
                                    字号：{fontSize.map((item,i) => (
                                        <Text key={'size' + i} className={`plr10 ${fontSizeIndex == i ? 'text-sub' : ''}`} onClick={this.changeFontSize.bind(this,i)} >{item.text}</Text>
                                    ))}
                                </View>
                            </View>
                        </View>
                        <View style={`font-size: ${fontSize[fontSizeIndex].size}`}>
                            {newDetail.C_SN_DES != '' && <View className='detail-des'>简介：{newDetail.C_SN_DES}</View>}
                            {richTxt ? <RichText className='goodsRichText' nodes={richTxt}></RichText> : <View className='no-data'>暂无详情</View>}
                        </View>
                    </Block>
                }
            </View>
        )
    }
}

