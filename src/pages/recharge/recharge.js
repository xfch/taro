import Taro, { Component } from '@tarojs/taro'
import { View, Text, Input, Button, Image, Textarea } from '@tarojs/components'

import './recharge.scss'

import Authorize from '../../components/authorize/authorize'

import { set as setGlobalData, get as getGlobalData } from '../../static/js/global_data'
import api from '../../service/api'
import common from '../../service/common'
import base from '../../static/js/base'

export default class Index extends Component {
    config = {
        navigationBarTitleText: '充值'
    }
    constructor() {
        super(...arguments)
        this.state = {
            domainImg: getGlobalData('domainImg'),
            rechargeDtIndex: 0,
            rechargeNum: 100, //充值金额
            rechargeAct: false, // 是否参与活动
            disabled: false,
        }
    }

    componentWillMount() {
        const userInfo = Taro.getStorageSync('userInfo')
        base.showLoading()
        this.setState({
            userInfo: userInfo
        })
        this.getRechargeScale()
    }
    // 获取充值活动
    getRechargeScale() {
        var str = `ShopNo`
        var params = {
            bNo: 417
        };
        api.get(str, params).then((err) => {
            console.log(err)
            Taro.hideLoading()
        }).catch((res) => {
            if (res.rechargeDt && res.rechargeDt.length > 0) {
                res.rechargeDt[0].select = true
                this.state.rechargeNum = res.rechargeDt[0].N_RechargeAccount
                this.state.rechargeAct = true
            }
            this.setState({
                rechargeDt: res.rechargeDt,
                rechargeNum: this.state.rechargeNum,
                rechargeAct: this.state.rechargeAct
            })
            Taro.hideLoading()
        })
    }
    // 切换充值活动
    rechargeDtChange(index) {
        this.setState({
            rechargeDtIndex: index,
            rechargeNum: this.state.rechargeDt[index].N_RechargeAccount
        })
    }
    // 获取充值金额
    onInput(e){
        const value = e.detail.value
        if(this.state.rechargeDt){
            let arr = []
            this.state.rechargeAct = this.state.rechargeDt.some((item) => {
                return value >= item.N_RechargeAccount;
            });

            this.state.rechargeDt.map((item,i) => {
                if(value >= item.N_RechargeAccount){
                    arr.push(item.N_RechargeAccount)
                }
            })
            arr.sort(this.sortNumber)
            this.state.rechargeDt.map((item,i) => {
                if(arr[arr.length - 1] == item.N_RechargeAccount){
                    this.state.rechargeDtIndex = i
                }
            })
        }
        this.setState({
            rechargeNum: value,
            rechargeAct: this.state.rechargeAct,
            rechargeDtIndex: this.state.rechargeDtIndex
        })
    }
    // 排序
    sortNumber(a,b){
        return a - b
    }
    isDisabled(callback){
        let {rechargeNum} = this.state
        if(!rechargeNum){
            base.showModal('请输入充值金额')
            return
        }
        if(callback){callback()}
    }
    // 充值接口
    rechargeSubmit(){
        this.isDisabled(() => {
            if(this.state.disabled){
                return
            }
            base.showLoading('充值中...')
            const userInfo = Taro.getStorageSync('userInfo')
            this.state.disabled = true
            const mckid = this.state.rechargeAct ? this.state.rechargeDt[this.state.rechargeDtIndex].N_Id : 0
            const str = `mebno&chargeamount&zsje&zsjf&ShopNo`
            const params = {
                bNo: '410',
                mebno: userInfo.C_MB_NO,
                chargeamount: this.state.rechargeNum,
                zsje: 0,
                zsjf: 0,
                mckid: mckid,
            }
            api.get(str, params).then((res) => {
                this.setState({
                    orderNO: res.orderNO
                })
                common.payment(res.orderNO,null,null,() => {
                    this.state.disabled = false
                    Taro.hideLoading()
                })
            }).catch((err) => {
                console.log(err)
                Taro.hideLoading()
            })
        })
        
    }
    // authorize获取作用域
    getIsLogin(obj){
        console.log(obj)
        if(obj.C_MB_NO){ // 登陆成功后充值
            this.rechargeSubmit()
        }else{
            this.isLogin = obj
        }
        
    }
    // 充值
    submit(){
        this.isDisabled(() => {
            this.isLogin.isLogin(() => {
                this.rechargeSubmit()   
            })
        })
        
    }

    render() {
        let { rechargeDt, rechargeDtIndex,rechargeAct,rechargeNum } = this.state
        return (
            <View className='main-recharge'>
                <View className='p20'>充值金额</View>
                <View className='bg-white'>
                    <View className='weui-cell'>
                        <View className='weui-cell__hd'>￥</View>
                        <View className='weui-cell__bd'>
                            <Input type='digit' placeholderClass='text-placeholder' className='recharge-num' onInput={this.onInput} placeholder='充值金额' value={this.state.rechargeNum} />
                        </View>
                        <View className='weui-cell__ft'>
                            <Button className={`recharge-btn ${rechargeNum ? '' : 'recharge-btn-disabled'}`}  onClick={this.submit}>充值</Button>
                        </View>
                    </View>
                    {rechargeDt && rechargeAct &&
                        <View className='weui-cell'>
                            <View className='weui-cell__hd'>
                                <Text className='activity-tag'>活动</Text>
                            </View>
                            <View className='weui-cell__bd'>充值 {rechargeDt[rechargeDtIndex].N_RechargeAccount}元
                                {rechargeDt[rechargeDtIndex].N_GiftAccount != 0 && rechargeDt[rechargeDtIndex].N_GiftInteger != 0 && <Text className='pl20'>获赠</Text>}
                                {rechargeDt[rechargeDtIndex].N_GiftAccount != 0 &&
                                    <Text className='pl10'> {rechargeDt[rechargeDtIndex].N_GiftAccount}元</Text>
                                }
                                {rechargeDt[rechargeDtIndex].N_GiftInteger != 0 &&
                                    <Text className='pl10'> {rechargeDt[rechargeDtIndex].N_GiftInteger}积分</Text>
                                }
                            </View>
                        </View>
                    }
                </View>
                <View className='recharge-preset weui-flex'>
                    {rechargeDt && rechargeDt.map((item, i) => {
                        return (
                            <View for={i} key={i} className={`tag ${rechargeDtIndex == i && rechargeAct ? 'tag-active' : ''}`} onClick={this.rechargeDtChange.bind(this, i)}>
                                <View className='text-big strong'>充值{item.N_RechargeAccount}元</View>
                                <View>
                                    <Text>赠送</Text>
                                    {item.N_GiftAccount != 0 && <Text className='mt10'>{item.N_GiftAccount}元</Text>}
                                    {item.N_GiftAccount != 0 && item.N_GiftInteger != 0 && <Text>、</Text>}
                                    {item.N_GiftInteger != 0 && <Text className='mt10'>赠送{item.N_GiftInteger}积分</Text>}
                                </View>
                            </View>
                        )
                    })}
                </View>
                <Authorize page='onSetUserInfo' onSetUserInfo={this.getIsLogin.bind(this)}></Authorize>
            </View>
        )
    }
}

