import Taro, { Component } from '@tarojs/taro'
import { View, Text } from '@tarojs/components'

import './index.scss'

import Nav from '../../components/good/nav'
import ProductShow from '../../components/index/productShow'

import { set as setGlobalData, get as getGlobalData } from '../../static/js/global_data'
import api from '../../service/api'
import base from '../../static/js/base'
import common from '../../service/common'

export default class Index extends Component {
    config = {
        navigationBarTitleText: '挑选商品/服务',
    }
    constructor() {
        super(...arguments)
        this.state = {
            cid: null,
            type: 'vw',
        }
    }
    componentWillMount() {
         
    }

    tabNav(cid){
        this.setState({
            cid: cid
        },() => {
            if (this.fallsVh) {
                setGlobalData('subscribeProject', null)
                this.fallsVh.setUpData(true)
            }
        })
        
    }
    
    onReachBottom() {
        if (this.fallsVh) {
            this.fallsVh.setUpData()
        }
    }
    
    getFallsVh(obj) {
        this.fallsVh = obj
    }

    render() {
        let { cid,type } = this.state
        return (
            <View className='page-subscribeProject'>
                {/* 一级分类 */}
                <View className='mainNav bg-white'>
                    <Nav scrollType='x' onTabNav={this.tabNav.bind(this)}></Nav>
                </View>
                {/* 分类商品列表 */}
                {cid && 
                    <View className='mainContent'>
                        <View className='mainList'>
                            <ProductShow onGetThis={this.getFallsVh.bind(this)} type={type} cid={cid} isFalls page='subscribeProject' />
                        </View>
                    </View>
                }
            </View>
        )
    }
}

