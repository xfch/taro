import Taro, { Component } from '@tarojs/taro'
import { View, Text, Block, Image } from '@tarojs/components'
import './index.scss'

import ExchangeItem from '../../components/index/exchangeItem'
import NoData from '../../components/noData'

import { set as setGlobalData, get as getGlobalData } from '../../static/js/global_data'
import api from '../../service/api'
import base from '../../static/js/base'
import common from '../../service/common'

export default class Index extends Component {
    config = {
        navigationBarTitleText: '',
        navigationBarBackgroundColor: "#fe690f",
    }
    constructor() {
        super(...arguments)
        this.state = {
            pageindex: 1,
            hasList: false,
            hasEd: false,
            modelId: null,
        }
    }
    componentWillMount() {
        let cname = '积分兑换' 
        this.state.modelId = this.$router.params.modelId
        if(this.state.modelId == '61'){ //拼团
            cname = '拼团列表'
        }
        this.setState({
            modelId: this.state.modelId
        },() => {
            this.getGoodList()
        })
        Taro.setNavigationBarTitle({
            title: cname
        })
    }
    // 获取列表
    getGoodList(){
        let {modelId} = this.state
        const that = this;
        if(modelId == '61'){//拼团
            common.getPintuanList(that)
        }else{//积分兑换
            common.getExchangeList(that)
        }
    }
    onReachBottom() {
        if(this.state.hasEd){return}
        this.state.pageindex ++
        this.getGoodList()
    }
    
    render() {
        let {modelId, hasList, goodList, hasEd } = this.state
        return (
            <View className='main-goodlist'>
                {hasList && 
                    <Block>
                        {goodList.length > 0 ? 
                            <Block>
                                <ExchangeItem modelId={modelId} goodList={goodList}></ExchangeItem>
                                {hasEd && <NoData type='2'></NoData>}
                            </Block> :
                            <NoData></NoData>
                        }
                    </Block>
                }
            </View>
        )
    }
}

