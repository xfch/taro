import Taro, { Component } from '@tarojs/taro'
import { View, Text, Radio } from '@tarojs/components'

import './index.scss'

import Authorize from '../../components/authorize/authorize'
import Classify from '../../components/orderFood/classify'
import GoodList from '../../components/orderFood/goodList'

import { set as setGlobalData, get as getGlobalData } from '../../static/js/global_data'
import api from '../../service/api'
import base from '../../static/js/base'
import common from '../../service/common'
import order from '../../service/order'


export default class Index extends Component {
    config = {
        navigationBarTitleText: '无桌点餐/外卖-店铺名称',
    }
    constructor() {
        super(...arguments)
        this.state = {
            cid: null
        }
    }
    // authorize获取作用域
    getIsLogin(obj){
        if(obj){
            if(obj.C_MB_NO){ 
                this.goodListScope.getGoodList(this.state.cid,true)
            }else{
                obj.isLogin()
            }
        }	
    }
    // 获取 goodList 的作用域
    getGoodListScope(obj){
        this.goodListScope = obj
    }

    componentWillMount(obj) {
        this.getIsLogin(obj)
        this.getGoodListScope(obj)
    }
    // 切换分类
    tabNav_(cid){
        this.setState({cid})
        const userInfo = Taro.getStorageSync('userInfo')
        if(userInfo){
            this.goodListScope.getGoodList(cid,true)
        }
    }
    // 切换分类
    tabNav(cid){
        this.setState({cid})
        this.goodListScope.getGoodList(cid,true)
    }


    render() {
        return (
            <View className='main-orderFood'>
                {/* 一级分类 */}
                <View className='main-classify'>
                    <Classify onTabNav={this.tabNav.bind(this)}></Classify>
                </View>
                {/* 商品列表 */}
                <View className='main-goodList'>
                    <GoodList onGoodListScope={this.getGoodListScope.bind(this)}></GoodList>
                </View>
                {/* 登陆 */}
                <Authorize page='onSetUserInfo' onSetUserInfo={this.getIsLogin.bind(this)}></Authorize>
            
            </View>
        )
    }
}

