import Taro, { Component } from '@tarojs/taro'
import { View, Text, Input, Button, Image, Textarea } from '@tarojs/components'

import './collect.scss'


import { set as setGlobalData, get as getGlobalData } from '../../static/js/global_data'
import api from '../../service/api'
import common from '../../service/common'
import base from '../../static/js/base'

import Collect from '../../components/collect'

export default class Index extends Component {
    config = {
        navigationBarTitleText: '我的收藏'
    }
    constructor() {
        super(...arguments)
        this.state = {
            domainImg: getGlobalData('domainImg'),
            iscollect: 1, // 收藏商品
            pageindex: 1,
            hasList: false,
            hasEnd: false,
            goodsList: [],
        }
    }

    componentWillMount() {
        this.getGoodList()
    }
    // 获取收藏列表
    getGoodList() {
        const that = this;
        let goodsLists = []
        common.getGoodList(that, (goodLists, hasEd, res) => {
            if (res.goodsList) {
                res.goodsList.map((item,i) => {
                    item.isCollect = 1
                })
                goodsLists = that.state.pageindex == 1 ? res.goodsList : that.state.goodsList.concat(res.goodsList)
                that.state.hasEnd = res.goodsList.length < getGlobalData("pagesize") ? true : false
            }
            that.setState({
                goodsList: goodsLists,
                hasList: true,
                hasEnd: that.state.hasEnd
            })
        })
    }

    render() {
        let {domainImg,hasList, goodsList, hasEnd} = this.state
        return (
            <View className='main-collect'>
                {hasList && <Block>
                    {goodsList.length > 0 ? 
                    <Block>
                        {goodsList.map((item,i) => (
                            <View className='weui-cell'>
                                <View className='weui-cell__hd'>
                                    <Image className='item-img' src={domainImg + item.C_MAINPICTURE}></Image>
                                </View>
                                <View className='weui-cell__bd mlr20'>
                                    <View>{item.GOODS_NAME}</View>
                                </View>
                                <View className='weui-cell__ft'>
                                    <Collect  gid={item.ID} choose={item.isCollect}/>
                                </View>
                            </View>
                        ))}
                    </Block> : 
                    <View className='weui-cell'>
                        <View className='weui-cell__bd ac'>暂无收藏</View>
                    </View> 
                }
                </Block>}
                
            </View>
        )
    }
}

