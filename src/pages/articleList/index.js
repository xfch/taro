import Taro, { Component } from '@tarojs/taro'
import { View, Swiper, SwiperItem, Image, Text, RichText } from '@tarojs/components'
import './index.scss'

import ArticleList from '../../components/index/articleList'
import NoData from '../../components/noData'

import { set as setGlobalData, get as getGlobalData } from '../../static/js/global_data'

import base from '../../static/js/base'
import article from '../../service/article'


export default class Index extends Component {
    config = {
        navigationBarTitleText: '',
        navigationBarBackgroundColor: "#fe690f",
    }
    constructor() {
        super(...arguments)
        this.state = {
            domainImg: getGlobalData('domainImg'),
            pageindex: 1,
            pagesize: getGlobalData('pagesize'),
            sncategory: '',
            articleLists: [],
            hasView: false,
            hasEd: false,
        }
    }

    componentDidShow(){
        this.state.sncategory = this.$router.params.sncategory
        this.getNewsList((res) => {
            Taro.setNavigationBarTitle({
                title: res.length > 0 ? res[0].C_CATEGORY_NAME : '分类列表'
            })
        })
        this.setState({sncategory: this.state.sncategory})
    }

    getNewsList(call){
        const that = this;
        base.showLoading()
        article.getNewsList(that,(res,hasEd) => {
            console.log(res)
            console.log(hasEd)
            if(call){call(res)}
            that.setState({
                hasView: true,
                hasEd: hasEd,
                articleList: res
            })
            Taro.hideLoading()
        })
    }

    onReachBottom(){
        if(this.state.hasEd){return}
        this.state.pageindex++
        this.getNewsList()
    }

    

    render() {
        let { hasView, hasEd, articleList} = this.state
        return (
            <View className='main-articleList'>
                {hasView && 
                    <Block>
                        {articleList.length > 0 ? <Block>
                            <ArticleList articleLists={articleList}></ArticleList>
                            {hasEd && <NoData type='2'></NoData>}
                            </Block>: 
                            <NoData></NoData>
                        }
                    </Block>
                }
            </View>
        )
    }
}

