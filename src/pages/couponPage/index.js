import Taro, { Component } from '@tarojs/taro'
import { View, Text } from '@tarojs/components'
import './index.scss'

import base from '../../static/js/base'
import coupon from '../../service/coupon'

import Authorize from '../../components/authorize/authorize'



export default class Index extends Component {
    config = {
        navigationBarTitleText: '优惠卷'
    }
    constructor() {
        super(...arguments)
    }
    state = {
        couponList: [],
        hasView: false
    }
    // authorize获取作用域
    getIsLogin(obj) {
        if (obj) {
            if (obj.C_MB_NO) {
                this.getCouponListAll()
            } else {
                obj.isLogin(() => {
                    this.getCouponListAll()
                })
            }
        }
    }
    componentDidMount(obj) {
        this.getIsLogin(obj)
    }
    // 获取优惠券
    getCouponListAll(){
        Taro.showLoading()
        coupon.getCouponListAll(null,(res) => {
            Taro.hideLoading()
            this.setState({
                couponList: res.Coupons,
                hasView: true
            })
        },(err) => {
            Taro.hideLoading()
            base.showToast(err.msg,'none')
        })
    }
    // 领取优惠券
    getCoupon(index){
        Taro.showLoading()
        let { couponList } = this.state;
        let param = {
            mid: couponList[index].N_MC_ID,
            mname: couponList[index].c_mc_name,
        }
        coupon.getCoupon(param,(res) => {
            Taro.hideLoading();
            if (res.result == 1) {
                couponList.map((item,i) => {
                    if (index == i) {
                        item.lqzt = 1
                    }
                })
                this.setState({
                    couponList
                })
            } else {
                base.showToast(res.msg,'none')
            }
        },(err) => {
            Taro.hideLoading()
            base.showToast(err.msg,'none')
        })
    }
    render() {
        const { hasView,couponList } = this.state;
        return (
            <View className='couponPage'>
                {hasView && <Block>
                    {
                    couponList.length > 0 ? couponList.map((val, i) => (
                        <View className='couponItem' key={'couponItem' + i}>
                            {
                                val.N_MC_BANKNOTE ? (
                                    <View className='couponLeft'>
                                        {val.C_MC_TYPE == 1 ? <Text>￥</Text> : ''}
                                        <Text className='couponMoney'>{val.N_MC_BANKNOTE}</Text>
                                        {val.C_MC_TYPE == 1 ? '' : <Text>折</Text>}
                                    </View>
                                ) : (
                                        <View className='couponLeft'>
                                            <Text>随机代金券</Text>
                                        </View>
                                    )
                            }
                            <View className='couponCenter'>
                                <View className='ctop'>{val.c_mc_name}</View>
                                <View className='cbottom'>{val.N_MC_Scope}</View>
                            </View>
                            <View className='couponRight'>
                                {val.lqzt == 0 ? <View className='couponGet' onClick={this.getCoupon.bind(this, i)}>领取</View> : <View className='couponRedio'>已领取</View>}
                            </View>
                        </View>
                    )) : <View className='hasNoImg'>暂无数据</View>
                }
                </Block>}
                {/* 登陆 */}
                <Authorize page='onSetUserInfo' onSetUserInfo={this.getIsLogin.bind(this)}></Authorize>
            </View>
        )
    }
}

