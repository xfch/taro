import Taro, { Component } from '@tarojs/taro'
import { View, Text } from '@tarojs/components'

import './index.scss'

import NoData from '../../components/noData'

import { set as setGlobalData, get as getGlobalData } from '../../static/js/global_data'
import api from '../../service/api'
import base from '../../static/js/base'
import common from '../../service/common'

export default class Index extends Component {
    config = {
        navigationBarTitleText: '我的预约',
    }
    constructor() {
        super(...arguments)
        this.state = {
            userInfo: getGlobalData('userInfo'), // .C_MB_NO
            StartTime: "",
            EndTime: "",
            pageindex: 1,
            pagesize: getGlobalData('pagesize'),
            subscribeList: [],
            hasView: false,
            hasEd: false
        }
    }
    componentWillMount() {
        this.getSubscribeList()
    }
    // 获取预约列表
    getSubscribeList() {
        base.showLoading()
        const str = `ShopNo&MebNo&StartTime&EndTime&pageIndex&pageSize`
        const params = {
            bNo: 5005,
            MebNo: getGlobalData('userInfo').C_MB_NO,
            StartTime: this.state.StartTime,
            EndTime: this.state.StartTime,
            pageIndex: this.state.pageindex,
            pageSize: this.state.pagesize,
        }
        let subscribeLists = []
        api.get(str, params).then((res) => {
            if(!res.recdt){res.recdt = []}
            subscribeLists = this.state.pageindex == 1 ? res.recdt : this.state.subscribeList.concat(res.recdt)
            this.state.hasEd = this.state.pagesize > res.recdt.length ? true : false
            this.setState({
                subscribeList: subscribeLists,
                hasEd: this.state.hasEd,
                hasView: true
            })
            Taro.hideLoading()
        }).catch((err) => {
            base.showModal(err.msg)
            Taro.hideLoading()
        })
    }
    // 日期去掉T
    setDate(date){
        return date.replace('T', ' ')
    }
    // 获取预约状态
    getStateView(states) {
        let statesTxt = ''
        switch (states) {
            case 1:
                statesTxt = '未处理'
                break;
            case 2:
                statesTxt = '已到店'
                break;
            case 3:
                statesTxt = '已爽约'
                break;
            case 4:
                statesTxt = '已拒绝'
                break;
            case 5:
                statesTxt = '未到店'
                break;
            default:
                break;
        }
        return statesTxt
    }
    // 确认下单
    onReachBottom() {
        if(this.state.hasEd){return}
        this.state.pageindex++;
        this.getSubscribeList()
    }

    render() {
        let {hasView, subscribeList, hasEd } = this.state
        return (
            <View className='main-subscribeList'>
                {hasView &&
                    <Block>
                        {subscribeList.map((item, i) => (
                            <View key={'list' + i} className='list-item'>
                                <View className='weui-cell bg-white'>
                                    <View className='weui-cell__bd'>
                                        <Text>{item.C_MebName}</Text>
                                    </View>
                                    <View className='weui-cell__ft'>
                                        <Text className='text-gray text-small'>{this.setDate(item.D_OpDate)}</Text>
                                    </View>
                                </View>
                                <View className='weui-cell'>
                                    <View className='weui-cell__bd'>
                                        <View className='text-gray text-small'>到店时间：{this.setDate(item.D_StartDate)} - {this.setDate(item.D_EndDate)}</View>
                                        <View className='weui-flex'>
                                            <View className='weui-flex__item'>
                                                <View className='text-gray text-small'>消费人数：{item.N_CustomerNum}</View>
                                                <View className='text-gray text-small'>预约技师：{item.c_name}</View>
                                            </View>
                                            <View className={`item-state state${item.N_State}`}>{this.getStateView(item.N_State)}</View>
                                        </View>
                                        
                                    </View>
                                </View>
                            </View>
                        ))}
                        {subscribeList.length > 0 && hasEd && <NoData type='2'></NoData>}
                        {subscribeList.length == 0 && <NoData></NoData>}
                    </Block> 
                }
            </View>
        )
    }
}

