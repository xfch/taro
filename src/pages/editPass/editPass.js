import Taro, { Component } from '@tarojs/taro'
import { View, Text, Input, Button } from '@tarojs/components'
import './editPass.scss'


import { set as setGlobalData, get as getGlobalData } from '../../static/js/global_data'
import api from '../../service/api'
import common from '../../service/common'
import base from '../../static/js/base'

export default class Index extends Component {
    config = {
        navigationBarTitleText: '修改密码'
    }
    constructor() {
        super(...arguments)
        this.state = {
            disabled: true,
            oldpwd: '',
            pwd1: '',
            pwd2: '',
        }
    }
    // 
    onInput(id,e){
        const value = e.detail.value
        if(id == 'oldpwd'){
            this.state.oldpwd = value
        }else if(id == 'pwd1'){
            this.state.pwd1 = value
        }else if(id == 'pwd2'){
            this.state.pwd2 = value
        }
        this.setState({
            oldpwd: this.state.oldpwd,
            pwd1: this.state.pwd1,
            pwd2: this.state.pwd2
        }) 
        this.setDisabled()
    }
    // 设置disabled
    setDisabled() {
        if(this.state.oldpwd != '' && this.state.pwd1 != '' && this.state.pwd2 != ''){
            this.setState({disabled: false})
        }else{
            this.setState({disabled: true})
        }
    }
    submit(){
        if(this.state.pwd1 != this.state.pwd2){
            base.showToast('两次密码输入不一样！')
            return
        }
        if(this.state.disabled){
            return
        }
        base.showLoading()
        const userInfo = Taro.getStorageSync('userInfo')
        const str = `mebno&pwd&oldpwd&ShopNo`;
        let params = {
            bNo: 303,
            mebno: userInfo.C_MB_NO,
            pwd: this.state.pwd1,
            oldpwd: this.state.oldpwd,
        }
        api.get(str,params,(res) => {
            base.showToast(res.msg)
            setTimeout(function(){
                Taro.navigateBack()
            },500)
        },(err) => {
            base.showToast(err.msg,'none')
        },() => {
            Taro.hideLoading()
            this.setState({disabled: false})
        })
    }

    render() {
        return (
            <View className='mainLogin'>
                <View className='weui-tab'>
                    <View className='weui-navbar'>
                        <View className="weui-navbar__item">
                            <View className='weui-navbar__title'>修改密码</View>
                        </View> 
                    </View>
                    <View className='weui-tab__panel'>
                        <View className='weui-tab__content'>
                            <View class="weui-cells weui-cells_after-title">
                                <View class="weui-cell weui-cell-none weui-cell_input weui-cell-border">
                                    <View class="weui-cell__hd">
                                        <View class="weui-label"><Text className='iconfont icon-password'></Text></View>
                                    </View>
                                    <View class="weui-cell__bd">
                                        <Input class="weui-input" password placeholder="请输入旧密码" onInput={this.onInput.bind(this, 'oldpwd')} />
                                    </View>
                                </View>
                            </View>
                            <View class="weui-cells weui-cells_after-title">
                                <View class="weui-cell weui-cell-none weui-cell_input weui-cell-border">
                                    <View class="weui-cell__hd">
                                        <View class="weui-label"><Text className='iconfont icon-password'></Text></View>
                                    </View>
                                    <View class="weui-cell__bd">
                                        <Input class="weui-input" password placeholder="请输入至少为6位的新密码" onInput={this.onInput.bind(this, 'pwd1')} />
                                    </View>
                                </View>
                            </View>
                            <View class="weui-cells weui-cells_after-title">
                                <View class="weui-cell weui-cell-none weui-cell_input weui-cell-border">
                                    <View class="weui-cell__hd">
                                        <View class="weui-label"><Text className='iconfont icon-password'></Text></View>
                                    </View>
                                    <View class="weui-cell__bd">
                                        <Input class="weui-input" password placeholder="请确认新密码" onInput={this.onInput.bind(this, 'pwd2')} />
                                    </View>
                                </View>
                            </View>
                            <View class="weui-btn-area">
                                <Button class="weui-btn" type="primary" disabled={this.state.disabled} onClick={this.submit}>确定</Button>
                            </View>
                        </View>
                    </View>
                </View>
            </View>
        )
    }
}

