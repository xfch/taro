import Taro, { Component } from '@tarojs/taro'
import { View, Text, Image } from '@tarojs/components'

import './index.scss'

import ShopInfo from '../../components/index/shopInfo'

import Cgnav from '../../components/category/cgnav'
import ProductShow from '../../components/index/productShow'

import { set as setGlobalData, get as getGlobalData } from '../../static/js/global_data'
import api from '../../service/api'
import base from '../../static/js/base'
import common from '../../service/common'

export default class Index extends Component {
    config = {
        navigationBarTitleText: '',
    }
    constructor() {
        super(...arguments)
        this.state = {
            shopInfo: getGlobalData('shopInfo'),
            pageindex: 1,
            hasEd: false,
            hasGoodList: false,
            goodList: [],
            hasView: false,
            type: 'vh'
        }
    }

    componentWillMount() {
        const that = this
        Taro.setNavigationBarTitle({
            title: this.$router.params.titleText
        })
        common.getUserInfo(that,function(data){
            that.setState({userInfo: data})
        })
        common.getCategory(that,(category) => {
            console.log(category)
            that.state.cid = category[0].ID
            common.getGoodList(that,(goodList,hasEd) => {
                that.setState({
                    goodList: goodList,
                    hasEd: hasEd,
                })
            })
            let cgnav = [],cgnav_str = ''; //一级分类
            category.map((item,i) => {
                cgnav_str = { ID: item.ID, name: item.name };
                cgnav.push(cgnav_str)
            })
            that.setState({
                cgnav: cgnav,
                cgnavId: cgnav[0].ID,
                cid: that.state.cid,
                category: category
            })
        })
       
    }

    // 一级分类
    tabNav(id,e){
        const that = this;
        if(id == that.state.cgnavId){return}
        that.state.cid = id
        that.setState({
            cid: id,
            cgnavId: id
        },() => {
            if (this.fallsVh) {
                this.fallsVh.setUpData(e)
            }
            if (this.fallsVw) {
                this.fallsVw.setUpData(e)
            }
        })
    }
    // 切换样式
    changeListStyle(){
        this.setState({
            type: this.state.type == 'vh' ? 'vw' : 'vh'
        })
    }

    onReachBottom() {
        if (this.fallsVh) {
            this.fallsVh.setUpData()
        }
        if (this.fallsVw) {
            this.fallsVw.setUpData()
        }
    }
    getFallsVw(obj) {
        //获取瀑布流的作用域
        this.fallsVw = obj
    }
    getFallsVh(obj) {
        //获取瀑布流的作用域
        this.fallsVh = obj
    }

    render() {
        let {cgnav,cgnavId,cid,type} = this.state
        return (
            <View className='mainShopInfo bg-white'>
                <ShopInfo modelId='24' btnGroup='0'></ShopInfo>
                {/* 一级分类 */}
                <View className='mainCgnav'>
                    <Cgnav scrollType='x' cgnav={cgnav} cgnavId={cgnavId} onTabNav={this.tabNav.bind(this)}></Cgnav>
                    <View className='img-menu' onClick={this.changeListStyle}><Text className='iconfont icon-payother iconmenu'></Text></View>
                </View>
                <View className='mainContent'>
                    {/* 分类商品列表 */}
                    <View className='mainList'>
                        <ProductShow onGetThis={this.getFallsVh.bind(this)} type={type} cid={cid} isFalls />
                    </View>
                </View>
            </View>
        )
    }
}

