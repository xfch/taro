import Taro, { Component } from '@tarojs/taro'
import { View, Text, Radio } from '@tarojs/components'

import './index.scss'

import Money from '../../components/payment/money'
import Method from '../../components/payment/method'
import Keyboard from '../../components/payment/keyboard'
import Authorize from '../../components/authorize/authorize'

import { set as setGlobalData, get as getGlobalData } from '../../static/js/global_data'
import api from '../../service/api'
import base from '../../static/js/base'
import common from '../../service/common'
import coupon from '../../service/coupon'
import order from '../../service/order'

export default class Index extends Component {
    config = {
        navigationBarTitleText: '付款到商家',
    }
    constructor() {
        super(...arguments)
        this.state = {
            amount: null, // 支付金额
            couponed: {}, // 已选择的优惠券
            disabled: false,
        }
    }
    // authorize获取作用域
    getIsLogin(obj){
        if(obj){
            if(obj.C_MB_NO){ 
            }else{
                obj.isLogin(() => {

                })
            }
        }	
    }
    componentWillMount(obj) {
        this.getIsLogin(obj)
    }
    // 点击输入金额
    focusInput(){
        this.keyboard.setState({
            keyboardState: true
        })
    }
    // 获取键盘作用域
    getKeyboard(obj){
        if(obj){
            this.keyboard = obj
        }
    }
    // 点击键盘
    onKeyboard(amount){
        this.setState({
            amount,
            couponed: {}
        })
    }
    // 选择支付方式
    selectPayMethod(isBalance){
        this.setState({
            isBalance
        })
    }
    // 确认支付
    submit(){
        let {amount,couponed,isBalance} = this.state
        if(!amount || parseFloat(amount) == 0){
            base.showModal('请输入付款金额')
            return
        }
        this.submitResult(1)
        let params = {
            amount,
            yhqstr: couponed.MarketingCId ? couponed.MarketingCId : '',
            isBalance: isBalance,
        }
        order.payToMerchants(params,(res) => {
            console.log(res)
            if(res.code == '100'){
                if(res.payamount > 0){
                    order.payment(res.orderNo,() => {
                        this.submitResult(3)
                    },() => {
                        this.state.disabled = false
                    })
                }else{
                    this.submitResult(3,res)
                }
            }else{ // 101
                base.showModal(res.msg)
            }
            this.submitResult(2)
        },(err) => {
            console.log(err)
            this.submitResult(2)
        })
    }
    submitResult(result,res){
        if(result == '1'){
            if(this.state.disabled){ return }
            base.showLoading()
            this.state.disabled = true
        }else if(result == '2'){
            this.state.disabled = false
            Taro.hideLoading()
        }else {
            common.getUserInfo()
            Taro.reLaunch({
                url: `/pages/payResult/payResult?result=0&orderno=${res.orderNo}&checkOrder=0`
            })
        }
        
    }

    render() {
        let {amount,couponed} = this.state
        return (
            <View className='main-payment'>
                <View className='ac pt20'>
                    <Text className='iconfont icon-store mr10 store-img'></Text><Text className='text-big'>微客掌柜商城</Text>
                </View>
                <View className='box-item'>
                    <Money amount={amount} onFocusInput={this.focusInput.bind(this)}></Money>
                </View>
                <View className='box-item'>
                    <Method amount={amount} couponed={couponed} onSelectPayMethod={this.selectPayMethod.bind(this)}></Method>
                </View>
                <View className='keyboard'>
                    <Keyboard getKeyboard={this.getKeyboard.bind(this)} onKeyboard={this.onKeyboard.bind(this)} onSubmit={this.submit.bind(this)}></Keyboard>
                </View>
                <View className='p20'>
                    <Button className='btn-main' onClick={this.submit}>付款</Button>
                </View>
                {/* 登陆 */}
                <Authorize page='onSetUserInfo' onSetUserInfo={this.getIsLogin.bind(this)}></Authorize>
            </View>
        )
    }
}

