import Taro, { Component } from '@tarojs/taro'
import { View, Text, Input, Button } from '@tarojs/components'
import './login.scss'


import { set as setGlobalData, get as getGlobalData } from '../../static/js/global_data'
import api from '../../service/api'
import common from '../../service/common'
import base from '../../static/js/base'

import LoginregFor from '../../components/login/loginregFor'

export default class Index extends Component {
    config = {
        navigationBarTitleText: '用户登陆'
    }
    constructor() {
        super(...arguments)
        this.state = {
            tabList: [],
            activeIndex: 0,
            disabled: [true, true],
            userName: '',
            userPwd: '',
            phoneNum: '',
            phoneCode: '',
            codeDisabled: true,
            codeVal: '获取验证码',
            detailUserInfo: null, //授权的用户信息
            pages: '', //当前页面
            phonePwd: '',//注册
            userPwd1: '',//注册
            userPwd2: '',//注册
        }
    }

    componentWillMount() {
        console.log(this.$router.params)
        this.state.activeIndex = this.$router.params.activeIndex ? this.$router.params.activeIndex : this.state.activeIndex
        const detailUserInfo = this.$router.params.detail ? JSON.parse(this.$router.params.detail) : null
        if(this.$router.params.pages){
            let title = '用户登陆'
            let tabList = ['用户名登陆', '手机号登陆']
            let mainBtn = '登陆'
            if(this.$router.params.pages == 'register'){ //注册
                title = '用户注册'
                tabList = ['用户名注册', '手机号注册']
                mainBtn = '注册'
            }else if(this.$router.params.pages == 'forgetPwd'){ // 忘记密码
                title = '找回密码'
                tabList = ['找回密码']
                mainBtn = '找回密码'
                this.state.activeIndex = 1
            }
            Taro.setNavigationBarTitle({title: title})
            this.setState({
                pages: this.$router.params.pages,
                tabList: tabList,
                activeIndex: this.state.activeIndex,
                mainBtn: mainBtn,
                detailUserInfo: detailUserInfo,
            })
        }
    }

    tab(index) {
        this.setState({ activeIndex: index })
    }
    onInput(names, e) {
        console.log(names)
        console.log(e)
        const value = e.detail.value
        if (names == 'userName') {
            this.state.userName = value
        } else if (names == 'userPwd') {
            this.state.userPwd = value
        } else if (names == 'phoneNum') {
            this.state.codeDisabled = value.length == 11 ? false : true
            this.state.phoneNum = value
        } else if (names == 'phoneCode') {
            this.state.phoneCode = value
        } else if(names == 'phonePwd'){
            this.state.phonePwd = value
        }else if(names == 'userPwd1'){
            this.state.userPwd1 = value
        }else if(names == 'userPwd2'){
            this.state.userPwd2 = value
        }

        if(this.state.pages == 'login'){ 
            this.state.disabled[0] = this.state.userName != '' && this.state.userPwd2 != '' ? false : true
            this.state.disabled[1] = this.state.phoneNum != '' && this.state.phoneCode != '' ? false : true
        }else if(this.state.pages == 'register'){
            if (this.state.userName != '' && this.state.userPwd1 != '' && this.state.userPwd2 != '') {
                if(this.state.userPwd1.length >= 6 && this.state.userPwd2.length >= 6 && this.state.userPwd1 == this.state.userPwd2){
                    this.state.disabled[0] = false
                }else{
                    this.state.disabled[0] = true
                }
            }else{
                this.state.disabled[0] = true
            }
            if (this.state.phoneNum != '' && this.state.phonePwd != '' && this.state.phoneCode != '') {
                this.state.disabled[1] = this.state.phonePwd.length >=6 ? false : true
            }else{
                this.state.disabled[1] = true
            }
        }else if(this.state.pages == 'forgetPwd'){
            if (this.state.phoneNum != '' && this.state.phonePwd != '' && this.state.phoneCode != '') {
                this.state.disabled[1] = this.state.phonePwd.length >=6 ? false : true
            }else{
                this.state.disabled[1] = true
            }
        }
        this.setState({
            userName: this.state.userName,
            userPwd: this.state.userPwd,
            phoneNum: this.state.phoneNum,
            phoneCode: this.state.phoneCode,
            phonePwd: this.state.phonePwd,//手机注册密码
            userPwd1: this.state.userPwd1,//用户名注册密码
            userPwd2: this.state.userPwd2,//用户名注册确认密码
            disabled: this.state.disabled,
            codeDisabled: this.state.codeDisabled
        })
    }
    // 获取验证码
    getCode(){
        const that = this;
        common.getCode(that,(res) => {
            that.setState({Vcode: res.Vcode})
        })
    }
    // 设置disabled
    setDisabled(that, index, states, loading) {
        if (loading) {
            base.showLoading()
        } else {
            Taro.hideLoading()
        }
        that.state.disabled[index] = states
        that.setState({ disabled: that.state.disabled })
    }
    // 注册、忘记密码跳转
    navigator(id,pages) {
        if(id == 'index'){
            Taro.reLaunch({url: '/pages/index/index'})
        }
    }
    // 登陆接口
    userLogin(that,openId,names){
        let str = `userName&userPwd&ShopNo` //用户名
        let params = { //用户名
            bNo: names == 'username' ? 104 : 103,
            ShopNo: getGlobalData('SHOP_NO'),
            openId: openId || '',
            type: 2,
        }
        if(names == 'username'){ //用户名
            params['userName'] = that.state.userName
            params['userPwd'] = that.state.userPwd2
        }else if(names == 'phone'){
            str = `phone&yzm&ShopNo`
            params['phone'] = that.state.phoneNum
            params['yzm'] = that.state.phoneCode
        }
        api.get(str, params, function (res) {
            if(res.code == '100'){
                setGlobalData('isLoginState',false)
                if(that.state.detailUserInfo){
                    const userInfo = Taro.getStorageSync('userInfo')
                    common.weappLoginModify(userInfo,that.state.detailUserInfo)
                }else{
                    Taro.setStorageSync('userInfo', res.member[0])
                }
                Taro.setStorageSync('loginType', 'account')
                setTimeout(function () {
                    Taro.reLaunch({
                        url: '/pages/index/index'
                    })
                },500)
            }
            base.showToast(res.msg,'none')
        }, function (res) {
            base.showModal(res.msg)
        }, function () {
            that.setDisabled(that, that.state.activeIndex, false)
        })
    }
    // 注册接口
    register(that,names){
        that.setDisabled(that, that.state.activeIndex, true, true)
        const userInfo = Taro.getStorageSync('userInfo')
        const str = `phone&pwd&ShopNo`
        let params = {
            bNo: "002",
            phone: names == 'phone' ? that.state.phoneNum : that.state.userName,
            pwd: names == 'phone' ? that.state.phonePwd : that.state.userPwd1,
            ShopNo: getGlobalData('SHOP_NO'),
        }
        if(names == 'phone'){ //手机号注册
            params['openid'] = userInfo.C_XCXOPENID
        }
        api.get(str,params,function(res){
            base.showToast(res.msg,'none')
            setTimeout(function(){
                Taro.reLaunch({url: '/pages/login/login'})
            },500)
        },function(res){
            base.showToast(res.msg,'none')
        },function(){
            that.setDisabled(that, that.state.activeIndex, false)
        })
    }
    // 找回密码接口
    forgetPwd(that){
        that.setDisabled(that, that.state.activeIndex, true, true)
        let str = `phone&pwd&ShopNo`
        let params = {
            bNo: 307,
            phone: that.state.phoneNum,
            pwd: that.state.phonePwd,
            yzm: that.state.phoneCode,
            ShopNo: getGlobalData('SHOP_NO'),
        }
        api.get(str,params,function(res){
            console.log(res)
            base.showToast(res.msg)
            setTimeout(function(){
                Taro.reLaunch({url: '/pages/login/login?pages=login'})
            },400)
        },function(res){
            base.showToast(res.msg,'none')
        },function(){
            that.setDisabled(that, that.state.activeIndex, false)
        })
    }
    // 点击注册、登陆、修改密码 names: phone/username 手机号注册/用户名注册
    submit(names){
        const that = this;
        const openid = Taro.getStorageSync('openid') || ''
        if(that.state.pages == 'login'){ //登陆
            that.userLogin(that,openid,names)
        }else{ 
            if(names == 'phone'){
                if(that.state.phoneCode != that.state.Vcode){
                    base.showToast('验证码输入错误！', 'none')
                    return
                }
            }
            if(that.state.pages == 'register'){ //注册
                that.register(that,names)
            }else if(that.state.pages == 'forgetPwd'){ //修改密码
                that.forgetPwd(that)
            } 
        }
    }

    render() {
        return (
            <View className='mainLogin'>
                <View className='weui-tab'>
                    <View className='weui-navbar'>
                        {this.state.tabList.map((item, i) => {
                            return (
                                <View key={i} className={this.state.activeIndex == i ? "weui-bar__item_on weui-navbar__item" : "weui-navbar__item"} onClick={this.tab.bind(this, i)}>
                                    <View className='weui-navbar__title'>{item}</View>
                                </View>
                            )
                        })}
                    </View>

                    <View className='weui-tab__panel'>
                        {/* ---用户名-- */}
                        <View className='weui-tab__content' hidden={this.state.activeIndex != 0}>
                            <View class="weui-cells weui-cells_after-title">
                                <View class="weui-cell weui-cell-none weui-cell_input weui-cell_vcode">
                                    <View class="weui-cell__hd">
                                        <View class="weui-label"><Text className='iconfont icon-phone'></Text></View>
                                    </View>
                                    <View class="weui-cell__bd">
                                        <Input class="weui-input" placeholder={this.state.pages == 'login' ? '手机号/用户名/会员卡号' : '请填写用于登录的会员账号'} onInput={this.onInput.bind(this, 'userName')} />
                                    </View>
                                </View>
                                {this.state.pages == 'register'&&
                                    <View class="weui-cell weui-cell-none weui-cell_input weui-cell-border">
                                        <View class="weui-cell__hd">
                                            <View class="weui-label"><Text className='iconfont icon-password'></Text></View>
                                        </View>
                                        <View class="weui-cell__bd">
                                            <Input class="weui-input" password placeholder="请输入至少为6位的密码" onInput={this.onInput.bind(this, 'userPwd1')} />
                                        </View>
                                    </View>
                                }
                                <View class="weui-cell weui-cell-none weui-cell_input weui-cell-border">
                                    <View class="weui-cell__hd">
                                        <View class="weui-label"><Text className='iconfont icon-password'></Text></View>
                                    </View>
                                    <View class="weui-cell__bd">
                                        <Input class="weui-input" password placeholder={this.state.pages == 'login' ? '请输入您的密码' : '请确认您的密码'}onInput={this.onInput.bind(this, 'userPwd2')} />
                                    </View>
                                </View>
                            </View>
                            <View class="weui-btn-area">
                                <Button class="weui-btn" type="primary" disabled={this.state.disabled[0]} onClick={this.submit.bind(this, 'username')}>{this.state.mainBtn}</Button>
                                {this.state.pages == 'login'&&<LoginregFor></LoginregFor>}
                            </View>
                        </View>
                        {/* -----手机号----- */}
                        <View className='weui-tab__content' hidden={this.state.activeIndex != 1}>
                            <View class="weui-cells weui-cells_after-title">
                                <View class="weui-cell weui-cell-none weui-cell_input weui-cell_vcode">
                                    <View class="weui-cell__hd">
                                        <View class="weui-label"><Text className='iconfont icon-phone'></Text></View>
                                    </View>
                                    <View class="weui-cell__bd">
                                        <Input class="weui-input" placeholder="请输入您的手机号码" onInput={this.onInput.bind(this, 'phoneNum')} />
                                    </View>
                                    <View class="weui-cell__ft">
                                        <View class={this.state.codeDisabled ? 'weui-vcode weui-vcode-dis' : 'weui-vcode'} onClick={this.getCode}>{this.state.codeVal}</View>
                                    </View>
                                </View>
                                <View class="weui-cell weui-cell-none weui-cell_input weui-cell-border">
                                    <View class="weui-cell__hd">
                                        <View class="weui-label"><Text className='iconfont icon-phoneCode'></Text></View>
                                    </View>
                                    <View class="weui-cell__bd">
                                        <Input class="weui-input" placeholder="请输入您收到的手机验证码" onInput={this.onInput.bind(this, 'phoneCode')} />
                                    </View>
                                </View>
                                {this.state.pages != 'login'&&
                                    <View class="weui-cell weui-cell-none weui-cell_input weui-cell-border">
                                        <View class="weui-cell__hd">
                                            <View class="weui-label"><Text className='iconfont icon-password'></Text></View>
                                        </View>
                                        <View class="weui-cell__bd">
                                            <Input class="weui-input" password placeholder="请输入至少为6位的密码" onInput={this.onInput.bind(this, 'phonePwd')} />
                                        </View>
                                    </View>
                                }
                            </View>
                            <View class="weui-btn-area">
                                <Button class="weui-btn" type="primary" disabled={this.state.disabled[1]} onClick={this.submit.bind(this, 'phone')}>{this.state.mainBtn}</Button>
                                {this.state.pages == 'login'&&<LoginregFor></LoginregFor>}
                            </View>
                        </View>
                        {/* ---- */}
                    </View>
                </View>
                {this.state.pages == 'login'&&
                    <View className='weui-footer weui-footer_fixed-bottom'>
                        <View className='ac' onClick={this.navigator.bind(this, 'index')}>返回首页</View>
                    </View>
                }
            </View>
        )
    }
}

