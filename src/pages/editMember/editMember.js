import Taro, { Component } from '@tarojs/taro'
import { View, Text, Input, Button,Label,Radio,Picker} from '@tarojs/components'
// import { View, Text, Input, Button,RadioGroup,Label,Radio,Picker} from '@tarojs/components'
import './editMember.scss'


import { set as setGlobalData, get as getGlobalData } from '../../static/js/global_data'
import api from '../../service/api'
import common from '../../service/common'
import base from '../../static/js/base'

import MemberFace from '../../components/member/mbface/memberFace'

export default class Index extends Component {
    config = {
        navigationBarTitleText: '完善个人资料'
    }
    constructor() {
        super(...arguments)
        this.state = {
            tabList: ['个人资料','绑定手机号'],
            sliderWidth: 96,
            sexlist: [{'txt': '男','id': '1'},{'txt': '女','id': '2'},{'txt': '密','id': '3'}],
            sliderOffset: 0,
            sliderLeft: 0,
            activeIndex: 0,
            disabled: [false, true],
            phoneNum: '',
            phoneCode: '',
            codeVal: '获取验证码',
            codeDisabled: true,
            userInfo: {
                C_MB_NAME: null,
                C_MB_SEX: null,
                D_MB_BIRTHDAY: null,
                C_MB_PHONE: null,
            },
            domainImg: getGlobalData('domainImg'),
        }
    }
    componentDidShow(){
        const that = this;
        console.log(this.$router.params)
        console.log(base.prevPage().state)
        this.state.activeIndex = this.$router.params.activeIndex || 0
        this.setState({
            userInfo: base.prevPage().state.userInfo,
            activeIndex: this.state.activeIndex
        },() => {
            console.log(this.state.userInfo)
        })
        Taro.getSystemInfo({
            success: function(res) {
                that.setState({
                    sliderLeft: (res.windowWidth / that.state.tabList.length - that.state.sliderWidth) / 2,
                    sliderOffset: res.windowWidth / that.state.tabList.length * that.state.activeIndex
                });
            }
        });
    }
    // 选择生日
    changePicker(e){
        this.state.userInfo.D_MB_BIRTHDAY = e.detail.value
        this.setState({userInfo: this.state.userInfo})
    }
    // 切换性别
    changeSex(e){
        this.state.userInfo.C_MB_SEX = e.detail.value
        this.setState({userInfo: this.state.userInfo})
    }
    // 切换选项卡
    tab(index,e) {
        this.setState({
            sliderOffset: e.currentTarget.offsetLeft,
            activeIndex: index
        });
    }
    onInput(names, e) {
        const value = e.detail.value
        if (names == 'mbName') {
            this.state.userInfo.C_MB_NAME = value
        } else if (names == 'phoneNum') {
            this.state.codeDisabled = value.length == 11 ? false:true
            this.state.phoneNum = value
        } else if (names == 'phoneCode') {
            this.state.phoneCode = value
        }
        if(this.state.activeIndex == '1'){
            this.state.disabled[1] = this.state.phoneNum != '' && this.state.phoneCode != '' ? false:true
        }
        this.setState({
            userInfo: this.state.userInfo,
            disabled: this.state.disabled,
            codeDisabled: this.state.codeDisabled
        })
    }
    // 获取验证码
    getCode(){
        const that = this;
        common.getCode(that,(res) => {
            that.setState({Vcode: res.Vcode})
        })
    }
    // 设置disabled
    setDisabled(that, index, states, loading) {
        if (loading) {
            base.showLoading()
        } else {
            Taro.hideLoading()
        }
        that.state.disabled[index] = states
        that.setState({ disabled: that.state.disabled })
    }
    // 保存、绑定
    submit(names){
        console.log(base.prevPage().state)
        const that = this;
        const openid = Taro.getStorageSync('openid') || ''
        const userInfo = that.state.userInfo
        console.log(userInfo)
        let str = `phone&mebno&ShopNo`
        let params = {
            ShopNo: getGlobalData('SHOP_NO'),
            mebno: userInfo.C_MB_NO,
        }
        if(names == 'editUserInfo'){ //修改个人资料
            str = `name&sex&headimg&mebno&ShopNo&mebbirth`
            params['bNo'] = 1754
            params['name'] = userInfo.C_MB_NAME
            params['sex'] = userInfo.C_MB_SEX
            params['headimg'] = userInfo.C_MB_PIC
            params['mebbirth'] = userInfo.D_MB_BIRTHDAY || ''
        }else if(names == 'bindPhone'){ //绑定手机
            params['bNo'] = 309
            params['phone'] = that.state.phoneNum
            params['yzm'] = that.state.phoneCode
            params['openid'] = openid
            params['type'] = 2
        }
        that.setDisabled(that, that.state.activeIndex, true, true)
        api.get(str,params,function(res){
            base.showModal(res.msg,false,function(){
                Taro.reLaunch({url: '/pages/personal/personal'})
            })
        },function(res){
            base.showToast(res.msg)
        },function(){
            that.setDisabled(that, that.state.activeIndex, false)
        })
    }

    render() {
        let {sexlist,userInfo} = this.state
        return (
            <View className='mainLogin'>
                <View>
                    <MemberFace userInfo={userInfo}></MemberFace>
                </View>
                <View className='weui-tab'>
                    <View className='weui-navbar'>
                        {this.state.tabList.map((item, i) => (
                             <View key={'tabList' + i} className={this.state.activeIndex == i ? "weui-bar__item_on weui-navbar__item" : "weui-navbar__item"} onClick={this.tab.bind(this, i)}>
                                <View className='weui-navbar__title'>{item}</View>
                            </View>
                        ))}
                        <view class="weui-navbar__slider" style="left: {{sliderLeft}}px; transform: translateX({{sliderOffset}}px); -webkit-transform: translateX({{sliderOffset}}px);"></view>
                    </View>

                    <View className='weui-tab__panel'>
                        {/* ---个人资料-- */}
                        <View className='weui-tab__content' hidden={this.state.activeIndex != 0}>
                            <View class="weui-cells weui-cells-none weui-cells-mt0">
                                <View class="weui-cell weui-cell_input">
                                    <View class="weui-cell__hd">
                                        <View class="weui-label">昵称</View>
                                    </View>
                                    <View class="weui-cell__bd ar">
                                        <Input class="weui-input" placeholder='请输入您的昵称' onInput={this.onInput.bind(this, 'mbName')} value={userInfo.C_MB_NAME} />
                                    </View>
                                </View>
                                <View class="weui-cell">
                                    <View class="weui-cell__hd">
                                        <View class="weui-label">性别</View>
                                    </View>
                                    <View class="weui-cell__bd ar">
                                        <RadioGroup onChange={this.changeSex}>
                                            {sexlist.map((item, i) => (
                                                <Label className='radio-list__label' key={'sexlist' + i}>
                                                    <Radio className='radio-list__radio' value={item.id} checked={item.id == userInfo.C_MB_SEX} ></Radio>
                                                    <Text className='text-input'>{item.txt}</Text>
                                                </Label>
                                            ))}
                                        </RadioGroup>
                                    </View>
                                </View>
                                <View class="weui-cell">
                                    <View class="weui-cell__hd">
                                        <View class="weui-label">生日</View>
                                    </View>
                                    <View class="weui-cell__bd weui-cell__ft_in-access">
                                        <Picker mode='date' onChange={this.changePicker}>
                                            <view class="ar text-input">{userInfo.D_MB_BIRTHDAY ? userInfo.D_MB_BIRTHDAY : '请选择生日'}</view>
                                        </Picker>
                                    </View>
                                </View>
                            </View>
                            <View class="weui-btn-area">
                                <Button class="weui-btn" type="primary" disabled={this.state.disabled[0]} onClick={this.submit.bind(this, 'editUserInfo')}>保存</Button>
                            </View>
                        </View>
                        {/* -----绑定手机号----- */}
                        <View className='weui-tab__content' hidden={this.state.activeIndex != 1}>
                            <View class="weui-cells weui-cells-none weui-cells-mt0">
                                <View class="weui-cell curr-num ">
                                    <View class="weui-cell__bd ac">
                                        <View className='text-input'>{userInfo.C_MB_PHONE && userInfo.C_MB_PHONE !=''? `当前绑定的手机号${userInfo.C_MB_PHONE}` : '当前未绑定手机号'}</View>
                                    </View>
                                </View>
                                <View class="weui-cell weui-cell_input weui-cell-none">
                                    <View class="weui-cell__bd">
                                        <Input class="weui-input block" type='number' placeholder="请输入要更换的手机号码" onInput={this.onInput.bind(this, 'phoneNum')} />
                                    </View>
                                </View>
                                <View class="weui-cell weui-cell_input">
                                    <View class="weui-cell__bd">
                                        <Input class="weui-input" placeholder="请输入您收到的手机验证码" onInput={this.onInput.bind(this, 'phoneCode')} />
                                    </View>
                                    <View class="weui-cell__ft">
                                        <View class={this.state.codeDisabled ? 'weui-vcode weui-vcode-dis' : 'weui-vcode'} onClick={this.getCode}>{this.state.codeVal}</View>
                                    </View>
                                </View>
                            </View>
                            <View class="weui-btn-area">
                                <Button class="weui-btn" type="primary" disabled={this.state.disabled[0]} onClick={this.submit.bind(this, 'bindPhone')}>绑定</Button>
                            </View>
                        </View>
                        {/* ---绑定手机号--- */}
                    </View>
                </View>
            </View>
        )
    }
}

