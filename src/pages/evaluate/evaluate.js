import Taro, { Component } from '@tarojs/taro'
import { View, Text, Input, Button,Image,Textarea } from '@tarojs/components'

import './evaluate.scss'


import { set as setGlobalData, get as getGlobalData } from '../../static/js/global_data'
import api from '../../service/api'
import common from '../../service/common'
import base from '../../static/js/base'

import Checkboxbar from '../../components/checkboxbar'
import Good from '../../components/evaluate/good'
import Star from '../../components/evaluate/star'
import ChooseImage from '../../components/evaluate/chooseImage'

export default class Index extends Component {
    config = {
        navigationBarTitleText: '评价'
    }
    constructor() {
        super(...arguments)
        this.state = {
            stars: 5, //总星数
            stared: 0, //评价星数
            starList: [
                {stared: '0','txt': '请选择评分'},
                {stared: '1','txt': '很差'},
                {stared: '2','txt': '一般'},
                {stared: '3','txt': '满意'},
                {stared: '4','txt': '非常满意'},
                {stared: '5','txt': '无可挑剔'},
            ],
            imgList: [], //上传的图片列表
            imgListMax: 4, //最多传多少张
            domainImg: getGlobalData('domainImg'),
            select: false, //是否匿名
            orderItem: {
                glist: null
            },
        }
    }

    componentWillMount() {
        console.log(this.$router.params)
        console.log(JSON.parse(this.$router.params.orderItem))
        this.setState({orderItem: JSON.parse(this.$router.params.orderItem)})
    }
    // 评星
    changeStar(index){
        this.setState({stared: index + 1})
    }
    // 评价
    onInput(e){
        this.setState({ccontent: e.detail.value})
    }
    // 上传图片
    changeUploadImg(imgList){
        this.setState({imgList})
    }
    // 是否匿名
    selectCheck(){
        this.setState({select: !this.state.select})
    }
    // 提交
    submit(){
        const userInfo = Taro.getStorageSync('userInfo')
        const str = `mebno&orderID&pstime&pspy&star&ccontent&anonymous&ShopNo`
        let goods = [];
        this.state.orderItem.glist.map((item,i) => {
            var pj_type = this.state.stared > 2 ? 1 : 2;
            goods.push(`${item.N_GOODS_ID}|${item.C_GOODS_NAME}|${pj_type}`);
        })
        let anonymous = this.state.select ? '1' : '0';
        let mebname = userInfo.C_MB_NAME ? userInfo.C_MB_NAME : userInfo.C_MB_NICKNAME
        let params = {
            bNo: '1900',
            mebno: userInfo.C_MB_NO,
            orderID: this.state.orderItem.c_ord_no,
            pstime: this.state.pstime || '',
            pspy: this.state.pspy || '',
            star: this.state.stared || '0',
            ccontent: this.state.ccontent || '',
            anonymous: anonymous, //0不匿名,1匿名
            images: this.state.imgList.join(','),
            goods: goods.join(","),
            mebname: anonymous == '1' ? '' : mebname,
            headimg: anonymous == '1' ? '' : userInfo.C_MB_PIC,
        }
        api.get(str,params,(res) => {
            base.showToast(res.msg)
            setTimeout(function(){
                Taro.reLaunch({url: '/pages/personal/personal'})
            },500)
        })
    }

    render() {
        return (
            <View className='main-evaluate'>
                <View className='bg-white'>
                    <Good glist={this.state.orderItem.glist} domainImg={this.state.domainImg}></Good>
                </View>
                <View className='weui-cells weui-cells-none'>
                    <View className='weui-cell'>
                        <View className='weui-cell__hd'><View>评分</View></View>
                    </View>
                    <View className='weui-cell'>
                        <View className='weui-cell__bd'>
                            <View className='ac'>
                                <Star star-class={`main-star ${this.state.stared > 0 ? 'text-main' : 'text-gray'}`} stars={this.state.stars} stared={this.state.stared} onChangeStar={this.changeStar.bind(this)}>
                                    <View className={`pb10 ${this.state.stared > 0 ? 'text-sub' : 'text-gray'}`}>
                                        {this.state.starList[this.state.stared].txt}
                                    </View>
                                </Star>
                            </View>
                            <View>
                                <Textarea onInput={this.onInput} className='textarea' placeholder="亲，您对商品还满意吗？请写下您的评语吧！" autoHeight/>
                            </View>
                            <ChooseImage onChangeUploadImg={this.changeUploadImg.bind(this)} imgListMax={this.state.imgListMax} imgList={this.state.imgList}class-photo='item-photo iconfont icon-home' class-item='item-photo-img'>
                                <View className='text-small text-gray pt20'>已上传 {this.state.imgList.length}/{this.state.imgListMax}</View>
                            </ChooseImage>
                        </View>
                    </View>
                </View>
                <View className='p20'>
                    <Checkboxbar select={this.state.select} onSelectCheck={this.selectCheck.bind(this)}>
                        <Text className='pl20'>匿名评论</Text>
                    </Checkboxbar>
                </View>
                <View className='weui-btn-area'>
                    <Button className='weui-btn' type='main' onClick={this.submit}>提交</Button>
                </View>
            </View>
        )
    }
}

