import Taro, { Component } from '@tarojs/taro'
import { View, Text } from '@tarojs/components'
import './index.scss'

import ShopInfos from '../../components/index/shopInfos'

export default class Index extends Component {
    config = {
        navigationBarTitleText: '',
    }
    constructor() {
        super(...arguments)
        this.state = {
            modelId: '22',
            showNumState: false,
            proData: {
                isMap: 1
            }
        }
    }

    componentWillMount() { 
        Taro.setNavigationBarTitle({
            title: this.$router.params.titleText
        })
    }
    render() {
        let {modelId,showNumState,proData} = this.state
        return (
            <View className='mainShopList'>
                <ShopInfos modelId={modelId} showNumState={showNumState} proData={proData}></ShopInfos>
            </View>
        )
    }
}

