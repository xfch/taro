import Taro, { Component } from '@tarojs/taro'
import { View, Text, Input, Button, Image, Textarea } from '@tarojs/components'

import './addCard.scss'


import { set as setGlobalData, get as getGlobalData } from '../../static/js/global_data'
import api from '../../service/api'
import common from '../../service/common'
import base from '../../static/js/base'

import ItemInput from '../../components/itemInput'

export default class Index extends Component {
    config = {
        navigationBarTitleText: '绑定购物卡'
    }
    constructor() {
        super(...arguments)
        this.state = {
            cardNo: '',
            pwd: '',
        }
    }
    onInput(names,e){
        const value = e.detail.value
        if(names == 'cardNo'){
            this.state.cardNo = value
        }else if(names == 'pwd'){
            this.state.pwd = value
        }
        this.setState({
            cardNo: this.state.cardNo,
            pwd: this.state.pwd
        })
    }
    bindCard() {
        Taro.showLoading()
        let userInfo = Taro.getStorageSync('userInfo')
        const str = `mebno&cardNo&pwd&ShopNo`
        let params = {
            bNo: 431,
            mebno: userInfo.C_MB_NO,
            cardNo: this.state.cardNo,
            pwd: this.state.pwd,
        }
        api.get(str, params, (res) => {
            base.showModal(res.msg,null,() => {
                base.prevPage().setState({
                    refresh: true
                })
                Taro.navigateBack()
            })
		},(err) => {
            base.showModal(err.msg)
        },() => {
            Taro.hideLoading()
        })
    }
    render() {
        return (
            <View className='main-card'>
                <View className='weui-cells weui-cells-none'>
                    <ItemInput label='卡号' placeholder='请输入卡号' onInput={this.onInput.bind(this,'cardNo')}></ItemInput>
                    <ItemInput label='密码' placeholder='不区分大小写' onInput={this.onInput.bind(this,'pwd')} type='password' password='true'></ItemInput>
                </View>
                <View className='p20 mt20'>
                    <Button class="weui-btn" type="primary" onClick={this.bindCard}>绑定</Button>
                </View>
            </View>
        )
    }
}

