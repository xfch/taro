import Taro, { Component } from '@tarojs/taro'
import { View, Text, Checkbox, Icon } from '@tarojs/components'
import './sendMethod.scss'

import Payway from '../../components/sendMethod/payway'
import Sendway from '../../components/sendMethod/sendway'
import Selflift from '../../components/sendMethod/selflift'
import Address from '../../components/sendMethod/address'

import { set as setGlobalData, get as getGlobalData } from '../../static/js/global_data'
import api from '../../service/api'
import common from '../../service/common'
import base from '../../static/js/base'

export default class Index extends Component {
    config = {
        navigationBarTitleText: '支付配送'
    }
    constructor() {
        super(...arguments)
        this.state = {
            btnList: [],//支付方式
            scopeAddress: false,
            orderSubmit: true,
        }
    }

    componentWillMount() {
        const that = this;
        let prevData = base.prevPage().state
        console.log('prevData-----')
        console.log(prevData)
        prevData.posmethod.map((item,i) => {
            item.n_isdefault = item.c_psname == prevData.psmethod ? 1 : 0
        })
        prevData.selfliftShow = prevData.psmethod == '上门自提' ? true : false
        this.setState(prevData)
        let env = process.env.TARO_ENV;
        if(env == 'weapp'){
            this.state.btnList = [{'txt': '微信支付','type': '1'}] 
        }else if(env == 'alipay'){
            this.state.btnList = [{'txt': '支付宝支付','type': '2'}] 
        }
        setGlobalData('payway', this.state.btnList[0].type)
        this.setState({
            btnList: this.state.btnList,
            payway: this.state.btnList[0].type,
            env: env
        })
        Taro.getSetting({
            success(res) {
                console.log(res)
                if(res.authSetting['scope.address']){
                    that.setState({scopeAddress: true})
                }
            }
        }) 
    }
    // 没点确认的配送方式
    selectSendWay(index,e){
        console.log(index)
        this.state.posmethod.map((item,i) => {
            item.n_isdefault = index == i ? 1 : 0
        })
        let selfliftShow = this.state.posmethod[index].c_psname == '上门自提' ? true : false
        this.setState({
            psmethod: this.state.posmethod[index].c_psname,
            posmethod: this.state.posmethod,
            selfliftShow: selfliftShow
        })
    }
    // 没点确认的自提门店
    selectSelflift(index){
        this.state.selflift.map((item,i) => {
            item.def = index == i ? 1 : 0
        })
        this.setState({
            selflift: this.state.selflift,
            selfliftSelect: this.state.selflift[index]
        })
    }
    // 新增地址
    selectAddress(){
        if(this.state.env == 'weapp'){
            common.addressAdd(this)
        }else if(this.state.env == 'alipay'){
            Taro.navigateTo({url: '/pages/addressList/addressList'})
            console.log('alipay')
        }
    }
    // 确定
    submitBtn(){
        if(this.state.selfliftShow){ //门店自提
            this.setPrevPageState()
        }else{
            if(!this.state.address){
                base.showToast('请添加收货地址！','none')
                return
            }
            this.setPrevPageState()
        } 
    }

    setPrevPageState(){
        console.log('设置了')
        base.prevPage().setState({
            selflift: this.state.selflift,
            selfliftSelect: this.state.selfliftSelect,
            selfliftShow: this.state.selfliftShow,
            posmethod: this.state.posmethod,
            address: this.state.address,
            psmethod: this.state.psmethod,
            refresh: true
        })
        Taro.navigateBack()
    }

    render() {
        return (
            <View className='sendMethod'>
                <View className='weui-cells weui-cells-none m0'>
                    {/* 支付方式 */}
                    <View className='posmethod weui-cell'>
                        <View className='weui-cell__bd'>
                            <Payway btnList={this.state.btnList} payway={this.state.payway}></Payway>
                        </View>
                    </View>
                </View>
                <View className='weui-cells weui-cells-none'>
                    {/*  配送方式*/}
                    <View className='goodList weui-cell'>
                        <View className='weui-cell__bd'>
                            <Sendway posmethod={this.state.posmethod} selfliftShow={this.state.selfliftShow} onSelectSendWay={this.selectSendWay.bind(this)}></Sendway>
                        </View>
                    </View>
                </View>
                <View className='weui-cells weui-cells-none'>
                    {/* 收货地址、自提列表 */}
                    <View className='goodList weui-cell weui-cell-p0'>
                        <View className='weui-cell__bd'>
                            {!this.state.selfliftShow ?
                                <Address address={this.state.address} scopeAddress={this.state.scopeAddress} onSelectAddress={this.selectAddress.bind(this)}></Address> :
                                <Selflift selflift={this.state.selflift} onSelectSelflift={this.selectSelflift.bind(this)}></Selflift>
                            }
                        </View>
                    </View>
                </View>
                <View className='weui-footer weui-footer_fixed-bottom submitBtn-bar'>
                    <Button className='submitBtn' type="main" size='default' plain="true" onClick={this.submitBtn}>确定</Button>
                </View>
            </View>
        )
    }
}

