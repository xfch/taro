import Taro, { Component } from '@tarojs/taro'
import { View, Text, Input, Button, Image, Textarea } from '@tarojs/components'

import './shopCard.scss'


import { set as setGlobalData, get as getGlobalData } from '../../static/js/global_data'
import api from '../../service/api'
import common from '../../service/common'
import base from '../../static/js/base'

// import cardBg from '../../assets/img/card-bg.png'

export default class Index extends Component {
    config = {
        navigationBarTitleText: '我的购物卡'
    }
    constructor() {
        super(...arguments)
        this.state = {
            domainImg: getGlobalData('domainImg'),
            iscollect: 1, // 收藏商品
            pageindex: 1,
            hasList: false,
            hasEnd: false,
        }
    }

    componentWillMount() {
        this.getShoppingCard()
    }
    componentDidShow(){
        if(this.state.refresh){
            this.getShoppingCard()
        }
    }
    // 获取购物卡
    getShoppingCard() {
        Taro.showLoading({ title: '加载中...' })
        let userInfo = Taro.getStorageSync('userInfo')
        const str = `cids&mebno&ShopNo`
        let params = {
          bNo: 421,
          cids: this.state.cids || 0,
          mebno: userInfo.C_MB_NO
        };
        api.get(str, params, (res) => {
            console.log(res)
            Taro.hideLoading();
            res.goodcard.map((item,i) => {
                item.scope = item.N_GF_Scope == 0 ? '全场通用' : '部分商品可用'
                item.D_DUETIME = item.D_DUETIME.substr(0, 10).replace(/\-/g, ".");
            })
            res.dtAllCard.map((item,i) => {
                item.scope = item.N_GF_Scope == 0 ? '全场通用' : '部分商品可用'
                item.D_DUETIME = item.D_DUETIME.substr(0, 10).replace(/\-/g, ".");
            })
            this.setState({
                goodcard: res.goodcard,
                dtAllCard: res.dtAllCard, //所有的购物卡
            })
		})
    }
    // 新增购物卡
    addCard(){
        Taro.navigateTo({url: '/pages/addCard/addCard'})
    }
    render() {
        return (
            <View className='main-card'>
                {this.state.dtAllCard && this.state.dtAllCard.map((item,i) => (
                    <View for={i} key={i} className='card-item mb20'>
                        {/* <Image src={cardBg} className='card-bg' /> */}
                            <View className='card-item-con'>
                                <View className='p20 ar'>
                                    <View className=''>No.{item.C_GF_ACCOUNT}</View>
                                    <View className='text-num'>￥{item.N_FACE_VALUE}</View>
                                    <View className='text-small'>购物卡</View>
                                    <View className='mt10'>{item.N_REMAMOUNT}元可用 / {item.scope}</View>
                                    <View className='text-little'>{item.D_DUETIME} 到期</View>
                                </View>
                            </View>
                    </View>
                ))}
                <View className='p20 mt20'>
                    <Button class="weui-btn" type="primary" onClick={this.addCard}>绑定购物卡</Button>
                </View>
            </View>
        )
    }
}

