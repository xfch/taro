import Taro, { Component } from '@tarojs/taro'
import { View, Text, Input, Button, Image, Textarea } from '@tarojs/components'

import './wallet.scss'

import Authorize from '../../components/authorize/authorize'

import { set as setGlobalData, get as getGlobalData } from '../../static/js/global_data'
import api from '../../service/api'
import common from '../../service/common'
import base from '../../static/js/base'

export default class Index extends Component {
    config = {
        navigationBarTitleText: '我的钱包'
    }
    constructor() {
        super(...arguments)
        this.state = {
            domainImg: getGlobalData('domainImg'),
        }
    }
    // authorize获取作用域
    getIsLogin(obj){
        if(obj){
            if(obj.C_MB_NO){ 
                this.setState({userInfo: obj})
            }else{
                obj.isLogin(() => {
                    const userInfo = Taro.getStorageSync('userInfo')
                    this.setState({userInfo: userInfo})
                })
            }
        }
    }
    componentWillMount(obj) {
        this.getIsLogin(obj)
    }
    navTo(id){
        Taro.navigateTo({
            url: `/pages/${id}/${id}`
        })
    }

    render() {
        let {userInfo} = this.state
        return (
            <View className='main-wallet'>
                <View className='balance-bar'>
                    <View className='ar'>总消费：{userInfo.N_CON_AMOUNT}</View>
                    <View className='ac balance'>
                        <View>￥<Text className='text-num'>{userInfo.N_MB_AMOUNT}</Text></View>
                        <View className='ac'>当前余额</View>
                    </View>
                </View>
                <View className='weui-flex recharge-bar bg-white'>
                    <View className='weui-flex__item recharge-item' onClick={this.navTo.bind(this,'rechargeHistory')}>充值记录</View>
                    <View className='weui-flex__item recharge-item' onClick={this.navTo.bind(this,'recharge')}>我要充值</View>
                </View>
                {/* <View className='weui-cells weui-cells-none'>
                    <View className='weui-cell' onClick={this.navTo.bind(this,'scoreHistory')}>
                        <View className='weui-cell__bd'>可用积分</View>
                        <View className='weui-cell__ft weui-cell__ft_in-access'>{userInfo.N_MB_INTEGRAL}</View>
                    </View>
                </View> */}
                <View className='weui-cells weui-cells-none'>
                    <View className='weui-cell'>
                        <View className='weui-cell__bd'>可用积分</View>
                        <View className='weui-cell__ft'>{userInfo.N_MB_INTEGRAL}</View>
                    </View>
                </View>
                <Authorize page='personal' onSetUserInfo={this.getIsLogin.bind(this)}></Authorize>
            </View>
        )
    }
}

