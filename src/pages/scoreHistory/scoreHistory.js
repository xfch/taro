import Taro, { Component } from '@tarojs/taro'
import { View, Text, Input, Button, Image, Textarea } from '@tarojs/components'

import './scoreHistory.scss'


import { set as setGlobalData, get as getGlobalData } from '../../static/js/global_data'
import api from '../../service/api'
import common from '../../service/common'
import base from '../../static/js/base'

export default class Index extends Component {
    config = {
        navigationBarTitleText: '积分记录'
    }
    constructor() {
        super(...arguments)
        this.state = {
            domainImg: getGlobalData('domainImg'),
        }
    }

    componentWillMount() {
        console.log(this.$router.params)
    }

    render() {
        return (
            <View className='main-wallet'>
                <View className='balance-bar'>
                    <View className='ar'>总消费：0.00</View>
                    <View className='ac balance'>
                        <View>￥<Text className='text-num'>0</Text></View>
                        <View className='ac'>当前余额</View>
                    </View>
                </View>
                <View className='weui-flex recharge-bar bg-white'>
                    <View className='weui-flex__item recharge-item'>充值记录</View>
                    <View className='weui-flex__item recharge-item'>我要充值</View>
                </View>
                <View className='weui-cells weui-cells-none'>
                    <View className='weui-cell'>
                        <View className='weui-cell__bd'>可用积分</View>
                        <View className='weui-cell__ft weui-cell__ft_in-access'>0</View>
                    </View>
                </View>
            </View>
        )
    }
}

