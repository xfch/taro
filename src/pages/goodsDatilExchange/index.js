import Taro, { Component } from '@tarojs/taro'
import { View, Swiper, SwiperItem, Image, Text, RichText } from '@tarojs/components'
import './index.scss'

import GoodSwiper from '../../components/goodsDatil/goodSwiper'
import ExchangeSpec from '../../components/goodsDatil/exchangeSpec'
import Tabs from '../../components/tabs'
import parseHtml from 'mini-html-parser2'; //如果是微信小程序   需要注释这一行以及其使用的地方   不然会报错
import AddCart from '../../components/addCart'
import Authorize from '../../components/authorize/authorize'

import { set as setGlobalData, get as getGlobalData } from '../../static/js/global_data'
import api from '../../service/api'
import base from '../../static/js/base'
import common from '../../service/common'

export default class Index extends Component {
    config = {
        navigationBarTitleText: '商品详情',
        navigationBarBackgroundColor: "#fe690f",
    }
    constructor() {
        super(...arguments)
        this.state = {
            domainImg: getGlobalData('domainImg'),
            hasView: false,
            warnLine: 15, // 库存紧张分界点
            richTxt: null,
            tabsTitle: ['商品详情'],
            popCoupon: false, // 是否显示兑换弹窗
        }
    }

    componentDidShow(){
        console.log(this.$router.params)
        this.state.gid = this.$router.params.goodsId
        this.getPromiseAll()
    }

    componentDidHide(){
        this.onHidePopCoupon()
    }
    // 统一调用接口
    getPromiseAll(){
        let getGoodPic = new Promise((resolve,reject) => {
            this.getGoodPic((res) => {
                resolve(res)
            })
        })
        let getGoodDetail = new Promise((resolve,reject) => {
            this.getGoodDetail((res) => {
                resolve(res)
            })
        })
        Promise.all([getGoodPic,getGoodDetail]).then((res) => {
            console.log(res)
            this.setRichHtml(res[1].GiftGoods[0].GOODS_DETAIL)
            this.setState({
                goodsImg: res[0].goodspic,
                giftGoods: res[1].GiftGoods[0],
                hasView: true
            })
            Taro.hideLoading()
        });
    }
    // 获取图片
    getGoodPic(callback) {
        const str = 'gid'
        const params = {
            bNo: 700,
            gid: this.state.gid,
        }
        api.get(str, params, (res) => {
            if(!res.goodspic){res.goodspic = []}
            res.goodspic.map((item) => {
                item.pic = item.picname
            })
            if(callback){callback(res)}
        })
    }
    // 获取商品详情
    getGoodDetail(callback){
        const str = 'ShopNo&gid'
        const params = {
            bNo: 1303,
            gid: this.state.gid,
        }
        api.get(str, params, (res) => {
            if(callback){callback(res)}
        })
    }
    setRichHtml(h) {
        let html = h.replace(/\<img/gi, '<img style="max-width:100%;height:auto" ');
        if (process.env.TARO_ENV == 'weapp') {
            this.setState({
                richTxt: html
            })
        } else if (process.env.TARO_ENV == 'alipay') {
            parseHtml(html, (err, nodes) => {
                if (!err) {
                    this.setState({
                        richTxt: nodes
                    })
                }
            }) 
        }
    }
    // 点击我要兑换
    onChoose(type, N_GSPrice){
        this.isLogin.isLogin(() => {
             console.log('登陆后的操作') 
             this.onHidePopCoupon()
        })
    }
    // 点击关闭兑换弹窗
    onHidePopCoupon(){
        this.setState({
            popCoupon: !this.state.popCoupon
        })
    }
    // authorize获取作用域
    getIsLogin(obj){
        console.log(obj)
        if(obj){
            if(obj.C_MB_NO){ 
                console.log('登陆后的操作') 
            }else{
                this.isLogin = obj
            }
        }	
    }

    render() {
        let { hasView, goodsImg, giftGoods, warnLine,popCoupon } = this.state
        return (
            <View className='main-goodDetail'>
                {/* 图片 */}
                {hasView && <GoodSwiper goodsImg={goodsImg}></GoodSwiper>}
                {hasView && 
                    <Block>
                        <View className='goodDetail-title'>
                            <View className='weui-cell'>
                                <View className='weui-cell__bd'><Text className='text-big'>{giftGoods.GOODS_NAME}</Text></View>
                            </View>
                            <View className='weui-cell'>
                                <View className='weui-cell__bd'><Text className='text-sub'>兑换所需：{giftGoods.N_IG_INTEGRAL}积分</Text></View>
                            </View>
                            <View className='weui-cell'>
                                <View className='weui-cell__bd'><Text className='text-gray'>参考价：￥{giftGoods.SALE_PRICE}</Text></View>
                                <View className='weui-cell__ft'>
                                    {giftGoods.STOCK <= 0 && <Text>库存不足</Text>}
                                    {giftGoods.STOCK < warnLine && <Text>库存紧张</Text>}
                                    {giftGoods.STOCK >= warnLine && <Text>库存充足</Text>}
                                    <Text>({giftGoods.STOCK}件)</Text>
                                </View>
                            </View>
                        </View>

                        {/* 商品详情 */}
                        <Tabs titles={this.state.tabsTitle} />
                            <View className='goodsRichText-bar'>
                                {richTxt ? <RichText className='goodsRichText' nodes={richTxt}></RichText> : <View className='no-data'>暂无详情</View>}
                            </View>

                        {/* 底部购买栏 */}
                        <AddCart goodsInfo={giftGoods} goodExchange='true' onChoose={this.onChoose.bind(this)} />
                    </Block>
                }
                {/* 登陆 */}
                <Authorize page='onSetUserInfo' onSetUserInfo={this.getIsLogin.bind(this)}></Authorize>
                {/* 兑换弹窗 */}
                <ExchangeSpec onHidePopCoupon={this.onHidePopCoupon.bind(this)} popCoupon={popCoupon} goodInfo={giftGoods}></ExchangeSpec>
            </View>
        )
    }
}

