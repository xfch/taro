import Taro, { Component } from '@tarojs/taro'
import { View, Text } from '@tarojs/components'

import './index.scss'

import ArtificerPic from '../../components/subscribeDate/artificerPic'

import { set as setGlobalData, get as getGlobalData } from '../../static/js/global_data'
import base from '../../static/js/base.js'

let d = new Date()
export default class Index extends Component {
    config = {
        navigationBarTitleText: '选择到店时间',
    }
    constructor() {
        super(...arguments)
        this.state = {
            shopInfo: getGlobalData('shopInfo'),
            dateArray: [],
            dateIndex: 0,
            am: [],
            pm: [],
            zm: [],
            artificerItem: null,
            industry: getGlobalData('industry'), 
        }
    }

    componentWillMount() {
        const artificerItem = this.$router.params.artificerItem ? JSON.parse(this.$router.params.artificerItem) : null 
        console.log(artificerItem)
        this.setState({
            artificerItem
        })
        this.getDateArr()
        this.getTimeSlot()
    }
    // 获取日期
    getDateArr() {
        let myDate = new Date(); //获取今天日期
        myDate.setDate(myDate.getDate());
        let dateArray = [];
        let dateTemp;
        let flag = 1;
        for (let i = 0; i < 7; i++) {
            let m = myDate.getMonth() + 1;
            let d = myDate.getDate();
            m = m < 10 ? "0" + m : m
            d = d < 10 ? '0' + d : d
            dateTemp = m + "-" + d;
            if (myDate.getDay() == 0) {
                dateArray.push({
                    d: dateTemp,
                    day: this.getDay(7),
                });
            } else {
                dateArray.push({
                    d: dateTemp,
                    day: this.getDay(myDate.getDay()),
                });
            }
            myDate.setDate(myDate.getDate() + flag);
        }
        dateArray[0].day = "今天"
        this.setState({
            dateArray,
        })
    }
    getDay(day) {
        switch (day) {
            case 1:
                return "周一";
            case 2:
                return "周二";
            case 3:
                return "周三";
            case 4:
                return "周四";
            case 5:
                return "周五";
            case 6:
                return "周六";
            case 7:
                return "周日";
        }
    }
    // 切换日期
    changeDate(dateIndex) {
        this.setTimeArr(dateIndex == 0)
        this.setState({ dateIndex })
    }
    // 获取时间段
    getTimeSlot() {
        var inf = this.state.shopInfo.model.c_house;
        var start = inf.substr(0, 5);
        var startH = parseInt(start.substr(0, 2));
        var end = inf.substr(6, 5);
        var endH = parseInt(end.substr(0, 2))
        var timeArr = [];

        if (startH < endH) {
            for (var i = startH; i < endH; i++) {
                if (i < 10) {
                    timeArr.push({
                        over: 1,
                        t: "0" + i + ":00",
                        ti: i,
                    })
                    timeArr.push({
                        over: 1,
                        t: "0" + i + ":30",
                        ti: i,
                    })
                } else {
                    timeArr.push({
                        over: 1,
                        t: i + ":00",
                        ti: i,
                    })
                    timeArr.push({
                        over: 1,
                        t: i + ":30",
                        ti: i,
                    })
                }
            }
        } else {
            for (var i = startH; i < 24; i++) {
                timeArr.push({
                    over: 1,
                    t: i + ":00",
                    ti: i,
                })
                timeArr.push({
                    over: 1,
                    t: i + ":30",
                    ti: i
                })
            }
            for (var i = 0; i < endH; i++) {
                if (i < 10) {
                    timeArr.push({
                        over: 2,
                        t: "0" + i + ":00",
                        ti: i,
                    })
                    timeArr.push({
                        over: 2,
                        t: "0" + i + ":30",
                        ti: i,
                    })
                } else {
                    timeArr.push({
                        over: 2,
                        t: i + ":00",
                        ti: i,
                    })
                    timeArr.push({
                        over: 2,
                        t: i + ":30",
                        ti: i,
                    })
                }
            }
        }

        this.state.timeArrData = timeArr;
        this.setTimeArr(true)
    }
    setTimeArr(f) {
        let am = [],
            pm = [],
            zm = [];
        this.state.timeArrData.map((item, i) => {
            item.disabled = false
            item.checked = false
            if (f) {
                if (item.ti <= d.getHours()) {
                    item.disabled = true;
                }
            }
            if (item.ti <= 12 && item.over == 1) {am.push(item)}
            if (item.ti > 12 && item.over == 1) {pm.push(item)}
            if (item.over == 2) {zm.push(item)}
        })
        this.setState({
            am,
            pm,
            zm,
        })
    }
    // 切换时间段
    setTimeFunc(dataset, index, tp) {
        if (!dataset.disabled) {
            var str = d.getFullYear() + "-" + this.state.dateArray[this.state.dateIndex].d + " " + dataset.t;
            this.state.sureTime = str;
            this.state.am.map((item) => item.checked = false)
            this.state.pm.map((item) => item.checked = false)
            this.state.zm.map((item) => item.checked = false)
            if (tp == "am") {
                this.state.am[index].checked = true;
            }
            else if (tp == "pm") {
                this.state.pm[index].checked = true;
            }
            else if (tp == "zm") {
                this.state.zm[index].checked = true;
            }
            let time = new Date(this.state.sureTime.replace("-","/"));
            const b = 30; //分钟数
            time.setMinutes(time.getMinutes() + b, time.getSeconds(), 0);
            const dates = new Date(time)
            const endDate = dates.getFullYear() + '-' + this.p((dates.getMonth() + 1)) + '-' + this.p(dates.getDate()) + ' ' + this.p(dates.getHours()) + ':' + this.p(dates.getMinutes())
            const C_AddDate = d.getFullYear() + '-' + this.p((d.getMonth() + 1)) + '-' + this.p(d.getDate()) // 当前日期
            this.setState({
                am: this.state.am,
                pm: this.state.pm,
                zm: this.state.zm,
                sureTime: this.state.sureTime,
                endDate,
                C_AddDate,
            })
        }
    }
    p(s) {
        return s < 10 ? '0' + s : s
    }
    parserDate(date) {
        var t = Date.parse(date)
        if (!isNaN(t)) {
          return new Date(Date.parse(date.replace(/-/g, '/')))
        }
      }
    // 确定
    submit(){
        let {industry,artificerItem,sureTime,endDate,C_AddDate} = this.state
        if(industry == 390){ // 美业
            if(!sureTime){
                base.showModal('请选择到店时间')
                return
            }
            Taro.navigateTo({
                url: `/pages/subscribeSubmit/index?artificerItem=${JSON.stringify(artificerItem)}&D_StartDate=${sureTime}&D_EndDate=${endDate}&C_AddDate=${C_AddDate}`
            })
        }else{
            base.prevPage().setState({
                D_StartDate: this.state.sureTime, // 开始时间
                D_EndDate: this.state.endDate, // 结束时间
                C_AddDate: this.state.C_AddDate,
            })
            Taro.navigateBack()
        }
    }

    render() {
        let { dateArray, dateIndex, am, pm, zm ,artificerItem} = this.state
        return (
            <View className='mainDate'>
                {artificerItem && <ArtificerPic artificerItem={artificerItem}></ArtificerPic>}
                {dateArray.length > 0 &&
                    <View className='weui-flex ac date-bar'>
                        {dateArray.map((item, i) => (
                            <View key={'date' + i} className={`weui-flex__item date-bar-item ${dateIndex == i ? 'active' : ''}`} onClick={this.changeDate.bind(this, i)}>
                                <View className='item-date'>{item.d}</View>
                                <View className='text-small'>{item.day}</View>
                            </View>
                        ))}
                    </View>
                }
                {am.length > 0 &&
                    <View className={`ac bottomLine`}>
                        <View className='p20'>上午</View>
                        <View className='weui-flex timesolt-bar'>
                            {am.map((item, i) => (
                                <View className={`item-timesolt ${item.disabled ? 'item-timesolt-disabled' : ''} ${item.checked ? 'active' : ''}`} onClick={this.setTimeFunc.bind(this, item, i, 'am')}>{item.t}</View>
                            ))}
                        </View>
                    </View>
                }
                {pm.length > 0 &&
                    <View className={`ac bottomLine`}>
                        <View className='p20'>下午</View>
                        <View className='weui-flex timesolt-bar'>
                            {pm.map((item, i) => (
                                <View className={`item-timesolt ${item.disabled ? 'item-timesolt-disabled' : ''} ${item.checked ? 'active' : ''}`} onClick={this.setTimeFunc.bind(this, item, i, 'pm')}>{item.t}</View>
                            ))}
                        </View>
                    </View>
                }
                {zm.length > 0 &&
                    <View className='ac bottomLine'>
                        <View className='p20'>次日</View>
                        <View className='weui-flex timesolt-bar'>
                            {zm.map((item, i) => (
                                <View className={`item-timesolt ${item.disabled ? 'item-timesolt-disabled' : ''} ${item.checked ? 'active' : ''}`} onClick={this.setTimeFunc.bind(this, item, i, 'zm')}>{item.t}</View>
                            ))}
                        </View>
                    </View>
                }

                <View className='p20'>
                    <Button className='btn-main' onClick={this.submit}>确定</Button>
                </View>


            </View>
        )
    }
}

