import Taro, { Component } from '@tarojs/taro'
import { View, Text, Image } from '@tarojs/components'
import './index.scss'

import Search from '../../components/index/search'
import NoData from '../../components/noData'
// import VwItem from '../../components/index/productVwItem'
import VhItem from '../../components/index/productVhItem'

import { set as setGlobalData, get as getGlobalData } from '../../static/js/global_data'

import api from '../../service/api'
import common from '../../service/common'
import base from '../../static/js/base'

export default class Index extends Component {
    config = {
        navigationBarTitleText: '搜索列表'
    }
    constructor() {
        super(...arguments)
        this.state = {
            pageindex: 1,
            hasEd: false,
            searchVal: '',
            searchState: false, // 是否点击搜索
            searchHistory: [], // 搜索历史记录
            goodList: [],
            hasView: false
        }
    }

    componentWillMount() {
        this.state.searchHistory = Taro.getStorageSync('searchHistory') || []
        this.setState({
            searchHistory: this.state.searchHistory
        })
    }
    setVal(val){
        this.setState({
            searchVal: val,
            searchState: false,
            hasView: false
        })
    }
    // 点击搜索框
    onInput(val){
        this.setVal(val)
    }
    // 选择历史记录
    selectSearchHistory(val){
        this.setVal(val)
    }
    // 点击搜索按钮
    searchIcon(){
        let pushState = this.state.searchHistory.some((item) => {
            return this.state.searchVal == item
        })
        if(!pushState){
            this.state.searchHistory.push(this.state.searchVal)
            Taro.setStorageSync('searchHistory',this.state.searchHistory)
        }
        this.setState({
            searchState: true,
            searchHistory: this.state.searchHistory,
            pageindex: 1,
            hasEd: false,
            goodList: [],
        })
        this.getGoodList()
    }
    // 获取兑换列表
    getGoodList() {
        const that = this
        base.showLoading()
        that.state.name = this.state.searchVal
        common.getGoodList(that,(res) => {
            console.log(res)
            that.setState({
                goodList: res,
                hasView: true
            })
            Taro.hideLoading()
        })
    }
    // 上拉加载
    onReachBottom(){
        if(this.state.hasEd){return}
        base.showLoading()
        this.state.pageindex ++;
        this.getGoodList()
    }
    // 订单详情
    navTo(orderId){
        Taro.navigateTo({url: `/pages/orderDetail/orderDetail?orderId=${orderId}&&myExchange=true`})
    }
    
    render() {
        let {goodList, hasView, searchState, searchHistory} = this.state
        return (
            <View className='mainList'>
                <Search type='input' onSearchVal={this.onInput.bind(this)} onSearchIcon={this.searchIcon} searchVal={this.state.searchVal}></Search>
                <View className='searchList'>
                    {!searchState ? 
                        <View className='search-history'>
                            <View className='title'>历史记录</View>
                            <View>
                                {searchHistory.length > 0 && searchHistory.map((item,i) => (
                                    <Text className='item' for={i} key={i} onClick={this.selectSearchHistory.bind(this,item)}>{item}</Text>
                                ))}
                            </View>
                        </View> : 
                        <View className='search-list'>
                            {hasView && <Block>
                                {goodList.length > 0 ? 
                                    <Block>
                                        {goodList.map((item,i) => (
                                            <VhItem pData={item} key={'goodList' + i}></VhItem>
                                        ))}
                                        {this.state.hasEd && <NoData type='2'></NoData>}
                                    </Block> : 
                                    <NoData></NoData>
                                }
                            </Block>}
                        </View>
                    }
                </View>
            </View>
        )
    }
}

