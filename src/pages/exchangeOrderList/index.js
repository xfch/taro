import Taro, { Component } from '@tarojs/taro'
import { View, Text, Image } from '@tarojs/components'
import './index.scss'

import Checkboxbar from '../../components/checkboxbar'
import NumberInput from '../../components/numInput/index'
import Footer from '../../components/cart/footer'
import Price from '../../components/cart/price'

import Authorize from '../../components/authorize/authorize'

import { set as setGlobalData, get as getGlobalData } from '../../static/js/global_data'
import api from '../../service/api'
import common from '../../service/common'
import apiAlipay from '../../service/apiAlipay'
import base from '../../static/js/base'

export default class Index extends Component {
    config = {
        navigationBarTitleText: '积分兑换'
    }
    constructor() {
        super(...arguments)
        this.state = {
            hasList: false,
            pageindex: 1,
            hasEnd: false,
            pagesize: getGlobalData("pagesize"),
            domainImg: getGlobalData('domainImg')
        }
    }

    componentWillMount() {
       this.getMyExchange()
    }
    // 获取兑换列表
    getMyExchange() {
        const str = `ShopNo&pageindex&pagesize`;
        const params = {
          bNo: 1305,
          pageindex: this.state.pageindex,
          pagesize: this.state.pagesize,
        };
        let changeRecords = []
        api.get(str, params, (res) => {
            console.log(res)
            res.ChangeRecord = !res.ChangeRecord ? [] : res.ChangeRecord
            changeRecords = this.state.pageindex == 1 ? res.ChangeRecord : this.state.changeRecord.concat(res.ChangeRecord)
            this.state.hasEnd = res.ChangeRecord < this.state.pagesize ? true : false
            this.setState({
                hasList: true,
                hasEnd: this.state.hasEnd,
                changeRecord: changeRecords,
            })
        }, (res) => {
            Taro.showToast({ title: res.msg })
        }, () => {
            Taro.hideLoading()
        })
    }
    // 上拉加载
    onReachBottom(){
        if(this.state.hasEnd){return}
        base.showLoading()
        this.state.pageindex ++;
        this.getMyExchange()
    }
    // 订单详情
    navTo(orderId){
        Taro.navigateTo({url: `/pages/orderDetail/orderDetail?orderId=${orderId}&&myExchange=true`})
    }
    
    render() {
        let {domainImg,hasList,changeRecord} = this.state
        return (
            <View className='mainList'>
                {hasList && 
                    <Block>
                        {changeRecord.length > 0 ? 
                            <Block>
                                {changeRecord.map((items,i) => (
                                    <View for={i} key={i} className='weui-cells' onClick={this.navTo.bind(this,items.N_IGR_NO)}>
                                        <View className='weui-cell'>
                                            <View className='weui-cell__bd'><Text className='text-gray'>订单编号：{items.N_IGR_NO}</Text></View>
                                            <View className='weui-cell__ft'><Text className='text-gray'>{items.D_APL_DATE}</Text></View>
                                        </View>
                                        {items.RecordDetail.map((item,j) => (
                                             <View for={j} key={j} className='weui-cell'>
                                                <View className='weui-cell__hd'><Image className='item-img' src={domainImg + item.C_MAINPICTURE}></Image></View>
                                                <View className='weui-cell__bd'>
                                                    <View>{item.GOODS_NAME}</View>
                                                </View>
                                                <View className='weui-cell__ft'><Text className='text-gray text-small'>× {item.N_IGRD_NUM}</Text></View>
                                            </View>
                                        ))}
                                        <View className='weui-cell'>
                                            <View className='weui-cell__bd'><Text className='text-sub text-small'>{items.C_IGR_REMARK}</Text></View>
                                            <View className='weui-cell__ft'>共计 <Text className='text-sub'>{items.N_IGR_INTEGRAL}</Text> 积分</View>
                                        </View>
                                    </View>
                                ))}
                                {this.state.hasEnd && <View className='p20 text-gray ac'>没有更多数据了</View>}
                            </Block> :
                            <View className='p20 ac'>暂无数据</View>
                        }
                    </Block>
                }
            </View>
        )
    }
}

