import Taro from '@tarojs/taro'
import { get as getGlobalData } from '../static/js/global_data'

export default {
    getAddRes(res) { //获取地址详细信息
        return Taro.request({
            url: 'https://restapi.amap.com/v3/geocode/regeo',
            data: {
                key: '1b15e2fec68e386ff3e071d42ef27644',
                location: res.longitude + "," + res.latitude,
                extensions: "all",
                s: "rsx",
                sdkversion: "sdkversion",
                logversion: "logversion"
            }
        })
    },
    openSet() {
        return new Promise((r) => {
            Taro.openSetting({
                success(data) {
                    let state = false;
                    if (getGlobalData('JRE') == 'weapp') {
                        state = data.authSetting["scope.userLocation"]
                    }
                    if (getGlobalData('JRE') == 'alipay') {
                        state = data.authSetting['location']
                    }
                    if (state) {
                        Taro.showToast({
                            title: '授权成功',
                            icon: 'success',
                            duration: 2000
                        })
                        r(data)
                    } else {
                        Taro.showToast({
                            title: '授权失败',
                            icon: 'none',
                            duration: 2000
                        })
                    }
                },
                fail() {

                }
            })
        })

    },
    getSet() {
        return new Promise((r, e) => {
            Taro.getSetting({
                success(res) {
                    var statu = res.authSetting;
                    if (getGlobalData('JRE') == 'weapp') {
                        if (!statu['scope.userLocation']) { e() }
                        else {
                            r()
                        }
                    } else if (getGlobalData('JRE') == 'alipay') {
                        if (!statu['location']) { e() }
                        else {
                            r()
                        }
                    }
                },
                fail: function () {
                    Taro.showToast({
                        title: '调用授权窗口失败',
                        icon: 'none',
                        duration: 2000
                    })
                }
            })
        })
    },
    getAddress() {
        //获取地理位置
        const _this = this;
        return new Promise((resolve) => {
            Taro.showLoading({ title: '加载中...' });
            const failFn = () => {
                _this.getSet().then(() => {

                }, () => {//查询用户是否已经授权，没有则执行下一步
                    Taro.showModal({
                        title: "位置信息",
                        content: '微客巴巴需要获取您的地理位置，请确认授权，否则地图功能将无法使用',
                        showCancel: true,
                        cancelText: "取消",
                        cancelColor: "#000",
                        confirmText: "确定",
                        confirmColor: "#0f0",
                        success: function (stares) {
                            if (stares.confirm) {
                                _this.openSet().then(() => {//用户关闭了微信权限管理页面--并且成功授权
                                    Taro.chooseLocation({
                                        success(rect) {
                                            _this.getAddRes(rect).then(info => {
                                                rect['regeocode'] = info.data.regeocode
                                                resolve(rect)
                                            })
                                        },
                                    })
                                })
                            }
                        }
                    })
                })
            }
            if (getGlobalData('JRE') == 'weapp') {
                Taro.chooseLocation({
                    success(res) {
                        Taro.hideLoading();
                        _this.getAddRes(res).then(info => {
                            res['regeocode'] = info.data.regeocode
                            resolve(res)
                        })
                    },
                    fail() {
                        Taro.hideLoading();
                        failFn()
                    }
                })
            } else if (getGlobalData('JRE') == 'alipay') {
                _this.getSet().then(() => {
                    Taro.hideLoading();
                    Taro.chooseLocation({
                        success(res) {
                            _this.getAddRes(res).then(info => {
                                res['regeocode'] = info.data.regeocode
                                resolve(res)
                            })
                        },
                        fail() {

                        }
                    })
                }, () => {
                    Taro.hideLoading();
                    Taro.showModal({
                        title: "位置信息",
                        content: '微客巴巴需要获取您的地理位置，请确认授权，否则地图功能将无法使用',
                        showCancel: true,
                        cancelText: "取消",
                        cancelColor: "#000",
                        confirmText: "确定",
                        confirmColor: "#0f0",
                        success: function (stares) {
                            if (stares.confirm) {
                                _this.openSet().then(() => {//用户关闭了微信权限管理页面--并且成功授权
                                    Taro.chooseLocation({
                                        success(rect) {
                                            _this.getAddRes(rect).then(info => {
                                                rect['regeocode'] = info.data.regeocode
                                                resolve(rect)
                                            })
                                        },
                                    })
                                })
                            }
                        }
                    })
                })
            }
        })
    },
    getDomInfo(id, scope) {
        return new Promise((s) => {
            if (process.env.TARO_ENV == 'alipay') {
                Taro.createSelectorQuery().select(id).boundingClientRect().exec((ret) => {
                    s(ret[0]);
                });
            } else if (process.env.TARO_ENV == 'weapp') {
                var query = Taro.createSelectorQuery().in(scope);
                query.select(id).boundingClientRect((rect) => {
                    s(rect)
                }).exec();
            }
        })

    },
    /**
   * 比较日期
   */
    formatDuring(mss) {
        let dayDiff = Math.floor(mss / (24 * 3600 * 1000));//计算出相差天数
        let leave1 = mss % (24 * 3600 * 1000)    //计算天数后剩余的毫秒数
        let hours = Math.floor(leave1 / (3600 * 1000))//计算出小时数
        //计算相差分钟数
        let leave2 = leave1 % (3600 * 1000)    //计算小时数后剩余的毫秒数
        let minutes = Math.floor(leave2 / (60 * 1000))//计算相差分钟数
        //计算相差秒数
        let leave3 = leave2 % (60 * 1000)      //计算分钟数后剩余的毫秒数
        let seconds = Math.round(leave3 / 1000)
        if (dayDiff < 10) {
            dayDiff = '0' + dayDiff
        }
        if (hours < 10) {
            hours = '0' + hours
        }
        if (minutes < 10) {
            minutes = '0' + minutes
        }
        if (seconds < 10) {
            seconds = '0' + seconds
        }
        return {
            d: dayDiff, h: hours, m: minutes, s: seconds
        }
    },
    getKillTime(endData) {
        let dateBegin = new Date();//获取当前时间
        //var dateBegin = new Date(startData.replace(/-/g, "/"));//将-转化为/，使用new Date
        let dateEnd = new Date(endData.replace(/-/g, "/"));//获取当前时间
        let dateDiff = dateEnd.getTime() - dateBegin.getTime();//时间差的毫秒数
        return dateDiff
    },
    getActType(type) { 
        switch (type) {
            case 1:
                return '促销'
                break;
            case 2:
                return '秒杀'
                break;
            case 3:
                return '满减'
                break;
            case 5:
                return '积分'
                break;
            case 12:
                return '打折'
                break;
            case 15:
                return '打折'
                break;
            
            default:
                break;
        }
    }
}