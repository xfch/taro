import Taro from '@tarojs/taro'

module.exports = {
    comNavTo,
}
// 跳转路径
function comNavTo(modelId,linktype,linkparam){
    let env = process.env.TARO_ENV;
    console.log(modelId + '-modelId,' + linktype + '-linktype,' + linkparam )
    if(!linkparam){linkparam = ''}
    if(linkparam.indexOf('=') != -1){
        linkparam = linkparam.split('=')
        linkparam = linkparam [1]
    }
    let _login = `/pages/login/login?pages=`
    let _index = `/pages/index/index`
    let _personal = `/pages/personal/personal`
    let _category = `/pages/category/category` // 分类
    let _shopcart = `/pages/shopcart/shopcart`
    let _shopList = `/pages/shopList/index` // 店铺列表
    let _goodList = `/pages/goodList/index?gcid=${linkparam}&modelId=${modelId}`  // 商品列表 - 商品分类 702b
    let _goodListMarketing = `/pages/goodListMarketing/index?modelId=${modelId}`  // 商品列表 - 积分、拼团
    let _goodsDatil = `/pages/goodsDatil/index?goodsId=${linkparam}` // 商品详情
    let _goodsDatilExchange = `/pages/goodsDatilExchange/index?goodsId=${linkparam}` // 商品详情 - 积分
    let _couponPage = `/pages/couponPage/index` // 领取优惠券
    let _searchList = `/pages/searchList/index` // 搜索列表
    let _articleList = `/pages/articleList/index?sncategory=${linkparam}` // 文章列表
    let _articleDetail = `/pages/articleDetail/index?ntid=${linkparam}` // 文章详情
    let _recharge = `/pages/recharge/recharge` // 充值
    let _order = `/pages/order/order?stateId=${linkparam}` // 订单列表
    let _commentList = `/pages/commentList/index` // 评论列表
    let _wallet = `/pages/wallet/wallet` // 我的钱包
    let _addressList = `/pages/addressList/addressList` // 收货地址
    let _subscribePeopleNum = `/pages/subscribePeopleNum/index` // 预约技师
    let _payment = `/pages/payment/index`  // 付款
    let _orderFood = '/pages/orderFood/index'    // 无桌点餐、外卖
    let _orderFoodTable = '/pages/orderFoodTable/index'   // 有桌点餐

    // 商品
    if(modelId == '27'){ // 商品-单商品-横幅
        if(linktype == '1'){ 
            Taro.navigateTo({url: _goodsDatil})
        }
        else if(linktype == '2'){ 
            Taro.navigateTo({url: _goodList})
        }
    }else if(modelId == '26'){ // 商品-多商品-积分商品
        if(linktype == 1){ // 列表
            Taro.navigateTo({url: _goodListMarketing})
        }else{
            Taro.navigateTo({url: _goodsDatilExchange})
        }
    }

    // 信息
    else if(modelId == '7' || modelId == '28'){ // 单条新闻
        if(modelId == '28'){
            Taro.navigateTo({url: _articleDetail})
        }
    }else if(modelId == '41' || modelId == '42' || modelId == '43'){ //信息 - 栏目样式1、栏目样式2、栏目样式3
        Taro.navigateTo({url: _articleList})
    }

    // 营销
    else if(modelId == '3'){ // 营销-优惠券
        Taro.navigateTo({url: _couponPage})
    }else if(modelId == '61'){ // 营销-拼团
        if(linktype == 1){ // 列表
            Taro.navigateTo({url: _goodListMarketing})
        }else if(linktype == '0'){
            Taro.navigateTo({url: `${_goodsDatil}&SaleTypeNo=pinTuan`})
        }
    }

    // 店铺相关

    // 公共组件
    else if(modelId == '51' || modelId == '52' || modelId == '53' || modelId == '54' || modelId == '55'){ // 公共组件-栏目入口-导航样式1-5   只做了商品列表分类跳转
        if(linktype == '0'){ // 积分商品
            Taro.navigateTo({url: _goodsDatilExchange})
        }else if(linktype == '1'){ 
            Taro.navigateTo({url: _articleDetail})
        }else if(linktype == '2'){ 
            Taro.navigateTo({url: _articleList})
        }else if(linktype == '3'){
            Taro.navigateTo({url: _goodList})
        }else if(linktype == '4'){
            Taro.navigateTo({url: _category})
        }else if(linktype == '5'){ // 商品分组
            // Taro.navigateTo({url: _goodList})
        }else if(linktype == '6'){ 
            Taro.navigateTo({url: _couponPage})
        }else if(linktype == '7'){ 
            Taro.reLaunch({url: _index})
        }else if(linktype == '8'){ 
            Taro.navigateTo({url: _recharge})
        }else if(linktype == '9'){ _personal
            Taro.navigateTo({url: _order})
        }else if(linktype == '10'){ 
            Taro.reLaunch({url: _personal})
        }else if(linktype == '11'){ 
            Taro.reLaunch({url: _shopcart})
        }else if(linktype == '12'){ // 积分列表
            Taro.reLaunch({url: _goodListMarketing})
        }else if(linktype == '13'){ 
            Taro.navigateTo({url: _commentList})
        }else if(linktype == '15'){ // 分店列表 
            Taro.navigateTo({url: _shopList})
        }else if(linktype == '17'){ 
            Taro.navigateTo({url: _wallet})
        }else if(linktype == '18'){ 
            if(env == 'weapp'){
                
            }else{
                Taro.navigateTo({url: _addressList})
            }
        }else if(linktype == '19'){ // 登陆
            Taro.reLaunch({url: _login + 'login'})  
        }else if(linktype == '20'){ // 注册
            Taro.reLaunch({url: _login + 'register'}) 
        }else if(linktype == '21'){  
            Taro.navigateTo({url: _goodsDatil})
        }else if(linktype == '22'){  
            Taro.navigateTo({url: _subscribePeopleNum})
        }else if(linktype == '23'){  // 预约桌台
            Taro.navigateTo({url: _subscribePeopleNum})
        }else if(linktype == '24'){  // 无桌台点餐
            Taro.navigateTo({url: _orderFood})
        }else if(linktype == '25'){  // 外卖
            Taro.navigateTo({url: _orderFood})
        }else if(linktype == '26'){  // 有桌点餐
            Taro.navigateTo({url: _orderFoodTable})
        }else if(linktype == '27'){  // 付款
            Taro.navigateTo({url: _payment})
        }
    }
    else if(modelId == '31' || modelId == '32' || modelId == '33'){ // 公共组件-栏目入口-商品栏目样式1-3
        Taro.navigateTo({url: _goodList})
    }

    // 页面
    else if(modelId == '0' || modelId == '14'){ // 搜索列表
        Taro.navigateTo({url: _searchList})
    }else if(modelId == '1'){ // banner
        if(linktype == 1){ // 商品详情
            Taro.navigateTo({url: _goodsDatil})
        }else if(linktype == 2){ // 商品列表
            Taro.navigateTo({url: _goodList})
        }
    }
}
