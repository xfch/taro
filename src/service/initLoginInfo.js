import Taro from '@tarojs/taro'
import base from '../static/js/base'
import common from './common'
import apiAlipay from './apiAlipay'
import { get as getGlobalData } from '../static/js/global_data'

export default {
    getInitInfo(that) {
        return new Promise((success, err) => {
            let getUserInfo = new Promise((s) => {
                const loginType = Taro.getStorageSync('loginType')
                const env = getGlobalData('JRE')
                if (!loginType) { //没有登陆
                    if (env == 'weapp') { //微信小程序
                        base.login(function (code) {
                            if (code) { //登陆过期
                                common.getOpenid(that, code, function (openid) {
                                    common.weappLogin(that, openid, (res) => {
                                        common.getUserInfo()
                                        // 获取diy配置
                                        s(res)
                                    })
                                })
                            } else { //登陆未过期
                                const openid = Taro.getStorageSync('openid')
                                common.weappLogin(that, openid, (res) => {
                                    common.getUserInfo()
                                    // 获取diy配置
                                    s(res)
                                })
                            }
                        })
                    } else if (env == 'alipay') { //支付宝登陆
                        s()
                    }
                } else { // 登陆
                    if (env == 'weapp') { //微信小程序
                        const openid = Taro.getStorageSync('openid')
                        common.weappLogin(that, openid, (res) => {
                            common.getUserInfo()
                            // 获取diy配置
                            s(res)
                        })
                    } else if (env == 'alipay') { //支付宝登陆
                        const userId = Taro.getStorageSync('openid')
                        common.weappLogin(that, userId, (res) => {
                            common.getUserInfo()
                            // 获取diy配置
                            s(res)
                        })
                    }
                }
            })
            let getShopInfo = new Promise((s) => {
                // 获取店铺信息
                common.getShopInfo(that, function (res) {
                    s(res)
                    Taro.setNavigationBarTitle({
                        title: res.model.c_name
                    })
                })
            })
            Promise.all([getUserInfo, getShopInfo]).then((res) => {
                success(res)
            }, (e) => {
                err(e)
            })
        })

    }
}
