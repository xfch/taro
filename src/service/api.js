import Taro, { Component } from '@tarojs/taro'
import { set as setGlobalData, get as getGlobalData } from '../static/js/global_data'
import md5 from '../static/js/MD5'
import base from '../static/js/base'
module.exports = {
  post: post,
  get: get,
  uploadImage: uploadImage,
  postAlipay: postAlipay,
  getAlipay: getAlipay,
}
function api(method, str, params, callback, errHandler, comBack) {
  return new Promise((proRes, proErr, proCom) => {
    const env = process.env.TARO_ENV;
    if (!params) {params = {};}
    if (!params.ShopNo) { params.ShopNo = getGlobalData('SHOP_NO')}
    var md5m = "";
    if (str) {
      let arr = str.split("&");
      let va = []
      for (let i = 0; i < arr.length; i++) {
        va.push(params[arr[i]])
      }
      md5m = arr.join('&') + '=' + va.join('*')
    } else {
      var arr = []
      var va = []
      for (var k in params) {
        if (k = "bNo") continue;
        arr.push(k);
        var v = params[k];
        va.push(v ? v : '');
      }
      md5m = arr.join("&") + "=" + va.join("*");
    }
    // console.log(str)
    // console.log(md5m)
    let md5s = md5.md5(md5m);
    params.MD5 = md5s.toUpperCase()
    if(!params.type){ params.type = env == 'weapp' ? '2' : '3'}
    Taro.request({
      url: getGlobalData("domain"),
      data: params,
      method: method,
      header: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'accept': 'application/json'
      },
      success: res => {
        // console.log('bNo-res: ' + params.bNo)
        console.log(res)
        if (typeof res.data == "string" && res.data != "") {
          res.data = res.data.replace("(", "");
          res.data = res.data.replace(")", "");
          res.data = JSON.parse(res.data)
        }
        if (res.data.code == '100' || res.data.code == '101' || res.data.code == 0) {
          if (callback) { callback(res.data); proRes(res.data) }
        } else {
          if (errHandler) { errHandler(res.data); proErr(res.data) }
        }

      },
      fail: err => {
        // console.log('bNo-err: ' + params.bNo)
        // console.log(err)
        if (err.errMsg == "request:fail timeout") {
          Taro.hideLoading()
          Taro.showToast({
            title: '请求超时！',
            icon: 'none'
            // image: "/images/warn.png"
          })
        }
      },
      complete: com => {
        if (comBack) { comBack(com)}
      }
    });
  })
}
function post(str, params, callback, errHandler, comBack) {
  if (!callback) { callback = () => { } }
  if (!errHandler) { errHandler = () => { } }
  if (!comBack) { comBack = () => { } }
  return api('POST', str, params, callback, errHandler, comBack)
}
function get(str, params, callback, errHandler, comBack) {
  if (!callback) { callback = () => { } }
  if (!errHandler) { errHandler = () => { } }
  if (!comBack) { comBack = () => { } }
  return api('GET', str, params, callback, errHandler, comBack)
}
function uploadImage(bNo, filePath, name, formData, callback) {
  console.log(getGlobalData("domain") + "?bNo=" + bNo)
  Taro.uploadFile({
    url: getGlobalData("domain") + "?bNo=" + bNo,
    filePath: filePath,
    name: name,
    formData: formData,
    header: {
      "Content-Type": "multipart/form-data"
    },
    success: res => {
    },
    fail: err => { },
    complete: com => {
      console.log(com)
      com.data = com.data.replace(/\(|\)/g, "");
      com.data = JSON.parse(com.data)
      console.log(com)
      if (com.data.code == '100') {
        if (callback) { callback(com.data) }
      } else {
        base.showToast(com.msg, 'none')
      }

    }
  })
}

// 支付宝
function apiAlipay(method, url, params, callback, errHandler, comBack) {
  return new Promise((proRes, proErr) => {
    if (!params) {params = {};}
    if (!params.ShopNo) { params.ShopNo = getGlobalData('SHOP_NO')}
    Taro.request({
      url: getGlobalData("domainAlipay") + url,//https://zgxcxssl.kantuzhuan.com/api/Alipay
      data: params,
      method: method,
      header: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Accept': 'application/json'
      },
      success: res => {
        console.log('apiAlipay-res')
        console.log(res)
        if (res.data.success) {
          if (callback) { callback(res.data); proRes(res.data) }
        } else {
          if (errHandler) { errHandler(res.data); proErr(res.data) }
        }
      },
      fail: err => {
        console.log('apiAlipay-err')
        console.log(err)
        if (err.errMsg == "request:fail timeout") {
          Taro.hideLoading()
          Taro.showToast({
            title: '请求超时！',
            icon: 'none'
          })
        }
      },
      complete: function (com) {
        if (comBack) { comBack(com)}
      }
    });
  })
}
function postAlipay(url, params, callback, errHandler, comBack) {
  if (!callback) { callback = () => { } }
  if (!errHandler) { errHandler = () => { } }
  if (!comBack) { comBack = () => { } }
  return apiAlipay('POST', url, params, callback, errHandler, comBack)
}
function getAlipay(params, url, callback, errHandler, comBack) {
  if (!callback) { callback = () => { } }
  if (!errHandler) { errHandler = () => { } }
  if (!comBack) { comBack = () => { } }
  return apiAlipay('GET', url, params, callback, errHandler, comBack)
}