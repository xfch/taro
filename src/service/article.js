import Taro from '@tarojs/taro'
import { set as setGlobalData, get as getGlobalData } from '../static/js/global_data'

import base from '../static/js/base'
import api from './api'

import parseHtml from 'mini-html-parser2'; //如果是微信小程序   需要注释这一行以及其使用的地方   不然会报错

module.exports = {
    getNewsList,
    getNewDetail,
}
// 获取文章列表
function getNewsList(that, callback,errback,comback) {
    const str = `pageindex&pagesize&sncategory&ShopNo`;
    let params = {
      bNo: "1100c",
      pageindex: that.state.pageindex,
      pagesize: that.state.pagesize,
      sncategory: that.state.sncategory,
      keyword: that.state.keyword || "",
      isNotice: that.state.isNotice || -1,
    }
    let articleLists = []
    api.get(str, params).then((res) => {
        if(!res.sys_notice){res.sys_notice = []}
        res.sys_notice.map((item) => {
            item.D_OP_DATE = item.D_OP_DATE.replace('T',' ')
        })
        articleLists = that.state.pageindex == 1 ? res.sys_notice : that.state.articleList.concat(res.sys_notice)
        let hasEd = that.state.pagesize > res.sys_notice.length ? true : false
        if(callback){callback(articleLists,hasEd)}
    })
}
// 获取文章详情
function getNewDetail(that,callback,errback,comback){
    const str = `ntid`;
    const params = {
      bNo: 1102,
      ntid: that.state.ntid,
    };
    api.get(str, params,(res) => {
        res.sys_notice.D_OP_DATE = res.sys_notice.D_OP_DATE.replace('T',' ')
        let html = res.sys_notice.C_SN_CONTENT.replace(/\<img/gi, '<img style="max-width:100%;height:auto" ');
        if (process.env.TARO_ENV == 'weapp') {
            if(callback){callback(res,html)}
        } else if (process.env.TARO_ENV == 'alipay') {
            parseHtml(html, (err, nodes) => {
                if (!err) {
                    if(callback){callback(res,nodes)}
                }
            }) 
        }
    },(err) => {
        if(errback){errback(err)}
    },() => {
        if(comback){comback()}
    })
}