import Taro from '@tarojs/taro'
import { set as setGlobalData, get as getGlobalData } from '../static/js/global_data'
import api from './api'
module.exports = {
    getCouponListAll,
    getCouponUse,
    getCouponList,
    getCoupon,
    getCouponNum
}
// 获取店铺的全部
function getCouponListAll(param,callback,errback){
    let userInfo = Taro.getStorageSync('userInfo')
    const str = `mebno&gids&state&istask&ShopNo`
    let params = {
        bNo: 603,
        mebno: userInfo ? userInfo.C_MB_NO : '',
        gids: param&&param.gids ? param.gids : '', //	一个或多个商品id”,”隔开；数据类型：string
        state: param&&param.state ? param.state : 0, //0、所有可用领取的优惠券 1、全场可以领用的优惠券  2、单个商品和全场的优惠券领取 3单个商品可以领取的优惠券
        istask: param&&param.istask ? param.istask : 0 , // 是否来源于任务 1是 2否 所有0
    }
    api.get(str,params).then((res) => {
        if(callback){callback(res)}
    }).catch((err) => {
        if(errback){errback(err)}
    })
}
// 结算时可用优惠券
function getCouponUse(param,callback,errback){
    let userInfo = Taro.getStorageSync('userInfo')
    const str = `mebno&gids&OrderPrice&csCids&ShopNo`
    let params = {
        bNo: 602,
        mebno: userInfo.C_MB_NO,
        gids: param&&param.gids ? param.gids : '', //	一个或多个商品id”,”隔开；数据类型：string
        OrderPrice: param&&param.OrderPrice ? param.OrderPrice : '', // 结算金额
        csCids: param&&param.csCids ? param.csCids : '', //已选择的活动编号
    }
    api.get(str,params).then((res) => {
        if(callback){callback(res)}
    }).catch((err) => {
        console.log(err)
        if(errback){errback(err)}
    })
}
// 我的优惠券列表
function getCouponList(param,callback,errback){
    let userInfo = Taro.getStorageSync('userInfo')
    const str = `mebno&state&ShopNo`
    let params = {
        bNo: 600,
        mebno: userInfo ? userInfo.C_MB_NO : '',
        state: param&&param.state ? param.state : 1, // 0全部，1未过期
        Scope: param&&param.Scope ? param.Scope : 0, // 0是全场 1是单个商品；数据类型：int
    }
    api.get(str,params).then((res) => {
        if(callback){callback(res)}
    }).catch((err) => {
        if(errback){errback(err)}
    })
}
// 领取优惠券
function getCoupon(param,callback,errback){
    let userInfo = Taro.getStorageSync('userInfo')
    const str =`mebno&mid&mname&source&ShopNo`
    const params = {
        bNo: 601,
        mebno: userInfo ? userInfo.C_MB_NO : '',
        mid: param.mid, //优惠券id,多个请用“,”
        mname: param.mname, //优惠券名称
        source: param&&param.source ? param.source : 1,//来源状态 1自己领取 2店铺发送；数据类型：int
    }
    api.get(str,params).then((res) => {
        if(callback){callback(res)}
    }).catch((err) => {
        if(errback){errback(err)}
    })
}
// 获取优惠券数量
function getCouponNum(param,callback,errback){
    let userInfo = Taro.getStorageSync('userInfo')
    const str = `mebno&state&ShopNo`
    let params = {
        bNo: 604,
        mebno: userInfo ? userInfo.C_MB_NO : '',
        state: param&&param.state ? param.state : 1, //0全部，1未过期
    }
    api.get(str,params).then((res) => {
        if(callback){callback(res)}
    }).catch((err) => {
        if(errback){errback(err)}
    })
}
