import Taro from '@tarojs/taro'
import { set as setGlobalData, get as getGlobalData } from '../static/js/global_data'
import api from './api'

module.exports = {
    getCategory,
    getGoodscate,
    changeClassify,
    getGoodList,
    getGoodDetail,
    getGoodDetailLvgg,
}
// 获取商品分类 - 有二级分类
function getCategory(callback) {
    const str = `ShopNo`
    let params = {
        bNo: 704,
    }
    api.get(str, params).then((res) => {
        if (callback) { callback(res.goodscate) }
    })
}
// 获取商品分类 - 无二级分类
function getGoodscate(param,callback) {
    const str = `ShopNo`
    let params = {
        bNo: 705,
        pid: param&&param.pid ? param.pid : 0, // 根据上级id查找下级分类(导航分类时pid直接传0)
    }
    api.get(str, params).then((res) => {
        if(callback){callback(res.goodscate)}
    })
}
// 切换商品分类
function changeClassify(cid){
    console.log('changeClassify----------')
    myEvent.emit("changeTab",({cid: cid}))
}
// 获取商品列表
function getGoodList(param, callback, errback) {
    const userInfo = Taro.getStorageSync('userInfo')
    const str = `pageindex&pagesize&cid&name&ShopNo`
    let params = {
        bNo: '702b',
        pageindex: param&&param.pageindex ? param.pageindex : 1,
        pagesize: param&&param.pagesize ? param.pagesize : getGlobalData('pagesize'),
        cid: param&&param.cid ? param.cid : '',
        name: param&&param.name ? param.name : '',
        cname: param&&param.cname ? param.cname : '',
        sort: param&&param.sort ? param.sort : "t.ID desc",
        desc: param&&param.desc ? param.desc : "",
        iscollect: param&&param.iscollect ? param.iscollect : '-1',
        mebno: userInfo ? userInfo.C_MB_NO : ''
    }

    api.get(str, params).then((res) => {
        if(!res.goodsList){res.goodsList = []}
        res.goodsList.map((item) => {
            item.activejson = JSON.parse(item.activejson)
            item.activejsonArr = item.activejson.map((_item) => {
                if(!_item.IsFullSale){
                    return _item
                }
            })
            item.activejsonObj = item.activejsonArr.length > 0 ? item.activejsonArr[0] : null
            delete item.activejsonArr
        })
        let hasEd = res.goodsList.length < getGlobalData('pagesize') ? true : false
        if (callback) { callback(res.goodsList, hasEd) }
    }).catch((err) => {
        console.log(err)
        base.showModal(err.msg, 'none')
        if(errback){errback()}
    })
}
// 获取商品详情
function getGoodDetail(param, callback) {
    let userInfo = Taro.getStorageSync('userInfo')
    let str = `gid&ShopNo`
    let params = {
        bNo: 701,
        gid: param.gid,
        isGoodsImg: param&&param.isGoodsImg ? param.isGoodsImg : '1',
        isgroupshop: param&&param.isgroupshop ? param.isgroupshop : '0',
        mebno: userInfo ? userInfo.C_MB_NO : ''
    }
    api.get(str, params).then((res) => {
        _goodDetailData(res,callback)
    })
}
// 切换商品详情规格
function getGoodDetailLvgg(param, callback) {
    let userInfo = Taro.getStorageSync('userInfo')
    let str = `Spv1&GbId&ShopNo`
    let params = {
        bNo: '701b',
        GbId: param.GbId, //goodsInfo.N_GB_ID
        isGoodsImg: "1",
        Spv1: param&&param.Spv1 ? param.Spv1 : '0',
        Spv2: param&&param.Spv2 ? param.Spv2 : '0',
        Spv3: param&&param.Spv3 ? param.Spv3 : '0',
        isgroupshop: param&&param.isgroupshop ? param.isgroupshop : '0',
        mebno: userInfo ? userInfo.C_MB_NO : ''
    }
    api.get(str, params).then((res) => {
        _goodDetailData(res,callback)
    })
}
// 获取商品详情、切换规格后的数据处理
function _goodDetailData(res,callback){
    let goodInfo = {}
    if(res.lvgg){ // 规格
        res.goods[0].C_SPID = res.goods[0].C_SPID.split("_") // 默认的规格
        res.lvgg.map((items,i) => {
            items.SpvList.map((item,j) => {
                item.select = false
                if(res.goods[0].C_SPID[i] == item.N_SPV_ID){
                    item.select = true
                }
            })
        })
    }
    let salemodels = [] // 
    if (res.salemodel) { //是否有活动
        res.salemodel = JSON.parse(res.salemodel)
        res.salemodel.map((item) => {
            if(item.IsFullSale === false){
                salemodels.push(item)
            }
        })
    }
    goodInfo = res.goods[0]
    res.goods[0].num = 1
    goodInfo.GoodsImgs = res.GoodsImgs 
    goodInfo.activejsonObj = salemodels.length > 0 ? salemodels[0] : null
    goodInfo.goodsgrupshop = res.goodsgrupshop  //商品可以使用的营销活动集合
    goodInfo.lptcdt = res.lptcdt  //参与套餐商品的集合
    goodInfo.lvgg = res.lvgg // 规格
    if(callback)(callback(goodInfo))
}
