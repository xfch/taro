import Taro from '@tarojs/taro'
import { set as setGlobalData, get as getGlobalData } from '../static/js/global_data'
import base from '../static/js/base'
import api from './api'
import common from './common'

module.exports = {
    getUserId: getUserId,
}
// 获取支付宝userId
function getUserId(authCode, callback) {
    let params = {
        authcode: authCode,
        shopno: getGlobalData('SHOP_NO')
    }
    api.getAlipay(params, '/UserInfo', (res) => {
        console.log(res)
        if (callback) { callback(res.data.userId) }
    },(err) => {
        console.log(err)
        Taro.hideLoading()
        Taro.showToast({
            title: err.msg,
            icon: ''
        })
    })
}