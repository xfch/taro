import Taro from "@tarojs/taro";
//验证网络请求状态
import GET_HTTP_STATUS from './HTTP_STATUS'
const errModal = (err_msg) => {
  // 错误信息
  return {
    data: {
      msg: err_msg
    }
  }
}
function myfetch(option) {
  return new Promise((resolve, reject) => {
    Taro.request(Object.assign({
      success(res) {
        const HTTP_STATUS = GET_HTTP_STATUS(res.statusCode)
        if (HTTP_STATUS.state) {
          resolve(res)
        } else {
          reject(errModal(HTTP_STATUS.msg))
        }
      },
      fail(err) {
        if (err.errMsg) {
          reject(errModal(err.errMsg))
        } else {
          reject(errModal('网络请求失败,请稍后再试!!!'))
        }
      }
    }, option))
  })
}

export default myfetch