

import { md5 } from './MD5'


const changeMD5 = (data) => {
  if (data.MD5) {
    //指定MD5的转换
    let arr = data.MD5.split('&')
    let newData: string[] = []
    arr.map((val) => {
      newData.push(data[val])
    })
    data['MD5'] = md5(arr.join('&') + '=' + newData.join('*')).toUpperCase()
  } else {
    //未指定MD5的转换
    let a: string[] = [], b: string[] = []
    for (let key in data) {
      if (key != 'bNo') {
        a.push(key)
        b.push(data[key])
      }
    }
    data['MD5'] = md5(a.join('&') + '=' + b.join('*')).toUpperCase()
  }
  return data
}

export default changeMD5