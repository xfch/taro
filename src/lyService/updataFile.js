const HTTP_STATUS = {
  SUCCESS: 200,
  CLIENT_ERROR: 400,
  AUTHENTICATE: 401,
  FORBIDDEN: 403,
  NOT_FOUND: 404,
  SERVER_ERROR: 500,
  BAD_GATEWAY: 502,
  SERVICE_UNAVAILABLE: 503,
  GATEWAY_TIMEOUT: 504
}
function isJsonStr(str) {
  //判断是否JSON字符串
  try {
    if (typeof JSON.parse(str) === "object") {
      return true;
    }
  } catch (e) { }
  return false
}
function uploadImg(imglo, c_shop_no, url) {
  return new Promise((s, r) => {
    wx.uploadFile({
      url: `${url}?bNo=1907`,//上传的服务器地址
      filePath: imglo,
      name: 'file',
      formData: { ShopNo: c_shop_no },
      header: { "Content-Type": "multipart/form-data" },
      success(_updata) {
        if (_updata.statusCode === HTTP_STATUS.NOT_FOUND) {
          r()
          wx.showToast({ title: '请求资源不存在', icon: 'none' })
        } else if (_updata.statusCode === HTTP_STATUS.BAD_GATEWAY) {
          r()
          wx.showToast({ title: '服务端出现了问题', icon: 'none' })
        } else if (_updata.statusCode === HTTP_STATUS.FORBIDDEN) {
          r()
          wx.showToast({ title: '没有权限访问', icon: 'none' })
        } else if (_updata.statusCode === HTTP_STATUS.SUCCESS) {
          if (typeof _updata.data === 'string') {
            let str = _updata.data.replace('(', '').replace(')', '')
            if (isJsonStr(str)) {
              let json = JSON.parse(str)
              if (Number(json.code) === 100) {
                if (json.data !== null) {
                  s(json)
                } else {
                  r()
                  wx.showToast({ title: json.msg||'未知错误', icon: 'none' })
                }
              } else {
                r()
                wx.showToast({ title: json.msg || '未知错误', icon: 'none' })
              }
            } else {
              r()
              wx.showToast({ title: _updata.msg || '未知错误', icon: 'none' })
            }
          } else {
            r()
            wx.showToast({ title: _updata.msg || '未知错误', icon: 'none' })
          }
        }
      },
      fail(err) {
        r(err)
      },
    })
  })
}


export default uploadImg
