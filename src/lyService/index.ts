import myfetch from './myfetch'
import upImg from './updataFile'
import { getGhCode, getSsCode } from './getCode'
import { get as getGlobalData } from '../static/js/global_data'



function uploadImg(imglo, c_shop_no) {
    return upImg(imglo, c_shop_no, getGlobalData("domain"))
}

function get(data) {
    return myfetch({ url: getGlobalData("domain"), data, method: 'GET', CODE: getGhCode(data.bNo) })
}
function post(data) {
    return myfetch({ url: getGlobalData("domain"), data, method: 'POST', CODE: getGhCode(data.bNo) })
}
function getS(data) {
    return myfetch({ url: getGlobalData("domain"), data, method: 'GET', CODE: getSsCode(data.bNo) })
}
function postS(data) {
    return myfetch({ url: getGlobalData("domain"), data, method: 'POST', CODE: getSsCode(data.bNo) })
}
export default { get, post, getS, postS, uploadImg }