function GET_HTTP_STATUS(RES_CODE = 0) {
    switch (Number(RES_CODE)) {
        case 200:
            return { state: true, msg: `成功 code:${RES_CODE}` }
            break;
        case 201:
            return { state: true, msg: `已创建 code:${RES_CODE}` }
            break;
        case 202:
            return { state: false, msg: `已接受，但尚未处理 code:${RES_CODE}` }
            break;
        case 203:
            return { state: false, msg: `非授权信息 code:${RES_CODE}` }
            break;
        case 204:
            return { state: false, msg: `无内容 code:${RES_CODE}` }
            break;
        case 205:
            return { state: false, msg: `重置内容 code:${RES_CODE}` }
            break;
        case 206:
            return { state: false, msg: `部分内容 code:${RES_CODE}` }
            break;


        case 300:
            return { state: false, msg: `多种选择 code:${RES_CODE}` }
            break;
        case 301:
            return { state: false, msg: `永久移动 code:${RES_CODE}` }
            break;
        case 302:
            return { state: false, msg: `临时移动 code:${RES_CODE}` }
            break;
        case 303:
            return { state: false, msg: `查看其他位置 code:${RES_CODE}` }
            break;
        case 304:
            return { state: false, msg: `未修改 code:${RES_CODE}` }
            break;
        case 305:
            return { state: false, msg: `使用代理 code:${RES_CODE}` }
            break;
        case 307:
            return { state: false, msg: `临时重定向 code:${RES_CODE}` }
            break;


        case 400:
            return { state: false, msg: `错误请求 code:${RES_CODE}` }
            break;
        case 401:
            return { state: false, msg: `未授权 code:${RES_CODE}` }
            break;
        case 403:
            return { state: false, msg: `拒绝请求 code:${RES_CODE}` }
            break;
        case 404:
            return { state: false, msg: `找不到请求 code:${RES_CODE}` }
            break;
        case 405:
            return { state: false, msg: `禁用请求中指定的方法 code:${RES_CODE}` }
            break;
        case 406:
            return { state: false, msg: `无法使用请求的内容特性响应 code:${RES_CODE}` }
            break;
        case 407:
            return { state: false, msg: `需要代理授权 code:${RES_CODE}` }
            break;
        case 408:
            return { state: false, msg: `请求超时 code:${RES_CODE}` }
            break;
        case 409:
            return { state: false, msg: `服务器在完成请求时发生冲突 code:${RES_CODE}` }
            break;
        case 410:
            return { state: false, msg: `资源已永久删除 code:${RES_CODE}` }
            break;
        case 411:
            return { state: false, msg: `需要有效长度 code:${RES_CODE}` }
            break;
        case 412:
            return { state: false, msg: `未满足前提条件 code:${RES_CODE}` }
            break;
        case 413:
            return { state: false, msg: `请求实体过大 code:${RES_CODE}` }
            break;
        case 414:
            return { state: false, msg: `请求的 URI 过长 code:${RES_CODE}` }
            break;
        case 415:
            return { state: false, msg: `不支持的媒体类型 code:${RES_CODE}` }
            break;
        case 416:
            return { state: false, msg: `请求范围不符合要求 code:${RES_CODE}` }
            break;
        case 417:
            return { state: false, msg: `未满足期望值 code:${RES_CODE}` }
            break;



        case 500:
            return { state: false, msg: `服务器内部错误 code:${RES_CODE}` }
            break;
        case 501:
            return { state: false, msg: `尚未实施 code:${RES_CODE}` }
            break;
        case 502:
            return { state: false, msg: `错误网关 code:${RES_CODE}` }
            break;
        case 503:
            return { state: false, msg: `服务不可用 code:${RES_CODE}` }
            break;
        case 504:
            return { state: false, msg: `网关超时 code:${RES_CODE}` }
            break;
        case 505:
            return { state: false, msg: `HTTP 版本不受支持 code:${RES_CODE}` }
            break;

        default:
            return { state: false, msg: `HTTP请求状态码 code:${RES_CODE}` }
            break;
    }
}



export default GET_HTTP_STATUS