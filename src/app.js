import Taro, { Component } from '@tarojs/taro'
import Index from './pages/index'

import './app.scss'

let edition = '0.0.18'

// 如果需要在 h5 环境中开启 React Devtools
// 取消以下注释：
// if (process.env.NODE_ENV !== 'production' && process.env.TARO_ENV === 'h5')  {
//   require('nerv-devtools')
// }

import { set as setGlobalData, get as getGlobalData } from './static/js/global_data'
// import './static/js/sensor'//引入神策初始化文件

// Taro.eventCenter.trigger('sensorInit')

//sensors.init();
 
class App extends Component {

    config = {
        pages: [
            
            'pages/index/index', 
            'pages/login/login',
            'pages/shopcart/shopcart',
            'pages/orderIndex/orderIndex',
            'pages/personal/personal',
            'pages/editMember/editMember',
            'pages/category/category',
            'pages/orderDetail/orderDetail',
            'pages/orderSubmitMarketing/index',
            'pages/sendMethod/sendMethod',
            'pages/goodsDatil/index',
            'pages/payResult/payResult',
            'pages/order/order',
            'pages/couponPage/index',
            'pages/evaluate/evaluate',
            'pages/afterSale/afterSale',
            'pages/commentList/index',
            'pages/addressEdit/addressEdit',
            'pages/addressList/addressList',
            'pages/viewCoupon/viewCoupon',
            'pages/rechargeHistory/rechargeHistory',
            'pages/recharge/recharge',
            'pages/wallet/wallet',
            'pages/scoreHistory/scoreHistory',
            'pages/collect/collect',
            'pages/editPass/editPass',
            'pages/shopCard/shopCard',
            'pages/addCard/addCard',
            'pages/exchangeOrderList/index',
            'pages/goodList/index',
            'pages/goodListMarketing/index',
            'pages/goodsDatilExchange/index',
            'pages/searchList/index',
            'pages/articleList/index',
            'pages/articleDetail/index',
            'pages/shopInfo/index',
            'pages/shopList/index',
            'pages/subscribePeopleNum/index',
            'pages/goodCart/index',
            'pages/subscribeDate/index',
            'pages/subscribeSubmit/index',
            'pages/subscribeList/index',
            'pages/subscribeProject/index',
            'pages/payment/index',
            'pages/orderFood/index',
            'pages/orderFoodTable/index'
            
        ],
        window: {
            backgroundTextStyle: 'dark',
            navigationBarBackgroundColor: '#000000',
            navigationBarTitleText: 'WeChat',
            navigationBarTextStyle: 'white'
        },
        "networkTimeout": {
            "request": 50000,
            // "downloadFile": 10000
        },
    }

    componentDidMount() {
        const userInfo = Taro.getStorageSync('userInfo')
        if(userInfo){
            setGlobalData('userInfo', userInfo)
        }
        setGlobalData("pagesize", '10') //全局页码条数
        setGlobalData('isLoginState',true) //登陆弹窗是否显示，否
        setGlobalData('edition',edition) //当前版本
    }

    setJRE() {
        //获取并且设置本小程序的运行环境
        const evn = process.env.TARO_ENV;
        setGlobalData('JRE', evn);
    }
    // 在 App 类中的 render() 函数没有实际作用
    // 请勿修改此函数
    render() {
        return (
            <Index />
        )
    }
}

Taro.render(<App />, document.getElementById('app'))
