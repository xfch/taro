
import Taro, { Component } from '@tarojs/taro'
import { View, Block } from '@tarojs/components'

import './index.scss'


class Index extends Component {
	static options = {
		addGlobalClass: true
	}
	static defaultProps = {
		item: {},
	}
	constructor() {
		super(...arguments)
	}
	state = {
	};
	render() {
		const { item } = this.props;
		return (
			<Block>
				{item.activejsonObj ? <Block>
					{/* 积分 */}
					{item.activejsonObj.SaleTypeNo == 5 && <View>￥{item.activejsonObj.saledata.ReallSalePrice}<Text className='text-small text-gray' hidden={item.activejsonObj.saledata.N_IntegralCount == 0}> + {item.activejsonObj.saledata.N_IntegralCount} 积分</Text></View>}
					{/* 秒杀 */}
					{item.activejsonObj.SaleTypeNo == 2 && <View>￥{item.activejsonObj.saledata.SalePrice}<Text className='text-small text-gray pl10 line-through'>￥{item.SALE_PRICE}</Text></View>}
					{/* 满减 */}
					{item.activejsonObj.SaleTypeNo == 3 && <View>￥{item.SALE_PRICE}</View>}
				</Block>
				:
				<Text>￥{item.SALE_PRICE}</Text>
				}
			</Block>
		)
	}
}
export default Index;
