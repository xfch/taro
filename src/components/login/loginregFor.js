
import Taro, { Component } from '@tarojs/taro'
import { View, Text, Button } from '@tarojs/components'

import './loginregFor.scss'

class LoginregFor extends Component {
	static defaultProps = {
		addGlobalClass: true, //全局样式
	}
	constructor() {
		super(...arguments)
		this.state = {};
	}
	componentDidMount() {
	}

	navigator(id,pages) {
		Taro.navigateTo({
			url: `/pages/${id}/${id}?pages=${pages}`
		})
    }

	render() {
		return (
			<View className='loginregFor'>
				<View className='weui-flex account-link'>
					<View className='weui-flex__item' onClick={this.navigator.bind(this, 'login','register')}>注册新用户</View>
					<View className='weui-flex__item ar' onClick={this.navigator.bind(this, 'login', 'forgetPwd')}>忘记密码？</View>
				</View>
			</View>
		)
	}
}
export default LoginregFor;