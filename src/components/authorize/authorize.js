
import Taro, { Component } from '@tarojs/taro'
import { View, Text, Button, Block } from '@tarojs/components'

import { set as setGlobalData, get as getGlobalData } from '../../static/js/global_data'

import './authorize.scss'

import common from '../../service/common'
import apiAlipay from '../../service/apiAlipay'
import base from '../../static/js/base'

// 授权登陆弹窗
class authorize extends Component {
	static options = {
		addGlobalClass: true
	}
	static defaultProps = {
		page: '', //是否是个人中心页面，如果是个人中心页面，许提示绑定手机号//personal
		onSetUserInfo: () => { }
	}
	constructor() {
		super(...arguments)
		this.state = {
			isLoginState: false, //不显示
			alipayAuthorize: false, // 支付宝
		};
	}
	componentDidMount() {
		this.props.onSetUserInfo(this)
		let env = process.env.TARO_ENV;
		this.setState({
			env,
			page: this.props.page, 
		})
	}
	isText(){
		console.log('isText')
	}
	// 支付宝判断是否登陆
	isLogin(callback) {
		let env = process.env.TARO_ENV;
		if (env == 'alipay') { //如果是支付宝，没有登陆先登陆
			const loginType = Taro.getStorageSync('loginType')
			if (!loginType) {
				this.setState({ alipayAuthorize: true})
				return
			}else{
				if(callback){callback()}
			}
		}else{
			if(callback){callback()}
		}
	}

	componentDidShow() {
		const loginType = Taro.getStorageSync('loginType')
		this.state.isLoginState = loginType ? false : getGlobalData('isLoginState')
		this.setState({
			isLoginState: this.state.isLoginState,
		})
	}

	// 微信信息
	onGetUserInfo(id, e) {
		if (e) {
			this.closeAuthorize(id, e)
		}
	}
	// 支付宝登陆 
	getUserInfo(id) {
		this.closeAuthorize(id)
	}

	closeAuthorize(id, e) {
		console.log('closeAuthorize')
		console.log(e)
		const that = this;
		if (this.state.env == 'weapp') {
			if (e.detail) {
				const userInfo = Taro.getStorageSync('userInfo')
				if (e.detail.userInfo) {//允许
					const detail = e.detail.userInfo
					if (id == 'weapp') { //微信登陆
						common.weappLoginModify(userInfo, detail)
						Taro.setStorageSync('loginType', 'weapp')
						this.setState({ isLoginState: false })
						setGlobalData('isLoginState', false)
					} else if (id == 'account') {//账号登陆
						Taro.reLaunch({
							url: '/pages/login/login?pages=login&detail=' + JSON.stringify(detail)
						})
					}
				} else {//拒绝授权
					if (id == 'weapp') { //微信登陆
						Taro.setStorageSync('loginType', 'weapp')
						this.setState({ isLoginState: false })
						setGlobalData('isLoginState', false)
					} else if (id == 'account') {//账号登陆
						Taro.reLaunch({
							url: '/pages/login/login?pages=login'
						})
					}
				}

			}
		} else if (this.state.env == 'alipay') { //支付宝登陆
			const userInfo = Taro.getStorageSync('userInfo')
			if (id == 'alipay') { //支付宝登陆
				base.login((authCode) => { //点击确定
					if (!authCode) { return }
					Taro.showLoading({ title: '登陆中....' })
					apiAlipay.getUserId(authCode, (userId) => {
						Taro.setStorageSync('openid', userId)
						Taro.setStorageSync('loginType', 'alipay')
						that.setState({ isLoginState: false })
						setGlobalData('isLoginState', false)
						common.weappLogin(userId, (res) => {
							base.setSensorsCommonParse()
							const authorizeUserInfo = getGlobalData('authorizeUserInfo')
							Taro.hideLoading()
							if (this.state.page == 'personal' || this.state.page == 'shopcart' || this.state.page == 'onSetUserInfo') {
								common.weappLoginModify(res, authorizeUserInfo, () => {
									common.getUserInfo(this, (data) => {
										base.showToast('登陆成功')
										this.props.onSetUserInfo(data)
									})
								})
							} else {
								common.weappLoginModify(res, authorizeUserInfo,() => {
									base.showToast('登陆成功')
								})
							}
						})
					}, (err) => {
						console.log(err)
					})
				})
			} else if (id == 'account') { //账号登陆
				Taro.reLaunch({
					url: '/pages/login/login?pages=login&detail=' + JSON.stringify(userInfo)
				})
			}
		}

	}

	render() {
		// // authorize获取作用域
		// getIsLogin(obj){
		// 	console.log(obj)
		// 	if(obj){
		// 		if(obj.C_MB_NO){ 
		// 			console.log('登陆后的操作') 
		// 		}else{
		// 			this.isLogin = obj
		// 		}
		// 	}	
		// }
		// 支付宝判断是否登陆
		// this.isLogin.isLogin(() => {
			//  console.log('登陆后的操作') 
        // })
		// <Authorize page='onSetUserInfo' onSetUserInfo={this.getIsLogin.bind(this)}></Authorize>
		let { env, isLoginState, alipayAuthorize} = this.state
		let authorizeState = env == 'weapp' ? true : alipayAuthorize
		// let authorizeState = true
		return (
			<Block>
				{authorizeState && <Block>
					<View className={`authorize ${isLoginState ? 'popup-show' : ''}`}>
						<View className='mask'></View>
						<View className='popup-content popup-content-center'>
							<View className='weui-cell'>
								<View className='weui-cell__bd title'>欢迎光临</View>
								<View className='weui-cell__ft'>
									{/* <Text className='iconfont icon-close' onClick={this.changeAuthorize}></Text> */}
								</View>
							</View>
							{env == 'weapp' &&
								<Block>
									<View className='weui-cell weui-cell-left'>
										<View className='weui-cell__bd pt20'>
											<Button open-type="getUserInfo" lang="zh_CN" onGetUserInfo={this.onGetUserInfo.bind(this, 'weapp')} class="weui-btn" type="primary">微信登陆</Button>
										</View>
									</View>
									<View className='weui-cell weui-cell-none'>
										<View className='weui-cell__bd'>
											<Button open-type="getUserInfo" lang="zh_CN" onGetUserInfo={this.onGetUserInfo.bind(this, 'account')}>手机号/账号登录</Button>
										</View>
									</View>
								</Block>
							}
							{env == 'alipay' &&
								<Block>
									<View className='weui-cell weui-cell-left'>
										<View className='weui-cell__bd pt20'>
											<Button class="weui-btn" type="primary" onClick={this.getUserInfo.bind(this, 'alipay')}>支付宝登陆</Button>
										</View>
									</View>
									<View className='weui-cell weui-cell-none'>
										<View className='weui-cell__bd'>
											<Button onClick={this.getUserInfo.bind(this, 'account')}>手机号/账号登录</Button>
										</View>
									</View>
								</Block>
							}
						</View>
					</View>
				</Block>}
			</Block>
		)
	}
}
export default authorize;