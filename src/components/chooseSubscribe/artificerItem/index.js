
import Taro, { Component } from '@tarojs/taro'
import { View, Button} from '@tarojs/components'
import './index.scss'

import { set as setGlobalData, get as getGlobalData } from '../../../static/js/global_data'

import SexIcon from '../../sexIcon'

// 预约技师
class Index extends Component {
	static options = {
		addGlobalClass: true
	}
	static defaultProps = {
		item: {}
	}
	constructor() {
		super(...arguments)
	}
	state = {
		domainImg: getGlobalData('domainImg')
	}
	// 跳转
	comNavTo(){
		const {item} = this.props;
		Taro.navigateTo({
			url: `/pages/subscribeDate/index?artificerItem=${JSON.stringify(item)}`
		})
	}

	render() {
		const {domainImg} = this.state;
		const {item} = this.props;
		return (
			<View className='list-item ac' onClick={this.comNavTo}>
				{item.c_headimg&&item.c_headimg != '' ? <Image className='item-img' src={domainImg + item.c_headimg}></Image> : <View className='item-img bg-img'></View>}
				<View className='sexIcon'><SexIcon sex={item.c_sex}></SexIcon></View>
				<View className='ptb10'>{item.c_name}</View>
			</View>
		)
	}
}
export default Index;