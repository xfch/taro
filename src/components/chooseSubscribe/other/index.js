
import Taro, { Component } from '@tarojs/taro'
import { View, Button} from '@tarojs/components'

import './index.scss'

import common from '../../../service/common'
import base from '../../../static/js/base'

let numArr = ['0','×']
for(let i = 9;i > 0;i--){
	numArr.unshift(i.toString())
}

class Index extends Component {
	static options = {
		addGlobalClass: true
	}
	static defaultProps = {
		peopleNum: '0',
	}
	constructor() {
		super(...arguments)
	}
	state = {
		numArr: numArr,
	}
	componentWillMount(){
		this.setState({
			peopleNum: this.props.peopleNum
		})
	}
	// 点击键盘
	selectNum(num){
		let peopleNum = '0'
		if(num == '×'){
			peopleNum = this.state.peopleNum.length > 1 ? this.state.peopleNum.substr(0, this.state.peopleNum.length - 1) : '0'
		}else if(num == '0'){
			if(this.state.peopleNum == "0"){
				return
			}
			peopleNum = this.state.peopleNum + num
		}else{
			peopleNum = this.state.peopleNum == '0' ? num : this.state.peopleNum + num
		}
		if(Number(peopleNum) > 99){
			base.showModal('人数最多99人？')
			return
		}else{
			this.setState({peopleNum})
		}
	}
	// 点击确定
	submit(){
		let {n_yjhid} = this.props
		if(this.state.peopleNum == 0){
			base.showModal('请先选择人数！')
			return
		}
		if(this.props.peopleNum > 0){
			base.prevPage().setState({
				peopleNum: this.state.peopleNum
			})
			Taro.navigateBack()
		}else{
			Taro.navigateTo({
				url: `/pages/goodCart/index?peopleNum=${this.state.peopleNum}&n_yjhid=${n_yjhid}`
			})
		}
	}
	// 跳转
	comNavTo(modelId,linktype,linkparam){
		common.comNavTo(modelId,linktype,linkparam)
	}

	render() {
		const {peopleNum,numArr} = this.state;
		return (
			<View className='main-other ac'>
				<View className='text-large'>客官您几位？</View>
				<View className='other-total'><Text className='num'>{peopleNum}</Text>人</View>
				<View className='weui-flex num-flex'>
					{numArr.map((item,i) => (
						<Block key={'num' + i}>
							<View className={`num-flex__item ${item == '0' ? 'num-flex__item2' : ''}`} onClick={this.selectNum.bind(this,item)}>
								<View className='item'>{item}</View>
							</View>
						</Block>
					))}
				</View>
				<View className='other-btn'>
					<Button className='btn' onClick={this.submit}>确定</Button>
				</View>
			</View>
		)
	}
}
export default Index;