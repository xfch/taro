
import Taro, { Component } from '@tarojs/taro'
import { View, Button} from '@tarojs/components'

import './index.scss'

import { set as setGlobalData, get as getGlobalData } from '../../../static/js/global_data'
import NoData from '../../../components/noData'
import ArtificerItem from '../artificerItem'

import api from '../../../service/api'
import common from '../../../service/common'
import base from '../../../static/js/base'

// 预约技师
class Index extends Component {
	static options = {
		addGlobalClass: true
	}
	static defaultProps = {
		
	}
	constructor() {
		super(...arguments)
	}
	state = {
		industry: getGlobalData('industry'),
		ApptDate: '',
		hasView: false,
	}
	componentWillMount(){
		this.getArtificerList()
	}
	// 获取服务员/技师列表
	getArtificerList(){
		const str = `ApptDate&ShopNo`
		const params = {
			bNo: 5001,
      		ApptDate: this.state.ApptDate,
		}
		api.get(str,params).then((res) => {
			if(!res.staffdt){res.staffdt = []}
			this.setState({
				artificerList: res.staffdt,
				hasView: true
			})
		}).catch((err) => {
			console.log(err)
			base.showModal(err.msg)
		})
	}
	// 跳转
	comNavTo(){
		Taro.navigateTo({
			url: `/pages/subscribeDate/index`
		})
	}

	render() {
		const {hasView,artificerList} = this.state;
		return (
			<View className='main-artificerList weui-flex'>
				{hasView && <Block>
					{artificerList.map((item,i) => (
						<ArtificerItem key={'list' + i} item={item}></ArtificerItem>
					))}
					{artificerList.length == 0 && <NoData></NoData>}
				</Block>}
				<View className='footer'>
					<Button className='btn-main' onClick={this.comNavTo}>暂不选择</Button>
				</View>
			</View>
		)
	}
}
export default Index;