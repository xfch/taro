import Taro, { Component } from '@tarojs/taro'
import { View, Text,Block,} from '@tarojs/components'

import { set as setGlobalData, get as getGlobalData } from '../../../static/js/global_data'

import './index.scss'

// 我的订单、必备工具
class Index extends Component {
	static options = {
		addGlobalClass: true
	}
	static defaultProps = {
	}
	constructor() {
		super(...arguments)
		this.state = {
		};
	}
	navTo(){
		let posmethod = JSON.stringify(this.props.posmethod)
		let selflift = JSON.stringify(this.props.selflift)
		// Taro.navigateTo({url: `/pages/sendMethod/sendMethod?posmethod=${posmethod}&&selflift=${selflift}`})
		Taro.navigateTo({url: `/pages/sendMethod/sendMethod`})
	}
	render() {
		return (
			<View className='mosmethod weui-cell'>
				<View className='weui-cell__bd'>
					<View>支付配送</View>
				</View>
				<View className='weui-cell__ft weui-cell__ft_in-access text-small' onClick={this.navTo}>
					{this.props.posmethod&&this.props.posmethod.map((item,i) => {
						return (
							<Block for={i} key={i}>
									{item.n_isdefault == '1'&&
										<Block>
											<View>{item.c_psname} + 在线支付</View>
											<View>营业时间：{this.props.postime}</View>
										</Block>
									}
							</Block>
						)
					})}
					
				</View>
			</View>
		)
	}
}
export default Index;