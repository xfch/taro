import Taro, { Component } from '@tarojs/taro'
import { View, Text,Block,Image} from '@tarojs/components'

import { set as setGlobalData, get as getGlobalData } from '../../../static/js/global_data'
import Checkboxbar from '../../../components/checkboxbar'

import './index.scss'

// 我的订单、必备工具
class Index extends Component {
	static options = {
		addGlobalClass: true
	}
	static defaultProps = {
	}
	constructor() {
		super(...arguments)
		this.state = {
			arrImg: ['','','','','','','','','']
		};
	}
	render() {
		return (
			<View className='goodprice'>
				<View className='weui-cell weui-cell-none'>
					<View className='weui-cell__bd'>
						<View>商品金额</View>
					</View>
					<View className='weui-cell__ft text-small text-sub'>
						<View>￥{this.props.orderyprice || '0.00'}</View>
					</View>
				</View>
				{this.props.dbje && this.props.dbje != 0 &&
					<View className='weui-cell weui-cell-none'>
						<View className='weui-cell__bd'>
							<View>打包金额</View>
						</View>
						<View className='weui-cell__ft text-small text-sub'>
							<View>+￥{this.props.dbje || '0.00'}</View>
						</View>
					</View>
				}
				<View className='weui-cell weui-cell-none'>
					<View className='weui-cell__bd'>
						<View>运费</View>
					</View>
					<View className='weui-cell__ft text-small text-sub'>
						<View>+￥{this.props.postmoney || '0.00'}</View>
					</View>
				</View>
				{this.props.MarketingPrice && this.props.MarketingPrice != 0 &&
					<View className='weui-cell weui-cell-none'>
						<View className='weui-cell__bd'>
							<View>优惠券</View>
						</View>
						<View className='weui-cell__ft text-small text-sub'>
							<View>-￥{this.props.MarketingPrice || '0.00'}</View>
						</View>
					</View>
				}
				{/* <View className='weui-cell weui-cell-none'>
					<View className='weui-cell__bd'>
						<View>优惠</View>
					</View>
					<View className='weui-cell__ft text-small text-sub'>
						<View>-￥{this.props.yhPrice || '0.00'}</View>
					</View>
				</View> */}
				{this.props.cxuserIntergral && this.props.cxuserIntergral != 0 &&
					<View className='weui-cell weui-cell-none'>
						<View className='weui-cell__bd'>
							<View>单品活动积分</View>
						</View>
						<View className='weui-cell__ft text-small text-sub'>
							<View>-￥{this.props.cxuserIntergral}</View>
						</View>
					</View>
				}
				{this.props.balance && this.props.balance != 0 &&
					<View className='weui-cell weui-cell-none'>
						<View className='weui-cell__bd'>
							<View>账户余额</View>
						</View>
						<View className='weui-cell__ft text-small text-sub'>
							<View>-￥{this.props.balance || '0.00'}</View>
						</View>
					</View>
				}
			</View>
		)
	}
}
export default Index;