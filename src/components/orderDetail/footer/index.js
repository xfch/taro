import Taro, { Component } from '@tarojs/taro'
import { View, Text,Block} from '@tarojs/components'

import { set as setGlobalData, get as getGlobalData } from '../../../static/js/global_data'

import './index.scss'

// 我的订单、必备工具
class cartbar extends Component {
	static options = {
		addGlobalClass: true
	}
	static defaultProps = {
	}
	constructor() {
		super(...arguments)
		this.state = {};
	}
	render() {
		return (
			<View className='footer'>
				<View className='weui-cell'>
					<View className='weui-cell__bd'>
						<View className='text-big text-sub'>实付款：{this.props.orderprice || 0}</View>
					</View>
					<View className='weui-cell__ft'>
						<View className='weui-btn settle-btn' onClick={this.props.onFootBtn.bind(this,'addOrder')}>提交订单</View>
					</View>
				</View>
                
			</View>
		)
	}
}
export default cartbar;