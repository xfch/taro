import Taro, { Component } from '@tarojs/taro'
import { View, Text, Block, Image, Icon } from '@tarojs/components'

import { set as setGlobalData, get as getGlobalData } from '../../../static/js/global_data'
import Checkboxbar from '../../../components/checkboxbar'

import './index.scss'

// 我的订单、必备工具
class Index extends Component {
	static options = {
		addGlobalClass: true
	}
	static defaultProps = {
	}
	constructor() {
		super(...arguments)
		this.state = {
			domainImg: getGlobalData("domainImg"),
		};
		// popCoupon
	}
	render() {
		return (
			<View className={`popop ${this.props.popCoupon ? 'popup-show' : ''}`}>
				<View className='mask'></View>
				<View className='popup-content'>
					<View className='weui-cell weui-cell-none bg-body'>
						<View className='weui-cell__bd ac'>
							<View className='text-big'>优惠券</View>
						</View>
						<View className='weui-cell__ft' onClick={this.props.onHidePopCoupon}>
							<Icon size='20' type='clear' color='red' />
						</View>
					</View>
					<View>
						{this.props.marketimg && this.props.marketimg.map((item, i) => {
							return (
								<View for={i} key={i} className='weui-cell'>
									<View className='weui-cell__hd'>
										<View className='item-MarketingPrice'>￥<Text className='price'>{item.MarketingPrice}</Text></View>
									</View>
									<View className='weui-cell__bd'>
										<View className='ellipsis'>{item.MarketingName}</View>
										<View className='text-gray text-little'>{item.MarketingBeginTime} - {item.MarketingEndTime}</View>
									</View>
									<View className='weui-cell__ft text-small'>
										<Checkboxbar select={item.select} onSelectCheck={this.props.onSelectCheck.bind(this,i)}></Checkboxbar>
									</View>
								</View>
							)
						})}
					</View>
				</View>
			</View>
		)
	}
}
export default Index;