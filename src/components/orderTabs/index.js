
import Taro, { Component } from '@tarojs/taro'
import { View, ScrollView } from '@tarojs/components'
import './index.scss'


class Index extends Component {
	static defaultProps = {
		
	}
	constructor() {
		super(...arguments)
	}
	state = { 
		
	}
	componentWillMount() { }

	componentDidMount() { }

	componentWillUnmount() { }

	componentDidShow() { }

	componentDidHide() { }
	render() {
		return (
			<View className='orderTabs'>
				<ScrollView scrollY className='orderScroll'>
					<View className='item'>全部</View>
					<View className='item'>待付款</View>
					<View className='item'>待发货</View>
					<View className='item'>待收货</View>
					<View className='item'>待自提</View>
					<View className='item'>已完成</View>
				</ScrollView>
			</View >
		)
	}
}
export default Index;