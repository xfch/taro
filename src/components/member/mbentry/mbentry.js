import Taro, { Component } from '@tarojs/taro'
import { View, Text, Image } from '@tarojs/components'

import './mbentry.scss'

// 我的订单、必备工具
class mbentry extends Component {
	static options = {
		addGlobalClass: true
	}
	static defaultProps = {
	}
	constructor() {
		super(...arguments)
		this.state = {};
	}

	render() {
		let entry = this.props.entry
		return (
			<View className='mbentry'>
				<View className='weui-cells weui-cells_after-title weui-cells-none'>
					<View className='weui-cell'>
						<View className='weui-cell__bd'>
							<Text className='text-title'>{entry.title}</Text>
						</View>
						<View className='weui-cell__ft'>
							<View className='text-gray text-small' onClick={this.props.onNavTo.bind(this, entry.more.id)}>{entry.more.txt}</View>
						</View>
					</View>
					<View className='weui-cell weui-cell-grids'>
						<View className='weui-cell__bd'>
							<View class="weui-grids">
								{entry.list.map((item, i) => {
									return (
										<View for={i} key={i} class="weui-grid ac" onClick={this.props.onNavTo.bind(this, item.id)}>
											<View className={item.icon}></View>
											<view className="weui-grid__label text-small text-gray">{item.txt}</view>
											{item.num && <Block>
												{item.num > 0 ? <View className='weui-badge' style='position: absolute;top: 0;right: 20%;'>{item.num}</View> : <View></View>}
											</Block>}
										</View>
									)
								})}
							</View>
						</View>
					</View>
				</View>
			</View>
		)
	}
}
export default mbentry;