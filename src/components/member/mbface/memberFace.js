
import Taro, { Component } from '@tarojs/taro'
import { View, Text, Image } from '@tarojs/components'

import './memberFace.scss'

import api from '../../../service/api'
import { set as setGlobalData, get as getGlobalData } from '../../../static/js/global_data'

// 更换会员头像
class MemberFace extends Component {
	static options = {
		addGlobalClass: true
	}
	static defaultProps = {
		userInfo: {}
	}
	constructor() {
		super(...arguments)
		this.state = {
			domainImg: getGlobalData('domainImg')
		};
	}
	componentDidMount(){
		this.setState({
			userInfo: this.props.userInfo,
		})
	}
    changeFace() {
		Taro.chooseImage({
            sizeType: "compressed",
            count: 1,
            success: res => {
                api.uploadImage('1907',res.tempFilePaths[0],'file',{ShopNo: getGlobalData('SHOP_NO')},(res) => {
					this.state.userInfo.C_MB_PIC = res.filepath
					setGlobalData('userInfo',this.state.userInfo)
					this.state.userInfo.C_MB_PIC = getGlobalData('domainImg') + this.state.userInfo.C_MB_PIC
					console.log(getGlobalData('userInfo'))
					console.log(this.state.userInfo)
					this.setState({
						userInfo: this.state.userInfo
					})
                })
            },
        })
    }

	render() {
		let {userInfo} = this.state
		return (
			<View className='memberFace'>
                <View className='face'>
					{userInfo.C_MB_PIC ? <Image className='face-img' src={userInfo.C_MB_PIC}></Image> : ''}
                </View>
                <View className='edit-face ac'>
                    <Text onClick={this.changeFace}>
                        <Text className='iconfont icon-edit'></Text><Text>更换</Text>
                    </Text>
                </View>
			</View>
		)
	}
}
export default MemberFace;