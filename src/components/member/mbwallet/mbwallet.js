import Taro, { Component } from '@tarojs/taro'
import { View, Text, Image } from '@tarojs/components'

import './mbwallet.scss'

import common from '../../../service/common'

// 更换会员头像
class mbwallet extends Component {
	static options = {
		addGlobalClass: true
	}
	static defaultProps = {
		wallet: {},
	}
	constructor() {
		super(...arguments)
		this.state = {
		}
	}

	// componentWillMount(){
	// 	this.setState({
	// 		wallet: this.props.wallet
	// 	},() => {
	// 		common.getAddressList(() => {
	// 			this.state.wallet.list[3].num = this.state.addressList.length
	// 			console.log(this.state.wallet)
	// 			this.setState({
	// 				wallet: this.state.wallet
	// 			})
	// 		})
	// 	})
	// }
	// pages/wallet/wallet
	navTo(){
		Taro.navigateTo({
			url: '/pages/wallet/wallet'
		})
	}

	

	render() {
		return (
			<View className='mbwallet'>
				<View className='weui-cells weui-cells_after-title weui-cells-none'>
					<View className='weui-cell'>
						<View className='weui-cell__bd text-title'>
							<Text>余额</Text><Text className='text-sub text-balance'>{this.props.userInfo.N_MB_AMOUNT ? this.props.userInfo.N_MB_AMOUNT : 0}元</Text>
						</View>
						<View className='weui-cell__ft'>
							<View className='mbwallet-btn' onClick={this.navTo}>我的钱包</View>
						</View>
					</View>
					<View className='weui-cell'>
						{this.props.wallet.list.map((item,i) => {
							return(
								<View for={i} key={i} className='weui-flex__item ac' onClick={this.props.onNavTo.bind(this,item.url)}>
									<View className='item-num text-sub'>({item.num})</View>
									<View className='text-gray text-small'>{item.txt}</View>
								</View>
							)
						})}
					</View>
				</View>
			</View>
		)
	}
}
export default mbwallet;