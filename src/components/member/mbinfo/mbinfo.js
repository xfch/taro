
import Taro, { Component } from '@tarojs/taro'
import { View, Text, Image ,Navigator} from '@tarojs/components'

import './mbinfo.scss'

// 更换会员头像
class mbinfo extends Component {
    static options = {
        addGlobalClass: true
      }
	static defaultProps = {
	}
	constructor() {
		super(...arguments)
		this.state = {};
    }
    // 更头像
    // 退出
    signout(){
        this.props.onSignout()
    }

	render() {
        let userInfo = this.props.userInfo;
		return (
			<View className='mbinfo'>
                <View className='weui-cell'>
                    <Navigator url='/pages/editMember/editMember' className='weui-cell__hd'>
                        <View className='face'>
                            <Image className='face-img' src={userInfo.C_MB_PIC}></Image>
                        </View>
                    </Navigator>
                    <View className='weui-cell__bd'>
                        <View className='mb-name'>{userInfo.C_MB_NAME}</View>
                        {userInfo.progress == 0 || userInfo.progress == 100 ? 
                            <Navigator url='/pages/editMember/editMember'>{userInfo.progress == 0 ? '完善':'查看'}会员资料</Navigator> :
                            <View>
                                <Text>个人信息已完善{userInfo.progress}%</Text>
                                <Navigator url='/pages/editMember/editMember' className='iconfont icon-edit'></Navigator>
                            </View>
                        }
                        <Navigator url='/pages/editMember/editMember?activeIndex=1'>{userInfo.C_MB_PHONE != '' ? '已绑定手机号' : '请绑定手机号'}</Navigator>
                    </View>
                    <View className='weui-cell__ft'>
                        <View className='mb-signout' onClick={this.signout}>
                            <Text className='iconfont icon-edit'></Text>
                            <Text>退出</Text>
                        </View>
                    </View>
                </View>
			</View>
		)
	}
}
export default mbinfo;