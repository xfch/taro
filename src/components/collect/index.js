
import Taro, { Component } from '@tarojs/taro'
import { View, Text } from '@tarojs/components'
import './index.scss'

import api from '../../service/api'
import base from '../../static/js/base'
import { get as getGlobalData } from '../../static/js/global_data'

class Index extends Component {
	static options = {
		addGlobalClass: true
	}
	static defaultProps = {
		choose: 0,
		gid: ''
	}
	constructor() {
		super(...arguments)
	}
	state = {
		isChoose: false
	}
	componentWillMount() { }

	componentDidMount() {
		this.setState({ isChoose: this.props.choose })
	}

	componentWillUnmount() { }

	componentDidShow() { }

	componentDidHide() { }

	onCollect() {
		let userInfo = Taro.getStorageSync('userInfo')
		if(!userInfo){
			base.showModals('登陆后才可以收藏',null,(res) => {
				if(res.confirm){
					this.props.onCollect()
				}
			})
			return
		}
		let { gid } = this.props;
		let { isChoose } = this.state;
		let str = `mebno&gid&issc&ShopNo`
		let params = {
			bNo: 711,
			gid,
			issc: isChoose == 1 ? 0 : 1,//是否收藏 如果收藏了就取消收藏，反之
			mebno: userInfo.C_MB_NO,//用户信息id
			ShopNo: getGlobalData('SHOP_NO'),
		}
		Taro.showLoading({ title: '加载中...' })
		api.get(str, params, (res) => {
			Taro.hideLoading();
			if (res.code == '100') {
				this.setState({
					isChoose: isChoose == 1 ? 0 : 1,
				})
			} else {
				Taro.showToast({ title: res.msg, icon: 'none' })
			}
		})
	}
	render() {
		const { isChoose } = this.state;
		return (
			<View className='collectBox' onClick={this.onCollect.bind(this)}>
				{isChoose == 1 ? <Text className='iconfont icon-stared choose'></Text> : <Text className='iconfont icon-star'></Text>}
				<View className='text'>收藏</View>
			</View >
		)
	}
}
export default Index;