import Taro, { Component } from '@tarojs/taro'
import { View, Image } from '@tarojs/components'

import './index.scss'

import { get as getGlobalData } from '../../static/js/global_data'
import common from '../../service/common'
import comNavTo from '../../service/comNavTo'


class tabBar extends Component {
	static options = {
		addGlobalClass: true
	}
	static defaultProps = {

	}
	constructor() {
		super(...arguments)
		this.state = {
			// //  316餐饮1  2626零售2  390美业3
			tabList: []
		};
	}
	componentDidMount() {
		// 获取底部tab
		common.tabList(this, (res) => {
			this.setState({
				tabList: res
			})
		})
	}

	clickTab(item) {
		comNavTo.comNavTo('55',item.N_LinkId, item.C_Params)
		// console.log(url, navigateType)
		// if (navigateType) {
		// 	Taro.navigateTo({ url: url })
		// } else {
		// 	Taro.reLaunch({ url: url })
		// }
	}

	render() {
		const {tabId} = this.props
		const {tabList } = this.state
		return (
			<View className='tabBar weui-flex'>
				{tabList.length > 0 ? tabList.map((item, i) => {
					return (
						<View for={i} key={i} className={`weui-flex__item ac ${tabId == item.ID ? 'active' : ''}`} onClick={this.clickTab.bind(this,item)}>
							{/* <View for={i} key={i} className={`weui-flex__item ac ${tabId == item.ID ? 'active' : ''}`} onClick={this.clickTab.bind(this, item.C_Link, item.navigateType)}> */}
							<Image className='icon' src={item.C_Navigation_ICO}></Image>
							<View className='tab-text'>{item.C_Navigation_Name}</View>
						</View>
					)
				}) : ''}
			</View>
		)
	}
}
export default tabBar;