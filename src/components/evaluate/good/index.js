import Taro, { Component } from '@tarojs/taro'
import { View, Text,Block} from '@tarojs/components'

import { set as setGlobalData, get as getGlobalData } from '../../../static/js/global_data'

import './index.scss'

class cartbar extends Component {
	static options = {
		addGlobalClass: true
	}
	static defaultProps = {
		glist: [], // 订单商品列表
	}
	constructor() {
		super(...arguments)
		this.state = {
		};
	}
	render() {
		return (
			<View className='good'>
				{this.props.glist&&this.props.glist.map((item,i) => (
					<View key={'glist' + i} className='weui-cell'>
						{this.props.children}
						<View className='weui-cell__hd'>
							<Image className='good-img' src={this.props.domainImg + item.C_MAINPICTURE}></Image>
						</View>
						<View className='weui-cell__bd'>
							<View className=''>{item.C_GOODS_NAME}</View>
						</View>
						<View className='weui-cell__ft'><Text>x{item.N_NUM}</Text></View>
					</View>
				))}
			</View>
		)
	}
}
export default cartbar;