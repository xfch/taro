import Taro, { Component } from '@tarojs/taro'
import { View, Text,Block} from '@tarojs/components'

import { set as setGlobalData, get as getGlobalData } from '../../../static/js/global_data'

import './index.scss'

class index extends Component {
	static options = {
		addGlobalClass: true
	}
	static externalClasses = ['star-class']
	static defaultProps = {
		stars: [1,2,3,4,5], //总分
        stared: 2,  //评价分数
	}
	constructor() {
		super(...arguments)
		this.state = {};
	}
	// 评价星
	changeStar(index){
		this.props.onChangeStar(index)
	}
	render() {
		let {stars, stared} = this.props
		return (
			<View>
				{this.props.children}
				{stars.map((item,i) => (
					<Block key={'stars' + i}>
						<Text onClick={this.changeStar.bind(this,i)} className={`star-class iconfont ${stared <= item ? 'icon-star' : 'icon-stared'}`}></Text>
					</Block>
				))}
			</View>
		)
	}
}
export default index;