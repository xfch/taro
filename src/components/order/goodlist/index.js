import Taro, { Component } from '@tarojs/taro'
import { View, Text,Block,Image} from '@tarojs/components'

import { set as setGlobalData, get as getGlobalData } from '../../../static/js/global_data'

import './index.scss'

class Index extends Component {
	static options = {
		addGlobalClass: true
	}
	static defaultProps = {
		glist: [], //订单列表商品
		domainImg: getGlobalData('domainImg'), //图片的http
	}
	constructor() {
		super(...arguments)
		this.state = {
			listUp: false, //是否展开列表
		};
	}
	componentWillMount(){
		this.setState({
			glists: this.props.glist,
			domainImg: this.props.domainImg,
		},() => {
			this.setGlist(this.state.listUp)
		})
	}

	listToggle(){
		this.setState({
			listUp: !this.state.listUp
		},() => {
			this.setGlist(this.state.listUp)
		})
	}
	setGlist(listUp){
		let goodlist = []
		if(listUp){
			goodlist = this.state.glists
		}else{
			goodlist.push(this.state.glists[0])
		}
		this.setState({goodlist: goodlist})
	}
	render() {
		return (
			<View className='glist'>
				{this.state.glists &&
					<Block>
						{this.state.goodlist.map((item,i) => {
							return (
								<View for={i} key={i} className='weui-cell'>
									<View className='weui-cell__hd'>
										<Image className='item-img' src={this.state.domainImg + item.imageUrl}></Image>
									</View>
									<View className='weui-cell__bd'>
										<View className='weui-media-box__desc'>{item.goodsName}</View>
										<View className='text-gray text-small'>× {item.num}</View>
									</View>
								</View>
							)
						})}
						{this.state.glists.length > 1 &&
							<View className='weui-cell list-up'>
								<View className='weui-cell__bd ac text-small' onClick={this.listToggle}>
									{!this.state.listUp ? <Block>查看全部{this.state.glists.length}件商品</Block> : <Block>收起</Block>}
								</View>
							</View>
						}
					</Block>
				}
			</View>
		)
	}
}
export default Index;
