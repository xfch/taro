import Taro, { Component } from '@tarojs/taro'
import { View, Text,Block,ScrollView} from '@tarojs/components'

import { set as setGlobalData, get as getGlobalData } from '../../../static/js/global_data'

import './index.scss'

class cartbar extends Component {
	static options = {
		addGlobalClass: true
	}
	static defaultProps = {
		tab: [
			{'txt': '全部','state': '0'},
			{'txt': '待付款','state': '1'},
			{'txt': '待发货','state': '2'},
			{'txt': '待收货','state': '3'},
			{'txt': '待自提','state': '5'},
			{'txt': '已完成','state': '4'},
			{'txt': '售后','state': '6'}
		], //订单头部tab
		states: '0', //默认 全部
		scrollIntoView: 'tab0'
	}
	constructor() {
		super(...arguments)
		this.state = {
		};
	}
	componentWillMount(){
		this.setState({
			tab: this.props.tab,
			states: this.props.states,
			scrollIntoView: 'tab' + this.props.states
		})
		
	}

	tabTap(id,index){
		this.state.scrollIntoView = 'tab'+ index;
		this.setState({
			states: id,
			scrollIntoView: this.state.scrollIntoView
		})
		this.props.onTabTap(id)
	}
	render() {
		return (
			<View className='tab-scroll'>
				<ScrollView
                    className='scrollview'
                    scrollIntoView={this.state.scrollIntoView}
                    scrollX>
                    {this.state.tab&&this.state.tab.map((item,i) => {
                        return(
                            <Text for={i} key={i} id={`tab${i}`} onClick={this.tabTap.bind(this,item.state,i)} className={`nav ${this.state.states == item.state ? 'active':''}`}>{item.txt}</Text>
                        )
                    })}
                </ScrollView>
			</View>
		)
	}
}
export default cartbar;