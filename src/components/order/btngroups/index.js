import Taro, { Component } from '@tarojs/taro'
import { View, Text, Block } from '@tarojs/components'

import { set as setGlobalData, get as getGlobalData } from '../../../static/js/global_data'

import './index.scss'

class Index extends Component {
	static options = {
		addGlobalClass: true
	}
	static defaultProps = {
		ozt: 1, //订单状态
	}
	constructor() {
		super(...arguments)
		this.state = {
		};
	}
	componentWillMount() {
	}
	render() {
		return (
			<View className='btn-groups'>
				{this.props.ozt != '退款受理中' && this.props.ozt != '已退款' && this.props.ozt != '驳回提现' &&
					<Block>
						{this.props.ozt == '待自提' &&
							<Block>
								<Button type='default' className='btn-item btn-default btn-plain' size='mini' onClick={this.props.onBtnGroups.bind(this, 'afterSale')}>申请售后</Button>
							</Block>
						}
						{this.props.ozt == '待付款' &&
							<Block>
								<Button type='default' className='btn-item btn-default btn-plain' size='mini' onClick={this.props.onBtnGroups.bind(this, 'cancelOrder')}>取消订单</Button>
								<Button type='default' className='btn-item btn-main btn-plain' size='mini' onClick={this.props.onBtnGroups.bind(this, 'confirmPayment')}>确认付款</Button>
							</Block>
						}
						{this.props.ozt == '待发货' || this.props.ozt == '待接单' &&
							<Block>
								<Button type='default' className='btn-item btn-main btn-plain' size='mini' onClick={this.props.onBtnGroups.bind(this, 'anotherOrder')}>再来一单</Button>
								<Button type='default' className='btn-item btn-default btn-plain' size='mini' onClick={this.props.onBtnGroups.bind(this, 'afterSale')}>申请售后</Button>
							</Block>
						}
						{this.props.ozt == '待收货' &&
							<Block>
								<Button type='default' className='btn-item btn-main btn-plain' size='mini' onClick={this.props.onBtnGroups.bind(this, 'confirmReceipt')}>确认收货</Button>
							</Block>
						}
						{this.props.ozt == '已完成' &&
							<Block>
								<Button type='default' className='btn-item btn-default btn-plain' size='mini' onClick={this.props.onBtnGroups.bind(this, 'afterSale')}>申请售后</Button>
								<Button type='default' className='btn-item btn-main btn-plain' size='mini' onClick={this.props.onBtnGroups.bind(this, 'anotherOrder')}>再来一单</Button>
								{this.props.N_ISCOMMENT == 0 &&
									<Button type='default' className='btn-item btn-main btn-plain' size='mini' onClick={this.props.onBtnGroups.bind(this, 'evaluate')}>去评价</Button>
								}
							</Block>
						}
					</Block>
				}
			</View>
		)
	}
}
export default Index;