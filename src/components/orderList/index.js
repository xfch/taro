
import Taro, { Component } from '@tarojs/taro'
import { View, Text } from '@tarojs/components'

import './index.scss'

class Index extends Component {
	static options = {
		addGlobalClass: true
	}
	static defaultProps = {
		icon: 'icon-home',
		title: '',
		rightIcon: true,
		onLyClick: () => { }
	}
	constructor() {
		super(...arguments)
	}
	state = {

	}
	componentWillMount() { }

	componentDidMount() { }

	componentWillUnmount() { }

	componentDidShow() { }

	componentDidHide() { }
	onComClick() { 
		const { rightIcon } = this.props;
		if (!rightIcon) { 
			return false;
		}
		this.props.onLyClick()
	}
	render() {
		const { icon, title, rightIcon } = this.props;
		return (
			<View className='list list-l' onClick={this.onComClick.bind(this)}>
				<View className='list-l'>
					<Text className={`iconfont icon ${icon}`}></Text>
					<Text className='shop-adr'>{title}</Text>
				</View>
				{rightIcon ? <Text className='iconfont icon-arrow-right icon'></Text>:''}
				
			</View>
		)
	}
}
export default Index;