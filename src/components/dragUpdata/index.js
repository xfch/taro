import Taro, { Component } from '@tarojs/taro'
import { View, Text, ScrollView, Swiper, SwiperItem } from '@tarojs/components'
import './index.scss'
import until from '../../untils/lyUntils'

export default class LyInput extends Component {
    static defaultProps = {
        pull: true,//上拉操作开关--默认打开
        down: true,//下拉操作开关--默认打开
        onPull: () => { },//上拉操作释放后触发事件
        onDown: () => { },//下拉操作释放后触发事件
        onUpper: () => { },//滚动到顶部事件
        onUpper: () => { }//滚动到底部事件
    }
    static propTypes = {

    }
    constructor(props) {
        super(props)
        this.state = {
            maxY: 100,//大于这个值的时候执行拉动操作触发事件
            downHeight: 0,//下拉时图标的标签的高度
            upHeight: 0,//上拉时图标的标签的高度

            downText: '下拉刷新',
            upText: '上拉加载',
        }
    }
    componentWillMount() { }

    componentDidMount() { }

    componentWillUnmount() { }

    componentDidShow() { }
    componentDidHide() { }
    pull() {//上拉
        this.props.onPull()
    }
    down() {//下拉
        this.props.onDown()
    }
    ScrollToUpper() { //滚动到顶部事件
        this.props.onUpper()
    }
    ScrollToLower() { //滚动到底部事件
        this.props.onLower()
    }
    moveChange(e) { //动画进行中执行
        const { dy } = e.detail;
        const { maxY } = this.state;
        //console.log(dy)
        if (dy < 0) {//下拉操作
            this.setState({ downHeight: -dy })
            if (-dy > maxY) {
                this.setState({ downText: '释放刷新' })
            } else {
                this.setState({ downText: '下拉刷新' })
            }
        } else if (dy > 0) {//上拉操作
            this.setState({ upHeight: dy })
            if (dy > maxY) {
                this.setState({ upText: '释放加载' })
            } else {
                this.setState({ upText: '上拉加载' })
            }
        } else {//复位的时候
            this.setState({ downHeight: 0, upHeight: 0 })
        }

    }
    touchEnd() {
        until.getDomInfo('#dragUpdata', this.$scope).then((rect) => {
            const { top } = rect;
            const { maxY } = this.state;
            const { pull, down } = this.props;
            if (top > maxY) {//下拉操作要执行的事件
                if (down) {
                    this.down()
                }
            } else if (top < -maxY) { //上拉操作要执行的事件
                if (pull) {
                    this.pull()
                }
            }
        })
    }
    render() {
        const { downHeight, upHeight, downText, upText } = this.state;
        const { pull, down } = this.props;
        return (
            <View className='dragUpdataPage'>
                {
                    down ? (
                        <View style={{ height: downHeight + 'px' }} className='downDragBox'>
                            <Text className='downText'>{downText}</Text>
                        </View>
                    ) : ''
                }
                <Swiper className='moveArea' vertical onTransition={this.moveChange}>
                    <SwiperItem className='moveArea'>
                        <ScrollView id='dragUpdata' className='dragUpdata' onTouchEnd={this.touchEnd} scrollY onScrollToUpper={this.ScrollToUpper} onScrollToLower={this.ScrollToLower}>
                            {this.props.children}
                        </ScrollView>
                    </SwiperItem>
                </Swiper>
                {
                    pull ? (
                        <View style={{ height: upHeight + 'px' }} className='upDragBox'>
                            <Text className='downText'>{upText}</Text>
                        </View>
                    ) : ''
                }
            </View>
        )
    }
}

