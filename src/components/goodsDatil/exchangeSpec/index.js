
import Taro, { Component } from '@tarojs/taro'
import { View, Swiper, SwiperItem, Image } from '@tarojs/components'

import './index.scss'

import NumInput from '../../numInput'

import { set as setGlobalData, get as getGlobalData } from '../../../static/js/global_data'

class Index extends Component {
	static options = {
		addGlobalClass: true
	}
	static defaultProps = {
		goodInfo: {}
	}
	constructor() {
		super(...arguments)
	}
	state = {
		domainImg: getGlobalData('domainImg'),
		num: 1
	}
	// 加减
	changeCartNum(bNo,types){
		let { goodInfo} = this.props;
		if(types == 'add'){ // +
			if(goodInfo.N_IG_LIMITNUM > 0){
				if(this.state.num >= goodInfo.N_IG_LIMITNUM){
					return
				}
			}else{
				if(this.state.num >= goodInfo.STOCK){
					return
				}
			}
			this.state.num++
		}else if(types == 'minus'){ // -
			if(this.state.num == 1){
				return
			}
			this.state.num--
		}
		this.setState({
			num: this.state.num
		})
	}
	// 确认兑换orderSubmitMarketing  
	submit(){
		Taro.navigateTo({
			url: '/pages/orderSubmitMarketing/index?goodType=exchange&totalNum=' + this.state.num
		})
	}

	render() {
		let { goodInfo} = this.props;
		return (
			<View className={`popop ${this.props.popCoupon ? 'popup-show' : ''}`}>
				<View className='mask'></View>
				<View className='popup-content'>
					<View className='weui-cell weui-cell-none bg-body'>
						<View className='weui-cell__bd ac'>
							<View className='text-big'>该商品单人{goodInfo.N_IG_LIMITNUM > 0 ? "单人限购" + goodInfo.N_IG_LIMITNUM + "件" : "不限购"}</View>
						</View>
						<View className='weui-cell__ft' onClick={this.props.onHidePopCoupon}>
							<Icon size='20' type='clear' color='red' />
						</View>
					</View>
					<View>
						<View className='weui-cell'>
							<View className='weui-cell__bd opts-goods'>
								<NumInput iconSize='1' onChangeCartNum={this.changeCartNum.bind(this)} nums={this.state.num}></NumInput>
							</View>
						</View>
						<View className='weui-cell weui-cell-none'>
							<View className='weui-cell__bd opts-btn'>
								<Button className='weui-btn btn-main' onClick={this.submit}>确认兑换</Button>
							</View>
						</View>
					</View>
				</View>
			</View>
		)
	}
}
export default Index;