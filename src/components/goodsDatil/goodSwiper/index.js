
import Taro, { Component } from '@tarojs/taro'
import { View, Swiper,SwiperItem, Image } from '@tarojs/components'
import './index.scss'
import until from '../../../untils/lyUntils'

import { set as setGlobalData, get as getGlobalData } from '../../../static/js/global_data'

class Index extends Component {
	static defaultProps = {
		goodsImg: []
	}
	constructor() {
		super(...arguments)
	}
	state = {
		domainImg: getGlobalData('domainImg'),
	}
	componentWillMount() { }

	componentDidMount() { 
		// this.setKillTime()
	}
	
	render() {
		const {domainImg} = this.state
		let { goodsImg} = this.props;
		return (

			<View className='swper' style={{ height: '100vw' }}>
				{
					goodsImg.length > 0 ? (
						<Swiper className='test-h' indicatorColor='#999' indicatorActiveColor='#333' indicatorDots autoplay>
							{
								goodsImg.map((val, index) => (
									<SwiperItem key={'swp' + index}>
										<Image src={domainImg + val}></Image>
										{/* <Image src={domainImg + val.PICNAME ? val.PICNAME : val.picname }></Image> */}
									</SwiperItem>
								))
							}
						</Swiper>
					) : <View className='hasNoImg'>暂无商品图片</View>
				}
			</View>
		)
	}
}
export default Index;