
import Taro, { Component } from '@tarojs/taro'
import { View, Text, Image } from '@tarojs/components'
import './index.scss'
import until from '../../../untils/lyUntils'

class Index extends Component {
	static defaultProps = {
		salemodel: null,
		price:''
	}
	constructor() {
		super(...arguments)
	}
	state = {
		seckillDate: null
	}
	componentWillMount() { }

	componentDidMount() { 
		this.setKillTime()
	}

	componentWillUnmount() { }

	componentDidShow() { }

	componentDidHide() { }

	setKillTime() {
		const { salemodel } = this.props;
		if (!salemodel.msEndTime) {
			return false;
		}
		let msEndTime=salemodel.msEndTime.substring(0,10) + ' ' +salemodel.msEndTime.substring(11,salemodel.msEndTime.length);
		const set = (d) => {
			this.setState({
				seckillDate: until.formatDuring(d)
			})
		}
		let diffTime = until.getKillTime(msEndTime);
		set(diffTime)
		this.timer = setInterval(() => {
			set(diffTime)
			diffTime -= 1000
		}, 1000)
	}
	render() {
		const { salemodel, price } = this.props;
		const { seckillDate } = this.state;
		return (
			<View className='killZoneBar'>
				<View className='killZoneL'>
					<View className='actPrice'>
						￥<Text className='actPriceMoney'>{salemodel.skuList[0].salePrice}</Text>
					</View>
					<View className='priceBuy'>
						<View className='orgPrice'>￥{price}</View>
						<View className='txtBuy'>
							<Text>限时秒杀</Text>
						</View>
					</View>
				</View>
				<View className='endTimeBox'>
					<Image className='endTimeBoxImg' src={require('../../../assets/img/bg-end-time.png')}></Image>
					<View className='endTime'>
						{/* <View>距离开始</View> */}
						<View className='endTimetit'>距结束仅剩</View>
						<View className='endTimeItem'>
							<Text className='endTimeItemtxt'>{seckillDate.d}</Text>:
                            <Text className='endTimeItemtxt'>{seckillDate.h}</Text>:
                            <Text className='endTimeItemtxt'>{seckillDate.m}</Text>:
                            <Text className='endTimeItemtxt'>{seckillDate.s}</Text>
						</View>
					</View>
				</View>
			</View >
		)
	}
}
export default Index;