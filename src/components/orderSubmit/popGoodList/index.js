import Taro, { Component } from '@tarojs/taro'
import { View, Text, Block, Image, Icon } from '@tarojs/components'

import { set as setGlobalData, get as getGlobalData } from '../../../static/js/global_data'
import Checkboxbar from '../../../components/checkboxbar'

import './index.scss'

// 我的订单、必备工具
class Index extends Component {
	static options = {
		addGlobalClass: true
	}
	static defaultProps = {
		goodType: ''
	}
	constructor() {
		super(...arguments)
		this.state = {
			domainImg: getGlobalData("domainImg"),
		};
	}
	render() {
		let { goodType } = this.props
		return (
			<View className={`popop ${this.props.popGoodList ? 'popup-show' : ''}`}>
				<View className='mask'></View>
				<View className='popup-content'>
					<View className='weui-cell weui-cell-none bg-body'>
						<View className='weui-cell__bd ac'>
							<View className='text-big'>商品</View>
						</View>
						<View className='weui-cell__ft' onClick={this.props.onHidePopGoodList}>
							<Icon size='20' type='clear' color='red' />
						</View>
					</View>
					<View>
						{this.props.goodList && this.props.goodList.map((item, i) => {
							return (
								<View for={i} key={i} className='weui-cell'>
									<View className='weui-cell__hd'>
									{this.state.domainImg&&
										<Image src={this.state.domainImg + item.C_MAINPICTURE} className='item-img'></Image>
									}
									</View>
									<View className='weui-cell__bd'>
										<View className='ellipsis'>{item.C_GOODS_NAME || item.GOODS_NAME}</View>
									</View>
									<View className='weui-cell__ft text-small'>
										<View>x{item.N_NUM}</View>
										{goodType == '' && <View className='text-gray'>￥{item.N_TOTALAMOUNT}</View>}
										{goodType == 'exchange' && <View className='text-gray'>{item.N_IG_INTEGRAL}<Text className='text-small'>积分</Text></View>}
										{goodType == 'pintuan' && <View className='text-gray'>￥{item.N_GSPrice}</View>}
									</View>
								</View>
							)
						})}
					</View>
				</View>
			</View>
		)
	}
}
export default Index;