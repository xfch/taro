import Taro, { Component } from '@tarojs/taro'
import { View, Text, Block, Image, Switch } from '@tarojs/components'

import { set as setGlobalData, get as getGlobalData } from '../../../static/js/global_data'
import Checkboxbar from '../../../components/checkboxbar'

import './index.scss'

// 我的订单、必备工具
class Index extends Component {
	static options = {
		addGlobalClass: true
	}
	static defaultProps = {
		marketimg: [],
	}
	constructor() {
		super(...arguments)
		this.state = {
		};
	}
	render() {
		let { goodType, marketimg} = this.props
		return (
			<View className='deduction'>
				{goodType == '' && 
					<Block>
						<View className='weui-cell'>
							<View className='weui-cell__bd'>
								<View>优惠券</View>
							</View>
							<View className='weui-cell__ft weui-cell__ft_in-access text-small' onClick={this.props.onCouponTap}>
								{this.props.yhqid == ''&& <Text>{marketimg.length}张</Text>}
								{this.props.yhqid != ''&& marketimg.map((item,i) => {
									return (
										<Block for={i} key={i}>
											{this.props.yhqid == item.MarketingCId && <Text>{item.MarketingName}</Text>}
										</Block>
									)
								})}
							</View>
						</View>
						<View className='weui-cell'>
							<View className='weui-cell__bd'>
								<View>礼品卡/购物卡</View>
							</View>
							<View className='weui-cell__ft weui-cell__ft_in-access text-small'>
								<Text>无可用</Text>
							</View>
						</View>
						<View className={`weui-cell ${this.props.userInfo.N_MB_INTEGRAL > 0 ? 'weui-cell-switch' : ''}`} >
							<View className='weui-cell__bd'>
								<View>当前拥有{this.props.userInfo.N_MB_INTEGRAL}积分
									{this.props.intergralState == 1 && this.props.Intergral &&
										<Text className='text-sub text-small pl30' >可用{this.props.Intergral}积分抵扣{this.props.IntergralMoney}元</Text>
									}
								</View>
							</View>
							<View className='weui-cell__ft'>
								{this.props.userInfo.N_MB_INTEGRAL > 0 &&
								<View onClick={this.props.onSwitchTap.bind(this,'intergral')}><Switch checked={this.props.intergralState == 1 ? true : false} /></View>
								}
							</View>
						</View>
					</Block>
				}
				<View className={`weui-cell ${this.props.userInfo.N_MB_AMOUNT > 0 ? 'weui-cell-switch' : ''}`}>
					<View className='weui-cell__bd'>
						<View>可用余额{this.props.userInfo.N_MB_AMOUNT}元</View>
					</View>
					<View className='weui-cell__ft'>
						{this.props.userInfo.N_MB_AMOUNT > 0 &&
							<View onClick={this.props.onSwitchTap.bind(this,'balance')}><Switch checked={this.props.balanceState == 1 ? true : false} /></View>
						}
					</View>
				</View>
			</View>
		)
	}
}
export default Index;