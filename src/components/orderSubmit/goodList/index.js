import Taro, { Component } from '@tarojs/taro'
import { View, Text,Block,Image} from '@tarojs/components'

import { set as setGlobalData, get as getGlobalData } from '../../../static/js/global_data'
import Checkboxbar from '../../../components/checkboxbar'

import './index.scss'

// 我的订单、必备工具
class Index extends Component {
	static options = {
		addGlobalClass: true
	}
	static defaultProps = {
	}
	constructor() {
		super(...arguments)
		this.state = {
			domainImg: getGlobalData("domainImg"),
		};
	}
	render() {
		return (
			<View className='goodList'>
				<View className='weui-cell weui-cell-p0' onClick={this.props.onShowPopGoodList}>
					<View className='weui-cell__bd '>
						<View className='item-img-box'>
							{this.props.goodlist&&this.props.goodlist.map((item,i) => {
								return (
									<Block for={i} key={i}>
										{this.state.domainImg &&
											<Image src={this.state.domainImg + item.C_MAINPICTURE} className='item-img'></Image>
										}
									</Block>
								)
							})}
						</View>
					</View>
					<View className='weui-cell__ft weui-cell__ft_in-access item-img-more'>
						<Text>共{this.props.totalNum}件</Text>
					</View>
				</View>
			</View>
		)
	}
}
export default Index;