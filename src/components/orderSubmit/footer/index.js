import Taro, { Component } from '@tarojs/taro'
import { View, Text,Block} from '@tarojs/components'

import { set as setGlobalData, get as getGlobalData } from '../../../static/js/global_data'

import './index.scss'

// 我的订单、必备工具
class cartbar extends Component {
	static options = {
		addGlobalClass: true
	}
	static defaultProps = {
		goodType: '',
		orderprice: 0,
		orderIntegral: 0,
		postmoney: 0,
	}
	constructor() {
		super(...arguments)
		this.state = {};
	}
	render() {
		let {goodType, orderprice, orderIntegral, postmoney} = this.props
		return (
			<View className='footer'>
				<View className='weui-cell'>
					<View className='weui-cell__bd'>
						{goodType == '' && <View className='text-big text-sub'>实付款：{orderprice}</View>}
						{/* 积分兑换 */}
						{goodType == 'exchange' && 
							<Block>
								<View className='text-default'>实扣积分：<Text className='text-sub pr10'>{orderIntegral} </Text> 积分</View>
								<View className='text-small text-gray'>另需配送费：<Text className='text-sub pr10'>{postmoney} </Text> 元</View>
							</Block>
						}
						{/* 拼团 */}
						{goodType == 'pintuan' && <View className='text-big text-sub'>实付款：{orderprice}</View>}
					</View>
					<View className='weui-cell__ft'>
						<View className='weui-btn settle-btn' onClick={this.props.onFootBtn.bind(this,'addOrder')}>提交订单</View>
					</View>
				</View>
                
			</View>
		)
	}
}
export default cartbar;