import Taro, { Component } from '@tarojs/taro'
import { View, Text,Block,} from '@tarojs/components'

import { set as setGlobalData, get as getGlobalData } from '../../../static/js/global_data'

import './index.scss'

class Index extends Component {
	static options = {
		addGlobalClass: true
	}
	static defaultProps = {
		posmethod: null
	}
	constructor() {
		super(...arguments)
		this.state = {
		};
	}
	navTo(){
		Taro.navigateTo({url: `/pages/sendMethod/sendMethod`})
	}
	render() {
		let {posmethod} = this.props
		return (
			<View className='mosmethod weui-cell'>
				<View className='weui-cell__bd'>
					<View>支付配送和地址</View>
				</View>
				<View className='weui-cell__ft weui-cell__ft_in-access text-small' onClick={this.navTo}>
					{posmethod&&posmethod.map((item,i) => {
						return (
							<Block for={i} key={i}>
									{item.n_isdefault == '1'&&
										<Block>
											<View>{item.c_psname} + 在线支付</View>
											{this.props.postime && <View>营业时间：{this.props.postime}</View>}
										</Block>
									}
							</Block>
						)
					})}
				</View>
			</View>
		)
	}
}
export default Index;