
import Taro, { Component } from '@tarojs/taro'
import { View, Image } from '@tarojs/components'

import './index.scss'

import { get as getGlobalData } from '../../../static/js/global_data'
import List from '../../list'
// import shopInfoDefault from '../../../assets/img/shopInfo-default.jpg'

class Index extends Component {
	static options = {
		addGlobalClass: true
	}
	static defaultProps = {
		proData: null,
		modelId: null,
		btnGroup: 1, // 1: 有按钮，0： 没按钮
	}
	constructor() {
		super(...arguments)
	}
	state = {
		shopInfo: getGlobalData('shopInfo'),
		domainImg: getGlobalData('domainImg')
	}
	componentWillMount() { 
		this.setState({
			shopInfo: getGlobalData('shopInfo').model
		})
	}

	componentDidMount() { }
	onAddress() {
		const { shopInfo } = this.state
		Taro.openLocation({
			name: shopInfo.c_name,
			address: shopInfo.c_address,
			latitude: shopInfo.n_dimension,
			longitude: shopInfo.n_longitude,
		})
	}
	onTell(pone) {
		//拨打电话
		Taro.makePhoneCall({
			phoneNumber: pone
		})
	}
	render() {
		const { proData,modelId,btnGroup } = this.props
		const { shopInfo, domainImg } = this.state;
		return (
			<Block>
				{modelId == '12' ? <View className={`shop-info ${proData.isbtm == 1 ? 'bottomLine' : ''}`}>
					<View className='weui-cell'>
						<View className='weui-cell__bd'>
							<View className='shop-name'>{shopInfo.c_name}</View>
							<View className='sale-time'>营业：{shopInfo.c_house}</View>
						</View>
						<View className='weui-cell__ft'>
							<Image className='shop-logo' src={domainImg + shopInfo.c_logo} mode='widthFix'></Image>
						</View>
					</View>
					{shopInfo.c_remark ? <View className='shop-ad'>{shopInfo.c_remark}</View> : ''}
					<View className='shop-cont'>
						<List onLyClick={this.onAddress.bind(this)} icon='icon-address' title={shopInfo.c_address} />
						<List onLyClick={this.onTell.bind(this, shopInfo.c_phone)} icon='icon-tel' title={shopInfo.c_phone} />
						<List icon='icon-send' title={`${shopInfo.c_deliver}期间配送`} rightIcon={false} />
					</View>
				</View> : ''}
				{modelId == '24' ?  <View className={`shop-info2 ${proData.isbtm == 1 ? 'bottomLine' : ''}`}>
					<View className='aptitude-img'>
						{shopInfo.aptitude != '' && <Image mode='widthFix' className='img' src={domainImg + shopInfo.aptitude}></Image>}
					</View>
					<View className={`shop-info-box ${shopInfo.aptitude != '' ? 'shop-info-box-mt' : ''}`}>
						<View className='weui-cell'>
							<View className='weui-cell__bd'>
								<View className='shop-name'>{shopInfo.c_name}</View>
							</View>
							<View className='weui-cell__ft'>
								<View className='iconTel' onClick={this.onTell.bind(this, shopInfo.c_phone)}><Text className='iconfont icon-tel'></Text></View>
							</View>
						</View>

						<View className='shop-cont'>
							<List icon='icon-time' title={`营业时间：${shopInfo.c_house}`} rightIcon={false}  />
							<List onLyClick={this.onAddress.bind(this)} icon='icon-address' title={`店铺地址：${shopInfo.c_address}`} />
							<List onLyClick={this.onTell.bind(this, shopInfo.c_phone)} icon='icon-tel' title={`服务热线：${shopInfo.c_phone}`}/>
						</View>
						{shopInfo.c_remark ? <View className='shop-ad'>{shopInfo.c_remark}</View> : ''}
						{btnGroup == 1 ? <View className='weui-cell weui-cell-none btn-group'>
							<Button className='btn-mall btn'>进入商城</Button>
							<Button className='btn-subscribe btn'>即刻预约</Button>
						</View> : ''}
					</View>
				</View> : ''}
			</Block>
		)
	}
}
export default Index;