
import Taro, { Component } from '@tarojs/taro'
import { View, Image, Text } from '@tarojs/components'
import './index.scss'

import api from '../../../service/api'
import until from '../../../untils/lyUntils'
import { get as getGlobalData } from '../../../static/js/global_data'

class Index extends Component {
	static defaultProps = {
		killData: null
	}
	constructor() {
		super(...arguments)
	}
	state = {
		seckillDate: {
			d: "00",
			h: "00",
			m: "00",
			s: "00"
		},

		item: [],
		imgUrl: getGlobalData('domainImg'),
		activeItem: null,
		isbtm: 0
	}
	componentWillMount() {
		this.getItem()
	}

	componentDidMount() { }

	componentWillUnmount() { }

	componentDidShow() { }

	componentDidHide() { }
	getItem() {
		clearInterval(this.timeer)
		const { killData } = this.props;
		let str = 'gid&ShopNo';
		//上传的gids数量是多少  则pagesize就等于多少
		var upData = {
			bNo: 701,
			gid: killData.goodsId,
			isGoodsImg: "0",
			mebno: getGlobalData('C_MB_NO'),//用户信息id
			ShopNo: getGlobalData('SHOP_NO'),//店铺编号
			isactive: 1,
			isgroupshop: "",
			N_LPTCL: "",
		};
		api.get(str, upData).then(res => {
			if (res.code === "100") {
				let acts = JSON.parse(res.salemodel)
				acts.map((val) => {
					if (val.SaleTypeNo == 2) {
						this.setState({
							activeItem: val
						})
						this.setKillDate(val.SaleEndTime)
					}
				})
				this.setState({
					item: res.goods,
					isbtm: killData.isbtm
				})
			}
		}, (err) => {
			Taro.showToast({
				title: err.errMsg,
				icon: 'none',
				duration: 2000
			})
		})
	}
	setKillDate(endTime) {//活动倒计时
		let diffTime = until.getKillTime(endTime);
		const set = (d) => {
			this.setState({
				seckillDate: until.formatDuring(d)
			})
		}
		set(diffTime)
		this.timeer = setInterval(() => {
			set(diffTime)
			diffTime -= 1000
		}, 1000);
	}
	pageTo(gid) {
		Taro.navigateTo({
			url: `../goodsDatil/index?goodsId=${gid}&SaleTypeNo=2`
		})
	}
	render() {
		const { item, imgUrl, seckillDate, activeItem, isbtm } = this.state;
		return (
			<View className={isbtm == 1 ? 'marginBottom' : ''}>
				{
					item.length > 0 ? item.map((val, i) => (
						<View className='skill-goods' key={'skill' + i}>
							<View className='skill-goods-info-1'>
								<View className='skill-goods-pic'>
									<Image src={imgUrl + val.C_MAINPICTURE} mode='widthFix'></Image>
									<View>{val.GOODS_NAME}</View>
								</View>
								<View className='skill-goods-info'>
									<View className='skill-title'>活动倒计时</View>
									<View className='skill-goods-date'>
										<Text className='skill-bg'>{seckillDate.d}</Text>
										<Text>天</Text>
										<Text className='skill-bg'>{seckillDate.h}</Text>
										<Text>时</Text>
										<Text className='skill-bg'>{seckillDate.m}</Text>
										<Text>分</Text>
										<Text className='skill-bg'>{seckillDate.s}</Text>
										<Text>秒</Text>
									</View>
									<View className='tc c-666 fs24'>截止当前还剩{activeItem.saledata.SaleStock ? ((activeItem.saledata.SaleStock * 100) / 100) : '0'}{val.C_UnitName ? val.C_UnitName : '件'}</View>
									<View onClick={this.pageTo.bind(this, val.ID)} className='skill-btn'>抢购</View>
									<View className='tc c-999 fs24 mt5' style='margin-top: 5px;'>活动期限：
										{
											activeItem ? (
												<Text>{activeItem.SaleStartTime.split(" ")[0]}~{activeItem.SaleEndTime.split(" ")[0]}</Text>
											) : <Text>活动未开始</Text>
										}
									</View>
								</View>
							</View >
						</View >
					)) : <View className='no-data'>本店无此商品</View>
				}
			</View >
		)
	}
}
export default Index;