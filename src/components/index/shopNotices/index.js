import Taro, { Component } from '@tarojs/taro'
import { View, Swiper, SwiperItem, Image, Block } from '@tarojs/components'

import './index.scss'

import { set as setGlobalData, get as getGlobalData } from '../../../static/js/global_data'
import NoData from '../../noData'

import article from '../../../service/article'
import base from '../../../static/js/base'


class Index extends Component {
	static options = {
		addGlobalClass: true
	}
	static defaultProps = {
		modelId: '',
		data: null,
	}
	constructor() {
		super(...arguments)
	}
	state = {
		newDetail: {},
		ntid: ''
	}
	componentWillMount(){
		let {data,modelId} = this.props
		const that = this;
		that.state.ntid = data.id
		article.getNewDetail(that,(res) => {
			that.setState({
				newDetail: res.sys_notice
			})
		})
	}
	
	// 点击跳转页面
	comNavTo(id){
		Taro.navigateTo({
			url: `/pages/articleDetail/index?ntid=${id}`
		})
	}
	render() {
		let {newDetail} = this.state
		let {data, modelId} = this.props
		return ( 
			<View className={`diyShopNotices ${data.isbtm == 1 ? 'bottomLine' : ''}`} onClick={this.comNavTo.bind(this,data.id)}>
				<View className='weui-cell'>
					<View className='weui-cell__hd mr20'>
						<Text className='iconfont icon-notice'></Text>
					</View>
					<View className='weui-cell__bd'>
						<View>{newDetail.C_SN_TITLE}</View>
					</View>
				</View>
			</View>
		)
	}
}
export default Index;