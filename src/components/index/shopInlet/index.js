
import Taro, { Component } from '@tarojs/taro'
import { View, Button } from '@tarojs/components'
import './index.scss'


class Index extends Component {
	static defaultProps = {
		data: null
	}
	constructor() {
		super(...arguments)
	}
	state = {

	}
	componentWillMount() { }

	componentDidMount() { }

	componentWillUnmount() { }

	componentDidShow() { }

	componentDidHide() { }
	linkToPage() {
		//跳转到购物车页面
		/* Taro.navigateTo({
			url: ''
		}) */
	}
	render() {
		const { data } = this.props
		return (
			data ? (
				<View className={`btn-link ${data.isbtm == 1 ? 'marginBottom' : ''}`}>
					{
						data ? <Button onClick={this.linkToPage.bind(this)} style={{ backgroundColor: data.color, color: '#fff' }}>{data.text}</Button> : ''
					}
				</View>
			) : ''
		)
	}
}
export default Index;