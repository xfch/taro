
import Taro, { Component } from '@tarojs/taro'
import { View, Image, Text } from '@tarojs/components'
import './index.scss'
import until from '../../../untils/lyUntils'
import {set as setGlobalData, get as getGlobalData } from '../../../static/js/global_data'

class Index extends Component {
	static options = {
		addGlobalClass: true
	}
	static defaultProps = {
		page: '',
		pData: {
			CATEGORY_NAME: "",
			C_MAINPICTURE: "",
			C_UnitName: "",
			D_SHELF_DATE: null,
			GOODS_ATTR: 0,
			GOODS_CODE: "",
			GOODS_NAME: "",
			ID: 0,
			MebIntegral: "",
			N_ISFM: 0,
			N_ISLP: 0,
			N_LPTC: 0,
			N_YCOUNT: 0,
			SALE_PRICE: 0,
			STOCK: 0,
			YSale_Price: 0,
			activejson: "[]",
			c_cxinfo: "",
			n_iscz: 0,
			remindzt: "库存充足",
		}
	}
	constructor() {
		super(...arguments)
	}
	state = {
		imgUrl: getGlobalData('domainImg'),
		activeInfo: null,
		selected: false,
		subscribeProject: []
	}
	componentWillMount() { }

	componentDidMount() {
		let { pData: { actTypes } } = this.props;
		let actJson = actTypes;
		if (actJson && actJson.length > 0) {
			let act = []
			actJson.map((val) => {
				act.push({
					actName:val.actName,
					actId:val.actId
				})
			})
			this.setState({
				activeInfo: act.length > 0  ? act : null
			})
			
		}
	}

	pageGo() {
		let { pData,page } = this.props;
		let { activeInfo } = this.state;

		let url = `../goodsDatil/index?goodsId=${pData.goodsDefaultId}&SaleTypeNo=${activeInfo  ? activeInfo : 0}&goodsAttr=${pData.goodsAttr}`
		if(page == ''){
			Taro.navigateTo({
				url: url
			})
		}else if(page == 'subscribeProject'){ // 预约 - 挑选商品/服务
			let subscribeProject = getGlobalData('subscribeProject') ? getGlobalData('subscribeProject') : []
			let curr = {"N_GoodsId": pData.goodsDefaultId,"GOODS_NAME": pData.goodsName,"N_Num": 1}
			subscribeProject.push(curr)
			setGlobalData('subscribeProject', subscribeProject)
			console.log("getGlobalData('subscribeProject')--------------")
			console.log(getGlobalData('subscribeProject'))
			this.setState({select: !this.state.select})
			// [{"N_GoodsId":106004,"GOODS_NAME":"脚部护理系列D","N_Num":1},{"N_GoodsId":77913,"GOODS_NAME":"脚部护理系列C","N_Num":1}]
			// console.log(pData)
			// console.log(123)
			// this.props.onSelectProject(this)
		}
	}
	render() {
		const { pData,page } = this.props;
		const { imgUrl, activeInfo,select } = this.state;
		// const promotion = activeInfo ? (
		// 	<View className='money'>
		// 		<View>
		// 			<Text className='moneyNum'>￥{(activeInfo.saledata.SalePrice * 1).toFixed(2)}</Text>
		// 			<Text className='goods-list-d-price'>原价￥{(pData.oldPrice * 1).toFixed(2)}</Text>
		// 		</View>
		// 		<Text className={`moneyIcon iconfont ${pData.goodsAttr == 1 ? 'icon-cart' : 'icon-shouchang'}`}></Text>
		// 	</View>
		// ) : ''
		// const integral = activeInfo ? (
		// 	<View className='money'>
		// 		<View>
		// 			<Text className='moneyNum'>￥{(activeInfo.saledata.ReallSalePrice * 1).toFixed(2)}</Text>
		// 			<Text className='godos-list-c-price'>+{activeInfo.saledata.N_IntegralCount}积分</Text>
		// 		</View>
		// 		<Text className={`moneyIcon iconfont ${pData.goodsAttr == 1 ? 'icon-cart' : 'icon-shouchang'}`}></Text>
		// 	</View>
		// ) : ''
		return (
			<View>
				{
					imgUrl ? (
						<View className='productVw' onClick={this.pageGo.bind(this)}>
							{
								activeInfo ? <View className='actType'>{activeInfo[0].actName}</View> : ''
								// activeInfo ? <View className='actType'>{until.getActType(activeInfo.actName)}</View> : ''
								/* 如果有活动，则通过活动类型去获取活动名称 */
							}
							<View className='img-box'>
								{page == 'subscribeProject' && select && <View className='img-mask'><View className='mask-text'>已选</View></View>}
								<Image className='img' mode='widthFix' src={imgUrl + pData.imageUrl}></Image>
							</View>
							<View className='pro'>
								<View className='proName'>{pData.goodsName}</View>
								{
									<View className='money'>
										<Text className='moneyNum'>￥{(pData.salePrice * 1).toFixed(2)}</Text>
										{page == '' && <Text className={`moneyIcon iconfont ${pData.goodsAttr == 1 ? 'icon-cart' : 'icon-shouchang'}`}></Text>}
									</View>
									// /* 促销 */
									// activeInfo && activeInfo.SaleTypeNo == 1 ? promotion :
									// 	/* 秒杀 */
									// 	activeInfo && activeInfo.SaleTypeNo == 2 ? promotion :
									// 		/* 积分 */
									// 		activeInfo && activeInfo.SaleTypeNo == 12 ? promotion :
									// 			/* 打折 */
									// 			activeInfo && activeInfo.SaleTypeNo == 15 ? promotion :
									// 				/* 打折 */
									// 				activeInfo && activeInfo.SaleTypeNo == 5 ? integral : (
									// 					<View className='money'>
									// 						<Text className='moneyNum'>￥{(pData.salePrice * 1).toFixed(2)}</Text>
									// 						{page == '' && <Text className={`moneyIcon iconfont ${pData.goodsAttr == 1 ? 'icon-cart' : 'icon-shouchang'}`}></Text>}
									// 					</View>
									// 				)
								}
							</View>
						</View >
					) : ''
				}
			</View>
		)
	}
}
export default Index;