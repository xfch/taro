import Taro, { Component } from '@tarojs/taro'
import { View, Swiper, SwiperItem, Image, Block } from '@tarojs/components'

import './index.scss'

import { set as setGlobalData, get as getGlobalData } from '../../../static/js/global_data'

import article from '../../../service/article'
import comNavTo from '../../../service/comNavTo'
import base from '../../../static/js/base'

class Index extends Component {
	static options = {
		addGlobalClass: true
	}
	static defaultProps = {
		modelId: '',
		data: null,
	}
	constructor() {
		super(...arguments)
	}
	state = {
		newDetail: {},
		domainImg: getGlobalData('domainImg'),
		richTxt: null,
	}
	componentWillMount(){
		let {data} = this.props
		const that = this;
		that.state.ntid = data.messageId
		base.showLoading()
		article.getNewDetail(that,(res,richTxt) => {
			that.setState({
				newDetail: res.sys_notice,
                richTxt: richTxt
			})
			Taro.hideLoading()
		})
	}
	// 点击跳转页面
	comNavTo(modelId,linkType,linkParams){
		comNavTo.comNavTo(modelId,linkType,linkParams)
	}
	render() {
		let {newDetail,domainImg} = this.state
		let { data, modelId} = this.props
		return ( 
			<View className='mainDiyNew'>
				<View className='new-title'>{newDetail.C_SN_TITLE}</View>
				<View className='ac'>
					{newDetail.C_SN_PIC && newDetail.C_SN_PIC != '' && <Image mode='widthFix' src={domainImg + newDetail.C_SN_PIC} onClick={this.comNavTo.bind(this,modelId,null,data.messageId)}></Image>}
					{/* <Image mode='widthFix' src={domainImg + newDetail.C_SN_PIC} onClick={this.comNavTo.bind(this,modelId,null,data.messageId)}></Image> */}
				</View>
				{modelId == 7 && <View><RichText className='goodsRichText' nodes={richTxt}></RichText></View>}
				{modelId == 28 && 
					<View className='weui-cell weui-cell-none'>
						<View className='weui-cell__bd'><text>简介：{newDetail.C_SN_DES}</text></View>
						<View className='weui-cell__ft'><text>日期：{newDetail.D_OP_DATE}</text></View>
					</View>
				}
			</View>
		)
	}
}
export default Index;