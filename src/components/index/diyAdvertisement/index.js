
import Taro, { Component } from '@tarojs/taro'
import { View, Image } from '@tarojs/components'
import './index.scss'

import common from '../../../service/common'

class Index extends Component {
	static defaultProps = {
		data: null,
		modelId: '27',
	}
	constructor() {
		super(...arguments)
	}
	state = {

	}

	// 点击跳转页面
	comNavTo(modelId,linktype,linkparam){
		common.comNavTo(modelId,linktype,linkparam)
	}

	render() {
		const {modelId, data } = this.props;
		return (
			<View>
				<View className={`diy-ad-pic ${data.isbtm == 1 ? ' marginBottom' : ''}`}  onClick={this.comNavTo.bind(this,modelId,data.linkType,data.linkparam)}>
					<Image src={data.img} mode='widthFix'></Image>
				</View>
			</View >
		)
	}
}
export default Index;