
import Taro, { Component } from '@tarojs/taro'
import { View, Text, Input } from '@tarojs/components'
import './index.scss'

import ActionSheet from '../../actionSheet'

class Index extends Component {
	static options = {
		addGlobalClass: true
	}
	static defaultProps = {
		type: '',/* 传入skip跳转  传入shop带店铺选择   不传则是输入*/
		pageUrl: '',//跳转时需要传入跳转的页面链接
		textVal: '搜索商品',
		value: '',
		placeholder: '请输入搜索内容',
		onSearInput: () => { }
	}
	constructor() {
		super(...arguments)
	}
	static state = {
		shops: [],//所有店铺
		nowShop: ''//当前店铺
	}
	componentWillMount() { }

	componentDidMount() { }

	componentWillUnmount() { }

	componentDidShow() { }

	componentDidHide() { }
	onSearch() {//跳转搜索页面
		const { type, pageUrl } = this.props;
		if (type === 'skip' || type === 'shop') {
			Taro.navigateTo({ url: pageUrl })
		}
	}
	onSInput(e) {//输出搜索内容
		const { type } = this.props;
		if (type != '') {
			this.props.onSearInput(e);
		}
	}
	onChangeShop() {//拉起店铺列表
		this.sheet.setItem([
			{ val: '第一个', sign: 1 },
			{ val: '第二个', sign: 2 },
			{ val: '第二个', sign: 2 },
			{ val: '第二个', sign: 2 },
			{ val: '第二个', sign: 2 },
			{ val: '第二个', sign: 2 },
			{ val: '第二个', sign: 2 },
			{ val: '第二个', sign: 2 }
		])
	}
	getSheet(o, type) {
		if (type === 0) {
			this.sheet = o;
		} else {
			//得到用户选择的项
			console.log(o)
		}
	}
	render() {
		const { type, textVal, value, placeholder } = this.props;
		return (
			<View>
				<View className='shop'>
					{
						type === 'shop' ? (
							<View className='change-shop icon-down' onClick={this.onChangeShop.bind(this)}>
								换店
						<Text className='iconfont icon-arrow-down'></Text>
							</View>
						) : ''
					}
					<View className={`search-container ${type === 'skip' || type === 'shop' ? 'flexCenter' : 'flexStart'}`} onClick={this.onSearch.bind(this)}>
						<Text className='iconfont icon-seach searchText'></Text>
						{
							type === 'skip' || type === 'shop' ? <View>{textVal}</View> :
								<Input className='searchInput' onInput={this.onSInput.bind(this)} value={value} placeholder={placeholder} />
						}
					</View>
				</View>
				<ActionSheet onGetSheet={this.getSheet.bind(this)} />
			</View>
		)
	}
}
export default Index;