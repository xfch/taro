import Taro, { Component } from '@tarojs/taro'
import { View, Swiper, SwiperItem, Image, Block } from '@tarojs/components'

import './index.scss'

import { set as setGlobalData, get as getGlobalData } from '../../../static/js/global_data'
import NoData from '../../noData'

import article from '../../../service/article'
import base from '../../../static/js/base'


class Index extends Component {
	static options = {
		addGlobalClass: true
	}
	static defaultProps = {
		modelId: '',
		data: null,
		articleLists: []
	}
	constructor() {
		super(...arguments)
	}
	state = {
		articleList: [],
		domainImg: getGlobalData('domainImg'),
		pageindex: 1,
		hasEd: false,
		pagesize: 10,
		sncategory: '',
		isLoading: true,
		hasView: false
	}
	componentWillMount(){
		let {data,modelId} = this.props
		const that = this;
		if(modelId == ''){
			this.setState({
				hasView: true
			})
		}else{
			that.state.pagesize = data.newsCount
			that.state.sncategory = data.messageCId
			article.getNewsList(that,(articleList) => {
				that.setState({
					articleList: articleList,
					hasView: true,
					isLoading: false
				})
			})
		}
	}
	
	// 点击跳转页面
	comNavTo(id){
		Taro.navigateTo({
			url: `/pages/articleDetail/index?ntid=${id}`
		})
	}
	render() {
		let {hasView,articleList,domainImg,isLoading} = this.state
		let {articleLists,data, modelId} = this.props
		return ( 
			<View className='mainDiyArticleList'>
				{hasView && 
					<View className={`articleList articleList${modelId} ${data.isbtm == 1 ? 'bottomLine' : ''}`}>
						{modelId != '' && <View className={`list${modelId}`}>
							{articleList.length > 0 && articleList.map((item,i) => (
								<View key={`list${modelId}${i}`} className={`weui-cell${modelId} ${modelId == 8 ? 'weui-cell' : ''}`} onClick={this.comNavTo.bind(this,item.ID)}>
									<View className={`weui-cell__hd${modelId} ${modelId == 8 ? 'weui-cell__hd' : ''}`}>
										{!item.C_SN_PIC || item.C_SN_PIC == '' ? <View className='item-img bg-img'></View> : <Image mode='aspectFill' className='item-img' src={domainImg + item.C_SN_PIC}></Image>}
									</View>
									<View className={`weui-cell__bd${modelId} ${modelId == 8 ? 'weui-cell__bd' : ''}`}>
										<View className='text-big ellipsis'>{item.C_SN_TITLE}</View>
										{modelId == 8 && <View className='clamp text-gray item-des'>{item.C_SN_DES}</View>}
										{modelId == 8 && <View className='text-gray text-small item-date'>{item.D_OP_DATE}</View>}
									</View>
								</View>
							))}
						</View>}
						{/* 文章列表 */}
						{modelId == '' && articleLists.length > 0 && articleLists.map((item,i) => (
							<View key={'list' + i} className='weui-cell' onClick={this.comNavTo.bind(this,item.ID)}>
								<View className='weui-cell__hd'>
									{!item.C_SN_PIC || item.C_SN_PIC == '' ? <View className='item-img bg-img'></View> : <Image mode='aspectFill' className='item-img' src={domainImg + item.C_SN_PIC}></Image>}
								</View>
								<View className='weui-cell__bd'>
									<View className='text-big ellipsis'>{item.C_SN_TITLE}</View>
									<View className='clamp text-gray item-des'>{item.C_SN_DES}</View>
									<View className='text-gray text-small item-date'>{item.D_OP_DATE}</View>
								</View>
							</View>
						))}
					</View>
				}
				{modelId != '' && isLoading && <NoData typy='3'></NoData>}
			</View>
		)
	}
}
export default Index;