
import Taro, { Component } from '@tarojs/taro'
import { View } from '@tarojs/components'
import './index.scss'
import VwItem from '../productVwItem'
import VhItem from '../productVhItem'

import api from '../../../service/api'
import { get as getGlobalData } from '../../../static/js/global_data'

class Index extends Component {
	static defaultProps = {
		type: 'vw',
		proData: null,
		isFalls: false,
		modelId: '',
		cid: '',
		page: '',
		onGetThis: () => { }
	}
	constructor() {
		super(...arguments)
	}
	state = {
		item: [],
		pageindex: 1,
		isLoading: false,
		hasMore: true,//是否还有更多
		pagesize: 10
	}
	componentWillMount() {
		this.props.onGetThis(this)
	}
	componentDidMount() {
		this.setUpData()
	}
	setUpData(e) {
		if(e){
			this.setState({
				item: [],
				hasMore: true,
				pageindex: 1,
			},() => {
				this.setParams(e)
			})
		}else{
			this.setParams()
		}
	}
	// 设置参数
	setParams(e){
		const { proData, isFalls,cid } = this.props;
		if (isFalls) { //如果是瀑布流
			let { pageindex, pagesize, hasMore } = this.state;
			// let upData = {
			// 	bNo: '702b',
			// 	pageindex: pageindex,
			// 	pagesize: pagesize,
			// 	cid: cid,
			// 	gids: '',
			// 	name: '',
			// 	cname: proData ? proData.cname : '',
			// 	sort: 't.ID desc',
			// 	sortpart: '',
			// 	desc: '',
			// 	mebno: getGlobalData('C_MB_NO'),//用户信息id
			// }
			let upData = {
				bNo: 'goods002',
				ShopNo: getGlobalData('SHOP_NO'),
				pageindex: pageindex,
				pagesize: pagesize,
				cid: cid,
				mebno: getGlobalData('C_MB_NO'),//用户信息id
				gids: ''
			}
			if (this.state.hasMore) {
				this.getData(upData,e)
			}else{
				Taro.showToast({ title: '没有更多了...', icon: 'none', duration: 500 })
			}
		} else {
			// let upData = {
			// 	bNo: '702b',
			// 	pageindex: 1,
			// 	pagesize: proData ? proData.gids.split(",").length : '10',  //上传的gids数量是多少  则pagesize就等于多少
			// 	cid: cid,
			// 	gids: proData ? proData.gids : '',
			// 	name: '',
			// 	cname: proData ? proData.cname : '',
			// 	sort: 't.ID desc',
			// 	sortpart: '',
			// 	desc: '',
			// 	mebno: getGlobalData('C_MB_NO') || '',//用户信息id
			// }
			let upData = {
				bNo: 'goods002',
				ShopNo: getGlobalData('SHOP_NO'),
				pageindex: 1,
				pagesize: proData ? proData.gids.split(",").length : '10',  //上传的gids数量是多少  则pagesize就等于多少
				cid: cid,
				mebno: getGlobalData('C_MB_NO'),//用户信息id
				gids: ''
			}
			this.getData(upData,e)
		}
	}

	getData(upData,e) {
		let str = 'ShopNo&pageindex&pagesize&cid&mebno&gids';
		const { isFalls } = this.props;
		let { isLoading } = this.state;
		if (isLoading) {
			Taro.showToast({ title: '数据加载中,请稍后...', icon: 'none', duration: 800 })
			return false
		}
		this.setState({
			isLoading: true
		})
		api.get(str, upData).then(res => {
			console.log(res,"goods002-----------------------------------------------")
			if(!res.goodsList){res.goodsList = []}
			res.goodsList.map((item) => {
				item.activejson = JSON.parse(item.activejson)
			})
				if (res.code == 0) {
					const { item, pagesize } = this.state;
					let setD = {
						isLoading: false
					}
					let { pageindex } = this.state;
					if (isFalls) {
						let page = pageindex + 1
						setD['pageindex'] = page;
						setD['item'] = item.concat(res.data.goodsList);
					} else if(e){
						setD['item'] = res.data.goodsList;
					}else {
						setD['item'] = res.data.goodsList;
					}
					if (res.data.goodsList.length < pagesize) {
						setD['hasMore'] = false;
					}

					console.log(setD,"goods002-----------------------------------------------")
					this.setState(setD)
				}else if (res.code == '100') {
				const { item, pagesize } = this.state;
				let setD = {
					isLoading: false
				}
				let { pageindex } = this.state;
				if (isFalls) {
					let page = pageindex + 1
					setD['pageindex'] = page;
					setD['item'] = item.concat(res.goodsList);
				} else if(e){
					setD['item'] = res.goodsList;
				}else {
					setD['item'] = res.goodsList;
				}
				if (res.goodsList.length < pagesize) {
					setD['hasMore'] = false;
				}
				this.setState(setD)
			}else if(res.code == '101'){
				console.log(e)
				console.log(res.goodsList)
				this.setState({ isLoading: false ,hasMore: true, item: e ? res.goodsList : this.state.item })
			}else {
				Taro.showToast({ title: res.msg, icon: 'none', duration: 2000 })
			}
		}, (err) => {
			this.setState({ isLoading: false,hasMore: true })
			Taro.showToast({
				title: err.errMsg,
				icon: 'none',
				duration: 2000
			})
		})
	}
	
	render() {
		// console.log('productShow-------------------------')
		// console.log(this.props)
		const { item, isLoading, hasMore } = this.state;
		const { type, proData,page } = this.props;
		return (
			<View className={`productShow ${proData.isbtm == '1' ? 'marginBottom' : ''}`}>
				{
					item.length > 0 ? item.map((v, i) => {
						if (type == 'vw') {
							/* 横向展示 */
							return <VwItem pData={v} key={'show' + i} page={page} />
						} else if (type == 'vh') {
							/* 纵向展示 */
							return <VhItem pData={v} key={'show' + i} page={page} />
						}
					}) : ''
				}
				{(item.length <= 0 && !isLoading) ? <View className='no-data'>暂无商品</View> : ''}
				{isLoading ? <View className='no-data'>加载中</View> : ''}
				{item.length > 0&&!isLoading && hasMore ? <View className='no-data no-more'>没有更多数据了</View> : ''}
			</View >
		)
	}
}
export default Index;
/*
活动类型
1.限时促销
2.秒杀
3.满减
4.全场打折
5.积分活动
6.优惠卷
7.礼品卡
8.支付及会员
9.充值活动
1001.发送营业报告
1002.智能营销优惠卷
10.多人拼团
11.砍价
12.打折
13.第二件打折
15.梯度优惠

*/