
import Taro, { Component } from '@tarojs/taro'
import { View, Text, Image } from '@tarojs/components'

import './index.scss'

import comNavTo from '../../../service/comNavTo'

class Index extends Component {
	static options = {
		addGlobalClass: true
	}
	static defaultProps = {
		titleData: null,
		modelId: '30'
	}
	constructor() {
		super(...arguments)
	}
	state = {

	}
	// 点击跳转页面  商品栏目样式有跳转、栏目样式无跳转
	comNavTo(modelId,linkparam){
		if(linkparam != ''){
			comNavTo.comNavTo(modelId,null,linkparam)
		}
	}

	render() {
		const { titleData, modelId } = this.props
		// 信息 - 栏目样式1、栏目样式2 41、42 
		const modelId4 = titleData ? (
			<View className='weui-cell cell-border' onClick={this.comNavTo.bind(this,modelId, titleData.nid)}>
				<View className='weui-cell__hd'><View className='line' style={`background: ${titleData.brColor}`}></View></View>
				<View className='weui-cell__bd' style={`color: ${titleData.ftColor}`}>{titleData.title}</View>
				<View className='weui-cell__ft weui-cell__ft_in-access'>{modelId == '42' && <Text className='text-small'>更多</Text>}</View>
			</View>
		) : ''
		// 信息 - 栏目样式3
		const modelId43 = titleData ? (
			<View className='cell-border' onClick={this.comNavTo.bind(this,modelId, titleData.nid)}>
				<View class="weui-loadmore weui-loadmore_line" style={`border-color: ${titleData.color}`}>
					<View class="weui-loadmore__tips weui-loadmore__tips_in-line" style={`color: ${titleData.color}`}>{titleData.title}</View>
				</View>
			</View>
		) : ''

		// 公共组件 - 栏目样式1
		const modelId30 = titleData ? (
			<View className='weui-cell cell-border'>
				<View className='weui-cell__hd'><View className='line' style={`background: ${titleData.brcolor}`}></View></View>
				<View className='weui-cell__bd' style={`color: ${titleData.ftcolor}`}>{titleData.title}</View>
			</View>
		) : ''
		// 公共组件 - 栏目样式2
		const modelId47 = titleData ? (
			<View className='weui-cell cell-border'>
				<View className='weui-cell__bd ac'>
					<View className='text-big' style={`color: ${titleData.color}`}>{titleData.title}</View>
					<View className='text-small text-gray'>{titleData.subtitle}</View>
				</View>
			</View>
		) : ''
		// 公共组件 - 栏目样式3 
		const modelId48 = titleData ? (
			<View className='weui-cell cell-border'>
				<View className='weui-cell__hd'>
					<Image className='item48-img' src={titleData.img} mode='aspectFill'></Image>
				</View>
				<View className='weui-cell__bd'>
					<View className='text-big' style={`color: ${titleData.color}`}>{titleData.title}</View>
					<View className='text-small text-gray'>{titleData.subtitle}</View>
				</View>
			</View>
		) : ''
		
		// 公共组件 - 商品栏目 - 样式1  
		const modelId31 = titleData ? (
			<View className='enter1 enter' onClick={this.comNavTo.bind(this,modelId,titleData.name)}>
				<View className='tab-line' style={{ color: titleData.ftColor, borderLeft: ' 4px solid ' + titleData.brColor }}>{titleData.title}</View>
				<Text className='iconfont icon-arrow-right icon enter-color'></Text>
			</View>
		) : ''
		// 公共组件 - 商品栏目 - 样式2  
		const modelId32 = titleData ? (
			<View className='enter1 enter' onClick={this.comNavTo.bind(this,modelId,titleData.name)}>
				<View className='tab-line' style={{ color: titleData.ftColor, borderLeft: ' 4px solid ' + titleData.brColor }}>{titleData.title}</View>
				<View className='enter-color'>
					更多 <Text className='iconfont icon-arrow-right icon'></Text>
				</View>
			</View>
		) : ''
		// 公共组件 - 商品栏目 - 样式3  
		const modelId33 = titleData ? (
			<View className='enter enter1' onClick={this.comNavTo.bind(this,modelId,titleData.name)}>
				<View className='enter-line' style={{ borderBottom: '1px solid ' + titleData.color }}>
					<View className='enter-text' style={{ color: titleData.color }}>{titleData.title}</View>
				</View>
			</View>
		) : ''
		
		return (
			<View className={`universalTitle ${titleData.isbtm == 1 ? ' marginBottom' : ''}`}>
				{
					modelId == '41' || modelId == '42' ? modelId4 :
					modelId == '43' ? modelId43 :
					modelId == '30' ? modelId30 :
					modelId == '48' ? modelId48 : 
					modelId == '47' ? modelId47 :

					modelId == '31' ? modelId31 :
					modelId == '32' ? modelId32 :
					modelId == '33' ? modelId33 : ''
				}
				{/* {
					modelId == '41' || modelId == '42' ? modelId4 :
					modelId == '43' ? modelId43 :
					modelId == '30' ? modelId30 :
					modelId == '48' ? modelId48 : 
					modelId == '47' ? modelId47 :

					modelId == '31' ? modelId31 :
					modelId == '32' ? modelId32 :
					modelId == '33' ? modelId33 : ''
				} */}
			</View >
		)
	}
}
export default Index;