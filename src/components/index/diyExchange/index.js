import Taro, { Component } from '@tarojs/taro'
import { View, Swiper, SwiperItem, Image, Block } from '@tarojs/components'

import './index.scss'

import ExchangeItem from '../exchangeItem'

import common from '../../../service/common'
import comNavTo from '../../../service/comNavTo'

class Index extends Component {
	static options = {
		addGlobalClass: true
	}
	static defaultProps = {
		modelId: '26',
		proData: null,
	}
	constructor() {
		super(...arguments)
	}
	state = {
		pageindex: 1,
		hasList: false
	}
	componentWillMount(){
		const that = this;
		that.state.gids = that.props.proData.gids
		that.setState({
			gids: that.props.proData.gids
		})
		common.getExchangeList(that)
	}
	// 点击跳转页面
	comNavTo(modelId,linkType){
		comNavTo.comNavTo(modelId,linkType)
	}
	render() {
		let { hasList, goodList } = this.state
		let { proData, modelId} = this.props
		return ( 
			<View className='mainDiyExchange'>
				{proData && proData.banner && <View onClick={this.comNavTo.bind(this,modelId,'1')}><Image className='banner' mode='widthFix' src={proData.banner}></Image></View>}
				{hasList && <ExchangeItem modelId={modelId} goodList={goodList}></ExchangeItem>}
			</View>
		)
	}
}
export default Index;