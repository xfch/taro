
import Taro, { Component } from '@tarojs/taro'
import { View, Image, Text } from '@tarojs/components'
import './index.scss'

import ExchangeItem from '../exchangeItem'

import api from '../../../service/api'
import common from '../../../service/common'
import comNavTo from '../../../service/comNavTo'
import { get as getGlobalData } from '../../../static/js/global_data'


class Index extends Component {
	static defaultProps = {
		data: null,
		modelId: '',
	}
	constructor() {
		super(...arguments)
	}
	state = {
		dataitem: [],
		goodList: [],
		pageindex: 1,
	}
	componentWillMount() { }

	componentDidMount() {
		const that = this;
		let { data } = this.props;
		that.state.gids = data.gids
		common.getPintuanList(that)
	}
	// 点击跳转页面
	comNavTo(modelId,linkType){
		comNavTo.comNavTo(modelId,linkType)
	}
	render() {
		const {modelId, data } = this.props;
		const { goodList} = this.state;
		return (
			data ? (
				<View className={`${data.isbtm == 1 ? 'marginBottom' : ''}`}>
					{data.banner != '' && <Image className='groupImg' src={data.banner} mode='widthFix' onClick={this.comNavTo.bind(this,modelId,'1')}></Image> }
					<View>
						<ExchangeItem modelId={modelId} goodList={goodList}></ExchangeItem>
					</View>
				</View >
			) : ''
		)
	}
}
export default Index;