
import Taro, { Component } from '@tarojs/taro'
import { View, Swiper, SwiperItem, Image } from '@tarojs/components'
import './index.scss'

import { set as setGlobalData, get as getGlobalData } from '../../../static/js/global_data'

class Index extends Component {
	static options = {
		addGlobalClass: true
	}
	static defaultProps = {
		modelId: '',
		data: {
			isbtm: '0',
			text: '预约服务',
			color: "#ff0000",
		}
	}
	constructor() {
		super(...arguments)
	}
	state = {
		shopInfo: getGlobalData('shopInfo')
	}
	// 跳转
	comNavTo(){
		Taro.navigateTo({url: `/pages/subscribePeopleNum/index?n_yjhid=${this.state.shopInfo.model.n_yjhid}`})
	}

	render() {
		const {modelId, data } = this.props;
		return (
			<View className='main-subscribeEntry'>
				<View className={`p20 ${data.isbtm == 1 ? 'marginBottom' : ''}`}>
					<Button className='btn' style={`background-color: ${data.color}`} onClick={this.comNavTo}>{data.text}</Button>
				</View>
			</View>
		)
	}
}
export default Index;