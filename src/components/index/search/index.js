
import Taro, { Component } from '@tarojs/taro'
import { View, Icon, Input } from '@tarojs/components'
import './index.scss'

import api from '../../../service/api'
import common from '../../../service/common'


class Index extends Component {
	static defaultProps = {
		modelId: '',
		type: 'style',
		onSearchVal: () => {},
		onSearchIcon: () => {},
	}
	constructor() {
		super(...arguments)
	}
	state = { 
		
	}
	componentWillMount() { }

	componentDidMount() { }

	componentWillUnmount() { }

	componentDidShow() { }

	componentDidHide() { }
	// 搜索输入
	onInput(e){
		this.props.onSearchVal(e.detail.value)
	}

	// 点击跳转页面  
	comNavTo(modelId){
		common.comNavTo(modelId,null,null)
	}

	render() {
		let {modelId,type} = this.props
		return (
			<View className={`search-bar ${type == 'absolute' ? 'search-bar-absolute' : ''}`}>
				{type == 'style' || type == 'absolute' && 
					<View class='weui-search-bar' onClick={this.comNavTo.bind(this,modelId)}>
						<View className='search-center'>
							<Icon class="weui-icon-search" color={`${type == 'absolute' ? '#ccc' : 'white'}`} type="search" size="14"></Icon>
							<View class="weui-search-bar__text">搜索商品</View>
						</View>
					</View>
				}
				{type == 'input' && 
					<View class="weui-search-bar_input">
						<Input type="text" class="weui-search-bar__input" placeholder="搜索商品" onInput={this.onInput} value={this.props.searchVal} />
						<View className='icon-search' onClick={this.props.onSearchIcon}><Icon class="weui-icon-search" type="search" size="14"></Icon></View>
					</View>
				}
			</View >
		)
	}
}
export default Index;