
import Taro, { Component } from '@tarojs/taro'
import { View, Swiper, SwiperItem, Image, Block } from '@tarojs/components'

import './index.scss'

import { set as setGlobalData, get as getGlobalData } from '../../../static/js/global_data'

import api from '../../../service/api'
import comNavTo from '../../../service/comNavTo'
import base from '../../../static/js/base'


class Index extends Component {
	static options = {
		addGlobalClass: true
	}
	static defaultProps = {
		modelId: '',
		goodList: []
	}
	constructor() {
		super(...arguments)
	}
	state = {
		domainImg: getGlobalData('domainImg')
	}
	componentWillMount(){
		
	}
	// 点击跳转积分商品详情
	comNavTo(modelId,linktype,linkparam){
		linkparam = linkparam.N_GOODSID || linkparam.N_SKU_ID
		comNavTo.comNavTo(modelId,linktype,linkparam.toString())
	}
	render() {
		let { domainImg } = this.state
		let { modelId, goodList } = this.props
		let integralGood = false  // 积分商品
		if(modelId == '51' || modelId == '52' || modelId == '53' || modelId == '54' || modelId == '55' || modelId == '26'){
			integralGood = true
		}
		return ( 
			<View className='mainGiftList'>
				<View className='weui-cell'>
					{goodList.length > 0 && goodList.map((item,i) => (
						<View key={'goodlist' + i} className='list-item' onClick={this.comNavTo.bind(this,modelId,'0',item)}>
							<View className='items-img'><Image className={`item-img ${item.C_MAINPICTURE == '' ? 'item-img-none' : ''}`} src={domainImg + item.C_MAINPICTURE}></Image></View> 
							<View className='item-name ellipsis'>{item.GOODS_NAME}</View>
							{/* 积分 */}
							{integralGood && 
								<View className='weui-flex item-bottom'>
									<View className='weui-flex__item text-sub'>{item.N_IG_INTEGRAL}积分</View>
									<View className='item-cart'><Text className='iconfont icon-cart'></Text></View>
								</View>
							}
							{/* 拼团 */}
							{modelId == '61' && 
								<View className='weui-flex item-bottom'>
									<View className='weui-flex__item'>
										<Text className='text-sub text-big pr20'>￥{item.N_GSPrice.toFixed(2)}</Text>
										{item.N_BuyAllCount > 0 ? <Text className='text-gray text-little'>已拼{item.N_BuyAllCount}件</Text> : ''}
									</View>
									<View className='item-pt'>拼团</View>
								</View>
							}
						</View>
					))}
				</View>
			</View>
		)
	}
}
export default Index;