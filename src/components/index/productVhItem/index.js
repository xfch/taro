
import Taro, { Component } from '@tarojs/taro'
import { View, Image, Text } from '@tarojs/components'
import './index.scss'
import { get as getGlobalData } from '../../../static/js/global_data'


class Index extends Component {
	static options = {
		addGlobalClass: true
	}
	static defaultProps = {
		page: '',
		pData: []
	}
	constructor() {
		super(...arguments)
	}
	state = {
		imgUrl: getGlobalData('domainImg'),
		activeInfo: null
	}
	componentWillMount() { }

	componentDidMount() {
		let { pData: { actTypes } } = this.props;
		let actJson = actTypes;
		
		if (actJson && actJson.length > 0) {
			let act = []
			// let act = null;
			actJson.map((val) => {
				/* 如果是全场活动 则不做展示 3--全场活动中的满减*/
				// if (val.SaleTypeNo != 3) { act = val }
					act.push({
						actName:val.actName,
						actId:val.actId
					})
			})
			this.setState({
				activeInfo: act.length > 0 ? act : null
			})
			
		}
	}

	pageGo() {
		let { pData,page } = this.props;
		let { activeInfo } = this.state;
		let url = `../goodsDatil/index?goodsId=${pData.goodsDefaultId}&SaleTypeNo=${activeInfo ? activeInfo.actId : 0}&goodsAttr=${pData.goodsAttr}`
		if(page == ''){
			Taro.navigateTo({
				url: url
			})
		}else if(page == 'subscribeProject'){ // 预约 - 挑选商品/服务

		}
	}
	render() {
		const { pData,page } = this.props;
		const { imgUrl, activeInfo } = this.state;
		// const promotion = activeInfo ? (
		// 	<View className='money'>
		// 		<View>
		// 			<Text className='moneyNum'>￥{(activeInfo.saledata.SalePrice * 1).toFixed(2)}</Text>
		// 			<Text className='goods-list-d-price'>原价￥{(pData.oldPrice * 1).toFixed(2)}</Text>
		// 		</View>
		// 		<Text className={`moneyIcon iconfont ${pData.goodsAttr == 1 ? 'icon-cart' : 'icon-shouchang'}`}></Text>
		// 	</View>
		// ) : ''
		// const integral = activeInfo ? (
		// 	<View className='money'>
		// 		<View>
		// 			<Text className='moneyNum'>￥{(activeInfo.saledata.ReallSalePrice * 1).toFixed(2)}</Text>
		// 			<Text className='godos-list-c-price'>+{activeInfo.saledata.N_IntegralCount}积分</Text>
		// 		</View>
		// 		<Text className={`moneyIcon iconfont ${pData.goodsAttr == 1 ? 'icon-cart' : 'icon-shouchang'}`}></Text>
		// 	</View>
		// ) : ''
		return (
			<View>
				{
					imgUrl ? (
						<View className='proVhBox' onClick={this.pageGo.bind(this)}>
							<View className='proVhBoxLeft'>
								<View className='goods-pic'>
									<Image className='img' src={imgUrl + pData.imageUrl} mode='widthFix'></Image>
								</View>
							</View>
							<View className='proVhBoxRight'>
								<View className='goods-info'> {pData.goodsName} </View >
								<View className='act-label'>
									{
										activeInfo  ? activeInfo.map((v, i) => {
												return (<Text>{activeInfo[i].actName}</Text>)
										}) : ''
									}
								</View>
								{
									<View className='money'>
										<Text className='moneyNum'>￥{(pData.salePrice * 1).toFixed(2)}</Text>
										{page == '' && <Text className={`moneyIcon iconfont ${pData.goodsAttr == 1 ? 'icon-cart' : 'icon-shouchang'}`}></Text>}
									</View>
									// /* 分时促销 */
									// activeInfo && activeInfo.SaleTypeNo == 1 ? promotion :
									// 	/* 秒杀 */
									// 	activeInfo && activeInfo.SaleTypeNo == 2 ? promotion :
									// 		/* 积分 */
									// 		activeInfo && activeInfo.SaleTypeNo == 12 ? promotion :
									// 			/* 打折 */
									// 			activeInfo && activeInfo.SaleTypeNo == 15 ? promotion :
									// 				/* 打折 */
									// 				activeInfo && activeInfo.SaleTypeNo == 5 ? integral : (
														
									// 				)
								}
							</View>
						</View >
					) : ''
				}
			</View>

		)
	}
}
export default Index;