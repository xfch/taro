
import Taro, { Component } from '@tarojs/taro'
import { View, Button } from '@tarojs/components'

import './index.scss'

import ItemInput from '../../../components/itemInput'

import common from '../../../service/common'
import base from '../../../static/js/base'


class Index extends Component {
	static options = {
		addGlobalClass: true
	}
	static defaultProps = {
		modelId: '',
		data: {
			isbtm: '0',
			name: '预约服务',
		}
	}
	constructor() {
		super(...arguments)
	}
	state = {
		codeVal: '获取验证码',
		disabled: true,
		codeDisabled: true,
		ydname: '',
		ydphone: '',
		code: '',
		remarks: '',
		Vcode: '',
	}

	onInput(names,e){
		const value = e.detail.value
		switch (names){
			case 'ydname': 
				this.state.ydname = value
			break;
			case 'ydphone': 
				this.state.ydphone = value
			break;
			case 'code': 
				this.state.code = value
			break;
			case 'remarks': 
				this.state.remarks = value
			break;
		}
		this.state.codeDisabled = this.state.ydphone.length == 11 ? false : true
		this.state.disabled = this.state.ydname != '' && this.state.ydphone != '' && this.state.code != '' ? false : true
		this.setState({
			ydname: this.state.ydname,
			ydphone: this.state.ydphone,
			code: this.state.code,
			remarks: this.state.remarks,
			codeDisabled: this.state.codeDisabled,
			disabled: this.state.disabled
		})
	}
	// 获取验证码
	getCode(){
		const that = this;
		that.state.phoneNum = that.state.ydphone
        common.getCode(that,(res) => {
			that.setState({Vcode: res.Vcode})
		})
	}
	// 提交信息
	submit(){
		const that = this
		if(that.state.Vcode != that.state.code){
			base.showModal('验证码错误')
		}
		base.showLoading()
		common.subscribe(that,(res) => {
			base.showModal('预约' + res.msg,null,() => {
				that.setState({
					codeVal: '获取验证码',
					disabled: true,
					codeDisabled: true,
					ydname: '',
					ydphone: '',
					code: '',
					remarks: '',
					Vcode: '',
				})
			})
		},(err) => {
			base.showModal('预约' + err.msg)
		},() => {
			Taro.hideLoading()
		})
	}

	render() {
		const {modelId, data } = this.props;
		const {disabled, codeDisabled } = this.state;
		return (
			<View className={`main-subscribe ${data.isbtm == 1 ? 'marginBottom' : ''}`}>
				<View className='title ac'>{data.name}</View>
				<View className='p20'>
					<ItemInput inputType='2' placeholder='请填写称呼，例如：陈女士/先生' value={this.state.ydname} onInput={this.onInput.bind(this,'ydname')} ></ItemInput>
					<ItemInput inputType='2' placeholder='请输入手机号码' type='number' value={this.state.ydphone} onInput={this.onInput.bind(this,'ydphone')} ></ItemInput>
					<ItemInput inputType='2' placeholder='请输入验证码' type='number' value={this.state.code} onInput={this.onInput.bind(this,'code')} >
						<Button className={`weui-btn btn-code ${codeDisabled ? 'disabled' : ''}`} disabled={codeDisabled} onClick={this.getCode}>{this.state.codeVal}</Button>
					</ItemInput>
					<View className='textarea-box'>
						<Textarea className='textarea' placeholder='请输入留言备注' value={this.state.remarks} autoHeight onInput={this.onInput.bind(this,'remarks')}/>
					</View>
					<Button className={`weui-btn ${disabled ? 'disabled' : ''}`} disabled={disabled} onClick={this.submit}>提交信息</Button>
				</View>
			</View>
		)
	}
}
export default Index;