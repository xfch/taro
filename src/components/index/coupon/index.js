
import Taro, { Component } from '@tarojs/taro'
import { View, Text } from '@tarojs/components'
import './index.scss'

import api from '../../../service/api'
import common from '../../../service/common'
import coupon from '../../../service/coupon'
import { get as getGlobalData } from '../../../static/js/global_data'

class Index extends Component {
	static defaultProps = {
		data: null
	}
	constructor() {
		super(...arguments)
	}
	state = {
		couList: []
	}
	componentWillMount() {
		this.getCoupons()
	}
	getCoupons() {
		const { data } = this.props;
		coupon.getCouponListAll(null,(res) => {
			let goupoms = data.couponIds.split(',')
			let couponList = []
			goupoms.map((val) => {
				res.Coupons.map((value) => {
					if (val == value.N_MC_ID) {
						couponList.push(value)
					}
				})
			})
			this.setState({ couList: couponList })
        },(err) => {
			console.log(err)
		})
	}
	// 点击跳转页面  营销-优惠券
	comNavTo(modelId,linkparam){
		common.comNavTo(modelId,null,null)
	}
	render() {
		let {modelId} = this.props
		const {couList } = this.state;
		const smConpon = (couList && couList.length > 0) ? (
			<View className='coupon-list'>
				{
					couList.map((val, i) => (
						val.N_MC_BANKNOTE ? (
							/* 随机代金券的时候这个字段会返回null */
							<View className='bgImg smCoupon' key={'sm' + i} onClick={this.comNavTo.bind(this,modelId)}>
								<View className='coupon-price fontSize80'>{val.N_MC_BANKNOTE ? val.N_MC_BANKNOTE : 'N'}</View>
								<View className='coupon-type'>
									<View className='oneHidText'>{val.C_MC_TYPE == 1 ? '元优惠券' : '折折扣券'}</View>
									<View className='oneHidText'>{val.N_MC_Scope ? val.N_MC_Scope : val.C_MC_CONTENT}</View>
								</View>
							</View>
						) : (
								<View className='bgImg smCoupon' key={'sm' + i} onClick={this.comNavTo.bind(this,modelId)}>
									<View className='coupon-type'>
										<View className='oneHidText'>随机代金券</View>
										<View className='oneHidText'>{val.N_MC_Scope ? val.N_MC_Scope : val.C_MC_CONTENT}</View>
									</View>
								</View>
							)
					))
				}
			</View >
		) : ''
		const bigConpon = (couList && couList.length > 0) ? (
			<View className='coupon-list'>
				{
					couList.map((val, i) => (
						<View className='bigCoupon bgImg' key={'sm' + i} onClick={this.comNavTo.bind(this,modelId)}>
							<View className='coupon-info'>
								{
									val.N_MC_BANKNOTE ? (
										/* 随机代金券的时候这个字段会返回null */
										<View className='coupon-price'>
											<Text className='fontSize80'>{val.N_MC_BANKNOTE}</Text>
											<Text className='fontSize36'>{val.C_MC_TYPE == 1 ? '元优惠券' : '折折扣券'}</Text>
										</View>
									) : (
											<View className='coupon-price'>
												<Text className='fontSize36'>随机代金券</Text>
											</View>
										)
								}
							</View>
							<View className='bigCoupon-date moreHidText'>
								{val.C_MC_CONTENT ? val.C_MC_CONTENT : val.c_mc_name}
							</View>
						</View>
					))
				}
			</View >
		) : ''
		return (
			<View>
				{
					(couList && couList.length > 0) ? couList.length > 1 ? smConpon : bigConpon : ''
				}
			</View >
		)
	}
}
export default Index;