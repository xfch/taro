
import Taro, { Component } from '@tarojs/taro'
import { View, Swiper, SwiperItem, Image, Block } from '@tarojs/components'

import './index.scss'

import comNavTo from '../../../service/comNavTo'


class Index extends Component {
	static options = {
		addGlobalClass: true
	}
	static defaultProps = {
		modelId: '51',
		data: {
			isbtm: '0',
			navUrl: [
				{img: "https://cdn.weikebaba.com/diy/246397_a983e4e9acae4…d117f2b.jpg?x-oss-process=style/Rectangle_240_120", linkurl: "/pages/list/list?gcid=手指美甲优惠", linktype: "3", linktitle: "商品列表(手指美甲优惠)", linkparam: "gcid=手指美甲优惠"}
			]
		}
	}
	constructor() {
		super(...arguments)
	}
	// 点击跳转页面
	comNavTo(modelId,linktype,linkparam){
		comNavTo.comNavTo(modelId,linktype,linkparam)
	}
	render() {
		let { data, modelId} = this.props
		return ( 
			<View className={`mainMenu mainMenu${modelId}`}>
					<View className='menuList'>
						<View className='menu-list weui-flex'>
							{modelId == '53' && 
								<Block>
									<View className='list-left'>
										{data.navUrl && data.navUrl.map((item,i) => (
											<Block key={i} >
												{i < 2 && <View className='item-img'><Image mode='widthFix' className='img' src={item.img}></Image></View>}
											</Block>
										))}
									</View>
									<View className='list-right'>
										{data.navUrl && data.navUrl.map((item,i) => (
											<Block key={i} >
												{i >= 2 && <View className='item-img'><Image mode='widthFix' className='img' src={item.img}></Image></View>}
											</Block>
										))}
									</View>
								</Block>
							}
							{modelId == '54' && 
								<View>
									<View className='list-top'>
										{data.navUrl && data.navUrl.map((item,i) => (
											<Block key={i} >
												{i < 1 && <View className='item-img'><Image mode='widthFix' className='img' src={item.img}></Image></View>}
											</Block>
										))}
									</View>
									<View className='list-bottom weui-flex'>
										{data.navUrl && data.navUrl.map((item,i) => (
											<Block key={i} >
												{i >= 1 && <View className='item-img'><Image mode='widthFix' className='img' src={item.img}></Image></View>}
											</Block>
										))}
									</View>
								</View>
							}
							{modelId != '53' && modelId != '54'&&
								<Block>
									{data.navUrl && data.navUrl.map((item,i) => (
										<View key={i} className='ac list-item' onClick={this.comNavTo.bind(this,modelId,item.linktype,item.linkparam)}>
											<View className='item-img'><Image mode='widthFix' className='img' src={item.img}></Image></View>
											<View className='item-txt ellipsis'>{item.name}</View>
										</View>
									))}
								</Block>
							}
						</View>
					</View>
			</View>
		)
	}
}
export default Index;