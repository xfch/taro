
import Taro, { Component } from '@tarojs/taro'
import { View, Swiper, SwiperItem, Image } from '@tarojs/components'
import './index.scss'

import common from '../../../service/common'


class Index extends Component {
	static options = {
		addGlobalClass: true
	}
	static defaultProps = {
		modelId: '',
		data: {
			height: '',
			isbtm: 0,
			imgUrl: [{ img: 'https://cdn.weikebaba.com//Content/100274/UpLoadImages/2018/5/30/thum_zg_1805301714162688578.jpg' }]
		}
	}
	constructor() {
		super(...arguments)
	}
	state = {
	}
	// 跳转
	comNavTo(modelId,linktype,linkparam){
		common.comNavTo(modelId,linktype,linkparam)
	}

	render() {
		console.log('this.props-------------------')
		console.log(this.props)
		const {modelId, data } = this.props;
		return (
			data ? (
				<View className={data.isbtm == 1 ? 'marginBottom' : ''}>
					<Swiper className='swiper-container' indicatorDots indicatorColor='rgba(255, 255, 255, .5)' indicatorActiveColor='#000' autoplay>
						{
							data.imgUrl.map((val, i) => (
								<SwiperItem className='item' key={i}>
									<Image className='img' src={val.img} mode='widthFix' onClick={this.comNavTo.bind(this, modelId,val.linkType,val.linkparam)}></Image>
								</SwiperItem>
							))
						}
					</Swiper>
				</View >
			) : ''
		)
	}
}
export default Index;