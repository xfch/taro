
import Taro, { Component } from '@tarojs/taro'
import { View, Image, Map, Icon, Input,Block,Button } from '@tarojs/components'

import './index.scss'

import {set as setGlobalData, get as getGlobalData } from '../../../static/js/global_data'

import Checkboxbar from '../../checkboxbar'

import api from '../../../service/api'

class Index extends Component {
	static options = {
		addGlobalClass: true
	}
	static defaultProps = {
		proData: {
			color: "#ff0000"
		},
		showNumState: true, // 显示点击查看更多
		modelId: null,
		onChangeShopList: () => {}
	}
	constructor() {
		super(...arguments)
	}
	state = {
		shopList: [],
		shopInfo: getGlobalData('shopInfo'),
		domainImg: getGlobalData('domainImg'),
		longitude: '0', // 当前地址的经度
		latitude: '0', // 当前地址的纬度
		Mlatitude: null, // 当前店铺经度
		Mlongitude: null, // 当前店铺纬度
		markers: [], // 地图标记
		showNum: 2, // 显示2个店铺
		hasShopList: false,
		searchShopName: '',
	}
	componentWillMount() {
		this.getWidth()
		let shopInfo = Taro.getStorageSync('shopInfo').model
		// let shopInfo = getGlobalData('shopInfo').model
		Taro.getLocation({
			success: (res) => {
				this.state.latitude = res.latitude
				this.state.longitude = res.longitude
				this.getShopList()
			},
			fail: (err) => {
				this.getShopList()
			}
		})
		this.setState({
			shopInfo: shopInfo
		})

	}
	// 获取连锁店铺列表
	getShopList() {
		let {showNumState} = this.props
		const str = `longitude&latitude&ShopNo`;
		let params = {
			bNo: 6001,
			longitude: this.state.longitude,
			latitude: this.state.latitude,
		}
		api.get(str, params).then((res) => {
			if (!res.Result) { res.Result = [] }
			res.Result.map((item, i) => {
				item.jl = parseFloat(item.jl)
				item.distance = item.jl > 1000 ? "(距离" + (item.jl / 1000).toFixed(2) + "km)" : "(距离" + item.jl.toFixed(2) + "m)"
				if(getGlobalData('SHOP_NO') == item.c_shop_no){
					this.state.Mlongitude = item.n_longitude
					this.state.Mlatitude = item.n_dimension
					item.selected = true
				}
				this.state.markers.push({
					id: i + 1,
					latitude: item.n_dimension,
					longitude: item.n_longitude,
					title: 'c_name',
					iconPath: "/assets/img/address.png",
					width: 36,
					height: 36
				})
			})

			this.setState({
				shopList: res.Result,
				markers: this.state.markers,
				Mlongitude: this.state.Mlongitude,
				Mlatitude: this.state.Mlatitude,
				hasShopList: true,
				showNum: showNumState ? this.state.showNum : res.Result.length
			})
		})
	}
	// 过滤店铺列表
	getFilterShopList(val){
		let { shopList } = this.state
		let arr = val != '' ? shopList.filter(item => item.c_name.indexOf(val) > 0) : shopList
		return arr
	}
	// 切换店铺
	changeShopList(index,shopInfo) {
		const {proData, modelId } = this.props
		let shopNo = this.state.shopList[index].c_shop_no
		setGlobalData('SHOP_NO',shopNo)
		if(modelId == 21){ // 样式一
			this.state.shopList.map((item,i) => {
				item.selected = index == i ? true : false
			})
			this.setState({
				shopList: this.state.shopList,
			})
			this.props.onChangeShopList()
		}else if(modelId == 22){ // 样式二 跳门店详情页
			Taro.navigateTo({
				url: `/pages/shopInfo/index?titleText=${shopInfo.c_name}`
			})
		}
	}
	comNavTo(){
		const {proData} = this.props
		Taro.navigateTo({
			url: `/pages/shopList/index?titleText=${proData.text}`
		})
	}
	// 点击查看更多
	showMore(){
		this.setState({
			showNum: this.state.shopList.length
		})
	}
	// 获取屏幕宽度
	getWidth() {
		Taro.getSystemInfo({
			success: res => {
				this.setState({ screenWidth: res.screenWidth })
			}
		})
	}
	// 输入店铺名
	onInput(e){
		const searchShopName = e.detail.value
		console.log(searchShopName)
		console.log(typeof searchShopName)
		console.log(this.getFilterShopList(searchShopName))
		this.setState({
			searchShopName
		})
	}

	render() {
		const {proData, modelId } = this.props
		const {showNum, hasShopList,screenWidth,Mlongitude,Mlatitude, shopList, searchShopName } = this.state;
		return (
			<View className='diyShopInfos'>
				{hasShopList ? 
					<Block>
						{modelId != 23 ? <Block>
							{proData.isTitle == 1 && <View className='shop-title'>附近门店</View>}
							{proData.isMap == 1 &&
								<View className='map'>
									<Map onClick={this.onTap} style={`height: ${screenWidth / 2}px;width: 100%;`} scale='15' longitude={Mlongitude} latitude={Mlatitude} markers={markers} />
								</View>
							}
							<View className='p20'>
								<View className='search weui-flex'>
									<Icon class="weui-icon-search" type="search" size="14"></Icon>
									<Input className='weui-flex__item' placeholder='请写入店铺名' onInput={this.onInput}></Input>
								</View>
							</View>
							<View className={`${this.getFilterShopList(searchShopName).length > showNum ? '' : 'pb20'}`}>
								{this.getFilterShopList(searchShopName).map((item, i) => (
									<Block key={'list' + i}>
										{i < showNum && <View className='shopList' onClick={this.changeShopList.bind(this,i,item)}>
											<View className='weui-cell'>
												{modelId == '21' && <View className='weui-cell__hd mr20'>
														{item.c_logo && item.c_logo != '' ? <Image className='item-logo' src={item.c_logo}></Image> : <View className='item-logo bg-img'></View>}
													</View>
												}	
												<View className='weui-cell__bd'>
													<View className='text-big'>{item.c_name} <Text className='text-gray text-small pl10'>{item.distance}</Text></View>
													<View className='weui-flex text-gray text-small item-house'>
														<Text className='iconfont icon-time pr10'></Text>
														<View className='weui-flex__item'><Text className='text-house'>营业时间：{item.c_house}</Text></View>
													</View>
												</View>
												<View className='weui-cell__ft'>
													{/* 样式一 */}
													{modelId == '21' && <Checkboxbar select={item.selected}></Checkboxbar>}
													{/* 样式二 */}
													{modelId == '22' && 
														<View className='item-logo-big'>{item.c_logo && item.c_logo != '' ? <Image className='item-logo' src={item.c_logo}></Image> : <View className='item-logo bg-img'></View>}</View>
													}
												</View>
											</View>
											<View className='text-gray text-small pl20 pr20 pb20'>
												<View className='weui-flex text-gray text-small item-house'>
													<Text className='iconfont icon-address pr10'></Text>
													<View className='weui-flex__item'><Text className='text-house'>店铺地址：{item.c_address}</Text></View>
												</View>
											</View>
										</View>}
									</Block>
								))}
								{this.getFilterShopList(searchShopName).length > showNum && <View className='weui-cell weui-cell-none'>
									<View className='weui-cell__bd ac'><Text className='text-small text-gray' onClick={this.showMore}>点击查看更多(共{shopList.length}个)</Text></View>
								</View>}
							</View>
						</Block> 
						:
						<View className={`weui-cell bg-white ${proData.isbtm == 1 ? 'bottomLine' : ''}`}>
							<Button className='btn-entry weui-cell__bd' style={`background-color: ${proData.color}`} onClick={this.comNavTo}>{proData.text}</Button>
						</View> 
					}	
					</Block> : ''
				}
				


				{/* {modelId == '12' ? <View className={`shop-info ${proData.isbtm == 1 ? 'bottomLine' : ''}`}>
					<View className='weui-cell'>
						<View className='weui-cell__bd'>
							<View className='shop-name'>{shopInfo.c_name}</View>
							<View className='sale-time'>营业：{shopInfo.c_house}</View>
						</View>
						<View className='weui-cell__ft'>
							<Image className='shop-logo' src={domainImg + shopInfo.c_logo} mode='widthFix'></Image>
						</View>
					</View>
					{shopInfo.c_remark ? <View className='shop-ad'>{shopInfo.c_remark}</View> : ''}
					<View className='shop-cont'>
						<List onLyClick={this.onAddress.bind(this)} icon='icon-address' title={shopInfo.c_address} />
						<List onLyClick={this.onTell.bind(this, shopInfo.c_phone)} icon='icon-tel' title={shopInfo.c_phone} />
						<List icon='icon-send' title={`${shopInfo.c_deliver}期间配送`} rightIcon={false} />
					</View>
				</View> : ''}
				{modelId == '24' ?  <View className={`shop-info2 ${proData.isbtm == 1 ? 'bottomLine' : ''}`}>
					<View className='shop-aptitude' style={`background: url(${domainImg + shopInfo.aptitude});background-size: cover;`}></View>
					<View className='shop-info-box'>
						<View className='weui-cell'>
							<View className='weui-cell__bd'>
								<View className='shop-name'>{shopInfo.c_name}</View>
							</View>
							<View className='weui-cell__ft'>
								<View className='iconTel'><Text className='iconfont icon-tel'></Text></View>
							</View>
						</View>

						<View className='shop-cont'>
							<List icon='icon-time' title={shopInfo.c_house} rightIcon={false}  />
							<List onLyClick={this.onAddress.bind(this)} icon='icon-address' title={shopInfo.c_address} />
							<List onLyClick={this.onTell.bind(this, shopInfo.c_phone)} icon='icon-tel' title={shopInfo.c_phone}/>
						</View>
						{shopInfo.c_remark ? <View className='shop-ad'>{shopInfo.c_remark}</View> : ''}
						<View className='weui-cell weui-cell-none btn-group'>
							<Button className='btn-mall btn'>进入商城</Button>
							<Button className='btn-subscribe btn'>即刻预约</Button>
						</View>
					</View>
				</View> : ''} */}
			</View>
		)
	}
}
export default Index;