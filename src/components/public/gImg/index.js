import Taro, { Component } from '@tarojs/taro'
import { View, Image} from '@tarojs/components'

import './index.scss'

import {get as getGlobalData } from '../../../static/js/global_data'

class Index extends Component {
	static options = {
		addGlobalClass: true
	}
	static defaultProps = {
		pic: null,
		size: 120,
	}
	constructor() {
		super(...arguments)
		this.state = {
			domainImg: getGlobalData('domainImg')
		};
	}
	render() {
		let { domainImg } = this.state
		let { pic, size } = this.props
		return (
			<Block>
				{pic && pic != '' ? <Image className={`gImg gImg-${size}`} src={domainImg + pic}></Image> : <View className={`gImg bg-img gImg-${size}`}></View>}
			</Block>
		)
	}
}
export default Index;