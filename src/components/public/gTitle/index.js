import Taro, { Component } from '@tarojs/taro'
import { View, Image} from '@tarojs/components'

import './index.scss'

import {get as getGlobalData } from '../../../static/js/global_data'

class Index extends Component {
	static options = {
		addGlobalClass: true
	}
	static defaultProps = {
		title: null,
		line: 1, // 1: 一行， 2： 两行
	}
	constructor() {
		super(...arguments)
		this.state = {
			domainImg: getGlobalData('domainImg')
		};
	}
	render() {
		let { title, line } = this.props
		return (
			<View className={`gTitle ${line == 1 ? 'ellipsis' : 'clamp'}`}>
				<Text>{title}</Text>
			</View>
		)
	}
}
export default Index;