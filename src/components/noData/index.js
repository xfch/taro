
import Taro, { Component } from '@tarojs/taro'
import { View, Text } from '@tarojs/components'
import './index.scss'

class Index extends Component {
	static options = {
		addGlobalClass: true
	}
	static defaultProps = {
		type: 1,
	}
	constructor() {
		super(...arguments)
	}
	state = {

	}
	render() {
		const { type } = this.props;
		return (
			<View className='no-data'>
				{type == 1 && <Text>暂无数据</Text>}
				{type == 2 && <Text>没有更多数据了</Text>}
				{type == 3 && <Text>加载中...</Text>}
			</View >
		)
	}
}
export default Index;