import Taro, { Component } from '@tarojs/taro'
import { View, Text, ScrollView } from '@tarojs/components'

import './index.scss'


import good from '../../../service/good'


// 分类
class Index extends Component {
	static options = {
		addGlobalClass: true
	}
	static defaultProps = {
		scrollType: 'y',
		orderType: '0', // 0：无桌台 1：有桌台
		onTabNav () {}
	}
	constructor() {
		super(...arguments)
		this.state = {
			classify: [],
			cid: '',
		};
	}

	componentWillMount(){
		let { orderType } = this.props
		if(orderType == '0'){
			this.getGoodscate()
		}
	}
	// 无桌台分类
	getGoodscate(){
		good.getGoodscate(null,(res) => {
			this.setState({
				classify: res,
				cid: res[0].ID
			})
			this.props.onTabNav(res[0].ID)
		})
	}
	// 切换分类
	tabNav(cid){
		this.setState({cid})
		this.props.onTabNav(cid)
	}

	render() {
		// console.log('this.props.classify---------------------------')
		// console.log(this.props)
		let {scrollType} = this.props
		let {classify,cid} = this.state
		return (
			<View className={`classify classify-${scrollType}`}>
				{scrollType == 'y' ?
					<ScrollView className={`scrollview scrollview-${scrollType}`} scrollY>
						{classify.map((item, i) => (
							<View key={'classify' + i} className={`nav ac ${scrollType == 'scrollY' ? 'ellipsis' : ''} ${cid == item.ID ? 'active text-sub' : ''}`} onClick={this.tabNav.bind(this, item.ID)}>{item.name || item.Name}</View>
						))}
					</ScrollView> :
					<ScrollView className={`scrollview scrollview-${scrollType}`} scrollX>
						{classify.map((item, i) => (
							<Text key={'classify' + i} className={`nav ac ${cid == item.ID ? 'active text-sub' : ''}`} onClick={this.props.onTabNav.bind(this, item.ID)}>{item.name || item.Name}</Text>
						))}
					</ScrollView>
				}
			</View>

		)
	}
}
export default Index;