import Taro, { Component } from '@tarojs/taro'
import { View, Text } from '@tarojs/components'

import './index.scss'

import Gtit from '../../public/gTitle'

// 规格
class Index extends Component {
	static options = {
		addGlobalClass: true
	}
	static defaultProps = {
		lvgg: [],
		onChangeLvgg() {}, // 切换规格
	}
	constructor() {
		super(...arguments)
		this.state = {

		};
	}
	componentWillMount(){

	}

	render() {
		let { lvgg } = this.props
		return (
			<View className='com-lvgg'>
				{lvgg.map((items,i) => (
					<View key={'lvgg' + i} className=''>
						<View className='pt20'>{items.C_SPE_NAME}</View>
						<View className='spe-list weui-flex'>
							{items.SpvList.map((item,j) => (
								<View key={'SpvList' + j} className={`item-spe ${item.select && 'item-spe-active'}`} onClick={this.props.onChangeLvgg.bind(this,i,item)}>{item.C_SPE_VALUE}</View>
							))}
						</View>
					</View>
				))}
			</View>
		)
	}
}
export default Index;
