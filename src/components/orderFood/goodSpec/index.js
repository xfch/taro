import Taro, { Component } from '@tarojs/taro'
import { View } from '@tarojs/components'

import './index.scss'

import base from '../../../static/js/base'
import good from '../../../service/good'
import orderFood from '../../../service/orderFood'

import GoodSpecGood from '../goodSpecGood'
import GoodSpecLptcdt from '../goodSpecLptcdt'
import GoodSpecLvgg from '../goodSpecLvgg'
import GoodSpecNum from '../goodSpecNum'

import GoodItem from '../goodItem'
import NoData from '../../noData'

class Index extends Component {
	static options = {
		addGlobalClass: true
	}
	static defaultProps = {
		specState: true,
		onGoodSpecScope() {}, // 作用域
		onChangeGoodSpec() {}, // 切换弹窗显示隐藏状态
	}
	constructor() {
		super(...arguments)
		this.state = {
			goodSpec: {
				C_MAINPICTURE: null,
				GOODS_NAME: null,
				lptcdt: null,

			}
		};
	}
	componentWillMount(){
		this.props.onGoodSpecScope(this)
	}
	componentDidMount(){
	}

	// 获取商品详情
	getGoodDetail(params,callback) {
		base.showLoading()
		good.getGoodDetail(params,(goodSpec) => {
			this.setState({goodSpec})
			Taro.hideLoading()
			if(callback){callback()}
		})
	}
	// 切换规格
	changeLvgg(index,item){
		base.showLoading()
		let { goodSpec } = this.state
		goodSpec.C_SPID[index] = item.N_SPV_ID
		let param = {
			Spv1: goodSpec.C_SPID[0],
			Spv2: goodSpec.C_SPID[1],
			Spv3: goodSpec.C_SPID[2],
			GbId: goodSpec.N_GB_ID,
		}
		good.getGoodDetailLvgg(param, (goodSpec) => {
			this.setState({goodSpec})
			Taro.hideLoading()
		})
	}
	// 加减数量
	changeGoodNum(bNo,types){
		console.log(bNo,types)
	}

	render() {
		// let {hasView,goodList,hasEd,cid} = this.state
		let { goodSpec } = this.state
		let { specState} = this.props
		return (
			<View className={`popop ${specState ? 'popup-show' : ''}`}>
				<View className='mask' onClick={this.props.onChangeGoodSpec}></View>
				<View className='popup-content'>
					<View className='bg-body'>
						{/* 商品名称、价格 */}
						<GoodSpecGood goodSpec={goodSpec} onChangeGoodSpec={this.props.onChangeGoodSpec}></GoodSpecGood>
						{/* 套餐 */}
						{goodSpec.lptcdt && <GoodSpecLptcdt lptcdt={goodSpec.lptcdt}></GoodSpecLptcdt>}
						{/* 规格 lvgg */}
						{goodSpec.lvgg && <GoodSpecLvgg lvgg={goodSpec.lvgg} onChangeLvgg={this.changeLvgg.bind(this)}></GoodSpecLvgg>}
						{/* 数量 */}
						<GoodSpecNum num={goodSpec.num} onChangeCartNum={this.changeGoodNum.bind(this)}></GoodSpecNum>
					</View>

				</View>
			</View>
		)
	}
}
export default Index;
