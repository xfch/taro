import Taro, { Component } from '@tarojs/taro'
import { View } from '@tarojs/components'

import './index.scss'

import good from '../../../service/good'
import order from '../../../service/order'

import Gimg from '../../../components/public/gImg'
import GTit from '../../../components/public/gTitle'
import Gprice from '../../../components/public/gPrice'
import NumInput from '../../../components/numInput'
import GoodSpec from '../goodSpec'

class Index extends Component {
	static options = {
		addGlobalClass: true
	}
	static defaultProps = {
		goodItem: {},
		onGoodListScope() {},
		onChangeGoodSpec() {}, // 切换商品弹窗状态
	}
	constructor() {
		super(...arguments)
		this.state = {
			nums: 0,
			specState: true
		};
	}
	componentWillMount(){

	}
	// 点击加减
	changeGoodNum(bNo, types){
		let {nums} = this.state
		let {goodItem} = this.props
		// if(types == 'add'){
		// 	nums++
		// }else if(types == 'minus'){
		// 	nums--
		// }
	}
	// 切换商品弹窗状态
	changeGoodSpec(){
		this.props.onChangeGoodSpec()
	}

	render() {
		let {nums,specState} = this.state
		let {goodItem} = this.props
		return (
			<View>
				{/* 列表-商品 */}
				<View className='goodItem weui-cell'>
					<View className='weui-cell__hd mr10'>
						<Gimg pic={goodItem.C_MAINPICTURE}></Gimg>
					</View>
					<View className='weui-cell__bd' onClick={this.changeGoodSpec.bind(this)}>
						<GTit title={goodItem.GOODS_NAME} line='2'></GTit>
						<View className='weui-flex mt10'>
							<View className='weui-flex__item text-sub'>
								<Gprice item={goodItem}></Gprice>
							</View>
							<NumInput nums={nums}></NumInput>
						</View>
					</View>
				</View>

			</View>

		)
	}
}
export default Index;
