import Taro, { Component } from '@tarojs/taro'
import { View, Text } from '@tarojs/components'

import './index.scss'

import Gtit from '../../public/gTitle'

// 套餐
class Index extends Component {
	static options = {
		addGlobalClass: true
	}
	static defaultProps = {
		lptcdt: []
	}
	constructor() {
		super(...arguments)
		this.state = {

		};
	}

	render() {
		let { lptcdt } = this.props
		return (
			<View className='com-lptcdt p20'>
				<View className='mb10'>套餐包含</View>
				{lptcdt.map((item,i) => (
					<View key={'lptcdt' + i} className='weui-flex plr10'>
						<View className='text-small text-gray pr10'>|-</View>
						<View className='weui-flex__item'><Gtit title={item.GOODS_NAME}></Gtit></View>
						<View className='text-small text-gray plr20'>×{item.N_COUNT}</View>
						<View className='text-small text-gray'>￥{item.SALE_PRICE}</View>
					</View>
				))}
			</View>
		)
	}
}
export default Index;
