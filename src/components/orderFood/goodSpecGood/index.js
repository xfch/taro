import Taro, { Component } from '@tarojs/taro'
import { View, Text } from '@tarojs/components'

import './index.scss'

import Gimg from '../../public/gImg'
import Gtit from '../../public/gTitle'
import Gprice from '../../public/gPrice'

// 规格
class Index extends Component {
	static options = {
		addGlobalClass: true
	}
	static defaultProps = {
		goodSpec: {},
		onChangeGoodSpec() {}, // 关闭商品弹窗
	}
	constructor() {
		super(...arguments)
		this.state = {

		};
	}
	componentWillMount(){

	}

	render() {
		let { goodSpec } = this.props
		return (
			<View className='com-good weui-cell'>
				<View className='weui-cell__hd mr10'>
					<Gimg pic={goodSpec.C_MAINPICTURE}></Gimg>
				</View>
				<View className='weui-cell__bd'>
					<View className='text-big'><Gtit title={goodSpec.GOODS_NAME}></Gtit></View>
					<View className='text-sub mt10'><Gprice item={goodSpec}></Gprice></View>
				</View>
				<View className='weui-cell__ft'>
					<View onClick={this.props.onChangeGoodSpec}><Icon size='20' type='clear' color='red' /></View>
				</View>
			</View>
		)
	}
}
export default Index;
