import Taro, { Component } from '@tarojs/taro'
import { View, Text } from '@tarojs/components'

import './index.scss'

import Gimg from '../../public/gImg'
import Gcount from '../../public/gCount'


// 规格
class Index extends Component {
	static options = {
		addGlobalClass: true
	}
	static defaultProps = {
		num: 1,
		onChangeCartNum() {}, // 加减商品数量
	}
	constructor() {
		super(...arguments)
		this.state = {

		};
	}
	componentWillMount(){

	}

	render() {
		let { num } = this.props
		return (
			<View className='com-goodNum'>
				<View className='weui-cell'>
					<View className='weui-cell__bd'>
						<View className='text-big'>数量</View>
					</View>
					<View className='weui-cell__ft'>
						<Gcount onChangeCartNum={this.props.onChangeCartNum}></Gcount>
					</View>
				</View>
			</View>
		)
	}
}
export default Index;
