import Taro, { Component } from '@tarojs/taro'
import { View, Text, Navigator, Image, Button } from '@tarojs/components'

import { set as setGlobalData, get as getGlobalData } from '../../../static/js/global_data'

import './index.scss'

import Checkboxbar from '../../checkboxbar'

class Index extends Component {
	static options = {
		addGlobalClass: true
	}
	static defaultProps = {
		couponed: {}, // 已选择的优惠券
		onSelectPayMethod () {}, // 切换支付方式
	}
	constructor() {
		super(...arguments)
		this.state = {
			payMethodList: [
				{color: '#379CF2',text: '其他支付',env: 'other'},
			],
			otherPayMethod: [],
			otherPayText: null,
			isBalance: 0, 
		};
	}
	componentWillMount() {
		const env = process.env.TARO_ENV
		if(env == 'weapp'){
			this.state.payMethodList.unshift({color: '#1AAD19',text: '微信支付',env: 'weapp',checked: true})
		}else if(env == 'alipay'){
			this.state.payMethodList.unshift({color: '#379CF2',text: '支付宝支付',env: 'alipay',checked: true})
		}
		this.state.otherPayMethod.push({
			pay: '余额支付'
		})
		this.setState({
			payMethodList: this.state.payMethodList,
			otherPayMethod: this.state.otherPayMethod
		})
	}
	// 跳优惠券页面
	navTo(){
		let { amount,couponed } = this.props
		if(amount && amount > 0){
			Taro.navigateTo({
				url: `/pages/viewCoupon/viewCoupon?choose=true&amount=${amount}&&MarketingCId=${couponed.MarketingCId ? couponed.MarketingCId : null}`
			})
		}
	}
	// 选择支付方式
	selectCheck(type,index){
		let userInfo = Taro.getStorageSync('userInfo')
		if(type == 'other'){
			this.state.otherPayMethod.map((item) => {
				if(item.pay == '余额支付'){
					item.text = `余额支付(￥${userInfo.N_MB_AMOUNT}可用)`
				}
			})
			let itemList = this.state.otherPayMethod.map((item) => {return item.text})
			Taro.showActionSheet({
				itemList: itemList
			})
			.then((res) => {
				if(res.tapIndex || res.tapIndex == 0){
					res.index = res.tapIndex 
				}
				if(res.index == -1){ return }
				this.state.otherPayText = this.state.otherPayMethod[res.index].pay 
				this.state.isBalance = 1
				this.setPayMethodList(index)
			})
		}else{
			this.state.otherPayText = null
			this.setPayMethodList(index)
		}
	}
	// 切换支付方式
	setPayMethodList(index) {
		let { payMethodList } = this.state
		payMethodList.map((item,i) => {
			item.checked = index == i ? true : false
		})
		this.setState({
			payMethodList,
			otherPayText: this.state.otherPayText,
		})
		this.props.onSelectPayMethod(this.state.isBalance)
	}

	render() {
		let {couponed} = this.props
		let { payMethodList,otherPayText} = this.state
		return (
			<View className='comMethod'>
				<View className='weui-cell' onClick={this.navTo}>
					<View className='weui-cell__hd mr10'>
						<Text className='iconfont icon-coupon' style='color: #D60051'></Text>
					</View>
					<View className='weui-cell__bd'>
						<Text>优惠券抵扣</Text>
					</View>
					<View className='weui-cell__ft weui-cell__ft_in-access'>
						{couponed.MarketingPrice ? <Text className='text-sub'>{couponed.MarketingPrice}元</Text> : <Text>请选择优惠券</Text>}
					</View>
				</View>
				{payMethodList.map((item,i) => (
					<View key={'payMethodList' + i} className='weui-cell'>
						<View className='weui-cell__hd mr10'>
							<Text className={`iconfont icon-pay${item.env}`} style={`color: ${item.color}`}></Text>
						</View>
						<View className='weui-cell__bd'>
							{item.env == 'other' ? 
								<Text>{otherPayText ? otherPayText : item.text}</Text>
							 	: 
								<Text>{item.text}</Text>
							}
						</View>
						<View className='weui-cell__ft'>
							<Checkboxbar select={item.checked} onSelectCheck={this.selectCheck.bind(this,item.env,i)}></Checkboxbar>
						</View>
					</View>
				))}
			</View>
		)
	}
}
export default Index;