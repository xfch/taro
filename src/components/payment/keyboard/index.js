import Taro, { Component } from '@tarojs/taro'
import { View, Text,Block,Image,Button} from '@tarojs/components'

import { set as setGlobalData, get as getGlobalData } from '../../../static/js/global_data'
import base from '../../../static/js/base'

import './index.scss'

class Index extends Component {
	static options = {
		addGlobalClass: true
	}
	static defaultProps = {
		getKeyboard(){}, // 获取键盘
		onKeyboard(){}, // 点击键盘
		onSubmit(){}, // 确认支付
	}
	constructor() {
		super(...arguments)
		this.state = {
			numArr: ['1','2','3','4','5','6','7','8','9','.','0','iconfont'],
			amount: '0',
			keyboardState: true,
		};
	}
	componentWillMount(){
		this.props.getKeyboard(this)
	}
	// 点击键盘
	tapKeyboard(num){
		if(num == 'submit'){ // 确认支付
			this.props.onSubmit()
		}else if(num == 'iconfont'){// 键盘
			this.setState({keyboardState: false})
		}else{
			let amount = '0'
			if(num == 'del'){// 键盘
				if(this.state.amount){
					amount = this.state.amount.length > 1 ? this.state.amount.substr(0, this.state.amount.length - 1) : null
				}
			}else if(num == '.'){
				if(this.state.amount.indexOf('.') != -1){
					return
				}
				amount = this.state.amount + num
			}else if(num == '0'){
				if(!this.state.amount){
					amount = num
				}else if(this.state.amount == "0"){
					amount = '0'
				}else{
					amount = this.state.amount + num
				}
			}else{
				amount = !this.state.amount || this.state.amount == '0' ? num : this.state.amount + num
			}
			this.setState({amount})
			this.props.onKeyboard(amount)
		}
	}

	render() {
		let {numArr,keyboardState} = this.state
		return (
			<View className='comKeyboard'>
				{keyboardState && 
					<View className='keyboard-box weui-flex'>
						<View className='weui-flex keyboard-box-left'>
							{numArr.map((item,i) => (
								<View key={'keyboard'+i} className='keyboard-item' onClick={this.tapKeyboard.bind(this,item)}>
									{item == 'iconfont' ? <Text className={`${item} icon-keyboard`}></Text> : <Text>{item}</Text>}
								</View>
							))}
						</View>
						<View className='keyboard-box-right'>
							<View className='keyboard-item' onClick={this.tapKeyboard.bind(this,'del')}>
								<Text className='iconfont icon-del'></Text>
							</View>
							<View className='keyboard-items' onClick={this.tapKeyboard.bind(this,'submit')}><View className='text'>确认支付</View></View>
						</View>
					</View>
				}
			</View>
		)
	}
}
export default Index;