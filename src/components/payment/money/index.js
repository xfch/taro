import Taro, { Component } from '@tarojs/taro'
import { View, Text,Block,Image,Button} from '@tarojs/components'

import { set as setGlobalData, get as getGlobalData } from '../../../static/js/global_data'

import './index.scss'

class Index extends Component {
	static options = {
		addGlobalClass: true
	}
	static defaultProps = {
		amount: null,
		onFocusInput() {}, // 点击输入金额
	}
	constructor() {
		super(...arguments)
		this.state = {
		};
	}
	componentWillMount(){
		let env = process.env.TARO_ENV;
		this.setState({
			env: env
		})
	}

	render() {
		let {amount} = this.props
		return (
			<View className='comMoney'>
				<View className='p10'>
					<View className='plr20'>金额</View>
					<View className='weui-cell weui-cell-none'>
						<View className='weui-cell__hd mr20'>
							<Text className='text-big text-gray'>￥</Text>
						</View>
						<View className='weui-cell__bd'>
							<View onClick={this.props.onFocusInput}><Text className={`num ${amount ? '' : 'text-placeholder text-big'}`}>{amount ? amount : '请输入金额'}</Text></View>
						</View>
					</View>
				</View>
			</View>
		)
	}
}
export default Index;