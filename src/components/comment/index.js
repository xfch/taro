
import Taro, { Component } from '@tarojs/taro'
import { View, Image } from '@tarojs/components'
import './index.scss'
import api from '../../service/api'
import { get as getGlobalData } from '../../static/js/global_data'

import StarEva from '../starEva'

class Index extends Component {
	static defaultProps = {
		goodsId: ''
	}
	constructor() {
		super(...arguments)
	}
	state = {
		comChoose: [],
		imgUrl: getGlobalData('domainImg'),
		commentList: null
	}
	componentWillMount() { }

	componentDidMount() {
		this.chooseBtn();
	}
	getComment(comtype = 0, isimg = 0, calBack = () => { }) {
		//获取评论
		let goodsId = this.props.goodsId
		let str = `pageindex&pagesize&gid&mebno&ShopNo`
		let params = {
			bNo: 1902,
			pageindex: 1,
			pagesize: 10,
			gid: goodsId,
			ShopNo: getGlobalData('SHOP_NO'),
			comtype,//评论方式0--全部，1--好评，2--差评
			isimg,//是否有图  0是无图
		}
		api.get(str, params, (res) => {
			if (res.code == '100') {
				this.setState({
					commentList: res.commentList
				}, () => {
					calBack()
				})
			} else {
				this.setState({
					commentList: []
				})
			}
		})
	}
	chooseBtn() {
		let goodsId = this.props.goodsId
		let str = `ShopNo&gid`
		let params = {
			bNo: 1909,
			gid: goodsId,
			ShopNo: getGlobalData('SHOP_NO'),
		}
		api.get(str, params, (res) => {
			if (res.code == '100') {
				let commentChoose = [
					{ isChoose: false, text: `全部(${res.allCount})` },
					{ isChoose: false, text: `有图(${res.hasimgcount})` },
					{ isChoose: false, text: `好评(${res.goodcount})` },
					{ isChoose: false, text: `差评(${res.badcount})` }
				]
				this.setState({
					comChoose: commentChoose
				}, () => {
					this.btnClick()
				})
			}
		})
	}
	btnClick(index = 0) {
		const { comChoose } = this.state;
		let com = comChoose.map((val, i) => {
			let v = val
			if (index == i) {
				v.isChoose = true
			} else {
				v.isChoose = false
			}
			return v;
		})

		this.setState({
			comChoose: com
		}, () => {
			if (index == 0) {
				this.getComment(0, 0)
			} else if (index == 1) {
				this.getComment(0, 1)
			} else if (index == 2) {
				this.getComment(1, 0)
			} else if (index == 3) {
				this.getComment(2, 0)
			}
		})
	}
	componentWillUnmount() { }

	componentDidShow() { }

	componentDidHide() { }
	render() {
		const { comChoose, commentList, imgUrl } = this.state;
		return (
			<View className='comment'>
				{
					commentList ? (
						<View className='commentBody'>
							<View className='commentButtons'>
								{
									comChoose.map((val, i) => (
										<View onClick={this.btnClick.bind(this, i)} key={'com' + i} className={`comButton ${val.isChoose ? 'isChoose' : ''}`}>{val.text}</View>
									))
								}
							</View>
							{
								commentList && commentList.length > 0 ? commentList.map((val, i) => (
									<View key={'com' + i} className='commentBox'>
										<View className='headBox'>
											<Image className='headImg' src='http://zxpic.imtt.qq.com/zxpic_imtt/2019/03/15/1210/originalimage/121436_3482427557_2_1000_539.jpg'></Image>
										</View>
										<View className='commentRight'>
											<View className='rightHead'>
												<View>
													<StarEva star={val.star} title={val.meb_name == '' ? '匿名评论' : val.meb_name}></StarEva>
												</View>
												<View className='time'>{val.adddate.replace(/T/g, ' ')}</View>
											</View>
											<View className='rightTxt'>{val.c_content}</View>
											<View className='rightImgs'>
												{
													val.imgsList.map((item, index) => (
														<Image key={'img' + index} className='rightImg' src={imgUrl + item.Images}></Image>
													))
												}
											</View>
										</View>
									</View >
								)) : <View className='hasNoImg'>暂无评论</View>
							}
						</View>
					) : <View className='hasNoImg'>加载中</View>
				}
			</View>
		)
	}
}
export default Index;