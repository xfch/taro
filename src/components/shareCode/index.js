
import Taro, { Component } from '@tarojs/taro'
import { View, Canvas, Text } from '@tarojs/components'
import './index.scss'

/* 分享组件 */
class Index extends Component {
	static defaultProps = {
		
	}
	constructor() {
		super(...arguments)
	}
	state = { 
		
	}
	componentWillMount() { }

	componentDidMount() { 
		this.resCode()
	}

	componentWillUnmount() { }

	componentDidShow() { }

	componentDidHide() { }
	resCode() { 
		Taro.request({
			url: 'https://api.weixin.qq.com/wxa/getwxacodeunlimit',
			data: {
				access_token: '',
				scene: '',
				page: 'pages/goodsDatil/index',
			}
		}).then((res) => { 
			console.log(res,'codecodecodecodecodecodecodecodecodecodecodecodecodecodecodecodecodecodecodecode')
		})
	}
	render() {
		return (
			<View className='shareCode'>
				<View className='codeBox'>
					<View className='head'>
						<Text>标题</Text>
					</View>
					<View className='canvasBox'>
						<Canvas id='myCanvas' className='bodyCanvas'></Canvas>
					</View>
					<View className='foot'>
						<View className='btn cel'>取消</View>
						<View className='btn save'>保存到相册</View>
					</View>
				</View>
			</View >
		)
	}
}
export default Index;