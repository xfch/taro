import Taro, { Component } from '@tarojs/taro'
import { View, Text,Block,Textarea} from '@tarojs/components'

import { set as setGlobalData, get as getGlobalData } from '../../../static/js/global_data'

import './index.scss'

class cartbar extends Component {
	static externalClasses=['class-mr','class-pb']
	static options = {
		addGlobalClass: true
	}
	static defaultProps = {
	}
	constructor() {
		super(...arguments)
		this.state = {
		};
	}
	componentDidMount(){
		this.setState({glist: this.props.glist})
	}
	onInput(e){
		this.setState({reson: e.detail.value})
		this.props.onInput(e.detail.value)
	}
	render() {
		return (
			<View className='reson'>
				<View className='class-pb'>请填写退款原因</View>
				<View className='textarea-box'>
					<Textarea className='textarea' onInput={this.onInput.bind(this)} placeholder='请填写最少10个字的退款原因' autoFocus/>
					<View className='text-small text-gray ar'>{this.state.reson.length}/300</View>
				</View>
			</View>
		)
	}
}
export default cartbar;