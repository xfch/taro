import Taro, { Component } from '@tarojs/taro'
import { View, Text,Block} from '@tarojs/components'

import { set as setGlobalData, get as getGlobalData } from '../../../static/js/global_data'

import './index.scss'

class cartbar extends Component {
	static externalClasses = ['class-mr']
	static options = {
		addGlobalClass: true
	}
	static defaultProps = {
		btnList: [],
		goodItem: {}, // 商品详情
	}
	constructor() {
		super(...arguments)
		this.state = {};
	}
	changeRelx(relx){
		this.props.onChangeRelx(relx)
	}
	render() {
		return (
			<View className='good'>
				<View className='weui-cell'>
					<View className='weui-cell__bd'><Text>服务项</Text></View>
				</View>
				<View className='weui-cell'>
					<View className='weui-cell__bd'>
						<View className='btn-groups'>
							{this.props.btnList.map((item,i) => (
								<Button key={'btnList' + i} className={`class-mr btn-plain ${this.props.relx == item.relx ? 'btn-main' : ''}`} size='mini' onClick={this.changeRelx.bind(this,item.relx)}>{item.txt}</Button>
							))}
						</View>
					</View>
				</View>
			</View>
		)
	}
}
export default cartbar;