import Taro, { Component } from '@tarojs/taro'
import { View, Text,Block} from '@tarojs/components'

import { set as setGlobalData, get as getGlobalData } from '../../../static/js/global_data'

import './index.scss'
import Checkboxbar from  '../../checkboxbar'
import NumInput from '../../numInput'

class cartbar extends Component {
	static externalClasses=['class-mr']
	static options = {
		addGlobalClass: true
	}
	static defaultProps = {
		// glist: [], // 订单商品列表
	}
	constructor() {
		super(...arguments)
		this.state = {
		};
	}
	componentDidMount(){
		this.setState({glist: this.props.glist})
	}
	selectCheck(index){
		this.state.glist.map((item,i) => {
			if(index == i){
				item.select = item.select ? false : true
			}
		})
		this.props.onSelectCheck(this.state.glist)
	}
	changeNum(index,bNo,types){
		this.props.onChangeCartNum(index,types)
	}
	render() {
		return (
			<View className='good'>
				{this.props.glist&&this.props.glist.map((item,i) => (
					<View key={'glist' + i} className='weui-cell'>
						<View className='class-mr'>
							<Checkboxbar select={item.select} onSelectCheck={this.selectCheck.bind(this,i)}></Checkboxbar>
						</View>
						<View className='weui-cell__hd'>
							<Image className='good-img' src={this.props.domainImg + item.C_MAINPICTURE}></Image>
						</View>
						<View className='weui-cell__bd'>
							<View className=''>{item.C_GOODS_NAME}</View>
							
								<View className='canreNum-bar'>
									{item.select ? 
										<View className='weui-flex'><Text className='text-small text-gray'>可退数量：</Text><NumInput onChangeCartNum={this.changeNum.bind(this,i)} nums={item.nums} type='square'></NumInput></View> : 
										<Text className='text-small text-gray'>数量：×{item.N_NUM}</Text>
									}
								</View>
						</View>
						<View className='weui-cell__ft'>{item.N_SalePrice}</View>
					</View>
				))}
			</View>
		)
	}
}
export default cartbar;