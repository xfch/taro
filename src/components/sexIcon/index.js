
import Taro, { Component } from '@tarojs/taro'
import { View, Text, Button, Block } from '@tarojs/components'

import { set as setGlobalData, get as getGlobalData } from '../../static/js/global_data'

import './index.scss'

// 性别图标
class Index extends Component {
	static options = {
		addGlobalClass: true
	}
	static defaultProps = {
		sex: '女',
	}
	constructor() {
		super(...arguments)
		this.state = {
			
		};
	}
	// 设置性别
	setSex(sex){
		let num = 1
		switch (sex) {
			case '男':
				num = 2
				break;
			default:
				break;
		}
		return num
	}

	render() {
		let {sex} = this.props
		return (
			<Text className={`icon-sex sex${this.setSex(sex)}`}><Text className={`iconfont icon-sex${this.setSex(sex)}`}></Text></Text>
		)
	}
}
export default Index;