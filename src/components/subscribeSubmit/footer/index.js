import Taro, { Component } from '@tarojs/taro'
import { View, Textarea} from '@tarojs/components'

import './index.scss'

import { set as setGlobalData, get as getGlobalData } from '../../../static/js/global_data'

// 备注
class Index extends Component {
	static options = {
		addGlobalClass: true
	}
	static defaultProps = {
		orderTotal: '0.00',
		onSubmitOrder(){}
	}
	constructor() {
		super(...arguments)
		this.state = {
		};
	}
	render() {
		let { orderTotal} = this.props
		return (
			<View className='footer weui-flex'>
				<View className='weui-flex__item footer-left pl10'>
					<View>￥{orderTotal}</View>
					<View className='text-little text-gray'>价格以实际到店为准</View>
				</View>
				<View className='btn-main' onClick={this.props.onSubmitOrder}>确认下单</View>
			</View>
		)
	}
}
export default Index;