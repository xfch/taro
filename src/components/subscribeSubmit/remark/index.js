import Taro, { Component } from '@tarojs/taro'
import { View, Textarea} from '@tarojs/components'

import './index.scss'

import { set as setGlobalData, get as getGlobalData } from '../../../static/js/global_data'

// 备注
class Index extends Component {
	static options = {
		addGlobalClass: true
	}
	static defaultProps = {
		cartList: [],
		onInput() {}
	}
	constructor() {
		super(...arguments)
		this.state = {
		};
	}
	// 备注
	bindinput(e){
		this.props.onInput(e.detail.value)
	}
	render() {
		let { C_Remark,title} = this.props
		return (
			<View className='sub-cartList plr10'>
				<View className='ptb10'>{title}</View>
				<View className=''>
					<Textarea className='textarea bg-white p10' placeholder='请填写备注等要求' value={C_Remark} onInput={this.bindinput.bind(this)}></Textarea>
				</View>
				
			</View>
		)
	}
}
export default Index;