import Taro, { Component } from '@tarojs/taro'
import { View, Text, Block, Image, Switch } from '@tarojs/components'

import './index.scss'

import { set as setGlobalData, get as getGlobalData } from '../../../static/js/global_data'

// 预约商品
class Index extends Component {
	static options = {
		addGlobalClass: true
	}
	static defaultProps = {
		cartList: [],
	}
	constructor() {
		super(...arguments)
		this.state = {
			domainImg: getGlobalData('domainImg'),
		};
	}
	render() {
		let {domainImg} = this.state
		let { cartList,title} = this.props
		return (
			<View className='sub-cartList plr10'>
				<View className='ptb10'>{title}</View>
				{cartList.length > 0 && 
					<View className='weui-cell weui-cell-none cart-list'>
						<View className='weui-cell__bd'>
							{cartList.map((item,i) => (
								<Block key={'img' +i}>
									{item.C_MAINPICTURE && item.C_MAINPICTURE != '' ? <Image className='item-img' src={domainImg + item.C_MAINPICTURE}></Image> : <View className='item-img bg-img'></View>}
								</Block>
							))}
						</View>
						<View className='weui-cell__ft'><Text>共{cartList.length}个商品</Text></View>
					</View>
				}
			</View>
		)
	}
}
export default Index;