import Taro, { Component } from '@tarojs/taro'
import { View, Text, Block, Input } from '@tarojs/components'

import './index.scss'

import { set as setGlobalData, get as getGlobalData } from '../../../static/js/global_data'

// 预约信息
class Index extends Component {
	static options = {
		addGlobalClass: true
	}
	static defaultProps = {
		C_MebName: '',
		C_MebPhone: '',
		peopleNum: 0,
		D_StartDate: '',
		title: '预约信息',
		onInput(){}
	}
	constructor() {
		super(...arguments)
		this.state = {
		};
	}
	onInput(name,e){
		this.props.onInput(e.detail.value,name)
	}

	// 跳转页面
	navTo(id){
		let {peopleNum} = this.props
		let {D_StartDate} = this.state
		if(id == 'N_CustomerNum'){ // 设置人数
			Taro.navigateTo({
				url: `/pages/subscribePeopleNum/index?peopleNum=${peopleNum}`
			})
		}else if(id == 'D_StartDate'){ // 设置时间
			Taro.navigateTo({
				url: `/pages/subscribeDate/index?D_StartDate=${D_StartDate}`
			})
		}
	}

	render() {
		let {title,C_MebName,C_MebPhone,peopleNum,D_StartDate} = this.props
		return (
			<View className='sub-cartList plr10'>
				<View className='ptb10'>{title}</View>
				<View className='info-box'>
					<View class="weui-cell weui-cell_input">
						<View class="weui-cell__hd">
							<View class="weui-label">联系人</View>
						</View>
						<View class="weui-cell__bd">
							<Input class="weui-input" placeholder="请填写联系人姓名" value={C_MebName} onInput={this.onInput.bind(this,'name')}/>
						</View>
					</View>
					<View class="weui-cell weui-cell_input">
						<View class="weui-cell__hd">
							<View class="weui-label">手机号</View>
						</View>
						<View class="weui-cell__bd">
							<Input class="weui-input" placeholder="请填写联系人手机号" value={C_MebPhone} onInput={this.onInput.bind(this,'phone')}/>
						</View>
					</View>
					<View class="weui-cell" onClick={this.navTo.bind(this,'N_CustomerNum')}>
						<View class="weui-cell__hd">
							<View class="weui-label">预约人数</View>
						</View>
						<View class="weui-cell__bd">
							<View className={`text-big pl10 ${peopleNum ? '' : 'text-placeholder'}`}>{peopleNum ? peopleNum : '请选择预约人数'}</View>
						</View>
						<View class="weui-cell__ft weui-cell__ft_in-access"></View>
					</View>
					<View class="weui-cell" onClick={this.navTo.bind(this,'D_StartDate')}>
						<View class="weui-cell__hd">
							<View class="weui-label">到店时间</View>
						</View>
						<View class="weui-cell__bd">
							<View className={`text-big pl10 ${D_StartDate&&D_StartDate != '' ? '' : 'text-placeholder'}`}>{D_StartDate ? D_StartDate : '请选择到店时间'}</View>
						</View>
						<View class="weui-cell__ft weui-cell__ft_in-access"></View>
					</View>
				</View>
			</View>
		)
	}
}
export default Index;