
import Taro, { Component } from '@tarojs/taro'
import { View, Text } from '@tarojs/components'

import './index.scss'

class Index extends Component {
	static options = {
		addGlobalClass: true
	}
	static defaultProps = {
		isBorder: false, // 是否有border
		icon: 'icon-home',
		title: '',
		rightIcon: true,
		rightTxt: '',
		onLyClick: () => { }
	}
	constructor() {
		super(...arguments)
	}
	state = {

	}
	componentWillMount() { }

	componentDidMount() { }

	componentWillUnmount() { }

	componentDidShow() { }

	componentDidHide() { }
	onComClick() {
		const { rightIcon } = this.props;
		if (!rightIcon) {
			return false;
		}
		this.props.onLyClick()
	}
	render() {
		const { icon, title, rightIcon, rightTxt, isBorder } = this.props;
		return (
			<View className={`weui-cell ${isBorder ? '' : 'weui-cell-none'}`} onClick={this.onComClick.bind(this)}>
				<View className='weui-cell__hd'><Text className={`iconfont icon ${icon}`}></Text></View>
				<View className='weui-cell__bd'>{title}</View>
				<View className={`weui-cell__ft ${rightIcon ? 'weui-cell__ft_in-access' : ''}`}><Text>{rightTxt}</Text></View>
			</View>
		)
	}
}
export default Index;