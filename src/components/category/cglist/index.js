import Taro, { Component } from '@tarojs/taro'
import { View, Text, ScrollView, Image } from '@tarojs/components'

import { set as setGlobalData, get as getGlobalData } from '../../../static/js/global_data'

import NumInput from '../../numInput/index'
import GoodListPrice from '../../../components/goodListPrice'

import api from '../../../service/api'
import base from '../../../static/js/base'
import common from '../../../service/common'

import './index.scss'

// 分类商品

class cglist extends Component {
	static options = {
		addGlobalClass: true
	}
	static defaultProps = {
		goodList: [],
		hasGoodList: false,
		hasEd: false,
		onGetcartNum: () => {},
		onShowPopSpec: () => {}
	}
	constructor() {
		super(...arguments)
		this.state = {
			domainImg: getGlobalData("domainImg")
		};
	}
	componentWillMount() {
	}
	// 点击加减获取购物车数量
	changeCartNum(index,bNo,types){
		this.props.onGetcartNum(index,bNo,types)
	}
	// 显示商品详情弹窗
	showPopSpec(good){
		this.props.onShowPopSpec(good)
	}
	// 上拉加载更多
	onScrollToLowerList(){
		this.props.onScrollToLowerList()
	}
	// 跳商品详情
	navTo(page,id) {
        if(page == 'index'){
            Taro.reLaunch({ url: '/pages/index/index' })
        }
        if(page == 'goodsDatil'){
            Taro.navigateTo({url: `/pages/goodsDatil/index?goodsId=${id}&SaleTypeNo=0`})
        } 
    }

	render() {
		let {goodList,hasGoodList,hasEd} = this.props
		return (
			<View className='cglist'>
				<ScrollView className='scrollview' onScrollToLower={this.onScrollToLowerList} scrollY>
					{goodList.map((item, i) => (
						<View key={'cglist' + i} className='weui-cell justify'>
							<View className='weui-cell__hd' onClick={this.navTo.bind(this, 'goodsDatil', item.ID)}>
								<View className='item-img-box' onClick={this.navTo.bind(this, 'goodsDatil', item.N_GOODS_ID)}>
									<Image className='item-img' src={this.state.domainImg + item.C_MAINPICTURE}></Image>
									{item.activejson.length > 0 && <Text className='item-saletype'>{item.activejson[0].SaleType}</Text>}
								</View>
							</View>
							<View className='weui-cell__bd' onClick={this.showPopSpec.bind(this,item.ID)}>
								<View className='ellipsis item-title'>{item.GOODS_NAME}</View>
								<View className='weui-flex'>
									<View className='weui-flex__item text-sub'>
										<GoodListPrice item={item}></GoodListPrice>
									</View>
									<View className=' ar '>
										<View className='count-bar'>
											<NumInput onChangeCartNum={this.changeCartNum.bind(this, i)} nums={item.N_NUM}></NumInput>
										</View>
									</View>
								</View>
							</View>
						</View>
					))}
					{hasGoodList&&goodList&&!hasEd&&<View className='weui-cell'>
						<View className='weui-cell__bd ac'><Text className='text-small text-gray'>上拉加载更多</Text></View>
					</View>}
					{goodList&&hasEd&&<View className='weui-cell'>
						<View className='weui-cell__bd ac'><Text className='text-small text-gray'>没有更多数据了</Text></View>
					</View>}
					{!goodList&&hasGoodList&&<View className='weui-cell'>
						<View className='weui-cell__bd ac'><Text className='text-gray'>暂无数据</Text></View>
					</View>}
				</ScrollView>
			</View>
		)
	}
}
export default cglist;