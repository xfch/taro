import Taro, { Component } from '@tarojs/taro'
import { View, Text, ScrollView,Block } from '@tarojs/components'

import './index.scss'

// 我的订单、必备工具
class subcgnav extends Component {
	static options = {
		addGlobalClass: true
	}
	static defaultProps = {
	}
	constructor() {
		super(...arguments)
		this.state = {
			arrowDown: true,
			scrollLeft: 0,
			scrollIntoView: 'tab0'
        };
	}
	componentWillMount(){
		let arr = []
		for(let i = 0;i <= 80;i++){
			arr.push(i)
		}
		this.setState({
			arr: arr
		})
	}
	tabNavSub(id,index,e){
		this.state.scrollIntoView = 'tab'+ index;
		console.log(this.state.scrollIntoView)
		this.setState({
			arrowDown: true,
			scrollIntoView: this.state.scrollIntoView
		})
		this.props.onTabNavSub(id)
	}

	changeArrowDown(){
		this.setState({
			arrowDown: !this.state.arrowDown
		})
	}

	render() {
		return (
			<View className='cgnavsub'>
				{this.props.cgnavsub&&
					<Block>
						<ScrollView
							className='scrollview'
							scrollIntoView={this.state.scrollIntoView}
							scrollX>
							{this.props.cgnavsub.map((item,i) => (
								<Text key={'cgnavsub' + i} id={`tab${i}`} onClick={this.tabNavSub.bind(this,item.ID,i)} className={`nav ac ${this.props.cgnavsubId == item.ID ? 'active text-sub':''}`}>{item.Name}</Text>
							))}
						</ScrollView>
						<View className={`arrow-down ac ${this.state.arrowDown ? '' : 'up'}`} onClick={this.changeArrowDown}>
							<Text className='iconfont icon-arrow-down'></Text>
						</View>
					</Block>
				}
				{!this.state.arrowDown&&
					<View className='arrow-down-list'>
						<View className='weui-grids'>
							{this.props.cgnavsub.map((item,i) => (
								<View key={'cgnavsub' + i} className='weui-grid'>
									<View onClick={this.tabNavSub.bind(this,item.ID,i)} className={`item-tag text-small ac ${this.props.cgnavsubId == item.ID ? 'active':''}`}>{item.Name}</View>
								</View>
							))}
						</View>
					</View>
				}
			</View>
		)
	}
}
export default subcgnav;