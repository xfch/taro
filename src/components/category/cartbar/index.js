import Taro, { Component } from '@tarojs/taro'
import { View, Text} from '@tarojs/components'

import { set as setGlobalData, get as getGlobalData } from '../../../static/js/global_data'

import './index.scss'

class cartbar extends Component {
	static options = {
		addGlobalClass: true
	}
	static defaultProps = {
		type: 'icon', // icon 只有图标
		cartNum: 0,
		price: {
			orderTotal: '0.00', // 实付款
			yhTotal: '0.00', // 优惠价格
			costTotal: '0.00', // 原价
			integralTotal: 0, // 总积分
		},
		onChangeCartList(){},
		onSubmitOrder(){}, // 点击去下单
	}
	constructor() {
		super(...arguments)
		this.state = {};
	}

	componentDidMount(){
		// this.props.onChangeCartList(this)
	}

	clickNavigator() {
		Taro.navigateTo({url: '/pages/shopcart/shopcart'})
	}

	render() {
		let {type, cartNum, price} = this.props
		return (
			<Block>
				{type == 'icon' && 
					<View className='cartbar' onClick={this.clickNavigator}>
						<Text className='iconfont icon-cart'></Text>
						{cartNum && <View className='weui-badge'>{cartNum}</View>}
					</View>
				}
				{type == 'list' && 
					<View className='weui-cell weui-cell-list'>
						<View className='cartbar' onClick={this.props.onChangeCartList}>
							<Text className='iconfont icon-cart'></Text>
							{cartNum && <View className='weui-badge'>{cartNum}</View>}
						</View>
						<View className='weui-cell__bd ar'>
							<View className='text-sub'>实付款： <Text>{price.orderTotal}</Text>元 {price.integralTotal != 0 && <Text className='text-small text-sub'>+{price.integralTotal}积分</Text>}</View>
							<View className='text-small text-gray'><Text >已优惠：{price.yhTotal}元</Text><Text className='pl10'>原价：{price.costTotal}元</Text></View>
						</View>
						<View className='weui-cell__ft'>
							<View className='btn-submit' onClick={this.props.onSubmitOrder}>去下单</View>
						</View>
					</View>
				}
			</Block>
		)
	}
}
export default cartbar;