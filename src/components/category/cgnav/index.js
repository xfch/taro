import Taro, { Component } from '@tarojs/taro'
import { View, Text, ScrollView } from '@tarojs/components'

import './index.scss'

// 分类
class cgnav extends Component {
	static options = {
		addGlobalClass: true
	}
	static defaultProps = {
		scrollType: 'y',
		cgnav: [
			{
				ChilId: "6290",
				ID: 6290,
				name: "小商品"
			}
		],
		cgnavId: 6290,
		onTabNav: () => { }
	}
	constructor() {
		super(...arguments)
		this.state = {
		};
	}

	render() {
		// console.log('this.props.cgnav---------------------------')
		// console.log(this.props)
		let { cgnav, scrollType , cgnavId} = this.props
		return (
			<View className={`cgnav cgnav-${scrollType}`}>
				{scrollType == 'y' ?
					<ScrollView className='scrollview scrollview-${scrollType}' scrollY>
						{cgnav.map((item, i) => (
							<View key={'cgnav' + i} className={`nav ac ${scrollType == 'scrollY' ? 'ellipsis' : ''} ${cgnavId == item.ID ? 'active text-sub' : ''}`} onClick={this.props.onTabNav.bind(this, item.ID)}>{item.name}</View>
						))}
					</ScrollView> :
					<ScrollView className={`scrollview scrollview-${scrollType}`} scrollX>
						{cgnav.map((item, i) => (
							<Text key={'cgnav' + i} className={`nav ac ${cgnavId == item.ID ? 'active text-sub' : ''}`} onClick={this.props.onTabNav.bind(this, item.ID)}>{item.name}</Text>
						))}
					</ScrollView>
				}
			</View>

		)
	}
}
export default cgnav;