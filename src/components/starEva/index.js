
import Taro, { Component } from '@tarojs/taro'
import { View, Text } from '@tarojs/components'
import './index.scss'


class Index extends Component {
	static options = {
		addGlobalClass: true
	}
	static defaultProps = {
		line: '2',
		title: '',
		star: 0,
		allStart: 5,
	}
	constructor() {
		super(...arguments)
	}
	state = {
		startArr: []
	}
	componentWillMount() { }

	componentDidMount() {
		this.setStart()
	}

	componentWillUnmount() { }

	componentDidShow() { }

	componentDidHide() { }
	setStart() {
		const { star, allStart } = this.props;
		let startArr = []
		for (let i = 0; i < allStart; i++) {
			let className = 'colorYellow';
			if (i > star - 1) {
				className = ''
			}
			startArr.push(className)
		}
		this.setState({ startArr })
	}
	render() {
		const { startArr } = this.state;
		const { title, line } = this.props;
		return (
			<View className={`startEva ${line == '2' ? '' : ' flexs'}`}>
				<View className='startsTitle'>{title}</View>
				<View className='starts'>
					{
						startArr.map((val, i) => (
							<Text className={`iconfont icon-stared startText ${val}`} key={'start' + i}></Text>
						))
					}
				</View>
			</View >
		)
	}
}
export default Index;