import Taro, { Component } from '@tarojs/taro'
import { View, Text, ScrollView } from '@tarojs/components'

import './index.scss'

import common from '../../../service/common'

// 分类
class cgnav extends Component {
	static options = {
		addGlobalClass: true
	}
	static defaultProps = {
		scrollType: 'y',
		onTabNav() {},
	}
	constructor() {
		super(...arguments)
		this.state = {
			cgnav: [],
			cgnavId: '',
			scrollIntoView: 'tab0'
		};
	}
	componentDidMount(){
		const that = this
		common.getCategory(that,(res) => {
			that.setState({
				cgnav: res,
			})
			this.onTabNav(res[0].ID, 0)
		})
	}

	onTabNav(id,index){
		this.props.onTabNav(id)
		this.setState({
			cgnavId: id,
			scrollIntoView: 'tab'+ index
		})
	}

	render() {
		// console.log('this.props.nav---------------------------')
		// console.log(this.props)
		let { cgnav, cgnavId,scrollIntoView} = this.state
		let { scrollType } = this.props
		return (
			<View className={`cgnav cgnav-${scrollType}`}>
				{scrollType == 'y' ?
					<ScrollView className='scrollview scrollview-${scrollType}' scrollY>
						{cgnav.map((item, i) => (
							<View key={'cgnav' + i} className={`nav ac ${scrollType == 'scrollY' ? 'ellipsis' : ''} ${cgnavId == item.ID ? 'active text-sub' : ''}`} onClick={this.props.onTabNav.bind(this, item.ID)}>{item.name}</View>
						))}
					</ScrollView> :
					<ScrollView className={`scrollview scrollview-${scrollType}`} scrollIntoView={scrollIntoView} scrollX>
						{cgnav.map((item, i) => (
							<Text id={`tab${i}`} key={'cgnav' + i} className={`nav ac ${cgnavId == item.ID ? 'active text-sub' : ''}`} onClick={this.onTabNav.bind(this, item.ID,i)}>{item.name}</Text>
						))}
					</ScrollView>
				}
			</View>

		)
	}
}
export default cgnav;