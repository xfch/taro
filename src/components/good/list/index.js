import Taro, { Component } from '@tarojs/taro'
import { View, Text, ScrollView } from '@tarojs/components'

import './index.scss'

import common from '../../../service/common'

// 分类
class cgnav extends Component {
	static options = {
		addGlobalClass: true
	}
	static defaultProps = {
		cid: null,
	}
	constructor() {
		super(...arguments)
		this.state = {
			pageindex: 1,
			goodList: [],
			hasEd: false,
		};
	}
	componentDidMount(){
		const that = this
		that.state.cid = this.props.cid
		// if(that.state.cid != ''){
			common.getGoodList(that,(goodLists, hasEd, res) => {
				console.log(333)
				console.log(goodLists, hasEd, res)
			})
		// }
		
	}

	render() {
		console.log('this.props.list---------------------------')
		console.log(this.props)
		let {goodList} = this.state
		
		return (
			<View className={`cgnav`}>
				{goodList.map((item,i) => (
					<View key={'list' + i} className='weui-cell'>
						<View className='weui-cell__bd'>{item.GOODS_NAME}</View>
					</View>
				))}
			</View>

		)
	}
}
export default cgnav;