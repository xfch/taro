import Taro, { Component } from '@tarojs/taro'
import { View, Textarea} from '@tarojs/components'

import './index.scss'

import { set as setGlobalData, get as getGlobalData } from '../../../static/js/global_data'
import SexIcon from '../../sexIcon'

// 备注
class Index extends Component {
	static options = {
		addGlobalClass: true
	}
	static defaultProps = {
		artificerItem: {}
	}
	constructor() {
		super(...arguments)
		this.state = {
			domainImg: getGlobalData('domainImg')
		};
	}
	render() {
		const {domainImg} = this.state
		let { artificerItem} = this.props
		return (
			<View className='artificerPic'>
				<Image mode='widthFix' className='bg-img' src={domainImg + artificerItem.c_headimg}></Image>
				<View className='artificer ac'>
					<Image className='item-img' src={domainImg + artificerItem.c_headimg}></Image>
					<View className='sexIcon'><SexIcon sex={artificerItem.c_sex}></SexIcon></View>
					<View className='text-white'>{artificerItem.c_name}</View>
				</View>
			</View>
		)
	}
}
export default Index;