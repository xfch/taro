
import Taro, { Component } from '@tarojs/taro'
import { View, Image, Button } from '@tarojs/components'
import './index.scss'


import MoveBtn from '../move-button'
import { get as getGlobalData } from '../../static/js/global_data'

class Index extends Component {
	static options = {
		addGlobalClass: true
	}
	static defaultProps = {
		phone: '13008305173'
	}
	constructor() {
		super(...arguments)
	}
	static state = {
		phoneNum: ''
	};
	componentWillMount() { }

	componentDidMount() {
		const { phone } = this.props;
		this.setState({ phoneNum: phone })
	}

	onCall() {
		const { phoneNum } = this.state;
		Taro.makePhoneCall({
			phoneNumber: phoneNum //仅为示例，并非真实的电话号码
		})
	}
	render() {
		{/* 客服组件只支持微信小程序中使用。支付宝小程序暂不支持open-type='contact',需要后台配置小程序'智能客服'入口 */}
		return (
			<Block>
				{process.env.TARO_ENV == 'weapp' && 
					<MoveBtn bgColor='#ccc'>
						<View className='call'>
							<Button className='callBtn' open-type='contact'></Button>
							<View className='callImg'><Text className='iconfont icon-service'></Text></View>
						</View>
					</MoveBtn>
				}
			</Block>
		)
	}
}
export default Index;