
import Taro, { Component } from '@tarojs/taro'
import { View, ScrollView, PickerView, PickerViewColumn } from '@tarojs/components'

import './index.scss'

import { set as setGlobalData, get as getGlobalData } from '../../static/js/global_data'

import common from '../../service/common'

class Index extends Component {
	static options = {
		addGlobalClass: true
	}
	static defaultProps = {
		onSelectArea() {}
	}
	constructor() {
		super(...arguments)
	}
	state = {
		provinceStr: '',
		cityStr: '',
		countyStr: '',
		selectArr: null,
	};
	componentWillMount() {
		const areaInfo = getGlobalData('areaInfo')
		if(areaInfo){
			this.setState({
				areaInfo: areaInfo,
				selectArr: areaInfo[0] //省
			})
		}else{
			common.getAddress((areaInfo) => {
				this.setState({
					areaInfo: areaInfo,
					selectArr: areaInfo[0] //省
				})
			})
		}
	}
	// 选择
	selectArea(obj) {
		this.state.selectArr = []
		if (obj.type == 1 || obj.type == 2) {
			this.state.selectArr = this.state.areaInfo[obj.type].filter((item, i) => {
				return obj.areaId == item.fatherId
			})
		}
		if (obj.type == 1) {
			this.state.provinceStr = obj.area
		} else if (obj.type == 2) {
			this.state.cityStr = obj.area
		} else if (obj.type == 3) {
			this.state.countyStr = obj.area
		}
		this.setState({
			selectArr: this.state.selectArr,
		})
		this.props.onSelectArea(obj.type, this.state.selectArr, this.state.provinceStr, this.state.cityStr, this.state.countyStr)
	}

	render() {
		let {selectArr} = this.state
		return (
			<View className='pickerView-area'>
				<View className='mask'></View>
				<View className='popup-content'>
					<ScrollView
						className='scrollview'
						scrollY
						scrollWithAnimation
						scrollTop='0'
						style='height: 400px;'
					>
						{/* 省 */}
						{selectArr && selectArr.map((item, i) => (
							<View key={'selectArr' + i} className='weui-cell' onClick={this.selectArea.bind(this, item)}>
								<View className='weui-cell__bd'>
									<Text>{item.area}</Text>
								</View>
							</View>
						))}
					</ScrollView>
				</View>
			</View >
		)
	}
}
export default Index;