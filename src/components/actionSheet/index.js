
import Taro, { Component } from '@tarojs/taro'
import { View, ScrollView } from '@tarojs/components'
import './index.scss'


class Index extends Component {
	static defaultProps = {
		onGetSheet: () => { }
	}
	constructor() {
		super(...arguments)
	}
	state = {
		show: false,
		item: []
	};
	componentWillMount() { }

	componentDidMount() {
		this.props.onGetSheet(this, 0)//获取此组件的作用域并且标记0
	}

	componentWillUnmount() { }

	componentDidShow() { }

	componentDidHide() { }
	onSwitchover() {//显示或者隐藏组件
		const { show } = this.state;
		this.setState({ show: !show });
	}
	setItem(item = []) {//设置item
		this.setState({ item }, () => {
			this.onSwitchover()
		})
	}
	outItem(v, e) {//导出用户选择的项目并且标记1
		e.stopPropagation();
		this.props.onGetSheet(v, 1)
		this.onSwitchover()
	}
	render() {
		const { show, item } = this.state;
		return (
			<View>
				{show ? (
					<View className='sheetModal' onClick={this.onSwitchover.bind(this)}>
						<ScrollView className='sheetView' scrollY>
							<View className='sheetBox'>
								{
									item.length > 0 ? item.map((v, i) => (
										<View className='sheetItem' key={i} onClick={this.outItem.bind(this, v)}>{v.val}</View>
									)) : ''
								}
							</View>
						</ScrollView>
					</View>
				) : ''}
			</View >
		)
	}
}
export default Index;
