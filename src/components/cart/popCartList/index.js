import Taro, { Component } from '@tarojs/taro'
import { View, Text,Block} from '@tarojs/components'

import { set as setGlobalData, get as getGlobalData } from '../../../static/js/global_data'
import GoodListPrice from '../../../components/goodListPrice'
import NumInput from '../../../components/numInput'

import './index.scss'

class cartbar extends Component {
	static options = {
		addGlobalClass: true
	}
	static defaultProps = {
		cartList: [],
		cartListState: false,
		onHideCartList() {},
		onClearCarList: () => {}, // 清空购物车
		onChangeCartNum: () => {}, // 加减购物车商品
		page: '',
	}
	constructor() {
		super(...arguments)
		this.state = {
			domainImg: getGlobalData('domainImg'),
		};
	}
	render() {
		// console.log('popCartList ----------------')
		// console.log(this.props)
		let {cartListState,cartList, page} = this.props
		return (
			<View className={`popup ${cartListState ? 'popup-show' : ''} ${page == 'popupBottom' ? 'popup-bottom' : ''}`}>
				<View className='mask' onClick={this.props.onHideCartList}></View>
				<View className='popup-content'>
					<View className='weui-cell weui-cell-none bg-body'>
						<View className='weui-cell__bd ac'>
							<View className='text-big'>已选商品</View>
						</View>
						<View className='weui-cell__ft' onClick={this.props.onClearCartList}>
							<Text>清空购物车</Text>
						</View>
					</View>
					<View>
						{cartList && cartList.map((item, i) => 
						(
							<View key={'cartList' + i} className='weui-cell'>
								<View className='weui-cell__hd'>
									<View className='ellipsis'>{item.C_GOODS_NAME || item.GOODS_NAME}</View>
								</View>
								<View className='weui-cell__bd plr20 text-sub'>
									<GoodListPrice item={item}></GoodListPrice>
								</View>
								<View className='weui-cell__ft text-small'>
									<NumInput onChangeCartNum={this.props.onChangeCartNum.bind(this,i,item)} nums={item.num}></NumInput>
								</View>
							</View>
						))}
					</View>
				</View>
			</View>
		)
	}
}
export default cartbar;