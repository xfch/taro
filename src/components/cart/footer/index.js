import Taro, { Component } from '@tarojs/taro'
import { View, Text,Block} from '@tarojs/components'

import { set as setGlobalData, get as getGlobalData } from '../../../static/js/global_data'
import Checkboxbar from '../../../components/checkboxbar'

import './index.scss'

// 我的订单、必备工具
class cartbar extends Component {
	static options = {
		addGlobalClass: true
	}
	static defaultProps = {
		footerData: {}, //合计底部数据
		onFootBtn: () => {}
	}
	constructor() {
		super(...arguments)
		this.state = {};
	}

	clickNavigator() {
		Taro.navigateTo({url: '/pages/shopcart/shopcart'})
	}

	selectCheck(types){
		this.props.onSelectCheck(types)
	}

	// 结算、删除
	footBtn(){
		this.props.onFootBtn(this)
	}

	render() {
		return (
			<View className='cartfooter'>
				<View className='weui-cell cartfooter-cell'>
					<View className='weui-cell__hd'>
						<View className='iblock checkboxbar'>
							<Checkboxbar select={this.props.selectAllStatus} onSelectCheck={this.selectCheck.bind(this,'all')}></Checkboxbar>
						</View>
						<Text>全选</Text>
					</View>
					<View className='weui-cell__bd total-bar'>
						{!this.props.editState&&
						<Block>
							<View className='text-big text-sub'><Text className='text-default'>合计：</Text>￥{this.props.footerData.orderprice} 
								{this.props.footerData.SaleUseInteger > 0 && <Text className='text-little'>+{this.props.footerData.SaleUseInteger}积分</Text>}
							</View>
							<View className='text-little text-gray'><Text>总金额：￥{this.props.footerData.orderyprice}</Text><Text>优惠：￥{this.props.footerData.cxreductions}</Text></View>
						</Block>
						}
					</View>
					<View className='weui-cell__ft'>
						<View className='weui-btn settle-btn' onClick={this.footBtn}>{this.props.editState ? '删除' : '去结算'}({this.props.footerData.settleNum})</View>
					</View>
				</View>
                
			</View>
		)
	}
}
export default cartbar;