import Taro, { Component } from '@tarojs/taro'
import { View, Text, Block } from '@tarojs/components'

import { set as setGlobalData, get as getGlobalData } from '../../../static/js/global_data'

import './index.scss'

class cartbar extends Component {
	static externalClasses = ['class-pl']
	static options = {
		addGlobalClass: true
	}
	static defaultProps = {
		cartItem: [], // 购物车单条列表
	}
	constructor() {
		super(...arguments)
		this.state = {};
	}

	render() {
		let  { cartItem } = this.props
		return (
			<Block>
				<Text className='item-price text-price'>￥
					{cartItem.salemodelIndex ? 
						cartItem.saledata[cartItem.salemodelIndex].SalePrice || cartItem.saledata[cartItem.salemodelIndex].ReallSalePrice || cartItem.salePrice  : 
						cartItem.salePrice
					} 
					{cartItem.salemodelIndex && cartItem.saledata[cartItem.salemodelIndex].SaleTypeNo == '5' ? 
						<Text className='text-little'>+{cartItem.saledata[cartItem.salemodelIndex].SaleUseInteger}积分</Text> : ''
					}
				</Text>
				{cartItem.salemodelIndex && cartItem.saledata[cartItem.salemodelIndex].SaleTypeNo != '5' &&
					<Text className='line-through text-gray text-small class-pl'>￥{cartItem.salePrice}</Text>
				}
			</Block>
		)
	}
}
export default cartbar;