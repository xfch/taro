import Taro, { Component } from '@tarojs/taro'
import { View, Text,Block,Image,Button} from '@tarojs/components'

import { set as setGlobalData, get as getGlobalData } from '../../../static/js/global_data'

import './index.scss'

// 我的订单、必备工具
class Index extends Component {
	static options = {
		addGlobalClass: true
	}
	static defaultProps = {
		btnList: [ //支付方式
			{'txt': '微信支付','id': '1'},
		] 
	}
	constructor() {
		super(...arguments)
	}
	
	render() {
		return (
			<View className='payway'>
				<View className='text-big'>支付方式</View>
				<View className='btn-groups'>
					{this.props.btnList.map((item,i) => {
						return (
							<Button for={i} key={i} class={`weui-btn ${this.props.payway == item.type ? 'active' : '' }`} type="default" size='mini' plain="true">{item.txt}</Button>
						)
					
					})}
				</View>
			</View>
		)
	}
}
export default Index;