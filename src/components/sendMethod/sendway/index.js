import Taro, { Component } from '@tarojs/taro'
import { View, Text,Block,Image,Button} from '@tarojs/components'

import { set as setGlobalData, get as getGlobalData } from '../../../static/js/global_data'

import './index.scss'

class Index extends Component {
	static options = {
		addGlobalClass: true
	}
	static defaultProps = {
	}
	constructor() {
		super(...arguments)
		this.state = {
		};
	}
	componentWillMount() {
		
	}
	render() {
		return (
			<View className='sendway'>
				<View className='text-big'>配送方式</View>
				<View className='btn-groups'>
					{this.props.posmethod&&this.props.posmethod.map((item,i) => {
						return (
							<Button for={i} key={i} class={`weui-btn ${item.n_isdefault == 1 ? 'active' : '' }`} type="default" size='mini' plain="true" onClick={this.props.onSelectSendWay.bind(this,i)}>{item.c_psname}</Button>
						)
					})}
				</View>
			</View>
		)
	}
}
export default Index;