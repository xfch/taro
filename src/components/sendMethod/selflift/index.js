import Taro, { Component } from '@tarojs/taro'
import { View, Text, Block, Image, Button } from '@tarojs/components'

import { set as setGlobalData, get as getGlobalData } from '../../../static/js/global_data'

import Checkboxbar from '../../../components/checkboxbar'

import './index.scss'

// 我的订单、必备工具
class Index extends Component {
	static options = {
		addGlobalClass: true
	}
	static defaultProps = {
		selflift: [], //门店自提列表
	}
	constructor() {
		super(...arguments)
		this.state = {
		};
	}
	componentWillMount() {
	}

	render() {
		return (
			<View className='selflift'>
				<View className='weui-cells weui-cells-none'>
					<View className='weui-cell'>
						<View className='text-big'>自提列表</View>
					</View>
					{this.props.selflift && this.props.selflift.map((item, i) => (
						<View key={'selflift' + i} className='weui-cell'>
							<View className='weui-cell__bd'>
								<View>{item.c_thname}</View>
								<View className='text-gray text-small'>{item.c_lxname} {item.c_lxphone}</View>
								<View className='text-gray text-small'>{item.c_adress}</View>
							</View>
							<View className='weui-cell__ft'>
								<Checkboxbar select={item.def == '1' ? true : false} onSelectCheck={this.props.onSelectSelflift.bind(this, i)}></Checkboxbar>
							</View>
						</View>
					))}
				</View>
			</View>
		)
	}
}
export default Index;