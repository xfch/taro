import Taro, { Component } from '@tarojs/taro'
import { View, Text,Block,Image,Button} from '@tarojs/components'

import { set as setGlobalData, get as getGlobalData } from '../../../static/js/global_data'


import Checkboxbar from '../../../components/checkboxbar'

import './index.scss'


import api from '../../../service/api'
import common from '../../../service/common'
import base from '../../../static/js/base'

class Index extends Component {
	static options = {
		addGlobalClass: true
	}
	static defaultProps = {
		address: null //地址列表
	}
	constructor() {
		super(...arguments)
		this.state = {
		};
	}
	componentWillMount(){
		let env = process.env.TARO_ENV;
		this.setState({
			env: env
		})
	}

	render() {
		return (
			<View className='selflift'>
				<View className='weui-cells weui-cells-none'>
					<View className='weui-cell'>
						<View className='text-big'>收货地址</View>
					</View>
					<View className='weui-cell'>
						{!this.props.address&&
							<Block>
								{this.state.env == 'alipay' && 
									<Button class='weui-btn btn-address' type="default" size='default' plain="true" onClick={this.props.onSelectAddress}>新增地址</Button>
								}
								{this.state.env == 'weapp' && 
									<Block>
										{!this.props.scopeAddress &&
											<Button class='btn-plain btn-address' open-type='openSetting' onOpensetting={this.props.onSelectAddress} >新增地址</Button>
										}
										{this.props.scopeAddress &&
											<Button class='weui-btn btn-address' type="default" size='default' plain="true" onClick={this.props.onSelectAddress}>新增地址</Button>
										}
									</Block>
								}
							</Block>
						}
						{this.props.address&&
							<Block>
								<View className='weui-cell__bd' onClick={this.props.onSelectAddress}>
									<View><Text className='weui-label'>收货人：</Text> <Text>{this.props.address.C_MB_NAME}</Text><Text className='pl20'>{this.props.address.C_MB_PHONE}</Text></View>
									<View><Text className='weui-label'>地 址: </Text><Text>{this.props.address.C_PROVINCE} {this.props.address.C_CITY} {this.props.address.C_COUNTY} {this.props.address.C_ADDRESS}</Text></View>
								</View>
								<View className='weui-cell__ft weui-cell__ft_in-access'></View>
							</Block>
						}
					</View>
				</View>
			</View>
		)
	}
}
export default Index;