
import Taro, { Component } from '@tarojs/taro'
import { View, ScrollView, Image, Text } from '@tarojs/components'

import './index.scss'

import { get as getGlobalData } from '../../static/js/global_data'


class Index extends Component {
	static options = {
		addGlobalClass: true
	}
	static defaultProps = {
		pinTuanAll: null,
		onGetScope: () => { },
		onPinTuan: () => { }
	}
	constructor() {
		super(...arguments)
	}
	state = {
		isShow: false,
		imgUrl: getGlobalData('domainImg')
	}
	componentWillMount() {
		this.props.onGetScope(this)
	}

	componentDidMount() { }

	componentWillUnmount() { }

	componentDidShow() { }

	componentDidHide() { }
	showCut() {
		this.setState({
			isShow: !this.state.isShow
		})
	}
	initTime(t) {
		return t.slice(5).replace(/T/g, ' ');
	}
	goPinTuan(val) {
		this.props.onPinTuan(val)
	}
	render() {
		const { isShow, imgUrl } = this.state;
		const { pinTuanAll } = this.props;
		return (
			isShow ? (
				<View className='pinTuanModal'>
					<View className='pinTuan'>
						<View className='head'>
							<Text>正在拼单</Text>
							<Text className='iconfont icon-close icon' onClick={this.showCut.bind(this)}></Text>
						</View>
						<ScrollView scrollY className='pinScroll'>
							{
								pinTuanAll ? pinTuanAll.gslistdt.map((val, i) => (
									<View className='item' key={'pinTuanAll' + i}>
										<Image className='headImg' src={imgUrl + val.C_MB_PIC}></Image>
										<View className='center'>
											<View className='cHead'>{val.C_MB_NAME}</View>
											<View className='cBody'>还差{pinTuanAll.dtgroupshop[0].N_GSNum - val.N_JoinNum}人拼成，{this.initTime(val.D_SussDate)}前可拼单</View>
										</View>
										<View className='btn' onClick={this.goPinTuan.bind(this,val)}> 去拼单 </View>
									</View>
								)) : ''
							}

						</ScrollView>
					</View>
				</View >
			) : ''
		)
	}
}
export default Index;