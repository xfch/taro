
import Taro, { Component } from '@tarojs/taro'
import { View, Icon } from '@tarojs/components'
import './index.scss'

class checkboxbar extends Component {
	static options = {
		addGlobalClass: true
	}
	static defaultProps = {
		onSelectCheck(){}, // 选择
	}
	constructor() {
		super(...arguments)
	}
	static state = {
	};
	componentWillMount() { 
	}

	render() {
		let {select} = this.props
		return (
			<View>
				<View className='checkbox'>
					<Text className={`iconfont ${select ? 'icon-radioed text-main' : 'icon-radio text-border'}`} onClick={this.props.onSelectCheck.bind(this,select)}></Text>
					{this.props.children}
				</View >
			</View >
		)
	}
}
export default checkboxbar;