
import Taro, { Component } from '@tarojs/taro'
import {Block, View, Input } from '@tarojs/components'

import './index.scss'


class Index extends Component {
	static options = {
		addGlobalClass: true
	}
	static defaultProps = {
		inputType: '1', // 1、有lebel 2、无label有边框的输入框
		label: '姓名', 
		type: 'text',
		password: false,
		placeholder: '请输入姓名',
		value: '',
		contentType: 'input', //默认内容是input
	}
	constructor() {
		super(...arguments)
	}
	state = {
	};
	componentWillMount() { }

	componentDidMount() {
	}
	render() {
		return (
			<Block>
				{this.props.inputType == '1' && 
					<View class="weui-cell weui-cell_input">
						<View class="weui-cell__hd">
							<View class="weui-label">{this.props.label}</View>
						</View>
						<View class="weui-cell__bd">
							{this.props.contentType == 'input' &&
								<Input class="weui-input" type={this.props.type} placeholderClass='text-placeholder' onInput={this.props.onInput.bind(this)} placeholder={this.props.placeholder} password={this.props.password} value={this.props.value} />
							}
							{this.props.children}
						</View>
					</View> 
				}
				{this.props.inputType == '2' &&
					<View className='weui-cell weui-cell-none weui-cell-plr0'>
						{this.props.contentType == 'input' &&
							<Input class="weui-input weui-cell__bd weui-input-border" type={this.props.type} placeholderClass='text-placeholder' onInput={this.props.onInput.bind(this)} placeholder={this.props.placeholder} password={this.props.password} value={this.props.value} />
						}
						{this.props.children}
					</View>
				}
			</Block >
		)
	}
}
export default Index;