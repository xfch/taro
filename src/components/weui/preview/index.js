
import Taro, { Component } from '@tarojs/taro'
import {Block, View, Input } from '@tarojs/components'

import './index.scss'


class Index extends Component {
	static options = {
		addGlobalClass: true
	}
	static defaultProps = {
		type: 'item',
		label: '标题',
		value: '内容',
	}
	constructor() {
		super(...arguments)
	}
	state = {
	};
	render() {
		return (
			<Block>
				{this.props.type =='view' &&
					<View className='weui-form-preview'>
						<View className='weui-form-preview__bd'>
							{this.props.children}
						</View>
					</View>
				}
				{this.props.type =='item' &&
					<View className='weui-form-preview__item'>
						<View className='weui-form-preview__label'>{this.props.label}</View>
						<View className='weui-form-preview__value'>{this.props.value}</View>
					</View>
				}
			</Block >
		)
	}
}
export default Index;