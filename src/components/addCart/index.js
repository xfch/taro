
import Taro, { Component } from '@tarojs/taro'
import { View, Image } from '@tarojs/components'

import './index.scss'
import { get as getGlobalData } from '../../static/js/global_data'
import api from '../../service/api'

class Index extends Component {
	static options = {
		addGlobalClass: true
	}
	static defaultProps = {
		goodsInfo: null,//商品信息
		//salemodel: null,//商品活动信息
		isPinTuan: false,//是否拼团
		pinTuanData: null,//拼团数据
		goodExchange: false, // 积分商品
		onChoose: () => { },//选择项
	}
	constructor() {
		super(...arguments)
	}
	state = {
		cartNum: 0
	}
	componentWillMount() { }

	componentDidMount() {
		this.getData()
	}
	getData() {//获取购物车商品数量
		let str = `mebno&ShopNo`
		let params = {
			bNo: 805,
			mebno: getGlobalData('C_MB_NO'),//用户信息id
			ShopNo: getGlobalData('SHOP_NO'),//店铺编号
		}
		api.get(str, params, (res) => {
			if (res.code == 100) {
				this.setState({
					cartNum: res.goodsnum
				})
			}
		})
	}
	
	onToPageCart() {
		Taro.navigateTo({
			url: '../shopcart/shopcart'
		})
	}
	onToPageIndex() {
		Taro.navigateTo({
			url: '../index/index'
		})
	}
	chooseOrder(type, N_GSPrice) {
		//立即购买
		//N_GSPrice--优惠金额
		this.props.onChoose(type, N_GSPrice)
	}

	render() {
		const { cartNum } = this.state;
		const { goodsInfo, pinTuanData, goodExchange } = this.props;
		return (
			<View className='addCartBox'>
				<View className='addCartL'>
					<View className='addCartL-bar'>
						<View className='addCartLItem' onClick={this.onToPageIndex.bind(this)}>
							<Image className='addCartLIcon' src='http://upfile.weikezhanggui.com//Content/246397/UpLoadImages/2019/1/2/zg_1901021828531690740.png' mode='aspectFit'></Image>
							<View className='addCartLText'>首页</View>
						</View>
						<View className='addCartLItem' onClick={this.onToPageCart.bind(this)}>
							<Image className='addCartLIcon' src='http://upfile.weikezhanggui.com//Content/246397/UpLoadImages/2018/12/12/zg_1812121408279140424.png' mode='aspectFit'></Image>
							{
								/* 购物车有数据的时候显示红点 */
								cartNum > 0 ? <View className='addCartLd'></View> : ''
							}
							<View className='addCartLText'>购物车</View>
						</View>
					</View>
				</View>
				{goodExchange ? 
					<View className='addCartBtn'>
						<View className='addCartC addCartItem' onClick={this.chooseOrder.bind(this, 2, 0)}>我要兑换</View>
					</View>
					: 
					<Block>
						{
							!pinTuanData ? (
								<View className='addCartBtn'>
									<View className='addCartC addCartItem' onClick={this.chooseOrder.bind(this, 0, 0)}>加入购物车</View>
									<View className='addCartR addCartItem' onClick={this.chooseOrder.bind(this, 1, 0)}>立即购买</View>
								</View>
							) : (
									<View className='addCartBtn'>
										<View className='addCartC pinItem' onClick={this.chooseOrder.bind(this, 2, 0)}>
											<View className='head'>￥{goodsInfo.SALE_PRICE}</View>
											<View className='body'>单独购买</View>
										</View>
										<View className='addCartR pinItem' onClick={this.chooseOrder.bind(this, 3, pinTuanData.N_GSPrice)}>
											<View className='head'>￥{pinTuanData.N_GSPrice}</View>
											<View className='body'>立即拼团</View>
										</View>
									</View>
								)
						}
					</Block>
				}
			</View >
		)
	}
}
export default Index;