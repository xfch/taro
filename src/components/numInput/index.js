
import Taro, { Component } from '@tarojs/taro'
import { View, Text } from '@tarojs/components'

import './index.scss'

class Index extends Component {
	static externalClasses = ['numInputClass']
	static defaultProps = {
		iconSize: 0, // 0:默认，1：较大
		nums: 0,
		type: 'round',/* 正方形的计数器-->square|| 圆形的计数器-->round*/
		onChangeCartNum: () => { },
	}
	constructor() {
		super(...arguments)
	}
	static state = {};
	// 加减购物车数量
	changeCart(types) {
		if (types == 'add') { //+
			this.props.onChangeCartNum('GO0002', types)
		} else if (types == 'minus') { // -
			this.props.onChangeCartNum('GO0002', types)
		}
	}
	render() {
		const { type,iconSize } = this.props;
		const square = (
			/* 正方形的计数器 */
			<View className='cart-num-opt'>
				<View className='opt-btn b1' onClick={this.changeCart.bind(this, 'minus')}>-</View>
				<Text>{this.props.nums}</Text>
				<View className='opt-btn b2' onClick={this.changeCart.bind(this, 'add')}>+</View>
			</View>
		)
		const round = (
			/* 圆形的计数器 */
			<View className='opt-goods'>
				<View>
					{
						this.props.nums > 0 ? <View className='jn' onClick={this.changeCart.bind(this, 'minus')}>-</View> : ''
					}
				</View>
				<View className='flex'>
					{
						this.props.nums > 0 ? <Text className='jdn'>{this.props.nums}</Text> : ''
					}
				</View>
				<View>
					<View className='j' onClick={this.changeCart.bind(this, 'add')}>+</View>
				</View>
			</View>
		)
		return (
			<View className={`numInput${iconSize}`}>
				{
					type == 'square' ? square : type == 'round' ? round : ''
				}
			</View >
		)
	}
}
export default Index;